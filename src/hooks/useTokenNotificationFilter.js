import { useMemo } from "react";
import useSWR from "swr";

export function useTokenNotificationFilter(defaultData = [], params) {
  const { data: response, error } = useSWR(
    "/v1/token/history?from_date=" +
      params.fromDate +
      "&to_date=" +
      params.toDate,
    {
      refreshInterval: 100000,
    }
  );

  const loading = !response && !error;

  const data = useMemo(() => {
    if (response) {
      return response.data.data;
    }
    return defaultData;
  }, [response, defaultData]);

  return { loading, data, error };
}
