import React from 'react'
import { Modal, ModalBody, Button } from 'reactstrap';
import { translate, t} from "react-switch-lang";


function ModalPrivilegeForbidden({isOpen, isClose, forbidden}){
    const forbiddenDesc = {
        canManagementTopUp: t('forbiddenDescTopUp'),
        canManagementJob: t('forbiddenDescJob'),
        canManagementToken: t('forbiddenDescToken'),
        canManagementCompany: t('forbiddenDescCompany'),
        canManagementUser: t('forbiddenDescUser'),
        canInternalAssesment: t('forbiddenDescAssessment')
    }
    
    return(
        <Modal isOpen={isOpen} className="modal-md" backdropClassName="back-home" style={{marginTop: '10vh'}}>
            <ModalBody>
                <div className="row justify-content-center mt-2">
                    <div className="col-12">
                        <div className="text-center" style={{ borderRadius: "5px" }}>
                            <i className="fa fa-2x fa-exclamation-triangle mb-2 text-danger" /><br />
                            <h5 className="my-3" style={{lineHeight:2}}>
                                {t('userHaveNoAccessDescSub1')} {forbiddenDesc[forbidden]},{" "}
                                {t('userHaveNoAccessDescSub2')}
                            </h5>
                            <Button className="button-netis-outline px-3" onClick={isClose}>
                                {t('okUnderstand')}
                            </Button>
                        </div>
                    </div>
                </div>
            </ModalBody>
        </Modal>
    )
}

export default translate(ModalPrivilegeForbidden);