import React from 'react';
import Illustration from '../assets/assets_ari/481.png';
import noDataEn from '../../src/assets/assets_ari/no_dataEn.png';
import langUtils from '../utils/language/index';

const DataNotFound = () => {
    return (
        <div className="text-center my-5">
            {langUtils.getLanguage() === "ID" ? (
                <img src={Illustration} alt="not found" />

            ):(
                <img src={noDataEn} alt="not found-en" />
            )
            }
        </div>
    );
}

export default DataNotFound;
