import React from 'react';
import { Row, Col } from 'reactstrap';
import Zoom from 'react-reveal/Zoom';
import Slide from 'react-reveal/Slide';
import { translate, t } from "react-switch-lang";

function Advantage() {
    return (
        <section id="advantage" className="advantages-landing">
            <div className="container-fluid pt-2 pb-5">
                <h3 className="text-center sub-title">{t('ourAdvantage')}</h3>
                <hr className="hr-work" />
                <Row className="row-advantage">
                    <Col xs="12" md="7" className="col-advantage-item">
                        <div className="row">
                            <div className="col-6 mb-2 advantage-item">
                                <AdvantageItem
                                    image={require('../../assets/img/landing-page/advantage/advantage-ai.png')}
                                    text={t('artificial')} />
                            </div>
                            <div className="col-6 mb-2 advantage-item">
                                <AdvantageItem
                                    image={require('../../assets/img/landing-page/advantage/advantage-fast.png')}
                                    text={t('fastProcess')} />
                            </div>
                            <div className="col-6 mb-2 advantage-item">
                                <AdvantageItem
                                    image={require('../../assets/img/landing-page/advantage/advantage-easy.png')}
                                    text={t('easyToUse')} />
                            </div>
                            <div className="col-6 mb-2 advantage-item">
                                <AdvantageItem
                                    image={require('../../assets/img/landing-page/advantage/advantage-realtime.png')}
                                    text={t('flexibleAccess')} />
                            </div>
                        </div>
                    </Col>
                    <Col xs="12" md="5" className="advantage-desc text-right">
                        <Slide right>
                            <hr className="hr-advantage mr-0" />
                            <h1>
                                {t('advantageTagLineSub1')}<br />
                                {t('advantageTagLineSub2')}<br />
                                {t('advantageTagLineSub3')}
                            </h1>
                        </Slide>
                    </Col>
                </Row>
            </div>
        </section>
    )
}

function AdvantageItem(props) {
    return (
        <Zoom>
            <div className="d-flex flex-column advantage-item">
                <img src={props.image} alt="illustration" />
                <hr className="hr-advantage-icon" />
                <h6 className="advantage-icon-desc">{props.text}</h6>
            </div>
        </Zoom>
    );
}

export default translate(Advantage);
