import React, { forwardRef, useState } from 'react';
import { Row, Col, Button, Collapse } from 'reactstrap';
import Slide from 'react-reveal/Slide';
import Fade from 'react-reveal/Fade';
import {t} from "react-switch-lang";
function Feature(props, ref) {

    const [collapseFeature, setCollapseFeature] = useState(false);
    const toggleCollapse = () => {
        setCollapseFeature(!collapseFeature);
    }
    return (
        <section ref={ref} className="text-center py-5 px-md-4 feature" id="feature">
            <h3 className="text-center sub-title">{t('ourFeature')}</h3>
            <hr className="hr-work" />
            <div className="px-md-5">
                <Slide left>
                    <FeatureItem
                        direction="left"
                        image={require('../../assets/img/landing-page/feature/feature-psikotes.png')}
                        title={t('psychotestOnline')}
                        alt="psikotes"
                        feature={t('psychotestOnlineDesc')}
                        class="flex-row"
                    />
                </Slide>
                <Slide right>
                    <FeatureItem
                        direction="right"
                        image={require('../../assets/img/landing-page/feature/feature-interview.png')}
                        title={t('onlineInterview')}
                        alt="interview"
                        feature={t('onlineInterviewDesc')}
                        class="flex-row-reverse"
                    />
                </Slide>
                <Slide left>
                    <FeatureItem
                        direction="left"
                        image={require('../../assets/img/landing-page/feature/feature-gestur.png')}
                        title={t('gestureAssessment')}
                        alt="gesture"
                        feature={t('gestureAssessmentDesc')}
                        class="flex-row"
                    />
                </Slide>
                {!collapseFeature &&
                    <Fade>
                        <Button className="btn-sm button-feature-collapse scale-div" onClick={toggleCollapse}>
                            <i className="fa fa-angle-double-down mr-2" />&nbsp;
                            {t('otherFeatures')}
                        </Button>
                    </Fade>
                }
                <Collapse isOpen={collapseFeature}>
                    <Slide right>
                        <FeatureItem
                            direction="right"
                            image={require('../../assets/img/landing-page/feature/feature-wajah.png')}
                            title={t('faceAssessment')}
                            alt="Wajah"
                            feature={t('faceAssessmentDesc')}
                            class="flex-row-reverse"
                        />
                    </Slide>
                    <Slide left>
                        <FeatureItem
                            direction="left"
                            image={require('../../assets/img/landing-page/feature/feature-tangan.png')}
                            title={t('handLineAssessment')}
                            alt="tangan"
                            feature={t('handLineAssessmentDesc')}
                            class="flex-row"
                        />
                    </Slide>
                    <Slide right>
                        <FeatureItem
                            direction="right"
                            image={require('../../assets/img/landing-page/feature/feature-shio.png')}
                            title={t('othersAssessment')}
                            alt="shio"
                            feature={t('othersAssessmentDesc')}
                            class="flex-row-reverse"
                        />
                    </Slide>
                </Collapse>
                {collapseFeature &&
                    <Fade>
                        <Button className="btn-sm button-feature-collapse scale-div" onClick={toggleCollapse}>
                            <i className="fa fa-angle-double-up mr-2" />&nbsp;
                            {t('close')}
                        </Button>
                    </Fade>
                }
            </div>
        </section>
    )
}

function FeatureItem(props) {
    return (
        <div className="feature-content">
            <div className="text-center feature-title">
                {props.title}
            </div>
            <Row className={props.class}>
                <Col sm="6" md="3" className="py-4 feature-icon text-center d-flex align-items-center">
                    <img src={props.image} width="150" alt={props.alt} className="mx-auto" />
                </Col>
                <Col sm="6" md="9" className={`py-4 feature-desc d-flex align-items-center`}>
                    {props.feature}
                </Col>
            </Row>
        </div>
    )
}

export default forwardRef(Feature);
