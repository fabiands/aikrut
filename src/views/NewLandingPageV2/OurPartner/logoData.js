import arofahmina from "../../../assets/img/newLandingPageV2/Partner/arofahmina.png"
import asuransibintang from "../../../assets/img/newLandingPageV2/Partner/asuransibintang.png"
import autoconz from "../../../assets/img/newLandingPageV2/Partner/autoconz.png"
import baramulti from "../../../assets/img/newLandingPageV2/Partner/baramulti-group.png"
import bkd from "../../../assets/img/newLandingPageV2/Partner/bkd-sumut.png"
import boutique from "../../../assets/img/newLandingPageV2/Partner/boutiquehotel.png"
import buanaprima from "../../../assets/img/newLandingPageV2/Partner/buanaprima.png"
import bumilangit from "../../../assets/img/newLandingPageV2/Partner/bumilangit.png"
import digindo from "../../../assets/img/newLandingPageV2/Partner/digindo.png"
import djsport from "../../../assets/img/newLandingPageV2/Partner/djsport.png"
import gemilang from "../../../assets/img/newLandingPageV2/Partner/gemilang.png"
import hestia from "../../../assets/img/newLandingPageV2/Partner/hestia.png"
import hidayahinsan from "../../../assets/img/newLandingPageV2/Partner/hidayahinsan.png"
import hokkymart from "../../../assets/img/newLandingPageV2/Partner/hokkymart.png"
import infitech from "../../../assets/img/newLandingPageV2/Partner/infitech.png"
import innagroup from "../../../assets/img/newLandingPageV2/Partner/innagroup.png"
import kasuari from "../../../assets/img/newLandingPageV2/Partner/kasuari.png"
import kominfo from "../../../assets/img/newLandingPageV2/Partner/kominfo.png"
import kontakperkasa from "../../../assets/img/newLandingPageV2/Partner/kontakperkasa.png"
import multidaya from "../../../assets/img/newLandingPageV2/Partner/multidaya.png"
import nasmoco from "../../../assets/img/newLandingPageV2/Partner/nasmoco.png"
import onnaprima from "../../../assets/img/newLandingPageV2/Partner/onnaprima.png"
import satoriaagro from "../../../assets/img/newLandingPageV2/Partner/satoriaagro.png"
import selamatsempurna from "../../../assets/img/newLandingPageV2/Partner/selamatsempurna.png"
import setoshotel from "../../../assets/img/newLandingPageV2/Partner/setoshotel.png"
import sinarbali from "../../../assets/img/newLandingPageV2/Partner/sinarbali.png"
import sreeya from "../../../assets/img/newLandingPageV2/Partner/sreeya.png"
import tigaraksa from "../../../assets/img/newLandingPageV2/Partner/tigaraksa.png"
import transcosmos from "../../../assets/img/newLandingPageV2/Partner/transcosmos.png"
import umg from "../../../assets/img/newLandingPageV2/Partner/umg.png"

export const firstLogo = [
  kominfo, bkd, selamatsempurna, nasmoco, tigaraksa, sreeya, kontakperkasa, arofahmina, 
  gemilang, buanaprima, innagroup, baramulti, transcosmos, satoriaagro, asuransibintang
]
export const secLogo = [
  djsport, onnaprima, sinarbali, hidayahinsan, hestia, hokkymart, multidaya, bumilangit,
  boutique, setoshotel, infitech, umg, autoconz, kasuari, digindo
]

