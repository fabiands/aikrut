import React, { useState } from "react";
import ScrollAnimation from "react-animate-on-scroll";
import { t } from "react-switch-lang";
import BenefitImg from "../../assets/img/newLandingPageV2/benefit-img.png";
// import BenefitImg1 from "../../assets/img/newLandingPageV2/benefit-1.png";
// import BenefitImg2 from "../../assets/img/newLandingPageV2/benefit-2.png";

function Benefit() {
  const [dataText, setDataText] = useState({
    dataText1: "Memotong 50% dari waktu biasanya",
    dataText2: "Hasil langsung muncul",
  });

  const data = [
    {
      id: 1,
      title: "Waktu rekrutmen berkurang",
      text1: "Memotong 50% dari waktu biasanya",
      text2: "Hasil langsung muncul",
    },
    {
      id: 2,
      title: "Biaya rekrutmen berkurang",
      text1: "Tersedia di satu platform",
      text2: "Tanpa biaya tambahan tiap bulan",
    },
    {
      id: 3,
      title: "Tidak perlu mobilitas",
      text1: "Bisa dari mana saja",
      text2: "Hanya perlu koneksi internet",
    },
    {
      id: 4,
      title: "Produktivitas HR meningkat",
      text1: "Rekrutmen yang mudah",
      text2: "Dapat dilakukan dengan cepat",
    },
    {
      id: 5,
      title: "Risiko pandemi berkurang",
      text1: "Tidak perlu tatap muka",
      text2: "Bisa saat WFH",
    },
    {
      id: 6,
      title: "Tanpa upaya yang berarti",
      text1: "Persiapan yang singkat",
      text2: "Dapat menghubungi admin",
    },
    {
      id: 7,
      title: "Kualitas kandidat meningkat",
      text1: "Standar karyawan naik",
      text2: "Analisa karakter yang sesuai dengan perusahaan",
    },
    {
      id: 8,
      title: "Minim bias, valid, dan akurat",
      text1: "Analisa AI canggih Aikrut",
      text2: "Hasil akurat dan sistematis",
    },
    {
      id: 9,
      title: "Hasil yang komprehensif",
      text1: "Asesmen prikologi yang detail",
      text2: "Hasil yang instan",
    },
    {
      id: 10,
      title: "Banyak saran yang bisa didapat",
      text1: "Masukan dari Aikrut untuk rekrutmen",
      text2: "Alat tes dan asesmen yang beragam",
    },
  ];

  return (
    <section className="container benefit-wrapper mb-5 pb-3">
      <div className="row benefit">
        <div className="benefit-content-left col-12 col-md-8">
          <h3
            style={{
              color: "#FF8A00",
              fontWeight: "600",
              fontSize: "16px",
              marginBottom: "15px",
              letterSpacing: "3px",
            }}
          >
            {t("Keuntungan Klien")}
          </h3>
          <h1
            className="feature-aikrut-heading-1"
            style={{
              fontWeight: "600",
              color: "#323232",
              marginBottom: "2rem",
            }}
          >
            {t(
              "Keuntungan yang akan dimiliki oleh perusahaan Anda dengan menggunakan Aikrut"
            )}
          </h1>
          <div className="assessment-list">
            <ol className="p-0">
              {data.map((data, i) => (
                <li key={i} className="d-inline-block mr-3">
                  <h5
                    className="m-0 p-1"
                    style={{ fontWeight: "600" }}
                    onMouseOver={() =>
                      setDataText({
                        ...dataText,
                        dataText1: data.text1,
                        dataText2: data.text2,
                      })
                    }
                  >
                    {data.title}
                  </h5>
                </li>
              ))}
            </ol>
          </div>
        </div>
        <div className="benefit-content-right col-12 col-md-4 pt-md-5">
          <ScrollAnimation animateIn="fadeIn">
            <img width="100%" src={BenefitImg} alt="benefit img" />
            <p className="benefit-text-holder text-1">{dataText.dataText1}</p>
            <p className="benefit-text-holder text-2">{dataText.dataText2}</p>
          </ScrollAnimation>
        </div>
      </div>
    </section>
  );
}

export default Benefit;
