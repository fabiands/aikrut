import React from "react";
import { Link } from "react-router-dom";
import { t } from "react-switch-lang";
import { isMobile } from "react-device-detect";
import OfferingLandingImg from "../../assets/img/newLandingPageV2/offering-landing.png";
import OfferingMobileLandingImg from "../../assets/img/newLandingPageV2/offering-mobile-landing.png";
import ScrollAnimation from "react-animate-on-scroll";

function Offering() {
  return (
    <section className="container offering-wrapper pb-3">
      <div className="text-center mb-3 mb-md-4">
        <ScrollAnimation animateIn="fadeIn">
          {isMobile ? (
            <img
              width="100%"
              src={OfferingMobileLandingImg}
              alt="offering table"
            />
          ) : (
            <img 
              width="100%" 
              src={OfferingLandingImg} 
              alt="offering table" 
            />
          )}
        </ScrollAnimation>
      </div>
      <div className="text-center">
        <Link
          to="/register"
          className="btn button-landing text-white"
          style={{
            width: "170px",
            fontSize: "18px",
          }}
        >
          {t("Hubungi Kami")}
        </Link>
      </div>
    </section>
  );
}

export default Offering;
