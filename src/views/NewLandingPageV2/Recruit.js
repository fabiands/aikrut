import React from "react";
import { t } from "react-switch-lang";
import { isMobile } from "react-device-detect";
// import icJobPosting from "../../assets/img/newLandingPageV2/ic_job_posting.png";
// import icAssessment from "../../assets/img/newLandingPageV2/ic_assessment.png";
// import icOffering from "../../assets/img/newLandingPageV2/ic_offering.png";
// import icResult from "../../assets/img/newLandingPageV2/ic_result.png";
// import icScreening from "../../assets/img/newLandingPageV2/ic_screening.png";
// import icUserInterview from "../../assets/img/newLandingPageV2/ic_user_interview.png";
import recruitImg from "../../assets/img/newLandingPageV2/recruit-landing.png";
import recruitMobileImg from "../../assets/img/newLandingPageV2/recruit-mobile-landing.png";
import { Link } from "react-router-dom";
import ScrollAnimation from "react-animate-on-scroll";

function Recruit() {
  return (
    <section className="container recruit-wrapper p-md-5 mb-5">
      <h2
        className="recruit-heading text-center mb-4 mb-md-5"
        style={{
          color: "#18568B",
          fontWeight: "600",
        }}
      >
        {t("Aikrut membantu perekrutan jadi lebih efisien dan akurat")}
      </h2>
      <div className="mb-4">
        <ScrollAnimation animateIn="fadeIn" animateOut="fadeOut">
          {isMobile ? (
            <img width="100%" src={recruitMobileImg} alt="recruit-landing" />
          ) : (
            <img width="100%" src={recruitImg} alt="recruit-landing" />
          )}
        </ScrollAnimation>
      </div>
      <div className="text-center">
        <Link
          to="/aikrut-recruitment"
          className="btn button-landing text-white"
          style={{
            width: "136px",
            fontSize: "18px",
          }}
        >
          {t("Lebih lanjut")}
        </Link>
      </div>
    </section>
  );
}

export default Recruit;
