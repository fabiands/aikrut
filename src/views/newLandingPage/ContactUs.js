import React, { forwardRef } from "react";
// import { translate, t} from "react-switch-lang";
import { Row, Col } from "reactstrap";

function ContactUs(props, ref) {
  return (
    <section
      id="landing-contact"
      style={{ paddingTop: "50px" }}
      ref={ref}
      className="landing-contact container mb-5"
    >
      <div className="contact-wrapper">
        <Row className="contact-card align-items-center p-5">
          <Col md="9" sm="12" className="contact-heading-section">
            <div className="contact-heading-wrap">
              <h2 className="contact-heading">
                Dapatkan informasi tentang Aikrut serta akun uji coba gratis.
              </h2>
            </div>
          </Col>
          <Col md="3" sm="12" className="contact-form-section text-center">
            <a
              href="/register"
              className="btn btn-lg btn-try-now px-5 py-3"
              style={{ backgroundColor: "#3767B9", color: "white" }}
            >
              Coba Sekarang
            </a>
            {/* <div>
              <Form>
                <h2 className="contact-form-heading">Hubungi Kami</h2>
                  <Row className="mb-md-2">
                    <Col md="6" sm="12" className="input-wrap">
                      <Input id="name-input" type="text" placeholder="Masukkan Nama Lengkap*" required />
                    </Col>
                    <Col md="6" sm="12" className="input-wrap">
                      <Input type="tel" placeholder="Masukkan No. HP*" required />
                    </Col>
                  </Row>
                  <Row className="mb-md-2">
                    <Col md="6" sm="12" className="input-wrap">
                      <Input type="text" placeholder="Masukkan Nama Perusahaan*" required />
                    </Col>
                    <Col md="6" sm="12" className="input-wrap">
                      <Input type="email" placeholder="Masukkan Email*" required />
                    </Col>
                  </Row>
                  <Row className="mb-md-3">
                    <Col md="12" className="input-wrap">
                      <Input type="textarea" rows="5" placeholder="Masukkan Deskripsi Pesan" />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Button type="submit" className="btn btn-lg text-white px-5" style={{backgroundColor: "#3767B9"}}>Kirim</Button>
                    </Col>
                  </Row>
              </Form>

            </div> */}
          </Col>
        </Row>
      </div>
    </section>
  );
}

export default forwardRef(ContactUs);
