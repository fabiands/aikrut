import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import Logo from "../../assets/assets_ari/logo.png";
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  Nav,
  Collapse,
  Modal,
  Container,
} from "reactstrap";
// import langUtils from "../../utils/language/index";
import { translate, t } from "react-switch-lang";
// import * as moment from "moment";

function NavbarComp() {
  const location = useLocation();
  // console.log(location)
  // eslint-disable-next-line
  const [curPosition, setCurPosition] = useState("home");
  // eslint-disable-next-line
  const [collapsed, setCollapsed] = useState(true);
  const [openDrawer, setOpenDrawer] = useState(false);
  const isHome = location.pathname === "/";
  const isAssessment = location.pathname === "/assessment-inventory";
  const isPricing = location.pathname === "/pricing";
  // const isAboutus = location.pathname === '/aboutus'

  const toggleNavbar = () => {
    setOpenDrawer(true);
  };

  const closeDrawer = () => {
    setOpenDrawer(false);
  };

  // const redirectLink = (link) => (e) => {
  //   e.preventDefault();
  //   window.location.replace(link)
  // }

  // const onSelectFlag = (countryCode) => {
  //   handleSetLanguage(countryCode)
  //   moment.locale(countryCode.toLowerCase())
  // }

  // const handleSetLanguage = (key) => {
  //   langUtils.setLanguage(key)
  // }
  const windowOnScroll = () => {
    if (window.scrollY > 0) {
      if (
        !document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.add("shadow-sm");
      }
    } else {
      if (
        document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.remove("shadow-sm");
      }
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", windowOnScroll);

    return () => {
      window.removeEventListener("scroll", windowOnScroll);
    };
  });

  const getNavItemClass = (pos) =>
    pos === curPosition ? "mr-3 active" : "mr-3";

  return (
    <>
      <Navbar
        color="white"
        className="navbar-expand-md fixed-top custom-nav"
        light
      >
        <Container>
          <NavbarBrand href="/" className="mr-auto">
            <img src={Logo} alt="widya-skilloka" className="navbar-logo" />
          </NavbarBrand>
          <div className="ml-auto d-flex">
            <Collapse isOpen={!collapsed} navbar>
              <Nav navbar>
                <NavItem
                  className={isHome ? "active-navbar" : ""}
                  onClick={() => {
                    window.scrollTo({
                      top: 0,
                      left: 0,
                      behavior: "smooth",
                    });
                  }}
                >
                  <a className="custom-nav" href="/" disabled>
                    {t("Beranda")}
                  </a>
                </NavItem>
                <NavItem
                  className={isAssessment ? "active-navbar" : ""}
                  onClick={() => {
                    window.scrollTo({
                      top: 0,
                      left: 0,
                      behavior: "smooth",
                    });
                  }}
                >
                  <Link
                    className="custom-nav"
                    to="/assessment-inventory"
                    disabled
                  >
                    {t("Inventori Asesmen")}
                  </Link>
                </NavItem>
                <NavItem
                  className={isPricing ? "active-navbar" : ""}
                  onClick={() => {
                    window.scrollTo({
                      top: 0,
                      left: 0,
                      behavior: "smooth",
                    });
                  }}
                >
                  <Link className="custom-nav" to="/pricing" disabled>
                    {t("Harga")}
                  </Link>
                </NavItem>
                {/* <NavItem
                            className={isAboutus ? 'active-navbar' : ''}
                            onClick={() => {
                              window.scrollTo({
                                top:0,
                                left: 0,
                                behavior:'smooth'
                              })
                            }}
                          >
                            <Link className="custom-nav" to="/aboutus" disabled>
                              {t("Tentang Kami")}
                            </Link>
                          </NavItem> */}
                <NavItem className={getNavItemClass("article")}>
                  <Link
                    className="custom-nav"
                    to={{ pathname: " https://blog.aikrut.id" }}
                    target="_parent"
                    disabled
                  >
                    {t("article")}
                  </Link>
                </NavItem>
                <NavItem className="has-subitem">
                  <Link
                    className="btn button-landing px-2"
                    to="/login"
                    style={{ color: "#fff" }}
                  >
                    {t("login")}
                  </Link>
                </NavItem>
              </Nav>
            </Collapse>
          </div>
          {/* <UncontrolledButtonDropdown nav direction="down" className="ml-0">
              <DropdownToggle nav>{langUtils.getLanguage()}</DropdownToggle>
              <DropdownMenu right>
                <DropdownItem header>{t("Pilih Bahasa")}</DropdownItem>
                <DropdownItem onClick={() => onSelectFlag("ID")}>
                  Bahasa Indonesia (ID)
                </DropdownItem>
                <DropdownItem onClick={() => onSelectFlag("EN")}>
                  English (US)
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledButtonDropdown> */}
          <NavbarToggler onClick={toggleNavbar} className="" />
        </Container>
      </Navbar>
      <Modal
        isOpen={openDrawer}
        toggle={closeDrawer}
        className={"modal-drawer"}
      >
        <div className="drawer container">
          <div className="drawer-header pb-2">
            <NavbarBrand href="/" className="mr-auto">
              <img src={Logo} alt="widya-skilloka" className="navbar-logo" />
            </NavbarBrand>
            <NavbarToggler
              onClick={closeDrawer}
              className="close-drawer ml-auto"
            />
          </div>
          <div className="text-center d-flex flex-column justify-content-center">
            <ul>
              <li
                className={`nav-item ${getNavItemClass("home")}`}
                onClick={() => {
                  closeDrawer();
                  window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <a href="/" className="nav-link m-0 py-2" disabled>
                  {t("Beranda")}
                </a>
              </li>
              <li
                className={`nav-item ${getNavItemClass("assessmentInven")}`}
                onClick={() => {
                  closeDrawer();
                  window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Link
                  to="/assessment-inventory"
                  disabled
                  className="nav-link m-0 py-2"
                >
                  {t("Inventori Asesmen")}
                </Link>
              </li>
              <li
                className={`nav-item ${getNavItemClass("pricing")}`}
                onClick={() => {
                  closeDrawer();
                  window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: "smooth",
                  });
                }}
              >
                <Link to="/pricing" disabled className="nav-link m-0 py-2">
                  {t("Harga")}
                </Link>
              </li>
              {/* <li
                  className={`nav-item ${getNavItemClass("aboutus")}`}
                  onClick={() => {
                    closeDrawer();
                    window.scrollTo({
                      top:0,
                      left: 0,
                      behavior:'smooth'
                    })
                    
                  }}
                >
                  <Link to="/aboutus" disabled className="nav-link m-0 py-2">
                    {t('Tentang Kami')}
                  </Link>
                </li> */}
              <li
                className={`nav-item ${getNavItemClass("article")}`}
                onClick={() => {
                  closeDrawer();
                }}
              >
                <Link
                  className="nav-link m-0 py-2"
                  to={{ pathname: " https://blog.aikrut.id" }}
                  target="_parent"
                  disabled
                >
                  {t("Artikel")}
                </Link>
              </li>

              <li className="nav-item">
                <Link
                  className="btn button-landing px-2 py-2"
                  to="/login"
                  style={{ color: "#fff" }}
                >
                  Masuk
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </Modal>
    </>
  );
}

export default translate(NavbarComp);
