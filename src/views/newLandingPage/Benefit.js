import React from 'react';
import { translate, t} from "react-switch-lang";
import { Link } from "react-router-dom";
import { Row, Col } from 'reactstrap';
import { ArcherContainer, ArcherElement } from 'react-archer';

const benefitContentLeft = [
  {id:1, title: 'Meningkatkan Produktivitas HR'},
  {id:2, title: 'Mengurangi Biaya Rekrutmen'},
  {id:3, title: 'Mengurangi Mobilitas'},
  {id:4, title: 'Mengurangi Waktu Rekrutmen'},
  {id:5, title: 'Mengurangi Risiko Pandemi'}
]

const benefitContentRight = [
  {id:6, title: 'Meningkatkan Kualitas Kandidat Rekrutmen'},
  {id:7, title: 'Banyak Saran yang Bisa Didapat'},
  {id:8, title: 'Minim Bias, Valid, dan Akurat'},
  {id:9, title: 'Usaha Sederhana'},
  {id:10, title: 'Hasil yang Komprehensif'}
]

function Benefit() {

  const scrollTo = () => {
    window.scrollTo({
      top:0,
      left:0,
      behavior:"smooth"
    })
  }

  return (
    <section className="container px-4 landing-benefit text-center pb-md-5 mt-md-5">
      <h3 className="sub-title mb-md-5" style={{fontWeight: "bold"}}>{t('benefitTitle')}</h3>
      <div className="d-none d-md-block">
        <ArcherContainer>
          <Row>
            <Col md="3">
              <Row>
                {benefitContentLeft.map((item, idx) => (
                  <Col xs="12" key={idx}  className="benefit-col text-right">
                    <ArcherElement id={`box-${item.id}`}>
                      <Link
                        to="/benefit-to-client"
                        className="benefit-point text-nowrap py-3"
                        onClick={() => scrollTo()}
                        >
                        {item.title}
                      </Link>
                    </ArcherElement>
                  </Col>
                ))}
              </Row>
            </Col>
            <Col md="5" style={{position:'relative', marginTop:"100px"}}>
              <div className="benefit-img">
                <img src={require('../../assets/img/new-landing-page/benefit1.png')} alt="benefit-img"/>
              </div>
              <div className="benefit-arrow">
                <Row className="d-flex justify-content-center">
                  <Col xs="1" className="d-flex flex-column">
                    {benefitContentLeft.map((item, idx) => (
                      <ArcherElement
                        key={idx}
                        id={`root-${item.id}`}
                        relations={[
                          {
                            targetId: `box-${item.id}`,
                            targetAnchor: 'right',
                            sourceAnchor: 'left',
                            style: { 
                              strokeColor: '#3767B9', 
                              strokeDasharray: '5,5',
                              lineStyle: 'straight'
                            },
                          },
                        ]}
                      >
                        <div className="text-white">{item.id}</div>
                      </ArcherElement>
                    ))}
                  </Col>
                  <Col xs="1" className="d-flex flex-column">
                    {benefitContentRight.map((item, idx) => (
                      <ArcherElement
                        key={idx}
                        id={`root-${item.id}`}
                        relations={[
                          {
                            targetId: `box-${item.id}`,
                            targetAnchor: 'left',
                            sourceAnchor: 'right',
                            style: { 
                              strokeColor: '#3767B9', 
                              strokeDasharray: '5,5',
                              lineStyle: 'straight'
                            },
                          },
                        ]}
                      >
                        <div className="text-white">{item.id}</div>
                      </ArcherElement>
                    ))}
                  </Col>
                </Row>
              </div>
            </Col>
            <Col md="4">
              <Row>
                {benefitContentRight.map((item, idx) => (
                  <Col xs="12" key={idx} className="benefit-col text-left">
                    <ArcherElement id={`box-${item.id}`}>
                      <Link
                        to="/benefit-to-client"
                        className="benefit-point text-nowrap py-3"
                        onClick={() => scrollTo()}
                        >
                        {item.title}
                      </Link>
                    </ArcherElement>
                  </Col>
                ))}
              </Row>
            </Col>
          </Row>
        </ArcherContainer>
      </div>
      <div className="d-md-none">
        <div style={{width:'85%'}} className="mx-auto my-4">
          <img src={require('../../assets/img/new-landing-page/benefit1.png')} alt="benefit-img" width="90%" />
        </div>
        <Row>
          {benefitContentLeft.map((item, idx) => (
            <Col xs="12" key={idx}  className="benefit-col text-left">
                <Link
                  to="/benefit-to-client"
                  className="benefit-point text-nowrap py-3"
                  onClick={() => scrollTo()}
                  >
                  {item.title}
                </Link>
            </Col>
          ))}
          {benefitContentRight.map((item, idx) => (
            <Col xs="12" key={idx+5} className="benefit-col text-left">
              <Link
                to="/benefit-to-client"
                className="benefit-point text-nowrap py-3"
                onClick={() => scrollTo()}
                >
                {item.title}
              </Link>
            </Col>
          ))}
        </Row>
      </div>
    </section>
  )
}

export default translate(Benefit)
