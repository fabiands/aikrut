import React from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { translate, t } from 'react-switch-lang';

function Partner() {
  return(
    <section id="partner" className="landing-partner text-center">
      <h3 className="text-center sub-title">{t('ourPartner')}</h3>
      <Carousel 
        additionalTransfrom={0}
        arrows={false}
        autoPlay
        autoPlaySpeed={1500}
        centerMode
        className="partner-row"
        draggable={false}
        focusOnSelect={false}
        infinite
        itemClass="partner-icon"
        keyBoardControl
        minimumTouchDrag={80}
        renderButtonGroupOutside={false}
        renderDotsOutside={false}
        responsive={{
          desktop: {
              breakpoint: {
                  max: 3000,
                  min: 1024
              },
              items: 3,
              partialVisibilityGutter: 40
          },
          mobile: {
              breakpoint: {
                  max: 464,
                  min: 0
              },
              items: 1,
              partialVisibilityGutter: 30
          },
          tablet: {
              breakpoint: {
                  max: 1024,
                  min: 464
              },
              items: 2,
              partialVisibilityGutter: 30
          }
        }}
          showDots={false}
          sliderClass=""
          slidesToSlide={1}
          swipeable
        >
          <div className="d-flex justify-content-center align-items-center">
            <img src={require('../../assets/img/new-landing-page/partner-logo/arutala.png')} width="120" className="arutala" alt="Arutala"/>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <img src={require('../../assets/img/new-landing-page/partner-logo/griyaton.png')} width="230" className="griyaton" alt="Griyaton"/>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <img src={require('../../assets/img/new-landing-page/partner-logo/msmb.png')} width="160" className="msmb" alt="Msmb"/>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <img src={require('../../assets/img/new-landing-page/partner-logo/autoconz.png')} width="140" className="autoconz" alt="Autoconz"/>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <img src={require('../../assets/img/new-landing-page/partner-logo/idealab.png')} width="120" className="idealab" alt="Idealab"/>
          </div>
      </Carousel>
      <div className="partner-more">
        <span>and many more</span>
      </div>
    </section>
  )
}

export default translate(Partner)