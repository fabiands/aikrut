import React from 'react';
import { translate, t } from "react-switch-lang";
import {
  Row,
  Col,

} from "reactstrap";

function WhyAikrut() {
  return (
    <section id="whyAikrut" className="container landing-whyAikrut text-center">
      <div className="d-flex justify-content-center text-center">
        <div className="landing-whyAikrut-heading">
          <h4 className="sub-title">{t('whyAikrutTitle')}</h4><br/>
            <p className="landing-why-desc">
              {t('whyAikrutDesc')}
            </p>
        </div>
      </div>
      <Row className="mb-md-5">
        <Col md="4" sm="12" className="mb-3">
          <div className="text-md-left">
          <img src={require('../../assets/img/new-landing-page/robot.png')} className="mb-3" width="80" height="70" alt="robot"/>
          <h4 style={{fontWeight:"bolder"}}>{t('assistantDigitalRobotTitle')}</h4>
          <p>{t('assistantDigitalRobotDesc')}</p>
          </div>
        </Col>
        <Col md="4" sm="12" className="mb-3">
          <div className="text-md-left">
          <img src={require('../../assets/img/new-landing-page/ai-anotation.png')} className="mb-3" width="90" height="70" alt="robot"/>
          <h4 style={{fontWeight:"bolder"}}>{t('aiAnotationTitle')}</h4>
          <p>{t('aiAnotationDesc')}</p>
          </div>
        </Col>
        <Col md="4" sm="12" className="mb-3">
          <div className="text-md-left">
          <img src={require('../../assets/img/new-landing-page/realtime.png')} className="mb-3" width="70" height="70" alt="robot"/>
          <h4 style={{fontWeight:"bolder"}}>{t('realtimePsychologicalTitle')}</h4>
          <p>{t('realtimePsychologicalDesc')}</p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col md="4" sm="12" className="mb-3">
          <div className="text-md-left">
          <img src={require('../../assets/img/new-landing-page/palmistry.png')} className="mb-3" width="90" height="70" alt="robot"/>
          <h4 style={{fontWeight:"bolder"}}>{t('physiognomyPalmistryTitle')}</h4>
          <p>{t('physiognomyPalmistryDesc')}</p>
          </div>
        </Col>
        <Col md="4" sm="12" className="mb-3">
          <div className="text-md-left">
          <img src={require('../../assets/img/new-landing-page/shio.png')} className="mb-3" width="70" height="70" alt="robot"/>
          <h4 style={{fontWeight:"bolder"}}>{t('shioBiometricTitle')}</h4>
          <p>{t('shioBiometricDesc')}</p>
          </div>
        </Col>
        <Col md="4" sm="12" className="mb-3">
          <div className="text-md-left">
          <img src={require('../../assets/img/new-landing-page/sentimenAnalisis.png')} className="mb-3" width="90" height="70" alt="robot"/>
          <h4 style={{fontWeight:"bolder"}}>{t('sentimentAnalysistTitle')}</h4>
          <p>{t('sentimentAnalysistDesc')}</p>
          </div>
        </Col>
      </Row>
    </section>
  )
}

export default translate(WhyAikrut)
