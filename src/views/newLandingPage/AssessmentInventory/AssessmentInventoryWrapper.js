import React, { useState } from 'react'
import ContainerComp from "./ContainerComp";
import {
  Row,
  Col,
} from "reactstrap";

function AssessmentInventoryWrapper() {
  const [isActive, setIsActive] = useState(0)

  const data = [
    {
      id: 1,
      title: "Leadership Test",
      text: "merupakan pengukuran kompetensi yang akan memberikan Anda data mengenai kemampuan dan model kepemimpinan individu dalam organisasi. Hasil yang didapatkan dari leadership test ini akan berupa 4 model kepemimpinan yang berupa mengukur seberapa jauh seorang pemimpin mampu mengembangkan tugas agar lebih sistematis dan mengukur kemampuan manajerial untuk mengumpulkan orang untuk bekerja sama dalam memecahkan permasalahan."
    },
    {
      id: 2,
      title: "Personality Test",
      text: "merupakan pengukuran kompetensi yang memberikan Anda data mengenai  kepribadian perilaku individu dalam situasi kerja. Hasil mengenai kepribadian individu sendiri diambil dari data-data berikut antara lain motivasi yang mendorong suatu perilaku individu dan gaya komunikasi individu dalam lingkungan kerja."
    },
    {
      id: 3,
      title: "Palmistry",
      text: "merupakan pengukuran karakteristik kepribadian individu yang terukur dari hasil analisis garis tangan. Sehingga dapat mengenali lebih dekat mengenai karakter individu dan menjadikan hal tersebut sebagai referensi nilai tambahan."
    },
    {
      id: 4,
      title: "Bazi",
      text: "merupakan pengukuran karakteristik kepribadian individu berdasarkan chinese astrology. Melalui pengukuran ini Anda akan mendapatkan detail karakteristik individu, bakat individu, dan saran pengembangan pekerjaan hingga partner dalam bekerja masing-masing individu."
    },
    {
      id: 5,
      title: "Work Style Test",
      text: "merupakan pengukuran potensi yang akan memberikan Anda data mengenai dinamika kepribadian Individu yang dipengaruhi oleh situasi kerja sekitarnya. Data-data tersebut terdiri atas kebiasaan pola pikir dan tingkah laku individu, tujuan ataupun dorongan kandidat untuk berperilaku dan mengapa individu berperilaku demikian. Melalui work style test ini menghasilkan gambaran kepribadian secara keseluruhan yang utuh tidak terpisah-pisah."
    },
    {
      id: 6,
      title: "Cognitive Ability Test",
      text: (<>merupakan pengukuran potensi yang akan memberikan Anda data berupa kapasitas intelegensi secara umum. Faktor umum dari data intelegensi ini mendasari beberapa kemampuan individu dalam melakukan hal-hal tertentu. Hasil yang dimunculkan dalam cognitive ability test akan berupa 5 level kecerdasan antara lain: <i>Intellectually superior, Definitely above the average in intellectual capacity, Intellectually average, Definitely below the average in intellectual capacity, Intellectually defective.</i></>)
    },
    {
      id: 7,
      title: "Eye Accesing Cues",
      text: "merupakan pengukuran melalui pola gerakan mata yang dilakukan di bawah alam sadar individu saat mengakses informasi tertentu dalam pikirannya menggunakan teknik Neuro-Linguistic Programming (NLP). Pola gerakan mata ini seringkali menjadi data tambahan sebagai alat observasi individu dalam lingkup rekrutmen."
    }
  ]
  
  const [currentMenu, setCurrentMenu] = useState(data[0])

  const changeMenu = (num) => {
    setCurrentMenu(data[num])
    setIsActive(num)
  }

  return (
    <section className="container mb-5" style={{paddingTop:"10px"}}>
      <div className="d-md-block text-center mb-md-5">
        <h2 style={{fontWeight:'bold'}}>Inventori Asesmen</h2><br/>
        <p>Dapatkan berbagai jenis asesmen untuk kandidat perusahaan anda</p>
      </div>
      <Row>
        <Col md="3" className="side-menu">
          <h3 className="side-menu-title" style={{color:"#18568B", fontWeight:"bold"}}>Inventori Asesmen</h3>
          <div className="side-menu-list text-left">
            <ul style={{padding:"0"}}>
              {data.map((menu, idx) => (
                <li>
                  <button 
                    type="button" 
                    className={isActive === idx ? "btn-sideMenu active" : "btn-sideMenu"} 
                    onClick={() => changeMenu(menu.id - 1)}
                  >
                    {menu.title}
                  </button>
                </li>
              ))}
            </ul>

          </div>
        </Col>
        <Col>
          <ContainerComp title={currentMenu.title} text={currentMenu.text} />
        </Col>
      </Row>
    </section>
  )
}

export default AssessmentInventoryWrapper
