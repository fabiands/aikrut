import React from "react";
import { Card, CardTitle, CardBody } from "reactstrap";

function ContainerComp(props) {
  return (
    <section>
      <Card className="container-assessment-inventory shadow">
        <CardTitle>
          <h3 className="ml-3" style={{ fontWeight: "bold", color: "#18568B" }}>
            {props.title}
          </h3>
        </CardTitle>
        <CardBody className="text-justify">
          <i>{props.title}</i> {props.text}
        </CardBody>
      </Card>
    </section>
  );
}

export default ContainerComp;
