import React, { useEffect } from 'react'
import PageLayout from '../../Pages/Layout/PageLayout';
import AssessmentInventoryWrapper from "./AssessmentInventoryWrapper";
import "./assessmentInventory.scss";
import TryForFreeComp from '../TryForFreeComp';


function AssessmentInventory() {
  const windowOnScroll = () => {
    if (window.scrollY > 0) {
      if (
        !document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.add("shadow-sm");
      }
    } else {
      if (
        document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.remove("shadow-sm");
      }
    }
  }
  
  useEffect(() => {
    window.addEventListener("scroll", windowOnScroll);
    
    return() => {
      window.removeEventListener("scroll", windowOnScroll);
  
    }
  })
  return (
    <PageLayout>
      <AssessmentInventoryWrapper/>
      <TryForFreeComp/>
    </PageLayout>
  )
}

export default AssessmentInventory
