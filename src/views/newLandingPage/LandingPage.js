import React, { useRef, useEffect } from 'react';
import Hero from "./Hero";
import PageLayout from '../Pages/Layout/PageLayout';
import Partner from './Partner';
import WhyAikrut from './WhyAikrut';
import OurProduct from './OurProduct';
import Benefit from './Benefit';
import ContactUs from './ContactUs';
import {translate} from "react-switch-lang";
import { useLocation } from 'react-router-dom';


function LandingPage() {
  const contactRef = useRef();
  const location = useLocation()
  // console.log(location)
  // console.log(contactRef)

  useEffect(() => {
    if(location.hash === '#try'){
      window.scrollTo({
        top: Number(contactRef?.current?.offsetTop) - 80,
        left: 0,
        behavior: 'smooth'
      })
    }
  }, [location])

  const windowOnScroll = () => {
    if (window.scrollY > 0) {
      if (
        !document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.add("shadow-sm");
      }
    } else {
      if (
        document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.remove("shadow-sm");
      }
    }
  }
  
  useEffect(() => {
    window.addEventListener("scroll", windowOnScroll);
    
    return() => {
      window.removeEventListener("scroll", windowOnScroll);
  
    }
  })

    return (
        <PageLayout>
            <Hero />
            <Partner/>
            <div className="wrapper-landing-bg">
              <WhyAikrut/>
              <OurProduct/>
              <Benefit/>
            </div>
            <ContactUs ref={contactRef} />
        </PageLayout>
    )
}

export default translate(LandingPage)
