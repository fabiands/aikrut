import React from 'react'
import PageLayout from '../Pages/Layout/PageLayout'
import {
  Container,

} from 'reactstrap'
import aboutUsImg from "../../assets/img/new-landing-page/aboutus.png"

function About() {
  return (
    <PageLayout>
      <Container> 
        <section className="px-5" style={{paddingTop:"10px", height:"100vh", marginBottom:"50px"}}>
          <div className="d-md-block text-center mb-md-2">
            <h2 style={{fontWeight:'bold'}}>Tentang Kami</h2><br/>
          </div>
          <div className="mb-4">
            <img src={aboutUsImg} width="100%" alt="about us" />
          </div>
          <div className="aboutus-desc-section">
            <h2 className="text-center" style={{fontWeight:'bold', color:"#3767B9"}}>Aikrut</h2>
            <p className="text-justify" style={{lineHeight:"2"}}>Aikrut adalah platform online interview dan online asesment dengan teknologi kecerdasan buatan yang mampu untuk meningkatkan efisiensi kinerja HR dalam rekrutmen. Aikrut.id dapat membantu melakukan interview online dengan para kandidat dimanapun dan kapanpun, tidak hanya itu aikrut.id dilengkapi fitur yang dapat memberikan perspektif lebih terhadap seorang pelamar dibanding hanya menggunakan CV konvensional. Dalam aikrut kami menghadirkan alat tes yaitu DISC, MBTI, PAPI Kostick, MSDT, Tes Kemampuan Kognitif, Gesture Asesmen, dan Biometric</p>
          </div>
        </section>
      </Container>
    </PageLayout>
  )
}

export default About
