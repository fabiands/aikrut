import React, { useEffect, useState } from "react";
import PageLayout from "../Pages/Layout/PageLayout";
import { Container } from "reactstrap";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";

function FAQ(props) {
  const [isOpen, setIsOpen] = useState(null);
  const data = [
    {
      id: 1,
      question: "Apa itu Aikrut.id",
      answer:
        "Kecerdasan buatan untuk menemukan kandidat yang tepat yang memiliki keselarasan nilai individual dengan perusahaan Anda.",
    },
    {
      id: 2,
      question: "Apakah Aikrut.id sama dengan  Job Portal dan Talent Hunting?",
      answer:
        "Aikrut berbeda dengan Job Portal dan Talent Hunting. Aikrut.id akan berperan  membantu Anda dalam simplifikasi proses rekrutmen dibantu dengan robot digital milik kami sehingga Anda bisa mendapat data saintifik kandidat secara tepat dimanapun dan kapanpun.",
    },
    {
      id: 3,
      question: "Apa saja yang bisa Aikrut.id bantu untuk perusahaan? ",
      answer:
        "Dengan teknologi kecerdasan buatan (Artificial Intelligence), aikrut membantu Anda menemukan kandidat-kandidat yang selaras dengan nilai perusahaan Anda. Melalui asesmen yang dilakukan bersama aikrut, Anda akan menemukan nilai-nilai individual pada masing-masing kandidat. Maka dengan begitu, Anda bisa memilih kandidat terbaik melalui nilai individual yang dimiliki oleh kandidat berdasarkan nilai yang dipegang oleh perusahaan.",
    },
    {
      id: 4,
      question: "Apa Saja Fitur Alat Tes yang tersedia di Aikrut.id?",
      answer: (
        <>
          Aikrut menghadirkan berbagai fitur terbaik yang tergabung dengan
          teknologi kecerdasan buatan antara lain online assessment yang terdiri
          atas{" "}
          <i>
            {" "}
            Aptitude Test, Personality Test, Work Style test, Entrepreneur Test,
            Learning Agility Test, Leadership Test, dan Cognitive Ability Test.
          </i>
        </>
      ),
    },
    {
      id: 5,
      question: "Apa Manfaat Bagi Perusahaan Jika Menggunakan Aikrut.id? ",
      answer: (
        <>
          <ul>
            <li>Meningkatkan kinerja HR untuk profiling pelamar.</li>
            <li>
              Membantu HR mengenali potensi dan kemampuan pelamar sejak awal.
            </li>
            <li>
              Tersedia berbagai macam fitur alat tes yang dapat dipilih oleh HR
              sesuai dengan kebutuhan.
            </li>
            <li>
              Kami sediakan recruitment platform untuk akses para pelamar dengan
              URL secara khusus.
            </li>
            <li>
              Hemat biaya karena tidak dikenakan biaya tambahan untuk
              maintenance, beban server, dan engineer.
            </li>
            <li>
              Tidak ada mobilitas dan kontak fisik, karena para pelamar dapat
              melakukan wawancara dan asesmen dimanapun dan kapanpun dan HR
              dapat mengakses hasil interview dan asesmen asesmen dimanapun dan
              kapanpun secara online .
            </li>
            <li>
              Hasil wawancara dan asesmen bisa dilihat oleh HR seketika setelah
              selesai melakukan wawancara dan asesmen dalam platform kami.
            </li>
            <li>
              Kami berikan akun uji coba (trial) selama 60 hari gratis akses
              semua fitur.
            </li>
          </ul>
        </>
      ),
    },
    {
      id: 6,
      question: "Bagaimana Jika Perusahaan Kami Ingin Menggunakan Aikrut?",
      answer:
        "Kami akan bekerjasama dengan perusahaan mitra kami dengan skema subscription (berlangganan) dimana Perusahaan mitra hanya membayar sekali di awal untuk mendapatkan Token yang nantinya akan digunakan dalam mengakses fitur-fitur yang tersedia di aikrut.",
    },
    {
      id: 7,
      question: "Apa itu Token?",
      answer:
        "Token adalah kredit yang digunakan untuk membuka akses fitur sesuai dengan kebutuhan perusahaan dan penggunaannya dikontrol oleh Team HR sehingga lebih efisien dan fleksibel.",
    },
  ];

  const windowOnScroll = () => {
    if (window.scrollY > 0) {
      if (
        !document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.add("shadow-sm");
      }
    } else {
      if (
        document.getElementsByTagName("nav")[0].classList.contains("shadow-sm")
      ) {
        document.getElementsByTagName("nav")[0].classList.remove("shadow-sm");
      }
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", windowOnScroll);

    return () => {
      window.removeEventListener("scroll", windowOnScroll);
    };
  });

  return (
    <PageLayout footer={props.footer} navbar={props.navbar}>
      <section className="container mb-5" style={{ paddingTop: "10px" }}>
        <div className="assessment-header d-md-block text-center mb-md-5">
          <h3 style={{ fontWeight: "bold" }}>FAQ</h3>
          <br />
        </div>
        <Container>
          <Accordion>
            {data.map((data, idx) => (
              <AccordionItem
                className="faq-accordion-card shadow px-4 pt-3 pb-2"
                key={idx}
                onClick={() => {
                  setIsOpen(idx);
                }}
              >
                <AccordionItemHeading className="faq-accordion-heading mb-2">
                  <AccordionItemButton
                    id="faq-heading"
                    className="d-flex justify-content-between align-items-center flex-rows"
                  >
                    <h4 className="faq-title" style={{ fontWeight: "bold" }}>
                      {data.question}
                    </h4>
                    {isOpen === idx ? (
                      <i className="fa fa-minus" />
                    ) : (
                      <i className="fa fa-plus" />
                    )}
                  </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel
                  id="faq-body"
                  className="faq-accordion-body"
                >
                  <p style={{ fontSize: "16px" }}>{data.answer}</p>
                </AccordionItemPanel>
              </AccordionItem>
            ))}
          </Accordion>
        </Container>
      </section>
    </PageLayout>
  );
}

export default FAQ;
