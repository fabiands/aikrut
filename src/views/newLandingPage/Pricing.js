import React from "react";
import { Row, Col, Card, Container } from "reactstrap";
import PageLayout from "../Pages/Layout/PageLayout";
import CheckList from "../../assets/img/new-landing-page/check-circle.png";
import { translate } from "react-switch-lang";

function Pricing() {
  const data = [
    {
      id: 1,
      color: "#3767B9",
      title: (
        <div className="text-left">
          <p className="titleFreePackage" style={{ position: "relative" }}>
            Free Package
            <span
              className="ml-2"
              style={{ position: "absolute", top: "10px" }}
            >
              *)
            </span>
          </p>
        </div>
      ),
      desc1: "Mempublikasikan lowongan pekerjaan tak terbatas",
      desc2:
        "Akses bebas 30 hari untuk AI Recruitment, Asesmen, dan fitur lainnya.",
      buttonGet: "Dapatkan Paket Gratis",
    },
    {
      id: 2,
      color: "#3EAF80",
      title: "Business Package",
      desc1: "Mempublikasikan lowongan pekerjaan tak terbatas",
      desc2:
        "Akses bebas 6 bulan untuk AI Recruitment, Asesmen, dan fitur lainnya.",
      buttonGet: "Dapatkan Paket",
    },
    {
      id: 3,
      color: "#F19B1A",
      title: "Pro Package",
      desc1: "Mempublikasikan lowongan pekerjaan tak terbatas",
      desc2:
        "Akses bebas 1 tahun untuk AI Recruitment dan Asesmen, tes psikologi, Palmistry, Astrologi china dan fitur lainnya",
      buttonGet: "Dapatkan Paket",
    },
  ];
  return (
    <PageLayout>
      <Container>
        <section
          className="mb-5"
          style={{ paddingTop: "10px", height: "100vh" }}
        >
          <div className="d-md-block text-center mb-5">
            <h2 style={{ fontWeight: "bold" }}>Harga</h2>
            <br />
            <p>
              Temukan paket harga terbaik Anda yang telah kami siapkan untuk
              memudahkan perekrutan perusahaan Anda
            </p>
          </div>
          <Row>
            {data.map((cardPackage, idx) => (
              <Col md="4">
                <Card className="pricing-card shadow pb-2" key={idx}>
                  <div
                    style={{
                      width: "100%",
                      height: "10px",
                      backgroundColor: `${cardPackage.color}`,
                    }}
                  />
                  <div className="container p-4">
                    <h3 className="text-center" style={{ minHeight: "50px" }}>
                      {cardPackage.title}
                    </h3>
                    <br />
                    <br />
                    <div className="mb-5" style={{ minHeight: "140px" }}>
                      <ul className="p-0">
                        <li className="d-flex flex-rows align-items-start">
                          <span>
                            <img
                              src={CheckList}
                              className="mr-2"
                              alt="checklist"
                            />
                          </span>
                          <p>{cardPackage.desc1}</p>
                        </li>
                        <li className="d-flex flex-rows align-items-start">
                          <span>
                            <img
                              src={CheckList}
                              className="mr-2"
                              alt="checklist"
                            />
                          </span>
                          <p>{cardPackage.desc2}</p>
                        </li>
                      </ul>
                    </div>
                    <div className="text-center">
                      <a
                        href="/register"
                        className="btn btn-lg btn-freePacket p-2 px-3"
                      >
                        {cardPackage.buttonGet}
                      </a>
                    </div>
                  </div>
                </Card>
              </Col>
            ))}
          </Row>
        </section>

        {/* <section>
        <div className="assessment-header d-md-block text-center mb-md-5">
          <h2 style={{fontWeight:'bold'}}>Pertanyaan yang sering diajukan</h2><br/>
        </div>
        <Accordion>
          <AccordionItem>
            <AccordionItemHeading>
              <AccordionItemButton>
                <h3>Lorem Ipsum Dolor Sit Amet</h3>
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </AccordionItemPanel>
          </AccordionItem>
        </Accordion>
      </section> */}
      </Container>
    </PageLayout>
  );
}

export default translate(Pricing);
