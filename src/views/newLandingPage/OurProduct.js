import React, {useRef, useState, useEffect} from 'react'
import {
  Row,
  Col
} from "reactstrap";
import { translate, t } from "react-switch-lang";
import CheckList from "../../assets/img/new-landing-page/check-circle.png"


function OurProduct() {
  const playerRef = useRef(null);
  const [isPlayed, setIsPlayed] = useState(false)

  useEffect(() => {
    const tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    const videoId = "7w8XDixv1Lw";
    console.log(playerRef);
    window.onYouTubeIframeAPIReady = function (e) {
        const YT = window.YT;
        playerRef.current = new YT.Player('playerId', {
            videoId,
            width: 600,
            height: 443
        })
    }

    return () => {
        tag.remove();
    }
  })


  useEffect(() => {
      if (playerRef && playerRef.current) { 
        if (!isPlayed) {
            setIsPlayed(true)
            playerRef.current.playVideo();
        }
      }
      // eslint-disable-next-line
  }, [])

  const hideSeeVideoComp = () => {
    document.getElementById("ourProduct-seevideo").style.display = "none";
  };
  const displayVideo = () => {
    document.getElementById("playerId").style.display = "block"; 
  };
  
  const showVideoPlayer = () => {
    hideSeeVideoComp()
    displayVideo();
  };

  return (
    <section id="ourProduct" className="container landing-ourProduct mx-auto pb-4">
      <Row>
        <Col md="7">
          <div className="d-flex justify-content-center align-items-center h-100">

            <div id="ourProduct-seevideo" className="ourProducySeeVideo text-center" style={{width:"400px"}}>
              <p><strong>Aikrut.id</strong>{" "} {t('ourProductVideoDesc')}</p>
              <button type="button" id="btn-playVideo" className="btn-play px-4 py-auto" onClick={showVideoPlayer}>
                <span className="d-flex flex-row align-items-center">
                  <i className="fa fa-caret-right" style={{fontSize:"40px", marginRight:"5px"}}></i>Lihat Video
                </span>
              </button>
            </div>
              <div id="playerId" style={{display:"none"}}/>
          </div>
        </Col>
        <Col md="5" className="ourProduct-section-right mt-5">
          <h3 className="ourProduct-heading mb-4" style={{fontWeight:"bold"}}>{t('Produk Kami')}</h3>
          <p className="mb-4">{t("ourProductDesc")}</p>
          <div className="ourProduct-list text-sm-left">
            <p><img className="mr-3" src={CheckList} alt="checklist" />Leadership Test</p>
            <p><img className="mr-3" src={CheckList} alt="checklist" />Personality Test</p>
            <p><img className="mr-3" src={CheckList} alt="checklist" />Palmistry</p>
            <p><img className="mr-3" src={CheckList} alt="checklist" />Bazi</p>
            <p><img className="mr-3" src={CheckList} alt="checklist" />Work Style Test</p>
            <p><img className="mr-3" src={CheckList} alt="checklist" />Cognitive ability Test</p>
            <p><img className="mr-3" src={CheckList} alt="checklist" />Eye Accesing Cues</p>
          </div>
        </Col>
      </Row>
    </section>
  )
}


export default translate(OurProduct)
