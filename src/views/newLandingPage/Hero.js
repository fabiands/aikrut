import React from 'react';
import Fade from 'react-reveal/Fade';
import { translate, t } from "react-switch-lang";
import HeroImg from "../../assets/img/new-landing-page/hero-aikrut.png";

function Hero(props) {
  return (
    <section className="container hero mb-3" id="home">
        <div className="d-flex justify-content-between align-items-center hero-content mb-2">
            <Fade>
          <div className="col-lg-6 col-md-6 col-12">
              <h1 className="mb-4">{t('#RekrutLebihCepat')}</h1>
              <h6 className="hero-text mb-md-5 mb-lg-5 mb-sm-3">{t('Online profiling platform berbasis analytics data, mempertemukan Anda dengan banyak kandidat secara efektif dan efisien. Profiling terkini dengan teknologi Artificial Intelligence minim bias dan hasil asesmen yang tetap informatif dan komprehensif. ')}</h6>

              <a
                href="/register"
                className="btn btn-lg btn-netis-primary btn-tryForFree px-5 py-3" 
                style={{backgroundColor:"#3767B9", color:"white"}}
                >
                  {t('cobagratis')}
                </a>
          </div>
          <div className="hero-image mt-4">
            <img src={HeroImg} alt="hero-aikrut" />
          </div>
          </Fade>
        </div>
    </section>
  )
}

export default translate(Hero)
