import React, { Suspense } from "react";
import { Switch, Redirect, withRouter } from "react-router-dom";
import { TabContent, TabPane } from "reactstrap";
import { translate } from "react-switch-lang";
import AuthRoute from "../../../components/AuthRoute";
import Spinner from "reactstrap/lib/Spinner";
import RecruitmentsFiltersProvider from "./Context/RecruitmentContext";
import GeneratePdfProvider from "./Applicants/ApplicantContext";

const SubscriptionVacancies = React.lazy(() =>
  import("./SubscriptionVacancies")
);
// const RecruitmentMenu = React.lazy(() => import("./RecruitmentMenu"));
// const RecruitmentCreate = React.lazy(() => import("./RecruitmentCreate"));
const ApplicantList = React.lazy(() => import("./Applicants/ApplicantList"));
const ApplicantDetail = React.lazy(() =>
  import("./Applicants/ApplicantDetail")
);
const ApplicantCompare = React.lazy(() =>
  import("./Applicants/Comparison/ApplicantCompare")
);
// const RecruitmentDetail = React.lazy(() => import("./RecruitmentDetail"));
// const VacancyApplicantList = React.lazy(() =>
//   import("./Applicants/VacancyApplicantList")
// );
const LinkApplicantCreate = React.lazy(() =>
  import("./Internal/ApplicantCreate")
);
const LinkAssessmentCreate = React.lazy(() =>
  import("./Internal/LinkAssessmentCreate")
);
const LinkAssessmentEdit = React.lazy(() =>
  import("./Internal/LinkAssessmentEdit")
);
const LinkAssessmentUser = React.lazy(() =>
  import("./Internal/InternalAssessmentDetail")
);

function SubscriptionWrapper({ location, match }) {
  const routes = [
    {
      path: match.path + "/",
      exact: true,
      // privileges: ["canManagementJob"],
      component: SubscriptionVacancies,
    },
    // {
    //   path: match.path + "/vacancies/create",
    //   exact: true,
    //   privileges: ["canManagementJob"],
    //   component: RecruitmentCreate,
    // },
    // {
    //   path: match.path + "/vacancies/:id/edit",
    //   exact: true,
    //   privileges: ["canManagementJob"],
    //   component: RecruitmentDetail,
    // },
    // {
    //   path: match.path + "/vacancies/:id/applicants",
    //   exact: true,
    //   // privileges: ["canManagementJob"],
    //   component: VacancyApplicantList,
    // },
    {
      path: match.path + "/applicants",
      exact: true,
      // privileges: ["canManagementJob"],
      component: ApplicantList,
    },
    {
      path: match.path + "/applicants/:applicantId",
      exact: true,
      // privileges: ["canManagementJob"],
      component: ApplicantDetail,
    },
    {
      path: match.path + "/applicants/:applicantId/compare",
      exact: true,
      component: ApplicantCompare,
    },
    // {081804111086
    //   path: match.path + "/applicants/:applicantId/download",
    //   exact: true,
    //   component: ApplicantDownloadAssessment,
    // },
    {
      path: match.path + "/internal/create",
      exact: true,
      privileges: ["canInternalAssesment"],
      component: LinkAssessmentCreate,
    },
    {
      path: match.path + "/internal/:id/edit",
      exact: true,
      privileges: ["canInternalAssesment"],
      component: LinkAssessmentEdit,
    },
    {
      path: match.path + "/internal/:id/participant",
      exact: true,
      privileges: ["canInternalAssesment"],
      component: LinkAssessmentUser,
    },
    {
      path: match.path + "/internal/applicant/create/:id",
      exact: true,
      privileges: ["canInternalAssesment"],
      component: LinkApplicantCreate,
    },
  ];
  return (
    <TabContent className="shadow-sm rounded">
      <TabPane className="p-0">
        <Suspense
          fallback={
            <div
              style={{
                position: "absolute",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                background: "rgba(255,255,255, 0.5)",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Spinner style={{ width: 48, height: 48 }} />
            </div>
          }
        >
          <RecruitmentsFiltersProvider>
            <GeneratePdfProvider>
              <Switch>
                {routes.map((route) => (
                  <AuthRoute key={route.path} {...route} />
                ))}
                {routes[0] && (
                  <Redirect exact from={match.path} to={routes[0].path} />
                )}
              </Switch>
            </GeneratePdfProvider>
          </RecruitmentsFiltersProvider>
        </Suspense>
      </TabPane>
    </TabContent>
  );
}

export default translate(withRouter(SubscriptionWrapper));
