import React, { useState, useEffect, useMemo, useRef } from "react";
import {
  Button,
  Form,
  Input,
  Label,
  Row,
  Col,
  CustomInput,
  Spinner,
} from "reactstrap";
import { useFormik } from "formik";
import Select from "react-select";
import CreatableSelect from "react-select/creatable";
import request from "../../../utils/request";
import { toast } from "react-toastify";
import NumberFormat from "react-number-format";
import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import * as Yup from "yup";
import { draftToMarkdown, markdownToDraft } from "markdown-draft-js";
import LoadingAnimation from "../../../components/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { t } from "react-switch-lang";
import * as moment from "moment";
import Modal from "reactstrap/lib/Modal";
import ModalBody from "reactstrap/lib/ModalBody";
import ModalHeader from "reactstrap/lib/ModalHeader";
import { useUserBalance } from "../../../hooks/useUserBalance";
import ModalError from "../../../components/ModalError";
import { useAuthUser } from "../../../store";
import { useDispatch } from "react-redux";
import disableScroll from "disable-scroll";
import Tour from "reactour";
import { getMe } from "../../../actions/auth";
import langUtils from "../../../utils/language/index";
import { connect } from "react-redux";
const SkillInput = ({ onChange, values = [], placeholder }) => {
  const [inputValue, setInputValue] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const handleInputChange = React.useCallback(
    function (value) {
      if (errorMessage) {
        setErrorMessage(null);
      }
      setInputValue(value);
    },
    [errorMessage]
  );

  function handleKeyDown(event) {
    if (!inputValue) return;
    switch (event.key) {
      case "Enter":
      case "Tab":
      case ",":
        if (
          values.some((val) => val.toLowerCase() === inputValue.toLowerCase())
        ) {
          setErrorMessage(`'${inputValue}' ${t("alreadyInTheField")}`);
          event.preventDefault();
          return;
        }
        setInputValue("");
        onChange([...values, inputValue]);
        event.preventDefault();
        break;
      default:
    }
  }

  return (
    <React.Fragment>
      <CreatableSelect
        components={{ DropdownIndicator: null }}
        isClearable
        isMulti
        menuIsOpen={false}
        placeholder={placeholder}
        value={values.map((value) => ({ value, label: value }))}
        onChange={(newItems) =>
          onChange(newItems ? newItems.map((item) => item.value) : [])
        }
        inputValue={inputValue}
        onInputChange={handleInputChange}
        onKeyDown={handleKeyDown}
      />
      {errorMessage && <small className="text-danger">{errorMessage}</small>}
    </React.Fragment>
  );
};

const options = [
  { value: "Full-time", label: "Full-Time" },
  { value: "Part-time", label: "Part-Time" },
  { value: "Kontrak", label: t("kontrak") },
  { value: "Freelance", label: "Freelance" },
  { value: "Magang", label: t("magang") },
];

const experience = [
  { value: 0, label: "Fresh Graduate" },
  { value: 1, label: t("1tahun") },
  { value: 2, label: t("2tahun") },
  { value: 3, label: t("3tahun") },
  { value: 4, label: t("4tahun") },
  { value: 5, label: t("5tahunlebih") },
];

const education = [
  { value: "sd", label: t("sd") },
  { value: "smp", label: t("smp") },
  { value: "sma", label: t("sma") },
  { value: "d1", label: t("d1") },
  { value: "d2", label: t("d2") },
  { value: "d3", label: t("d3") },
  { value: "d4", label: t("d4") },
  { value: "s1", label: t("s1") },
  { value: "s2", label: t("s2") },
  { value: "s3", label: t("s3") },
];

const country = [{ value: 1, label: "Indonesia" }];

const defaultContent = convertFromRaw(
  markdownToDraft(`
**${t("jobDescription")}**
- ${t("jobDescriptionPoint1")}
- ${t("jobDescriptionPoint2")}
- ${t("jobDescriptionPoint3")}
- ${t("jobDescriptionPoint4")}
- ${t("jobDescriptionPoint5")}

**${t("qualification")}**
- ${t("qualificationPoint1")}
- ${t("qualificationPoint2")}
- ${t("qualificationPoint3")}
- ${t("qualificationPoint4")}
- ${t("qualificationPoint5")}
`)
);
const defaultContentEditorState = EditorState.createWithContent(defaultContent);

function RecruitmentCreate(props) {
  const { loading, data } = useUserBalance();
  const myBalance = useMemo(() => data?.balance ?? 0, [data]);
  const isExpired = useMemo(() => data?.isExpired, [data]);

  const [province, setProvince] = useState([]);
  const [city, setCity] = useState([]);
  const [cityForm, setCityForm] = useState(true);
  const [notFound, setNotFound] = useState(false);
  const [submitLoadPublish, setSubmitLoadPublish] = useState(false);
  const [submitLoadDraft, setSubmitLoadDraft] = useState(false);
  const [type, setType] = useState(null);
  const [disablePublish, setDisablePublish] = useState(false);
  const [disableDraft, setDisableDraft] = useState(false);
  const [modalNonComplete, setModalNonComplete] = useState(false);
  const [modalTokenNull, setModalTokenNull] = useState(false);
  const user = useAuthUser();
  const [isTour, setIsTour] = useState(false);
  const disableBody = () => disableScroll.on();
  const enableBody = () => disableScroll.off();
  const accentColor = "#1d5a8e";
  const dispatch = useDispatch();
  const detailRef = useRef();
  const requirementRef = useRef();
  const descriptionRef = useRef();
  const publishRef = useRef();
  const [req, setReq] = useState([]);
  const [isDiscChecked, setIsDiscChecked] = useState(true);
  const [isVideoChecked, setIsVideoChecked] = useState(true);
  const [isProfileChecked, setIsProfileChecked] = useState(true);
  const [loadingReq, setLoadingReq] = useState(false);
  const [error, setError] = useState(false);

  const [editorState, setEditorState] = useState(defaultContentEditorState);
  const editorStateChange = (changeEditorState) => {
    setEditorState(changeEditorState);
    formik.setFieldValue(
      "description",
      draftToMarkdown(convertToRaw(changeEditorState.getCurrentContent()))
    );
  };

  useEffect(() => {
    setLoadingReq(true);
    request
      .get("v2/recruitment/vacancies/requirements")
      .then((res) => {
        setReq(res.data.data);
      })
      .catch(() => setError(true))
      .finally(() => setLoadingReq(false));
  }, []);

  const ValidationFormSchema = useMemo(() => {
    return Yup.object().shape({
      minSalary: Yup.number().required().label("gaji minimal"),
      requirements: Yup.array().test(
        "required",
        t("fillInAssessmentNeeds"),
        function (value) {
          return value.length > 0;
        }
      ),
      maxSalary: Yup.number()
        .min(
          Yup.ref("minSalary"),
          t("gaji maksimal harus lebih besar dari gaji minimal")
        )
        .required()
        .label("gaji maksimal"),
    });
  }, []);

  const { values, touched, errors, isSubmitting, ...formik } = useFormik({
    initialValues: {
      name: "",
      type: [],
      country: [],
      provinceId: "",
      cityId: "",
      minSalary: "",
      maxSalary: "",
      showSalary: false,
      minEducation: [],
      major: "",
      minYearExperience: "",
      description: "",
      skills: [],
      expiredAt: "",
      published: false,
      companyValue: [],
      requirements: [
        "mbti",
        "papikostick",
        "disc",
        "msdt",
        "spm",
        "bussiness-insight",
        "agility",
        "video",
        "fisiognomi",
        "palmistry",
        "profile",
        "shio",
        "zodiac",
        "fingerprint",
        "bazi",
      ],
    },
    validationSchema: ValidationFormSchema,
    onSubmit: (values, { setSubmitting, setErrors }) => {
      setSubmitting(true);
      if (!user.guidance.createJobTutorial) {
        request
          .put("auth/guidance", { guidance: "createJobTutorial" })
          .then(() => {
            dispatch(getMe());
          });
      }
      if (type === "publish") {
        setSubmitLoadPublish(true);
        setDisableDraft(true);
      }
      if (type === "draft") {
        setSubmitLoadDraft(true);
        setDisablePublish(true);
      }
      request
        .post("v1/recruitment/vacancies", {
          name: values.name,
          type: values.type.value,
          provinceId: values.provinceId.value,
          cityId: values.cityId.value,
          minSalary: values.minSalary,
          maxSalary: values.maxSalary,
          major: values.major,
          showSalary: values.showSalary,
          minEducation: values.minEducation.value,
          minYearExperience: values.minYearExperience.value,
          description: values.description,
          skills: values.skills,
          companyValue: values.companyValue,
          expiredAt: values.expiredAt
            ? moment(values.expiredAt).format("YYYY-MM-DD")
            : undefined,
          published: values.published,
          requirements: values.requirements,
        })
        .then(() => {
          toast.success(t("lowonganberhasildibuat"));
          if (!isComplete) {
            setModalNonComplete(true);
          } else if (isComplete) {
            props.history.goBack();
          }
        })
        .catch((err) => {
          if (err.response?.status === 422) {
            toast.error(`${t("errorFilledCheckData")}`);
            setErrors(err.response.data.errors);
            return;
          } else if (err.response?.status) {
            toast.error(`${t("errorFilledTryAgain")}`);
            setErrors(err.response.data.errors);
            return;
          }
          Promise.reject(err);
        })
        .finally(() => {
          setSubmitLoadPublish(false);
          setSubmitLoadDraft(false);
          setDisablePublish(false);
          setDisableDraft(false);
          setSubmitting(false);
        });
    },
  });

  const isComplete =
    values.name != null &&
    values.type?.value != null &&
    values.provinceId?.value != null &&
    values.cityId?.value != null &&
    values.minSalary != null &&
    values.maxSalary != null &&
    values.minEducation?.value != null &&
    values.major != null &&
    values.description != null &&
    values.skills?.length > 0;

  useEffect(() => {
    request
      .get("v1/location/provinces?countryId=1")
      .then((res) => {
        setProvince(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 404) {
          setNotFound(true);
        }
      });
  }, []);

  useEffect(() => {
    if (user.guidance.layout && user.guidance.header) {
      window.scroll({ top: 0, behavior: "smooth" });
      if (!user.guidance.createJob) {
        setIsTour(true);
      }
    }
  }, [user]);

  useEffect(() => {
    values.companyValue.map((data) => {
      data.label = t(data.label);
      return true;
    }); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [langUtils.getLanguage()]);

  const disableGuideCreateJob = () => {
    setIsTour(false);
    window.scroll({ top: 0, behavior: "smooth" });
    request.put("auth/guidance", { guidance: "createJob" }).then(() => {
      dispatch(getMe());
    });
  };

  const prov = province.map((province) => ({
    value: province.id,
    label: province.name,
  }));

  const changeProvince = function (province) {
    request
      .get(`v1/location/cities?provinceId=${province.value}`)
      .then((res) => {
        setCity(res.data.data);
      });
    setCityForm(false);
    formik.setFieldValue("provinceId", province);
    formik.setFieldValue("cityId", null);
    formik.setFieldTouched("provinceId", true);
  };

  const kota = city.map((city) => ({
    value: city.id,
    label: city.name.replace("KABUPATEN", t("district")).replace("KOTA", ""),
  }));

  const changeCity = function (city) {
    formik.setFieldValue("cityId", city);
    formik.setFieldTouched("cityId", true);
  };

  const changeJenisKerja = function (pilihan) {
    formik.setFieldValue("type", pilihan);
    formik.setFieldTouched("type", true);
  };

  const changeCountry = function (country) {
    formik.setFieldValue("country", country);
    formik.setFieldTouched("country", true);
  };

  const changePendidikan = function (edu) {
    formik.setFieldValue("minEducation", edu);
    formik.setFieldTouched("minEducation", true);
  };

  const changeExperience = function (edu) {
    formik.setFieldValue("minYearExperience", edu);
    formik.setFieldTouched("minYearExperience", true);
  };

  const formikSetValue = function (field) {
    return (value) => {
      formik.setFieldValue(field, value);
      formik.setFieldTouched(field, true);
    };
  };

  const companyValue = [
    { value: "F", label: t("companyValuePoint1") },
    { value: "W", label: t("companyValuePoint2") },
    { value: "N", label: t("companyValuePoint3") },
    { value: "G", label: t("companyValuePoint4") },
    { value: "A", label: t("companyValuePoint5") },
    { value: "L", label: t("companyValuePoint6") },
    { value: "P", label: t("companyValuePoint7") },
    { value: "I", label: t("companyValuePoint8") },
    { value: "T", label: t("companyValuePoint9") },
    { value: "V", label: t("companyValuePoint10") },
    { value: "X", label: t("companyValuePoint11") },
    { value: "S", label: t("companyValuePoint12") },
    { value: "B", label: t("companyValuePoint13") },
    { value: "O", label: t("companyValuePoint14") },
    { value: "R", label: t("companyValuePoint15") },
    { value: "D", label: t("companyValuePoint16") },
    { value: "C", label: t("companyValuePoint17") },
    { value: "Z", label: t("companyValuePoint18") },
    { value: "E", label: t("companyValuePoint19") },
    { value: "K", label: t("companyValuePoint20") },
  ];

  if (loading || loadingReq) {
    return <LoadingAnimation />;
  } else if (notFound) {
    return <DataNotFound />;
  } else if (error) {
    return <ModalError isOpen={true} />;
  }

  const toggle = () => {
    setModalTokenNull(!modalTokenNull);
  };

  const steps = [
    {
      selector: ".tour-detailJob",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("createJobVacanciesPoint1")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-12 text-right p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    enableBody();
                    window.scrollTo({
                      top: requirementRef.current.offsetTop - 80,
                    });
                    // window.scrollTo({
                    //     top: Number(requirementRef.current.offsetTop) - 80,
                    //     left: 0,
                    //     behavior: 'smooth'
                    // })
                    goTo(1);
                    disableBody();
                  }}
                >
                  {t("btnContinue")} <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-requirementJob",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("createJobVacanciesPoint2")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    enableBody();
                    window.scrollTo({ top: detailRef.current.offsetTop - 80 });
                    goTo(0);
                    disableBody();
                  }}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    enableBody();
                    window.scrollTo({
                      top: descriptionRef.current.offsetTop - 80,
                    });
                    goTo(2);
                    disableBody();
                  }}
                >
                  {t("btnContinue")} <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-descriptionJob",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("createJobVacanciesPoint3")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    enableBody();
                    window.scrollTo({
                      top: requirementRef.current.offsetTop - 80,
                    });
                    goTo(1);
                    disableBody();
                  }}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    enableBody();
                    window.scrollTo({ top: publishRef.current.offsetTop - 80 });
                    goTo(3);
                    disableBody();
                  }}
                >
                  {t("btnContinue")} <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-submitJob",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("createJobVacanciesPoint4")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    enableBody();
                    window.scrollTo({
                      top: descriptionRef.current.offsetTop - 80,
                    });
                    goTo(2);
                    disableBody();
                  }}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={() => {
                    disableGuideCreateJob();
                  }}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
  ];

  const changeRequirements = (e) => {
    const { value, checked } = e.target;
    if (value === "all") {
      if (checked) {
        const allSet = new Set(req);
        const allArr = Array.from(allSet);
        formik.setFieldValue("requirements", allArr);
        document.querySelector(".chooseCharacteristic").style.display = "flex";
        setIsDiscChecked(true);
        setIsVideoChecked(true);
        setIsProfileChecked(true);
      } else if (!checked) {
        formik.setFieldValue("requirements", []);
        formik.setFieldValue("companyValue", []);
        document.querySelector(".chooseCharacteristic").style.display = "none";
        setIsDiscChecked(false);
        setIsVideoChecked(false);
        setIsProfileChecked(false);
      }
    } else {
      let arr = values.requirements;
      arr.push(value);
      const set = new Set(arr);
      console.log(set);
      if (!checked) {
        set.delete(value);
      }

      const setArr = Array.from(set);
      // console.log(setArr)
      formik.setFieldValue("requirements", setArr);

      if (value === "papikostick") {
        if (checked) {
          document.querySelector(".chooseCharacteristic").style.display =
            "flex";
        } else if (!checked) {
          document.querySelector(".chooseCharacteristic").style.display =
            "none";
          formik.setFieldValue("companyValue", []);
        }
      }

      if (value === "disc") {
        if (checked) {
          setIsDiscChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter((e) => e !== "spm");
          setIsDiscChecked(false);
          formik.setFieldValue("requirements", newSetArr);
        }
      }

      if (value === "video") {
        if (checked) {
          setIsVideoChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter(
            (e) => e !== "fisiognomi" && e !== "palmistry"
          );
          formik.setFieldValue("requirements", newSetArr);
          setIsVideoChecked(false);
        }
      }

      if (value === "profile") {
        if (checked) {
          setIsProfileChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter(
            (e) => e !== "bazi" && e !== "zodiac" && e !== "shio"
          );
          formik.setFieldValue("requirements", newSetArr);
          setIsProfileChecked(false);
        }
      }
    }
  };

  return (
    <div className="animated fadeIn d-flex flex-column bd-highlight mb-3 p-4">
      <Tour
        steps={steps}
        showNavigation={false}
        accentColor={accentColor}
        showButtons={false}
        rounded={5}
        isOpen={isTour}
        closeWithMask={false}
        disableInteraction={true}
        disableFocusLock={true}
        onAfterOpen={disableBody}
        onBeforeClose={enableBody}
        onRequestClose={() => {
          disableGuideCreateJob();
        }}
      />
      <div className="bd-highlight mb-4">
        <h4>{t("createJobVacancies")}</h4>
        <hr />
      </div>

      <Form onSubmit={formik.handleSubmit}>
        <div className="row tour-detailJob" ref={detailRef}>
          <div className="col-md-2">
            <h5>{t("detailpekerjaan")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="12" className="mb-2">
                <Label htmlFor="name" className="input-label">
                  {t("posisi")} <span className="required">*</span>
                </Label>
                <Input
                  type="input"
                  value={values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="name"
                  id="name"
                  required
                  maxLength="255"
                  placeholder={t("namapekerjaan")}
                  className="form-control needs-validation"
                />
              </Col>

              <Col xs="12" className="mb-2">
                <Label htmlFor="type" className="input-label">
                  {t("Status Kepegawaian")} <span className="required">*</span>
                </Label>
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  isSearchable={false}
                  isClearable={true}
                  name="type"
                  id="type"
                  onChange={changeJenisKerja}
                  onBlur={formik.handleBlur}
                  value={values.type}
                  options={options}
                  placeholder={t("pilih")}
                  className="needs-validation"
                  required
                />
              </Col>

              <Col xs="12" className="mb-2">
                <Label htmlFor="country" className="input-label">
                  {t("negara")}
                </Label>
                <Select
                  isSearchable={false}
                  name="country"
                  id="country"
                  onChange={changeCountry}
                  onBlur={formik.handleBlur}
                  value={country[0]}
                  options={country}
                  defaultValue={country[0]}
                  placeholder={t("pilih")}
                />
              </Col>

              <Col xs="6" className="mb-2">
                <Label htmlFor="provinceId" className="input-label">
                  {t("provinsi")}
                  <span className="required">*</span>
                </Label>
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  isSearchable={true}
                  name="provinceId"
                  id="provinceId"
                  onChange={changeProvince}
                  onBlur={formik.handleBlur}
                  options={prov}
                  className="needs-validation"
                  required
                  placeholder={t("pilih")}
                />
              </Col>

              <Col xs="6" className="mb-2">
                <Label htmlFor="cityId" className="input-label">
                  {t("city")}
                  <span className="required">*</span>
                </Label>
                <Select
                  value={values.cityId}
                  isSearchable={true}
                  name="cityId"
                  id="cityId"
                  onChange={changeCity}
                  onBlur={formik.handleBlur}
                  isDisabled={cityForm}
                  options={kota}
                  // defaultValue={kota[0]}
                  className="needs-validation"
                  required
                  placeholder={t("pilih")}
                />
              </Col>
              <Col xs="6" className="mb-2">
                <Label htmlFor="minSalary" className="input-label">
                  {t("minSalary")}
                  <span className="required">*</span>
                </Label>
                <NumberFormat
                  // thousandSeparator={'.'}
                  // decimalSeparator={','}
                  // prefix={'Rp '}
                  name="minSalary"
                  id="minSalary"
                  allowNegative={false}
                  onChange={formik.handleChange}
                  value={values.minSalary}
                  className="form-control needs-validation"
                  required
                />
                {touched.minSalary && errors.minSalary && (
                  <small className="text-danger">{errors.minSalary}</small>
                )}
              </Col>

              <Col xs="6" className="mb-2">
                <Label htmlFor="maxSalary" className="input-label">
                  {t("maxSalary")}
                  <span className="required">*</span>
                </Label>
                <NumberFormat
                  // thousandSeparator={'.'}
                  // decimalSeparator={','}
                  // prefix={'Rp '}
                  // invalid={Boolean(touched.maxSalary && errors.maxSalary)}
                  name="maxSalary"
                  id="maxSalary"
                  allowNegative={false}
                  onChange={formik.handleChange}
                  value={values.maxSalary}
                  className="form-control needs-validation"
                  required
                />
                {touched.maxSalary && errors.maxSalary && (
                  <small className="text-danger">{errors.maxSalary}</small>
                )}
              </Col>
              <Col xs="6">
                <CustomInput
                  className="ml-1"
                  label={t("showSalary")}
                  id="showSalary"
                  name="showSalary"
                  type="checkbox"
                  checked={values.showSalary}
                  onChange={(event) =>
                    formik.setFieldValue("showSalary", event.target.checked)
                  }
                />
                {/* <small><i>
                                <span className="required">*</span>
                                {t('tandaigaji')}
                            </i></small> */}
              </Col>
            </Row>
          </div>
        </div>

        <hr />
        <>
          <div className="row">
            <div className="col-md-2">
              <h5>{t("Asesmen")}</h5>
            </div>
            <div className="col-sm-12 col-md-10">
              <Row form>
                <Col xs="12">
                  <CustomInput
                    type="checkbox"
                    id="all"
                    name="all"
                    label={t("semua")}
                    value="all"
                    onChange={changeRequirements}
                    checked={values?.requirements?.length === req?.length}
                  />
                  {req.length > 0 &&
                    req.map((item, idx) => {
                      return (
                        <>
                          {!isDiscChecked &&
                          item === "spm" ? null : !isVideoChecked &&
                            item === "palmistry" ? null : !isVideoChecked &&
                            item === "fisiognomi" ? null : !isProfileChecked &&
                            item === "bazi" ? null : !isProfileChecked &&
                            item === "shio" ? null : !isProfileChecked &&
                            item === "zodiac" ? null : (
                            <CustomInput
                              key={idx}
                              type="checkbox"
                              id={item}
                              name={item}
                              value={item}
                              label={t(item)}
                              onChange={changeRequirements}
                              checked={values?.requirements?.includes(item)}
                            />
                          )}
                        </>
                      );
                    })}
                  {touched.requirements && errors.requirements && (
                    <small className="text-danger">{errors.requirements}</small>
                  )}
                </Col>
              </Row>
              <Row className="chooseCharacteristic">
                <Col xs="12">
                  <Row form>
                    <Col className="mt-4">
                      <Label htmlFor="companyValue" className="input-label">
                        {t("karakteristik")}
                      </Label>
                      <Select
                        styles={{
                          menu: (provided) => ({ ...provided, zIndex: 9999 }),
                        }}
                        isSearchable={true}
                        name="companyValue"
                        id="companyValue"
                        closeMenuOnSelect={false}
                        onChange={(val) => {
                          if (val?.length > 3) return false;
                          formikSetValue("companyValue")(val);
                        }}
                        onBlur={formik.handleBlur}
                        value={values.companyValue}
                        options={companyValue}
                        isMulti
                      />
                      <small
                        className="text-form text-muted d-block"
                        style={{ fontSize: "9px" }}
                      >
                        {t("candidateCharacteristict")}
                      </small>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>

          <hr />
        </>

        <div className="row tour-requirementJob" ref={requirementRef}>
          <div className="col-md-2">
            <h5>{t("persyaratanpekerjaan")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="6" className="mb-2">
                <Label htmlFor="minEducation" className="input-label">
                  {t("pendidikanminimal")}
                  <span className="required">*</span>
                </Label>
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  isSearchable={false}
                  name="minEducation"
                  id="minEducation"
                  onChange={changePendidikan}
                  onBlur={formik.handleBlur}
                  value={values.minEducation}
                  options={education}
                  className="needs-validation"
                  required
                  placeholder={t("pilih")}
                />
              </Col>
              <Col xs="6" className="mb-2">
                <Label htmlFor="major" className="input-label">
                  {t("jurusan")}
                </Label>
                <Input
                  type="input"
                  value={values.major}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="major"
                  id="major"
                  maxLength="255"
                />
              </Col>
              <Col xs="12" className="mb-2">
                <Label htmlFor="minYearExperience" className="input-label">
                  {t("minimalpengalaman")}
                </Label>
                {/* <NumberFormat
                                suffix={' tahun'}
                                allowNegative={false}
                                name="minYearExperience"
                                id="minYearExperience"
                                onChange={formik.handleChange}
                                value={values.minYearExperience}
                                className="form-control"
                            />                         */}
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  isSearchable={false}
                  name="minYearExperience"
                  id="minYearExperience"
                  onChange={changeExperience}
                  onBlur={formik.handleBlur}
                  value={values.minYearExperience}
                  options={experience}
                  placeholder={t("pilih")}
                />
              </Col>
              <Col xs="12" className="mb-2">
                <Label htmlFor="skills" className="input-label">
                  {t("keterampilan")}
                </Label>
                <SkillInput
                  onChange={(newValues) =>
                    formik.setFieldValue("skills", newValues)
                  }
                  values={values.skills}
                  placeholder={t("daftarketerampilan")}
                />
                <small className="text-form text-muted d-block">
                  {t("contoh")}: Excel, Programming
                </small>
              </Col>
            </Row>
          </div>
        </div>
        <>
          <hr />
        </>
        <div className="row tour-descriptionJob" ref={descriptionRef}>
          <div className="col-md-2">
            <h5>{t("deskripsipekerjaan")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="12" className="mb-2">
                <Editor
                  editorState={editorState}
                  value={values.description}
                  editorClassName="border p-2 rounded"
                  onEditorStateChange={editorStateChange}
                  editorStyle={{ height: 500 }}
                  toolbar={{
                    options: ["inline", "list"],
                    inline: {
                      inDropdown: false,
                      options: ["bold", "italic", "underline"],
                    },
                    list: {
                      inDropdown: false,
                      options: ["unordered", "ordered"],
                    },
                  }}
                />
              </Col>
            </Row>
          </div>
        </div>

        <>
          <hr />
        </>

        <div className="row tour-submitJob" ref={publishRef}>
          <div className="col-md-2 mb-2">
            <h5>{t("publikasi")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="12">
                {user.personnel.company.paid === "pre" ? (
                  !myBalance || isExpired ? (
                    <Button
                      className="mr-2 mb-2"
                      color="netis-primary"
                      onClick={toggle}
                    >
                      {submitLoadPublish ? (
                        <>
                          <Spinner color="light" size="sm" /> {t("Loading...")}
                        </>
                      ) : (
                        t("publikasikan")
                      )}
                    </Button>
                  ) : (
                    <Button
                      className="mr-2 mb-2"
                      type="submit"
                      color="netis-primary"
                      disabled={disablePublish}
                      onClick={() => {
                        formik.setFieldValue("published", true);
                        setType("publish");
                      }}
                    >
                      {submitLoadPublish ? (
                        <>
                          <Spinner color="light" size="sm" /> {t("Loading...")}
                        </>
                      ) : (
                        t("publikasikan")
                      )}
                    </Button>
                  )
                ) : (
                  <Button
                    className="mr-2 mb-2"
                    type="submit"
                    color="netis-primary"
                    disabled={disablePublish}
                    onClick={() => {
                      formik.setFieldValue("published", true);
                      setType("publish");
                    }}
                  >
                    {submitLoadPublish ? (
                      <>
                        <Spinner color="light" size="sm" /> {t("Loading...")}
                      </>
                    ) : (
                      t("publikasikan")
                    )}
                  </Button>
                )}

                <Button
                  className="ml-2 mb-2"
                  type="submit"
                  color="secondary"
                  disabled={disableDraft}
                  onClick={() => {
                    setType("draft");
                  }}
                >
                  {submitLoadDraft ? (
                    <>
                      <Spinner color="dark" size="sm" /> {t("Loading...")}
                    </>
                  ) : (
                    t("simpandraft")
                  )}
                </Button>
              </Col>
            </Row>
          </div>
        </div>
      </Form>
      <Modal isOpen={modalNonComplete} style={{ marginTop: "40vh" }}>
        <ModalHeader className=" text-center border-bottom-0">
          {t("caution")}
        </ModalHeader>
        <ModalBody className="text-center">
          {t("noCompleteLater")}
          <br />
          <div
            className="d-flex justify-content-center mx-auto text-center mt-2"
            style={{ width: "60%" }}
          >
            <Button
              className="button-token"
              onClick={() => {
                setModalNonComplete(false);
                props.history.goBack();
              }}
            >
              {t("ok")}
            </Button>
          </div>
        </ModalBody>
      </Modal>
      <Modal isOpen={modalTokenNull} style={{ marginTop: "40vh" }}>
        <ModalHeader className=" text-center border-bottom-0">
          {t("caution")}
        </ModalHeader>
        <ModalBody className="text-center">
          {t("cannotPublish")}
          <br />
          <div
            className="d-flex justify-content-center mx-auto text-center mt-2"
            style={{ width: "60%" }}
          >
            <Button
              className="button-token"
              onClick={() => {
                setModalTokenNull(false);
              }}
            >
              {t("btnUnderstand")}
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps)(RecruitmentCreate);
