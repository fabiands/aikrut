import React from "react";
import {
  Document,
  Font,
  Image,
  Page,
  StyleSheet,
  Text,
  Svg,
  Line,
  View,
  Rect,
} from "@react-pdf/renderer";
import logo from "../../../assets/assets_ari/logo.png";
import { memo } from "react";
import { t } from "react-switch-lang";
import moment from "moment";
import langUtils from "../../../utils/language/index";
export default memo(
  ({ data, generatePdf, keyword, fromDate, toDate, name }) => {
    const action = {
      topup: t("topupToken"),
      usage: t("usageToken"),
    };

    const descs = {
      topup: t("amount"),
      usage: t("forFeature"),
    };
    return (
      <>
        <Document>
          <Page>
            <Svg height="10" width="100%" fixed>
              <Line
                x1="0"
                y1="0"
                x2="200"
                y2="0"
                strokeWidth={10}
                stroke="rgba(19, 85, 142, 1)"
              />
              <Line
                x1="200"
                y1="0"
                x2="400"
                y2="0"
                strokeWidth={10}
                stroke="rgba(231, 107, 36, 1)"
              />
            </Svg>
            <Image style={styles.logo} src={logo} fixed />
            <Svg height="50" width="100%" style={{ marginTop: "25px" }}>
              <Rect
                x="-20"
                rx="20"
                ry="20"
                width="220"
                height="45"
                fill="rgba(231, 107, 36, 1)"
              />
              <Rect
                x="-20"
                y="5"
                rx="20"
                ry="20"
                width="305"
                height="45"
                fill="rgba(19, 85, 142, 1)"
              />
              <Text x="15" y="32" fill="rgba(255, 255, 255, 1)">
                {t("laporanToken")}
              </Text>
            </Svg>
            <View style={styles.body}>
              <View
                style={{
                  display: "flex",
                  justifyContent: "left",
                  alignItems: "left",
                  textAlign: "left",
                }}
              >
                <Text
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "left",
                    textAlign: "left",
                    fontSize: 18.5,
                    margin: 2.5,
                    fontWeight: 500,
                    fontFamily: "Oswald",
                  }}
                >
                  {t("nameCompany")} : {name}
                </Text>
                <Text
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "left",
                    textAlign: "left",
                    fontSize: 18.5,
                    margin: 2.5,
                    fontWeight: 500,
                    fontFamily: "Oswald",
                  }}
                >
                  {t("totalPenggunaan")} Token : {data?.length} Token
                </Text>
                <Text
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    alignItems: "left",
                    textAlign: "left",
                    fontSize: 18.5,
                    margin: 2.5,
                    fontWeight: 500,
                    fontFamily: "Oswald",
                  }}
                >
                  {keyword
                    ? t("laporanPenggunaanPdfPada") +
                      ": " +
                      keyword +
                      " " +
                      t("periode") +
                      " " +
                      moment(fromDate).format("DD-MM-Y") +
                      " " +
                      t("sampai") +
                      " " +
                      moment(toDate).format("DD-MM-Y")
                    : t("laporanPenggunaanPdf") +
                      ": " +
                      moment(fromDate).format("DD-MM-Y") +
                      " " +
                      t("sampai") +
                      " " +
                      moment(toDate).format("DD-MM-Y")}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                }}
              >
                <Image
                  style={{ ...styles.image, width: "100%", marginTop: "20px" }}
                  src={generatePdf}
                />
                <View style={{ width: "90%" }} break>
                  <Text
                    style={{
                      display: "flex",
                      justifyContent: "left",
                      alignItems: "left",
                      textAlign: "left",
                      fontSize: 14.5,
                      marginBottom: 5.5,
                      fontWeight: 500,
                      fontFamily: "Oswald",
                    }}
                  >
                    {t("detailToken")}
                  </Text>
                  {data.map((desc, idx) => {
                    return (
                      <View
                        style={{
                          borderBottom: "1px solid #dfdfdf",
                        }}
                        key={idx}
                        break={idx > 0 && idx % 13 === 0}
                      >
                        <Text
                          style={{
                            fontWeight: 100,
                            fontSize: 13,
                            fontFamily: "Oswald",
                            textAlign: "left",
                          }}
                        >
                          - {t("featureUsage")} :&nbsp;
                          <Text
                            style={{
                              textTransform: "capitalize",
                              fontWeight: 200,
                            }}
                          >
                            {desc?.usage?.tokenType === "spm"
                              ? t("spm")
                              : desc?.usage?.tokenType === "fingerprint"
                              ? t("fingerprint")
                              : desc?.usage?.tokenType === "businessInsight"
                              ? t("businessInsight")
                              : desc?.usage?.tokenType}
                          </Text>
                        </Text>
                        <Text
                          style={{
                            textTransform: "capitalize",
                            fontWeight: 10,
                            textAlign: "left",
                            fontSize: 12,
                            fontFamily: "Oswald",
                            marginLeft: "6.5px",
                            color: "#5d6063",
                          }}
                        >
                          {desc?.causer?.name + " " + action[desc?.type]}{" "}
                          {descs[desc?.type] + " " + desc?.usage?.tokenType ===
                          "spm"
                            ? t("spm")
                            : desc?.usage?.tokenType === "fingerprint"
                            ? t("fingerprint")
                            : desc?.usage?.tokenType === "businessInsight"
                            ? t("businessInsight")
                            : desc?.usage?.tokenType +
                              " " +
                              t("onApplicant") +
                              " " +
                              desc?.usage?.identifier}
                        </Text>
                        <Text
                          style={{
                            textTransform: "capitalize",
                            fontWeight: 10,
                            textAlign: "right",
                            fontSize: 10,
                            fontFamily: "Oswald",
                            marginLeft: "6.5px",
                            color: "#73818f",
                          }}
                        >
                          {moment(desc?.date)
                            .locale(langUtils.getLanguage())
                            .format("DD MMMM YYYY LT")}
                        </Text>
                        <br />
                        <br />
                      </View>
                    );
                  })}
                </View>
              </View>
            </View>
            <Text
              style={styles.pageNumber}
              render={({ pageNumber, totalPages }) =>
                `${pageNumber} / ${totalPages}`
              }
              fixed
            />
          </Page>
        </Document>
      </>
    );
  }
);

Font.register(
  {
    family: "Oswald",
    src: "https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf",
  },
  {
    family: "Roboto",
    src: "https://fonts.googleapis.com/css2?family=Roboto&display=swap",
  }
);

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  logo: {
    marginVertical: 15,
    marginHorizontal: 25,
    width: 100,
  },
  title: {
    fontSize: 24,
    textTransform: "uppercase",
    fontFamily: "Oswald",
    marginTop: 25,
    marginBottom: 10,
    color: "rgba(19, 85, 142, 1)",
  },
  image: {
    marginBottom: 30,
  },
  subtitle: {
    fontSize: 16,
    margin: 5,
    fontWeight: 500,
    fontFamily: "Oswald",
  },
  text: {
    margin: 2.5,
    lineHeight: 1.5,
    fontSize: 12,
    textAlign: "justify",
    fontFamily: "Times-Roman",
  },
  row: {
    flexDirection: "row",
    fontStyle: "normal",
    fontSize: "18px",
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 15,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
  },
});
