import React, { useCallback, useState, useEffect } from "react";
import {
  ListGroup,
  ListGroupItem,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Row,
  Col,
  Button,
} from "reactstrap";
import { translate, t } from "react-switch-lang";
import { useTokenNotificationFilter } from "../../../hooks/useTokenNotificationFilter";
import LoadingAnimation from "../../../components/LoadingAnimation";
import { Bar } from "react-chartjs-2";
import classnames from "classnames";
import DataNotFound from "../../../components/DataNotFound";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import langUtils from "../../../utils/language/index";
import moment from "moment";
import DatePicker from "react-date-picker";
import TokenHistoryDownload from "./TokenHistoryDownload";
import { connect } from "react-redux";
import { withRouter } from "react-router";
function TokenHistory(props) {
  const [fromDate, setFromDate] = useState(
    moment().subtract(1, "months").format("YYYY-MM-DD")
  );
  const [toDate, setToDate] = useState(moment().format("YYYY-MM-DD"));

  const [activeTab, setActiveTab] = useState("1");
  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  const [list, setList] = useState([]);
  const [keywords, setKeywords] = useState(null);
  const { data, loading, error } = useTokenNotificationFilter([], {
    fromDate: fromDate,
    toDate: toDate,
  });
  // useEffect(() => {
  //   if (toDate && fromDate) {
  //     const { data, loading, error } = useTokenNotificationFilter([], {
  //       fromDate:fromDate,
  //       toDate:toDate,
  //     });
  //   }
  // }, [fromDate, toDate]);
  const action = {
    topup: t("topupToken"),
    usage: t("usageToken"),
  };
  const desc = {
    topup: t("amount"),
    usage: t("forFeature"),
  };
  const fisiognomi = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "fisiognomi"
  );
  const video = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "video"
  );
  const gesture = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "gesture"
  );
  const palmistry = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "palmistry"
  );
  const bazi = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "bazi"
  );
  const shio = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "shio"
  );
  const zodiac = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "zodiac"
  );
  const disc = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "disc"
  );
  const msdt = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "msdt"
  );
  const spm = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "spm"
  );
  const fingerprint = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "fingerprint"
  );
  const businessInsight = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "businessInsight"
  );
  const agility = data.filter(
    (a) => a.type === "usage" && a.usage.tokenType === "agility"
  );
  const listTopup = data.filter((a) => a.type === "topup");
  const dataChart = {
    labels: [
      "DISC",
      "MSDT",
      `${t("spm")}`,
      `${t("businessInsight")}`,
      "Agility",
      "Video",
      "Gesture",
      "Fisiognomi",
      "Palmistry",
      `${t("fingerprint")}`,
      "Bazi",
      "Shio",
      "Zodiac",
    ],
    datasets: [
      {
        label: `${t("tokenUsage")}`,
        backgroundColor: [
          "rgb(149, 228, 99)",
          "rgb(185, 1, 68)",
          "rgb(0, 255, 184)",
          "rgb(140, 126, 195)",
          "rgb(221, 105, 201)",
          "rgb(218, 236, 59)",
          "rgb(255, 185, 91)",
          "rgb(248, 108, 107)",
          "rgb(77, 189, 116)",
          "rgb(255, 229, 155)",
          "rgb(51, 88, 119)",
          "rgb(255, 138, 157)",
          "rgb(99, 194, 222)",
          // "rgb(140, 126, 195)",
        ],
        borderColor: [
          "rgb(149, 228, 99)",
          "rgb(185, 1, 68)",
          "rgb(0, 255, 184)",
          "rgb(140, 126, 195)",
          "rgb(221, 105, 201)",
          "rgb(218, 236, 59)",
          "rgb(255, 185, 91)",
          "rgb(248, 108, 107)",
          "rgb(77, 189, 116)",
          "rgb(255, 229, 155)",
          "rgb(51, 88, 119)",
          "rgb(255, 138, 157)",
          "rgb(99, 194, 222)",
          // "rgb(140, 126, 195)",
        ],
        borderWidth: 1,
        hoverBackgroundColor: [
          "rgba(149, 228, 99, 0.5)",
          "rgba(185, 1, 68, 0.5)",
          "rgba(0, 255, 184, 0.5)",
          "rgba(140, 126, 195, 0.5)",
          "rgba(221, 105, 201, 0.5)",
          "rgba(218, 236, 59, 0.5)",
          "rgba(255, 185, 91, 0.5)",
          "rgba(248, 108, 107, 0.5)",
          "rgba(77, 189, 116, 0.5)",
          "rgba(255, 229, 155, 0.5)",
          "rgba(51, 88, 119, 0.5)",
          "rgba(255, 138, 157, 0.5)",
          "rgba(99, 194, 222, 0.5)",
          // "rgba(140, 126, 195, 0.5)",
        ],
        hoverBorderColor: [
          "rgba(149, 228, 99, 0.5)",
          "rgba(185, 1, 68, 0.5))",
          "rgba(0, 255, 184, 0.5)",
          "rgba(140, 126, 195, 0.5)",
          "rgba(221, 105, 201, 0.5)",
          "rgba(218, 236, 59, 0.5)",
          "rgba(255, 185, 91, 0.5)",
          "rgba(248, 108, 107, 0.5)",
          "rgba(77, 189, 116, 0.5)",
          "rgba(255, 229, 155, 0.5)",
          "rgba(51, 88, 119, 0.5)",
          "rgba(255, 138, 157, 0.5)",
          "rgba(99, 194, 222, 0.5)",
          // "rgba(140, 126, 195, 0.5)",
        ],
        data: [
          disc.length,
          msdt.length,
          spm.length,
          businessInsight.length,
          agility.length,
          video.length,
          gesture.length,
          fisiognomi.length,
          palmistry.length,
          fingerprint.length,
          bazi.length,
          shio.length,
          zodiac.length,
          // businessInsight.length,
        ],
      },
    ],
  };

  useEffect(() => {
    if (data) {
      if (keywords) {
        setList(
          data.filter(
            (a) => a.type === "usage" && a.usage.tokenType === keywords
          )
        );
      } else {
        setList(data.filter((a) => a.type === "usage"));
      }
    } // eslint-disable-next-line
  }, [data]);
  const changeList = useCallback(
    (keyword) => {
      if (keyword === "Kemampuan Kognitif") {
        keyword = "spm";
      } else if (keyword === "Sidik Jari") {
        keyword = "fingerprint";
      } else if (keyword === "Tes Entrepeneur") {
        keyword = "businessInsight";
      } else {
        keyword = keyword.toLowerCase();
      }
      setKeywords(keyword);
      setList(
        data.filter((a) => a.type === "usage" && a.usage.tokenType === keyword)
      );
      window.scroll({ top: 500, behavior: "smooth" });
      // const list = document.getElementById("listgroup");
      // list.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    },
    [data]
  );

  const resetToken = () => {
    window.scroll({ top: 500, behavior: "smooth" });
    setKeywords(null);
    setList(data.filter((a) => a.type === "usage"));
  };

  return (
    <TabContent className="shadow-sm rounded">
      <TabPane>
        <div className="animated fadeIn">
          <div className="d-flex bd-highlight mb-3">
            <div className="mr-auto bd-highlight"></div>
          </div>
          {loading ? (
            <LoadingAnimation />
          ) : error ? (
            <div className="alert alert-danger">
              <h5>{t("galat")}</h5>
              <span>{t("galatText")}</span>
            </div>
          ) : (
            <>
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab === "1" })}
                    onClick={() => {
                      toggle("1");
                    }}
                  >
                    {t("tokenUsage")}
                  </NavLink>
                </NavItem>
                {props.user.personnel.company.paid === "pre" ? (
                  <NavItem>
                    <NavLink
                      className={classnames({ active: activeTab === "2" })}
                      onClick={() => {
                        toggle("2");
                      }}
                    >
                      Top Up
                    </NavLink>
                  </NavItem>
                ) : null}
              </Nav>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <Row>
                    {/* <Col sm="12">
                      <h4>
                        <b>Filter Tanggal:</b>
                      </h4>
                    </Col> */}
                    <Col sm="3">
                      <label>
                        <b>{t("dari")}</b>
                      </label>
                      <DatePicker
                        onChange={(date) => {
                          setFromDate(moment(date).format("YYYY-MM-DD"));
                        }}
                        format={"dd-MM-y"}
                        className="form-control"
                        monthPlaceholder="mm"
                        yearPlaceholder="yyyy"
                        dayPlaceholder="dd"
                        value={new Date(fromDate)}
                        locale={langUtils.getLanguage()}
                        maxDate={new Date(toDate)}
                        clearIcon={null}
                        clearAriaLabel={null}
                      />
                    </Col>
                    <Col sm="3">
                      <label>
                        <b>{t("hingga")}</b>
                      </label>
                      <DatePicker
                        onChange={(date) => {
                          setToDate(moment(date).format("YYYY-MM-DD"));
                        }}
                        format={"dd-MM-y"}
                        className="form-control"
                        monthPlaceholder="mm"
                        yearPlaceholder="yyyy"
                        dayPlaceholder="dd"
                        value={new Date(toDate)}
                        locale={langUtils.getLanguage()}
                        minDate={new Date(fromDate)}
                        maxDate={new Date(moment().format("YYYY-MM-DD"))}
                        clearIcon={null}
                        clearAriaLabel={null}
                      />
                    </Col>
                    <Col sm="6" className="text-right">
                      <TokenHistoryDownload
                        data={list}
                        keyword={keywords}
                        fromDate={fromDate}
                        toDate={toDate}
                        name={props?.user?.personnel?.company?.name}
                      />
                    </Col>
                    <Col sm="12">
                      <br />
                      <br />
                      <div style={{ maxHeight: "400px" }} className="pdf-token">
                        {
                          <Bar
                            data={dataChart}
                            width={100}
                            height={400}
                            options={{
                              maintainAspectRatio: false,
                              legend: false,
                              tooltips: {
                                mode: "label",
                              },
                              responsive: true,
                              responsiveAnimationDuration: 2000,
                              hover: {
                                intersect: true,
                                mode: "point",
                              },
                              onHover: (event, chartElement) => {
                                event.target.style.cursor = chartElement[0]
                                  ? "pointer"
                                  : "default";
                              },
                              scales: {
                                yAxes: [
                                  {
                                    ticks: {
                                      beginAtZero: true,
                                    },
                                  },
                                ],
                              },
                            }}
                            onElementsClick={(e) =>
                              changeList(e[0]?._model.label ?? "")
                            }
                          />
                        }
                      </div>
                      <ListGroup id="listgroup" className="mt-4">
                        {list.length === 0 ? (
                          <DataNotFound />
                        ) : (
                          <>
                            <Row>
                              <Col sm="8">
                                <h3 className="mb-3">
                                  {keywords
                                    ? list[0]?.usage?.tokenType === "spm"
                                      ? t("listOfTokenUsage") + " " + t("spm")
                                      : list[0]?.usage?.tokenType ===
                                        "fingerprint"
                                      ? t("listOfTokenUsage") +
                                        " " +
                                        t("fingerprint")
                                      : list[0]?.usage?.tokenType ===
                                        "businessInsight"
                                      ? t("listOfTokenUsage") +
                                        " " +
                                        t("businessInsight")
                                      : t("listOfTokenUsage") +
                                        " " +
                                        list[0]?.usage?.tokenType
                                    : t("semuaFilter")}
                                </h3>
                              </Col>
                              <Col sm="4" className="text-right">
                                {keywords ? (
                                  <Button
                                    className="btn btn-netis-color"
                                    onClick={resetToken}
                                  >
                                    Reset
                                  </Button>
                                ) : null}
                              </Col>
                            </Row>
                            {list?.map((data, idx) => {
                              return (
                                <ListGroupItem className="p-3" key={idx}>
                                  <Row>
                                    <Col xs="12">
                                      <i className="fa fa-lg fa-ticket text-info mr-1" />
                                      {t("featureUsage")} :&nbsp;
                                      <span className="text-capitalize">
                                        {data?.usage?.tokenType === "spm"
                                          ? t("spm")
                                          : data?.usage?.tokenType ===
                                            "fingerprint"
                                          ? t("fingerprint")
                                          : data?.usage?.tokenType ===
                                            "businessInsight"
                                          ? t("businessInsight")
                                          : data?.usage?.tokenType}
                                      </span>
                                    </Col>
                                    <Col md="9">
                                      <div className="my-2">
                                        <FontAwesomeIcon
                                          icon="coins"
                                          className="mx-auto"
                                          style={{ color: "#d6ab1d" }}
                                        />
                                        <span className="text-primary ml-2 mr-1">
                                          <b>{data?.causer?.name}</b>
                                        </span>
                                        {action[data?.type]} {desc[data?.type]}
                                        <span className="text-capitalize ml-1">
                                          {data?.usage?.tokenType === "spm"
                                            ? t("spm")
                                            : data?.usage?.tokenType ===
                                              "fingerprint"
                                            ? t("fingerprint")
                                            : data?.usage?.tokenType ===
                                              "businessInsight"
                                            ? t("businessInsight")
                                            : data?.usage?.tokenType}
                                        </span>{" "}
                                        {t("onApplicant")}
                                        <span className="text-capitalize ml-1">
                                          {data?.usage?.identifier}
                                        </span>
                                      </div>
                                    </Col>
                                    <Col md="3">
                                      <div className="text-muted float-right d-block d-md-inline">
                                        {moment(data?.date)
                                          .locale(langUtils.getLanguage())
                                          .format("DD MMMM YYYY LT")}
                                      </div>
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              );
                            })}
                          </>
                        )}
                      </ListGroup>
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2">
                  <Row>
                    <Col sm="12">
                      <ListGroup>
                        {listTopup.length === 0 ? (
                          <DataNotFound />
                        ) : (
                          listTopup?.map((data, idx) =>
                            data.causer.id === 0 ? (
                              data.nominal === 0 ? (
                                <ListGroupItem className="p-3" key={idx}>
                                  <Row>
                                    <Col md="9">
                                      <div className="ml-2">
                                        <FontAwesomeIcon
                                          icon="calendar-times"
                                          className="mx-auto"
                                          style={{ color: "#e0474d" }}
                                        />{" "}
                                        {t("tokenExpired")}
                                      </div>
                                    </Col>
                                    <Col md="3">
                                      <div className="text-muted float-right d-block d-md-inline">
                                        {moment(data?.date)
                                          .locale(langUtils.getLanguage())
                                          .format("DD MMMM YYYY LT")}
                                      </div>
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              ) : (
                                <ListGroupItem className="p-3" key={idx}>
                                  <Row>
                                    <Col md="9">
                                      <div className="ml-2">
                                        <FontAwesomeIcon
                                          icon="plus-square"
                                          className="mx-auto"
                                          style={{ color: "#e0474d" }}
                                        />{" "}
                                        {t("getFreeToken")} {data.nominal}{" "}
                                        {t("token")}
                                      </div>
                                    </Col>
                                    <Col md="3">
                                      <div className="text-muted float-right d-block d-md-inline">
                                        {moment(data?.date)
                                          .locale(langUtils.getLanguage())
                                          .format("DD MMMM YYYY LT")}
                                      </div>
                                    </Col>
                                  </Row>
                                </ListGroupItem>
                              )
                            ) : (
                              <ListGroupItem className="p-3" key={idx}>
                                <Row>
                                  <Col md="9">
                                    <div className="ml-2">
                                      <FontAwesomeIcon
                                        icon="plus-square"
                                        className="mx-auto"
                                        style={{ color: "#47e0af" }}
                                      />
                                      <span className="text-primary ml-2 mr-1">
                                        <b>{data?.causer?.name}</b>
                                      </span>
                                      {action[data?.type]} {desc[data?.type]}{" "}
                                      <span className="mr-1 text-primary">
                                        {data?.nominal}
                                      </span>
                                    </div>
                                  </Col>
                                  <Col md="3">
                                    <div className="text-muted float-right d-block d-md-inline">
                                      {moment(data?.date)
                                        .locale(langUtils.getLanguage())
                                        .format("DD MMMM YYYY LT")}
                                    </div>
                                  </Col>
                                </Row>
                              </ListGroupItem>
                            )
                          )
                        )}
                      </ListGroup>
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </>
          )}
        </div>
      </TabPane>
    </TabContent>
  );
}
const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps)(withRouter(translate(TokenHistory)));
