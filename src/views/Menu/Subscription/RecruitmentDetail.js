import React, { useState, useEffect, useMemo } from "react";
import {
  Button,
  Form,
  Input,
  Label,
  Row,
  Col,
  CustomInput,
  Spinner,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useRouteMatch } from "react-router-dom";
import Loading from "../../../components/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import Select from "react-select";
import CreatableSelect from "react-select/creatable";
import request from "../../../utils/request";
import { toast } from "react-toastify";
import { t, translate } from "react-switch-lang";
import NumberFormat from "react-number-format";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { draftToMarkdown, markdownToDraft } from "markdown-draft-js";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import * as moment from "moment";
import { useUserBalance } from "../../../hooks/useUserBalance";
import langUtils from "../../../utils/language/index";
import ModalError from "../../../components/ModalError";

const education = [
  { value: "sd", label: t("sd") },
  { value: "smp", label: t("smp") },
  { value: "sma", label: t("sma") },
  { value: "d1", label: t("d1") },
  { value: "d2", label: t("d2") },
  { value: "d3", label: t("d3") },
  { value: "d4", label: t("d4") },
  { value: "s1", label: t("s1") },
  { value: "s2", label: t("s2") },
  { value: "s3", label: t("s3") },
];

const SkillInput = ({ onChange, values = [], placeholder, isDisabled }) => {
  const [inputValue, setInputValue] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const handleInputChange = React.useCallback(
    function (value) {
      if (errorMessage) {
        setErrorMessage(null);
      }
      setInputValue(value);
    },
    [errorMessage]
  );

  function handleKeyDown(event) {
    if (!inputValue) return;
    switch (event.key) {
      case "Enter":
      case "Tab":
      case ",":
        if (
          values.some((val) => val.toLowerCase() === inputValue.toLowerCase())
        ) {
          setErrorMessage(`'${inputValue}' ${t("alreadyInTheField")}`);
          event.preventDefault();
          return;
        }
        setInputValue("");
        onChange([...values, inputValue]);
        event.preventDefault();
        break;
      default:
    }
  }

  return (
    <React.Fragment>
      <CreatableSelect
        components={{ DropdownIndicator: null }}
        isClearable
        isMulti
        isDisabled={isDisabled}
        menuIsOpen={false}
        placeholder={placeholder}
        value={values.map((value) => ({ value, label: value }))}
        onChange={(newItems) =>
          onChange(newItems ? newItems.map((item) => item.value) : [])
        }
        inputValue={inputValue}
        onInputChange={handleInputChange}
        onKeyDown={handleKeyDown}
      />
      {errorMessage && <small className="text-danger">{errorMessage}</small>}
    </React.Fragment>
  );
};

function RecruitmentDetail(props) {
  const { loading, data } = useUserBalance();
  const myBalance = useMemo(() => data?.balance ?? 0, [data]);
  const isExpired = useMemo(() => data?.isExpired, [data]);

  const [notFound, setNotFound] = useState(false);
  const [btnReopen, setBtnReopen] = useState(false);
  const [btnUpdate, setBtnUpdate] = useState(false);
  const [btnAkhiri, setAkhiri] = useState(false);
  const [btnRepost, setRepost] = useState(false);
  const matchRoute = useRouteMatch();
  const [province, setProvince] = useState([]);
  const [city, setCity] = useState([]);
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const editorStateChange = (changeEditorState) => {
    setEditorState(changeEditorState);
    formik.setFieldValue(
      "description",
      draftToMarkdown(convertToRaw(changeEditorState.getCurrentContent()))
    );
  };
  const [editDetail, setEditDetail] = useState(true);
  const [modalTokenNull, setModalTokenNull] = useState(false);
  const momentToday = moment().format("YYYY-MM-DD");
  const [isDiscChecked, setIsDiscChecked] = useState(false);
  const [isVideoChecked, setIsVideoChecked] = useState(false);
  const [isProfileChecked, setIsProfileChecked] = useState(false);
  const [error, setError] = useState(false);
  const [loadingReq, setLoadingReq] = useState(true);
  const [req, setReq] = useState([]);

  const toggle = () => {
    setModalTokenNull(!modalTokenNull);
  };

  const ValidationFormSchema = useMemo(() => {
    return Yup.object().shape({
      minSalary: Yup.number().required().label("gaji minimal"),
      requirements: Yup.array().test(
        "required",
        t("fillInAssessmentNeeds"),
        function (value) {
          return value.length > 0;
        }
      ),
      maxSalary: Yup.number()
        .min(
          Yup.ref("minSalary"),
          t("gaji maksimal harus lebih besar dari gaji minimal")
        )
        .required()
        .label("maxSalary"),
    });
  }, []);

  const { values, touched, errors, isSubmitting, setValues, ...formik } =
    useFormik({
      initialValues: {
        name: "",
        type: [],
        provinceId: "",
        cityId: "",
        minSalary: "",
        maxSalary: "",
        showSalary: false,
        minEducation: [],
        major: "",
        minYearExperience: "",
        description: "",
        skills: [],
        expiredAt: "",
        published: false,
        companyValue: [],
        requirements: [],
      },
      validationSchema: ValidationFormSchema,
      onSubmit: (values, { setSubmitting, setErrors }) => {
        setSubmitting(true);
        setBtnUpdate(true);
        request
          .put(`v1/recruitment/vacancies/${matchRoute.params.id}`, {
            name: values.name,
            type: values.type.value,
            provinceId: values.provinceId.value,
            cityId: values.cityId.value,
            minSalary: values.minSalary,
            maxSalary: values.maxSalary,
            major: values.major,
            showSalary: values.showSalary,
            minEducation: values.minEducation.value,
            minYearExperience: values.minYearExperience.value,
            description: values.description,
            skills: values.skills,
            expiredAt: values.expiredAt,
            published: values.published,
            companyValue: values.companyValue,
            requirements: values.requirements,
          })
          .then((res) => {
            toast.success("Success");
            props.history.goBack();
          })
          .catch((err) => {
            if (err.response?.status === 422) {
              toast.danger("Error");
              setErrors(err.response.data.errors);
              return;
            }
            Promise.reject(err);
          })
          .finally(() => {
            setSubmitting(false);
            setBtnUpdate(false);
          });
      },
    });

  useEffect(() => {
    request
      .get(`v1/recruitment/vacancies/${matchRoute.params.id}`)
      .then((response) => {
        var data = response.data.data;
        let company = data.companyValue ? JSON.parse(data.companyValue) : null;
        let companyList = [];
        let companies = {};
        if (company) {
          company.map((value) => {
            companies = {
              value: value.value,
              label: t(value.label),
            };
            return companyList.push(companies);
          });
        }
        setValues({
          name: data.name,
          type: { label: data.type, value: data.type },
          provinceId: { value: data.province.id, label: data.province.name },
          cityId: { value: data.city.id, label: data.city.name },
          minSalary: data.minSalary,
          maxSalary: data.maxSalary,
          major: data.major,
          showSalary: data.showSalary,
          minEducation: { label: data.minEducation, value: data.minEducation },
          minYearExperience: {
            label: data.minYearExperience,
            value: data.minYearExperience,
          },
          description: data.description,
          skills: data.skills,
          expiredAt: data.expiredAt,
          published: data.published,
          companyValue: companyList,
          requirements: data.requirements,
        });
        request
          .get(`v1/location/cities?provinceId=${data.province.id}`)
          .then((res) => {
            setCity(res.data.data);
          });
        setIsDiscChecked(
          data?.requirements?.filter((e) => e === "disc").length > 0
        );
        setIsVideoChecked(
          data?.requirements?.filter((e) => e === "video").length > 0
        );
        setIsProfileChecked(
          data?.requirements?.filter((e) => e === "profile").length > 0
        );
        request
          .get("v2/recruitment/vacancies/requirements")
          .then((res) => {
            setReq(res?.data?.data);
          })
          .catch(() => {
            setError(true);
          })
          .finally(() => setLoadingReq(false));
        const draftContent = convertFromRaw(markdownToDraft(data.description));
        setEditorState(EditorState.createWithContent(draftContent));
      })
      .catch((err) => {
        if (err.response && err.response.status === 404) {
          setNotFound(true);
        } else {
          toast.error("terjadikesalahan");
          setNotFound(true);
        }
      });
    // .finally(() => setLoading(false));
  }, [matchRoute.params.id, setValues]);
  useEffect(() => {
    console.log(values.companyValue);
    if (values.companyValue) {
      values.companyValue.map((data) => {
        data.label = t(data.label);
        return true;
      });
    } // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [matchRoute.params.id, langUtils.getLanguage()]);
  useEffect(() => {
    request
      .get("v1/location/provinces?countryId=1")
      .then((res) => {
        setProvince(res.data.data);
      })
      .catch((err) => {
        if (err.response.status === 404) {
          setNotFound(true);
        }
      });
    // .finally(() => setLoading(false));
  }, []);

  const options = [
    { value: "Full-time", label: "Full-Time" },
    { value: "Part-time", label: "Part-Time" },
    { value: "Kontrak", label: t("kontrak") },
    { value: "Freelance", label: "Freelance" },
    { value: "Magang", label: t("magang") },
  ];

  const experience = [
    { value: 0, label: "Fresh Graduate" },
    { value: 1, label: t("1tahun") },
    { value: 2, label: t("2tahun") },
    { value: 3, label: t("3tahun") },
    { value: 4, label: t("4tahun") },
    { value: 5, label: t("5tahunlebih") },
  ];

  const companyValue = [
    { value: "F", label: t("companyValuePoint1") },
    { value: "W", label: t("companyValuePoint2") },
    { value: "N", label: t("companyValuePoint3") },
    { value: "G", label: t("companyValuePoint4") },
    { value: "A", label: t("companyValuePoint5") },
    { value: "L", label: t("companyValuePoint6") },
    { value: "P", label: t("companyValuePoint7") },
    { value: "I", label: t("companyValuePoint8") },
    { value: "T", label: t("companyValuePoint9") },
    { value: "V", label: t("companyValuePoint10") },
    { value: "X", label: t("companyValuePoint11") },
    { value: "S", label: t("companyValuePoint12") },
    { value: "B", label: t("companyValuePoint13") },
    { value: "O", label: t("companyValuePoint14") },
    { value: "R", label: t("companyValuePoint15") },
    { value: "D", label: t("companyValuePoint16") },
    { value: "C", label: t("companyValuePoint17") },
    { value: "Z", label: t("companyValuePoint18") },
    { value: "E", label: t("companyValuePoint19") },
    { value: "K", label: t("companyValuePoint20") },
  ];

  const country = [{ value: 1, label: "Indonesia" }];

  const changeJenisKerja = function (pilihan) {
    formik.setFieldValue("type", pilihan);
    formik.setFieldTouched("type", true);
  };

  const prov = province.map((province) => ({
    value: province.id,
    label: province.name,
  }));

  const changeProvince = function (province) {
    request
      .get(`v1/location/cities?provinceId=${province.value}`)
      .then((res) => {
        setCity(res.data.data);
      });
    formik.setFieldValue("provinceId", province);
    formik.setFieldValue("cityId", null);
    formik.setFieldTouched("provinceId", true);
  };

  const kota = city.map((city) => ({ value: city.id, label: city.name }));

  const changeCity = function (city) {
    formik.setFieldValue("cityId", city);
    formik.setFieldTouched("cityId", true);
  };

  const changeCountry = function (country) {
    formik.setFieldValue("country", country);
    formik.setFieldTouched("country", true);
  };

  const changePendidikan = function (edu) {
    formik.setFieldValue("minEducation", edu);
    formik.setFieldTouched("minEducation", true);
  };

  const changeExperience = function (edu) {
    formik.setFieldValue("minYearExperience", edu);
    formik.setFieldTouched("minYearExperience", true);
  };

  const formikSetValue = function (field) {
    return (value) => {
      formik.setFieldValue(field, value);
      formik.setFieldTouched(field, true);
    };
  };

  const changeRequirements = (e) => {
    const { value, checked } = e.target;
    if (value === "all") {
      if (checked) {
        const allSet = new Set(req);
        const allArr = Array.from(allSet);
        formik.setFieldValue("requirements", allArr);
        document.querySelector(".chooseCharacteristic").style.display = "flex";
        setIsDiscChecked(true);
        setIsVideoChecked(true);
        setIsProfileChecked(true);
      } else if (!checked) {
        formik.setFieldValue("requirements", []);
        formik.setFieldValue("companyValue", []);
        document.querySelector(".chooseCharacteristic").style.display = "none";
        setIsDiscChecked(false);
        setIsVideoChecked(false);
        setIsProfileChecked(false);
      }
    } else {
      let arr = values.requirements;
      arr.push(value);
      // console.log(value, checked);
      const set = new Set(arr);
      if (!checked) {
        set.delete(value);
      }

      if (value === "papikostick") {
        if (checked) {
          document.querySelector(".chooseCharacteristic").style.display =
            "flex";
        } else if (!checked) {
          document.querySelector(".chooseCharacteristic").style.display =
            "none";
          formik.setFieldValue("companyValue", []);
        }
      }

      const setArr = Array.from(set);
      formik.setFieldValue("requirements", setArr);

      if (value === "disc") {
        if (checked) {
          setIsDiscChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter((e) => e !== "spm");
          setIsDiscChecked(false);
          formik.setFieldValue("requirements", newSetArr);
        }
      }

      if (value === "video") {
        if (checked) {
          setIsVideoChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter(
            (e) => e !== "fisiognomi" && e !== "palmistry"
          );
          formik.setFieldValue("requirements", newSetArr);
          setIsVideoChecked(false);
        }
      }

      if (value === "profile") {
        if (checked) {
          setIsProfileChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter(
            (e) => e !== "bazi" && e !== "zodiac" && e !== "shio"
          );
          formik.setFieldValue("requirements", newSetArr);
          setIsProfileChecked(false);
        }
      }
    }
  };

  if (loading || loadingReq) {
    return <Loading />;
  }
  if (notFound) {
    return <DataNotFound />;
  }
  if (error) {
    return <ModalError isOpen={true} />;
  }

  function edit() {
    setEditDetail(false);
  }

  function batal() {
    setEditDetail(true);
  }

  function akhiri() {
    setAkhiri(true);
    request
      .put(`v1/recruitment/vacancies/${matchRoute.params.id}`, {
        published: false,
      })
      .then((res) => {
        toast.success(t("berhasiltutup"));
        props.history.goBack();
      })
      .catch((err) => {
        setAkhiri(false);
        if (err.response?.status === 422) {
          toast.error("Gagal");
        }
        Promise.reject(err);
      });
    // .finally(() => setLoading(false));
  }

  function reopen() {
    setBtnReopen(true);
    request
      .put(`v1/recruitment/vacancies/${matchRoute.params.id}`, {
        published: true,
      })
      .then((res) => {
        props.history.goBack();
        toast.success("Success");
      })
      .catch((err) => {
        setBtnReopen(false);
        if (err.response?.status === 422) {
          toast.error("Failed");
        }
        Promise.reject(err);
      });
    // .finally(() => setLoading(false));
  }

  function repost() {
    setRepost(true);
    request
      .put(`v1/recruitment/vacancies/renew/${matchRoute.params.id}`, {
        published: true,
      })
      .then((res) => {
        props.history.goBack();
        toast.success("Success");
      })
      .catch((err) => {
        setRepost(false);
        if (err.response?.status === 422) {
          toast.error("Failed");
        }
        Promise.reject(err);
      });
    // .finally(() => setLoading(false));
  }

  return (
    <div className="animated fadeIn d-flex flex-column bd-highlight mb-3 p-4">
      <div className="bd-highlight mb-4">
        {/* <Button onClick={props.history.goBack} color="netis-color">
          <i className="fa fa-chevron-left mr-1"></i>
          {t("kembali")}
        </Button> */}

        <h4>{t("detailJob")}</h4>
        <hr />
      </div>
      <Form onSubmit={formik.handleSubmit}>
        <div className="row">
          <div className="col-md-2 mb-2">
            <h5>{t("publikasi")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="12">
                {values.expiredAt !== null && values.published ? (
                  moment(momentToday).isSameOrAfter(values.expiredAt, "day") ? (
                    !myBalance || isExpired ? (
                      <Button
                        className="mr-2 mb-2"
                        color="netis-primary"
                        onClick={toggle}
                      >
                        <i className="fa fa-refresh"></i>&nbsp;&nbsp; Repost
                      </Button>
                    ) : (
                      <Button
                        color="netis-primary"
                        onClick={() => repost()}
                        disabled={btnRepost}
                      >
                        {btnRepost ? (
                          <Spinner color="light" size="sm" />
                        ) : (
                          <>
                            <i className="fa fa-refresh"></i> Repost
                          </>
                        )}
                      </Button>
                    )
                  ) : (
                    <Button
                      color="danger"
                      className="btn btn-danger"
                      onClick={() => akhiri()}
                      disabled={btnAkhiri}
                    >
                      {btnAkhiri ? (
                        <Spinner color="light" size="sm" />
                      ) : (
                        <span>
                          <i className="fa fa-window-close mr-2"></i>{" "}
                          {t("tutup")}
                        </span>
                      )}
                    </Button>
                  )
                ) : !myBalance || isExpired ? (
                  <Button
                    className="mr-2 mb-2"
                    color="success"
                    onClick={toggle}
                  >
                    <i
                      className={
                        values.expiredAt ? "fa fa-repeat" : "fa fa-bullhorn"
                      }
                    ></i>
                    &nbsp;&nbsp;
                    {values.expiredAt ? "Reopen" : "Publish"}
                  </Button>
                ) : (
                  <Button
                    color="success"
                    className="btn btn-success"
                    onClick={() => reopen()}
                    disabled={btnReopen}
                  >
                    {btnReopen ? (
                      <Spinner color="light" size="sm" />
                    ) : (
                      <span>
                        <i
                          className={
                            values.expiredAt ? "fa fa-repeat" : "fa fa-bullhorn"
                          }
                        ></i>
                        &nbsp;&nbsp;
                        {values.expiredAt ? "Reopen" : "Publish"}
                      </span>
                    )}
                  </Button>
                )}
              </Col>
            </Row>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-12">
            {editDetail ? (
              <Button
                color="netis-color"
                className="btn-netis-color pull-right"
                onClick={() => edit()}
              >
                <i className="fa fa-pencil mr-2"></i>Edit
              </Button>
            ) : (
              <Button
                color="white"
                className="btn-white pull-right"
                onClick={() => batal()}
              >
                {t("batal")}
              </Button>
            )}
          </div>
          <div className="col-md-2">
            <h5>{t("detailpekerjaan")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="12" className="mb-2">
                <Label htmlFor="name" className="input-label">
                  {t("posisi")} <span className="required">*</span>
                </Label>
                <Input
                  type="input"
                  value={values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="name"
                  id="name"
                  required
                  maxLength="255"
                  placeholder={t("namapekerjaan")}
                  className="form-control needs-validation"
                  disabled={editDetail}
                />
              </Col>

              <Col xs="12" className="mb-2">
                <Label htmlFor="type" className="input-label">
                  {t("Status Kepegawaian")} <span className="required">*</span>
                </Label>
                <Select
                  isSearchable={false}
                  isClearable={true}
                  name="type"
                  id="type"
                  onChange={changeJenisKerja}
                  onBlur={formik.handleBlur}
                  value={values.type}
                  options={options}
                  className="needs-validation"
                  required
                  isDisabled={editDetail}
                />
              </Col>

              <Col xs="12" className="mb-2">
                <Label htmlFor="country" className="input-label">
                  {t("negara")}
                </Label>
                <Select
                  isSearchable={false}
                  name="country"
                  id="country"
                  onChange={changeCountry}
                  onBlur={formik.handleBlur}
                  value={country[0]}
                  options={country}
                  defaultValue={country[0]}
                  isDisabled={editDetail}
                />
              </Col>

              <Col xs="6" className="mb-2">
                <Label htmlFor="provinceId" className="input-label">
                  {t("provinsi")}
                  <span className="required">*</span>
                </Label>
                <Select
                  isSearchable={true}
                  name="provinceId"
                  id="provinceId"
                  onChange={changeProvince}
                  onBlur={formik.handleBlur}
                  options={prov}
                  value={values.provinceId}
                  className="needs-validation"
                  required
                  isDisabled={editDetail}
                />
              </Col>

              <Col xs="6" className="mb-2">
                <Label htmlFor="cityId" className="input-label">
                  {t("city")}
                  <span className="required">*</span>
                </Label>
                <Select
                  isSearchable={true}
                  name="cityId"
                  id="cityId"
                  onChange={changeCity}
                  onBlur={formik.handleBlur}
                  options={kota}
                  value={values.cityId}
                  className="needs-validation"
                  required
                  isDisabled={editDetail}
                />
              </Col>
              <Col xs="6" className="mb-2">
                <Label htmlFor="minSalary" className="input-label">
                  {t("minSalary")}
                  <span className="required">*</span>
                </Label>
                <NumberFormat
                  // thousandSeparator={'.'}
                  // decimalSeparator={','}
                  // prefix={'Rp '}
                  name="minSalary"
                  id="minSalary"
                  allowNegative={false}
                  onChange={formik.handleChange}
                  value={values.minSalary}
                  className="form-control needs-validation"
                  required
                  disabled={editDetail}
                />
                {touched.minSalary && errors.minSalary && (
                  <small className="text-danger">{errors.minSalary}</small>
                )}
              </Col>

              <Col xs="6" className="mb-2">
                <Label htmlFor="maxSalary" className="input-label">
                  {t("maxSalary")}
                  <span className="required">*</span>
                </Label>
                <NumberFormat
                  // thousandSeparator={'.'}
                  // decimalSeparator={','}
                  // prefix={'Rp '}
                  // invalid={Boolean(touched.maxSalary && errors.maxSalary)}
                  name="maxSalary"
                  id="maxSalary"
                  allowNegative={false}
                  onChange={formik.handleChange}
                  value={values.maxSalary}
                  className="form-control needs-validation"
                  required
                  disabled={editDetail}
                />
                {touched.maxSalary && errors.maxSalary && (
                  <small className="text-danger">{errors.maxSalary}</small>
                )}
              </Col>
              <Col xs="6">
                <CustomInput
                  className="ml-1"
                  label={t("showSalary")}
                  id="showSalary"
                  name="showSalary"
                  type="checkbox"
                  checked={values.showSalary}
                  onChange={(event) =>
                    formik.setFieldValue("showSalary", event.target.checked)
                  }
                  disabled={editDetail}
                />
              </Col>
              {/* <small><i>
                                <span className="required">*</span>
                                {t('tandaigaji')}
                            </i></small> */}
            </Row>
          </div>
        </div>
        <hr />
        <>
          <div className="row">
            <div className="col-md-2">
              <h5>{t("Asesmen")}</h5>
            </div>
            <div className="col-sm-12 col-md-10">
              <Row form>
                <Col xs="12">
                  <CustomInput
                    type="checkbox"
                    id="all"
                    name="all"
                    label={t("semua")}
                    value="all"
                    onChange={changeRequirements}
                    checked={values?.requirements?.length === req?.length}
                    disabled={editDetail}
                  />
                  {req.length > 0 &&
                    req.map((item, idx) => {
                      return (
                        <>
                          {!isDiscChecked &&
                          item === "spm" ? null : !isVideoChecked &&
                            item === "palmistry" ? null : !isVideoChecked &&
                            item === "fisiognomi" ? null : !isProfileChecked &&
                            item === "bazi" ? null : !isProfileChecked &&
                            item === "shio" ? null : !isProfileChecked &&
                            item === "zodiac" ? null : (
                            <CustomInput
                              key={idx}
                              type="checkbox"
                              id={item}
                              name={item}
                              value={item}
                              label={t(item)}
                              onChange={changeRequirements}
                              checked={values?.requirements?.includes(item)}
                              disabled={editDetail}
                            />
                          )}
                        </>
                      );
                    })}
                  {touched.requirements && errors.requirements && (
                    <small className="text-danger">{errors.requirements}</small>
                  )}
                </Col>
              </Row>
              <Row
                className="chooseCharacteristic"
                style={
                  values.requirements.length === req.length ||
                  values.requirements.includes("papikostick")
                    ? { display: "flex" }
                    : { display: "none" }
                }
              >
                <Col xs="12">
                  <Row form>
                    <Col className="mt-4">
                      <Label htmlFor="companyValue" className="input-label">
                        {t("karakteristik")}
                      </Label>
                      <Select
                        styles={{
                          menu: (provided) => ({ ...provided, zIndex: 9999 }),
                        }}
                        isSearchable={true}
                        name="companyValue"
                        id="companyValue"
                        closeMenuOnSelect={false}
                        onChange={(val) => {
                          if (val?.length > 3) return false;
                          formikSetValue("companyValue")(val);
                        }}
                        onBlur={formik.handleBlur}
                        value={values.companyValue}
                        options={companyValue}
                        isMulti
                        isDisabled={editDetail}
                      />
                      <small
                        className="text-form text-muted d-block"
                        style={{ fontSize: "9px" }}
                      >
                        {t("candidateCharacteristict")}
                      </small>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
          <hr />
        </>

        <div className="row">
          <div className="col-md-2">
            <h5>{t("persyaratanpekerjaan")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="6" className="mb-2">
                <Label htmlFor="minEducation" className="input-label">
                  {t("pendidikanminimal")}
                  <span className="required">*</span>
                </Label>
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  isSearchable={false}
                  name="minEducation"
                  id="minEducation"
                  onChange={changePendidikan}
                  onBlur={formik.handleBlur}
                  value={education.find(
                    (edu) => edu.value === values.minEducation.value
                  )}
                  options={education}
                  className="needs-validation"
                  required
                  isDisabled={editDetail}
                />
              </Col>
              <Col xs="6" className="mb-2">
                <Label htmlFor="major" className="input-label">
                  {t("jurusan")}
                </Label>
                <Input
                  type="input"
                  value={values.major}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="major"
                  id="major"
                  maxLength="255"
                  disabled={editDetail}
                />
              </Col>
              <Col xs="12" className="mb-2">
                <Label htmlFor="minYearExperience" className="input-label">
                  {t("minimalpengalaman")}
                </Label>
                {/* <NumberFormat
                                suffix={' tahun'}
                                allowNegative={false}
                                name="minYearExperience"
                                id="minYearExperience"
                                onChange={formik.handleChange}
                                value={values.minYearExperience}
                                className="form-control"
                            /> */}
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  isSearchable={false}
                  name="minYearExperience"
                  id="minYearExperience"
                  onChange={changeExperience}
                  onBlur={formik.handleBlur}
                  // value={values.minYearExperience}
                  value={experience.find(
                    (ex) => ex.value === values.minYearExperience.value
                  )}
                  options={experience}
                  isDisabled={editDetail}
                />
              </Col>

              <Col xs="12" className="mb-2">
                <Label htmlFor="skills" className="input-label">
                  {t("keterampilan")}
                </Label>
                <SkillInput
                  onChange={(newValues) =>
                    formik.setFieldValue("skills", newValues)
                  }
                  values={values.skills}
                  placeholder={t("daftarketerampilan")}
                  isDisabled={editDetail}
                />
                <small className="text-form text-muted d-block">
                  {t("contoh")}: Excel, Programming
                </small>
              </Col>
            </Row>
          </div>
        </div>
        <>
          <hr />
        </>
        <div className="row">
          <div className="col-md-2">
            <h5>{t("deskripsipekerjaan")}</h5>
          </div>
          <div className="col-sm-12 col-md-10">
            <Row form>
              <Col xs="12" className="mb-2">
                <div className="editor">
                  <Editor
                    editorState={editorState}
                    value={values.description}
                    editorClassName="border p-2 rounded"
                    onEditorStateChange={editorStateChange}
                    editorStyle={{ height: 200 }}
                    toolbar={{
                      options: ["inline", "list"],
                      inline: {
                        inDropdown: false,
                        options: ["bold", "italic", "underline"],
                      },
                      list: {
                        inDropdown: false,
                        options: ["unordered", "ordered"],
                      },
                    }}
                    readOnly={editDetail}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            {!editDetail ? (
              <Button
                className="float-right mr-1"
                type="submit"
                color="netis-primary"
                disabled={btnUpdate}
              >
                {btnUpdate ? <Spinner color="light" size="sm" /> : t("simpan")}
              </Button>
            ) : null}
          </div>
        </div>
      </Form>
      <Modal isOpen={modalTokenNull} style={{ marginTop: "40vh" }}>
        <ModalHeader className=" text-center border-bottom-0">
          {t("caution")}
        </ModalHeader>
        <ModalBody className="text-center">
          {t("cannotPublish")}
          <br />
          <div
            className="d-flex justify-content-center mx-auto text-center mt-2"
            style={{ width: "60%" }}
          >
            <Button
              className="button-token"
              onClick={() => {
                setModalTokenNull(false);
              }}
            >
              {t("btnUnderstand")}
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default translate(RecruitmentDetail);
