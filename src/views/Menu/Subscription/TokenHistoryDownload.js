import React, { useState, useCallback, memo } from "react";
import { Button, Modal, Spinner } from "reactstrap";
import html2canvas from "html2canvas";
import { BlobProvider } from "@react-pdf/renderer";
import TokenHistoryPdf from "./TokenHistoryPdf";
import Loader from "react-loader-spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import disableScroll from "disable-scroll";

export default memo(({ data, keyword, fromDate, toDate, name }) => {
  const [generatePdfToken, setGeneratePdfToken] = useState();
  const [dataLoading, setDataLoading] = useState(true);
  const [downloadLoading, setDownloadLoading] = useState(false);

  const scrollToTop = () => {
    // disableScroll.on()
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  };

  const handleDownload = useCallback(() => {
    window.scrollTo({ top: 0, left: 0 });
    setDownloadLoading(true);
    disableScroll.on();
    const token = window.document.getElementsByClassName("pdf-token")[0];
    Promise.all([token])
      .then(async ([tokenGraph]) => {
        await html2canvas(tokenGraph)
          .then((canvas) => {
            const imgData = canvas.toDataURL("image/png");
            setGeneratePdfToken(imgData);
          })
          .catch((err) => console.log(err));
      })
      .finally(() => setDataLoading(false));
  }, [setGeneratePdfToken]);
  return (
    <>
      {/* <PDFViewer style={{ width: '100%', height: 1000 }}>
                <ApplicantPdf data={data} generatePdf={generatePdf} />
            </PDFViewer> */}
      <Button
        color="netis-color"
        onClick={() => {
          scrollToTop();
          // handleDownload()
          setTimeout(() => handleDownload(), 250);
        }}
        disabled={downloadLoading}
        className="float-right"
      >
        {downloadLoading ? (
          <>
            <Spinner color="light" size="sm" className="mr-1" /> Downloading..
          </>
        ) : (
          <>
            <FontAwesomeIcon icon="file-download" className="mr-1" /> Download
            Report
          </>
        )}
      </Button>
      {!dataLoading && (
        <BlobProvider
          document={
            <TokenHistoryPdf
              data={data}
              generatePdf={generatePdfToken}
              keyword={keyword}
              fromDate={fromDate}
              toDate={toDate}
              name={name}
            />
          }
        >
          {({ blob, url, loading, error }) => {
            if (loading) {
              return null;
            }
            if (!loading && url) {
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", `token-usage-report.pdf`);
              document.body.appendChild(link);
              link.click();
              link.parentNode.removeChild(link);
              setDataLoading(true);
              setDownloadLoading(false);
              disableScroll.off();
              return null;
            }
            if (error) {
              setDownloadLoading(false);
              console.error(error);
              return <p>An error occurred</p>;
            }
            return null;
          }}
        </BlobProvider>
      )}
      <Modal
        isOpen={downloadLoading}
        size="lg"
        centered
        className="text-light"
        modalClassName="modal-downloading"
        backdropClassName="modal-downloading"
      >
        <div className="loading">
          <div className="d-block text-center">
            <Loader
              color="#fff"
              secondaryColor="#fff"
              type="Rings"
              height={150}
              width={150}
              radius={1}
            />
            <p
              className="mt-3 text"
              style={{ fontSize: "12pt", color: "#fff", fontWeight: "bold" }}
            >
              Downloading...
            </p>
          </div>
        </div>
      </Modal>
    </>
  );
});
