import React from "react";
import { Progress, UncontrolledTooltip } from "reactstrap";

function ProgressBarTooltip(props) {
  return (
    <>
      <p
        style={{
          fontSize: "9pt",
          margin: 0,
          whiteSpace: "nowrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          width: "100%",
        }}
        className="text-muted"
        id={props.id}
      >
        {props.text}
      </p>
      <Progress color={props.color} value={props.percentage} id={props.id}>
        <b style={{ color: "#555" }}>{props.percentage}%</b>
      </Progress>
      <UncontrolledTooltip placement="top" target={props.id}>
        {props.text}
      </UncontrolledTooltip>
    </>
  );
}

export default ProgressBarTooltip;
