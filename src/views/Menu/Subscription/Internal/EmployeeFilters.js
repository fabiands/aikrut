import React, { memo } from "react";
import { Row, Col } from "reactstrap";
import { DateFilters } from "../Filters/DateFilters";
import { ResetFilters } from "../Filters/ResetFilters";
import { SearchApplicantNameFilter } from "../Filters/SearchApplicantNameFilter";
// import { SelectJobFilter } from "../Filters/SelectJobFilter";
// import { StatusApplicantFilter } from "../Filters/StatusApplicantFilter";
import { translate, t } from "react-switch-lang";

export const EmployeeFilters = translate(memo((props) => {
    return (
        <Row>
            <Col sm="12">
                <SearchApplicantNameFilter classname={props.classSearch} data={t('searchEmployeeName')} />
                <hr />
            </Col>
                <Col sm="12" >
                    <DateFilters classname={props.classDateFilter}  />
                    <hr />
                </Col>
                {/* <Col sm="12">
                    <SelectJobFilter />
                    <hr />
                </Col>
                <Col sm="12">
                    <StatusApplicantFilter />
                    <hr />
                </Col> */}
                <Col sm="12">
                    <ResetFilters />
                </Col>
         
        </Row>
    )
}))

export const EmployeeModalFilters = memo(() => {
    return (
        <Row>
            <Col sm="12">
                <DateFilters />
                <hr />
            </Col>
            {/* <Col sm="12">
                <SelectJobFilter />
                <hr />
            </Col>
            <Col sm="12">
                <StatusApplicantFilter />
                <hr />
            </Col>
            <Col sm="12">
                <ResetFilters />
            </Col> */}
        </Row>
    )
})