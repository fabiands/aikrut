import React, { useCallback, useMemo, useState, memo, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Card, CardBody, Col, Modal, ModalBody, Row } from "reactstrap";
import DataNotFound from "../../../../components/DataNotFound";
import usePagination from "../../../../hooks/usePagination";
// import StatusBadge from '../Applicants/StatusBadge';
import { useRecruitmentsFiltersCtx } from "../Context/RecruitmentContext";
import { SearchApplicantNameFilter } from "../Filters/SearchApplicantNameFilter";
import * as moment from "moment";
import { t, translate } from "react-switch-lang";
import { EmployeeFilters, EmployeeModalFilters } from "./EmployeeFilters";
import { useAuthUser } from "../../../../store";
import { useDispatch } from "react-redux";
import disableScroll from "disable-scroll";
import { getMe } from "../../../../actions/auth";
import Tour from "reactour";
import { useMediaQuery } from "react-responsive";
import request from "../../../../utils/request";
import profilePhotoNotFound from "../../../../assets/img/no-photo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ProgressBarTooltip from "../Components/ProgressBarTooltip";

function EmployeeList({ data: applicants, data2 }) {
  const isSmallSize = useMediaQuery({ query: "(max-width: 768px)" });
  const [filters, setFilters] = useRecruitmentsFiltersCtx();
  const [modalFilter, setModalFilter] = useState(false);
  const [dataNotFound, setDataNotFound] = useState(false);
  const user = useAuthUser();
  const [isTour, setIsTour] = useState(false);
  const disableBody = () => disableScroll.on();
  const enableBody = () => disableScroll.off();
  const accentColor = "#1d5a8e";
  const dispatch = useDispatch();
  const perPage = 14;

  useEffect(() => {
    if (user.guidance.layout && user.guidance.header) {
      window.scroll({ top: 0, behavior: "smooth" });
      if (!user.guidance.internalAssessmentAfter) {
        setIsTour(true);
      }
    }
  }, [user]);

  const disableGuideAfter = () => {
    setIsTour(false);
    request
      .put("auth/guidance", { guidance: "internalAssessmentAfter" })
      .then(() => {
        dispatch(getMe());
      });
  };

  const filtered = useMemo(() => {
    let data = applicants;
    if (filters) {
      data = data
        ?.filter((item) =>
          filters.searchApplicant
            ? item.detail?.fullName
              ?.toLowerCase()
              .includes(filters.searchApplicant.toLowerCase())
            : true
        )
        ?.filter((item) => {
          return filters.statusApplicant.length > 0
            ? filters.statusApplicant.includes(item.status)
            : true;
        })
        ?.filter((item) => {
          const getFilters = filters.selectJob?.map((e) => e.value) ?? [];
          return getFilters.length > 0
            ? getFilters.includes(item.job_vacancy.id)
            : true;
        });
      if (filters.date.start && filters.date.end) {
        data = data.filter((item) => {
          return moment(moment(item.createdAt).format("YYYY-MM-DD")).isBetween(
            moment(filters.date.start).subtract(1, "day"),
            moment(filters.date.end).add(1, "day")
          );
        });
      }
    }
    data?.length > 0 ? setDataNotFound(false) : setDataNotFound(true);
    return data;
  }, [filters, applicants]);

  const handleChangeCurrentPage = useCallback(
    (page) => {
      setFilters((state) => ({ ...state, paginationApplicants: page }));
    },
    [setFilters]
  );

  const { data: groupFiltered, PaginationComponent } = usePagination(
    filtered,
    perPage,
    filters.paginationApplicants,
    handleChangeCurrentPage
  );

  const steps = [
    {
      selector: ".tour-employeeList",
      content: ({ goTo, inDOM }) => (
        <div>
          <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("detailSubsAssessmentGuideTitle")}
          </h5>
          <p>{t("detailSubsAssessmentGuideSubTitle")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-12 text-right p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(1)}
                >
                  {t("btnNext")} <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-searchEmployee",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("detailSubsAssessmentGuide1")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(0)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(2)}
                >
                  {t("btnNext")} <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-dateFilter",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("detailSubsAssessmentGuide2")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(1)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(3);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-addParticipant",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("detailSubsAssessmentGuide3")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(2)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(4);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-download",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("detailSubsAssessmentGuide4")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(3)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(5);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-upload",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("detailSubsAssessmentGuide5")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(4)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(6);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-cardEmployee",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("detailSubsAssessmentGuide6")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(5)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={() => {
                    disableGuideAfter();
                  }}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
  ];

  const companyValue = {
    F: t("companyValuePoint1"),
    W: t("companyValuePoint2"),
    N: t("companyValuePoint3"),
    G: t("companyValuePoint4"),
    A: t("companyValuePoint5"),
    L: t("companyValuePoint6"),
    P: t("companyValuePoint7"),
    I: t("companyValuePoint8"),
    T: t("companyValuePoint9"),
    V: t("companyValuePoint10"),
    X: t("companyValuePoint11"),
    S: t("companyValuePoint12"),
    B: t("companyValuePoint13"),
    O: t("companyValuePoint14"),
    R: t("companyValuePoint15"),
    D: t("companyValuePoint16"),
    C: t("companyValuePoint17"),
    Z: t("companyValuePoint18"),
    E: t("companyValuePoint19"),
    K: t("companyValuePoint20"),
  };
  return (
    <div className="px-1" style={{ borderBottom: 0 }}>
      <Tour
        steps={steps}
        accentColor={accentColor}
        showButtons={false}
        rounded={5}
        isOpen={isTour}
        closeWithMask={false}
        disableFocusLock={true}
        disableInteraction={true}
        onAfterOpen={disableBody}
        onBeforeClose={enableBody}
        onRequestClose={() => {
          disableGuideAfter();
        }}
      />
      <div className="menu-mobile">
        <Row className={`mb-1 ${isSmallSize ? `tour-filterEmployee` : ``}`}>
          <Col xs="9">
            <SearchApplicantNameFilter
              classname={isSmallSize ? "tour-searchEmployee" : ""}
              data={t("karyawan")}
            />
          </Col>
          <Col
            xs="3"
            className={`p-0 text-nowrap text-center ${isSmallSize ? `tour-dateFilter` : ``
              }`}
          >
            <Button
              onClick={() => setModalFilter(true)}
              style={{
                backgroundColor: "transparent",
                border: "0px",
              }}
            >
              <i className="fa fa-2x fa-filter" style={{ color: "#335877" }} />
              &nbsp;
              <small style={{ color: "#335877" }}>Filter</small>
            </Button>
          </Col>
        </Row>
      </div>
      <Row className="mb-5">
        <Col
          sm="3"
          className={`menu-dashboard ${!isSmallSize ? `tour-filterEmployee` : ``
            }`}
        >
          <EmployeeFilters
            classSearch="tour-searchEmployee"
            classDateFilter="tour-dateFilter"
          />
        </Col>
        <Col sm="9">
          {dataNotFound || filtered?.length === 0 ? (
            <DataNotFound />
          ) : (
            <CardEmployee
              data={groupFiltered}
              Pagination={
                filtered.length > perPage ? PaginationComponent : () => null
              }
              filtered={filtered}
              companyValue={companyValue}
              isPapi={data2}
            />
          )}
        </Col>
      </Row>

      {/* <Row className="mb-5">
                <Col md="4" className="mb-3">
                    <SearchApplicantNameFilter data="Karyawan" />
                </Col>
                <Col xs="12" className="mt-3">
                    {dataNotFound || filtered.length === 0 ?
                        <DataNotFound />
                            :
                            <TableListEmployee data={groupFiltered} Pagination={PaginationComponent} filtered={filtered} />
                    }
                </Col>
            </Row> */}
      <Modal
        className="bottom-small"
        isOpen={modalFilter}
        toggle={() => setModalFilter(false)}
      >
        <ModalBody>
          <div className="text-center">
            <Button
              className="mx-auto mt-1 mb-3"
              style={{
                width: "50%",
                height: "6px",
                borderRadius: "10px",
                backgroundColor: "#696969",
                padding: "0px",
              }}
              onClick={() => setModalFilter(false)}
            />
          </div>
          <EmployeeModalFilters />
          <div className="text-center">
            <Button
              color="netis-color"
              className="my-3"
              onClick={() => setModalFilter(false)}
            >
              {t("btnShowResult")}
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

// const TableListEmployee = translate(
//   memo(({ data = [], Pagination, filtered }) => {
//     return (
//       <div className="tour-employeeList">
//         <Row className="font-weight-bold d-none d-md-flex">
//           <Col sm md="3">
//             {t("applicantName")}
//           </Col>
//           <Col sm md="3">
//             {t("position")}
//           </Col>
//           <Col sm md="3">
//             {t("date")}
//           </Col>
//           <Col sm md="3"></Col>
//         </Row>
//         <hr />
//         {data.length > 0 ? (
//           data.map((data, idx) => (
//             <Card key={data.id} className="shadow-sm border-0">
//               <CardBody>
//                 <Row>
//                   <Col sm="12" md="3">
//                     {data.detail.fullName}
//                   </Col>
//                   <Col sm="12" md="3">
//                     {data.job_vacancy.name}
//                   </Col>
//                   <Col sm="12" md="3">
//                     {moment(data.createdAt)
//                       .locale(langUtils.getLanguage())
//                       .format("DD MMMM YYYY")}
//                   </Col>
//                   <Col sm="12" md="3">
//                     {/* <Row>
//                                             <Col sm="6" md="6" xl="6" className="mb-2">
//                                                 <StatusBadge status={data.status} size={11} />
//                                             </Col>
//                                             <Col sm="6" md="6" xl="6">
//                                                 <Link to={`/recruitment/applicants/${data.id}`}>
//                                                     <Button size="sm" color="netis-color" className={idx === 0 ? 'tour-detailbutton' : ''}>{t('lihatdetail')}</Button>
//                                                 </Link>
//                                             </Col>
//                                         </Row> */}
//                     <Link to={`/recruitment/applicants/${data.id}`}>
//                       <Button
//                         size="sm"
//                         color="netis-color"
//                         className={idx === 0 ? "tour-employeeDetail" : ""}
//                       >
//                         {t("lihatdetail")}
//                       </Button>
//                     </Link>
//                   </Col>
//                 </Row>
//               </CardBody>
//             </Card>
//           ))
//         ) : (
//           <DataNotFound />
//         )}
//         {filtered.length > 8 ? <Pagination /> : ""}
//       </div>
//     );
//   })
// );

const CardEmployee = translate(
  memo(({ data = [], Pagination, filtered, companyValue, isPapi }) => {
    return (
      <>
        {data?.length > 0 ? (
          <>
            <Row>
              {data?.map((data, idx) => (
                <Col
                  xs="12"
                  sm="6"
                  md="6"
                  lg="6"
                  key={idx}
                  className="pt-3 tour-cardEmployee"
                >
                  <Link
                    to={
                      !data.detail.idUser
                        ? "#"
                        : `/recruitment/applicants/${data.id}`
                    }
                    className={`card-detail-job`}
                  >
                    <Card
                      className={
                        data.job_vacancy?.resultCompanyValue
                          ? data.job_vacancy?.resultCompanyValue <= 0
                            ? "shadow-sm rounded border-0"
                            : "shadow-sm rounded border-0 ribbons-list"
                          : "shadow-sm rounded border-0"
                      }
                      style={{ minHeight: 100 }}
                      data-label={data.job_vacancy?.resultCompanyValue + "%"}
                    >
                      <div className="card-container">
                        <CardBody>
                          <Row>
                            <Col sm="4" className="text-center mt-2">
                              <div className="card-image d-flex justify-content-center p-3">
                                <img
                                  src={
                                    data.detail.avatar ?? profilePhotoNotFound
                                  }
                                  alt="photos profile"
                                  className="rounded-circle"
                                // onError={(e) => onErrorImage(e)}
                                />
                              </div>
                            </Col>
                            <Col xs="8" className="text-left pt-3">
                              <h4 className="mt-3 mb-2 text-netis-primary font-weight-bolder">
                                {data.detail.fullName}
                              </h4>
                              <Row>
                                <Col
                                  xs="1"
                                  className="text-center"
                                  style={{ color: "#909090" }}
                                >
                                  <b>
                                    <FontAwesomeIcon icon="university" />
                                  </b>
                                </Col>
                                <Col>
                                  <b>
                                    {data?.detail?.lastEducation
                                      ? t(data?.detail?.lastEducation)
                                      : "-"}
                                  </b>
                                </Col>
                              </Row>
                              <Row>
                                <Col
                                  xs="1"
                                  className="text-center"
                                  style={{
                                    color:
                                      data.detail.gender === "Pria"
                                        ? "#0F52BA"
                                        : "#F6A9A9",
                                  }}
                                >
                                  <b>
                                    <FontAwesomeIcon icon="venus-mars" />
                                  </b>
                                </Col>
                                <Col>
                                  <b>
                                    {data?.detail?.gender
                                      ? t(data?.detail?.gender)
                                      : "-"}
                                  </b>
                                </Col>
                              </Row>
                            </Col>
                            <Col xs="12">
                              <Row>
                                {!data.detail.idUser ? null : data.job_vacancy
                                  ?.detailCompanyValue ? (
                                  <Col
                                    sm="12"
                                    className="mt-3"
                                    style={{ minHeight: "61px" }}
                                  >
                                    <Row>
                                      {data.job_vacancy?.detailCompanyValue?.map(
                                        (dataDetail, idx) => (
                                          <>
                                            <Col
                                              sm={
                                                data.detail?.detailCompanyValue
                                                  ?.length >= 3
                                                  ? "4"
                                                  : data.detail
                                                    ?.detailCompanyValue
                                                    ?.length === 2
                                                    ? "6"
                                                    : "4"
                                              }
                                            >
                                              <ProgressBarTooltip
                                                percentage={
                                                  (dataDetail?.value / 5) * 100
                                                }
                                                color={
                                                  idx === 0
                                                    ? "success"
                                                    : idx === 1
                                                      ? "warning"
                                                      : "info"
                                                }
                                                text={
                                                  companyValue[
                                                  dataDetail?.label
                                                  ]
                                                }
                                                id={"tooltip" + idx}
                                              />
                                            </Col>
                                          </>
                                        )
                                      )}
                                    </Row>
                                  </Col>
                                ) : (
                                  <Col
                                    xs="12"
                                    className="row mt-3 justify-content-center align-items-center text-center"
                                    style={
                                      isPapi ? { minHeight: "61px" } : null
                                    }
                                  >
                                    {isPapi ? (
                                      <span>
                                        {t("hasNotCompleteAllAssessment")}
                                      </span>
                                    ) : null}
                                  </Col>
                                )}
                              </Row>
                            </Col>
                          </Row>
                        </CardBody>
                      </div>

                      <div className="header-card">
                        {data.detail.idUser ? (
                          <p className="m-0" style={{ color: "#fff" }}>{`${t(
                            "mengisiAsesmenPada"
                          )} ${moment(data.createdAt).format(
                            "DD MMMM YYYY"
                          )}`}</p>
                        ) : (
                          <p className="m-0" style={{ color: "#fff" }}>
                            User Belum Ada Data nya
                          </p>
                        )}
                      </div>
                    </Card>
                  </Link>
                </Col>
              ))}
            </Row>
            <Pagination />
          </>
        ) : (
          <DataNotFound />
        )}
      </>
    );
  })
);

export default translate(EmployeeList);
