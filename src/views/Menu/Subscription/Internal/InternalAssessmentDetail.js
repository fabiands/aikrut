import React, { useState, useMemo } from "react";
import DataNotFound from "../../../../components/DataNotFound";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import ModalError from "../../../../components/ModalError";
import request from "../../../../utils/request";
import EmployeeList from "./EmployeeList";
import { translate, t } from "react-switch-lang";
import { useRouteMatch } from "react-router-dom";
import readXlsxFile from "read-excel-file";
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  Spinner,
} from "reactstrap";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import useSWR from "swr";
function InternalAssessmentDetail() {
  const [popup, setPopup] = useState(false);
  const [loadingButton, setLoadingButton] = useState(false);
  const [excelData, setExcelData] = useState([]);
  const matchRoute = useRouteMatch();
  const maxSizInByte = 2000000;
  const {data: response, error, mutate} = useSWR("v1/recruitment/applicants/internal/" + matchRoute?.params?.id)
  const loading = !response && !error
  const data = useMemo(() => response?.data ?? [], [response])

  if (loading) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  const handleDownload = () => {};
  const handleUpload = () => {
    setPopup(!popup);
    setExcelData([]);
  };

  function onDrop(e) {
    if (
      e.target.files.length > 0 &&
      maxSizInByte &&
      e.target.files[0]?.size > maxSizInByte
    ) {
      toast.error("File melebihi ukuran maximal 2 Mb.");
      return false;
    }
    readXlsxFile(e.target.files[0]).then((rows) => {
      setExcelData(rows);
    });
  }

  function uploadExcel() {
    if (excelData.length > 1) {
      setLoadingButton(true);
      let dataJson = JSON.stringify(excelData);
      request
        .post(
          "v1/recruitment/applicant/internal/excel/" + matchRoute?.params?.id,
          {
            data_input: dataJson,
          }
        )
        .then(() => {
          mutate()
          toast.success("Success");
          setPopup(false)
          setLoadingButton(false)
          // window.location.reload()
        })
        .catch((err) => {
          setLoadingButton(false);
          toast.error("Error");
        });
    } else {
      toast.error("Error");
    }
  }

  return (
    <div>
      <Card>
        <CardHeader className="d-flex align-items-center bg-netis-primary">
          <h5 className="mb-0" style={{ color: "#ffff" }}>
            {data?.job}
          </h5>
        </CardHeader>
        <CardBody style={{ minHeight: "1000px" }}>
          <div className="d-flex justify-content-end">
                <Link
                  to={
                    `/recruitment/internal/applicant/create/` +
                    matchRoute?.params?.id
                  }
                >
                  <Button className="btn btn-sm btn-netis mr-1 tour-addParticipant">
                    <i className="fa fa-plus" /> Tambah Data
                  </Button>
                </Link>
            <Button
              className="btn btn-sm btn-success mr-1 tour-download"
              onClick={handleDownload}
            >
              <Link
                to={"/Template_Excel_Add_Peserta.xlsx"}
                target="_blank"
                download
                style={{ color: "#fff" }}
              >
                <i className="fa fa-download" /> Download Template
              </Link>
            </Button>
            <Button
              className="btn btn-sm tour-upload"
              style={{ backgroundColor: "#43A2C0", color: "white" }}
              onClick={handleUpload}
              >
              <i className="fa fa-upload" /> Upload Data
            </Button>
          </div>
          {data && data?.data?.length > 0 ? (
            <EmployeeList data={data?.data} data2={data?.papikostick} />
          ) : (
            // Kondisi ketika sudah ada karyawan yang mengsisi internal asesmen dan data ditampilkan
            <div className="my-5 text-center">
              <div
                className="text-center mx-auto mb-3 bg-secondary rounded  pt-3 pb-2"
                style={{ width: "75%", marginTop: "10vh" }}
              >
                <h5>{t("noInternalSub")}</h5>
              </div>
              <DataNotFound />
            </div>
          )}
        </CardBody>
      </Card>
      <Modal
        isOpen={popup}
        style={{ marginTop: "40vh" }}
        backdropClassName="back-home"
      >
        <ModalHeader className="border-bottom-0">Upload Excel Data</ModalHeader>
        <ModalBody>
          <Row>
            {!loadingButton ? (
              <div className="col-12 text-center">
                <Input
                  type="file"
                  id="document-img-upload"
                  name="document-img-upload"
                  accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                  onChange={onDrop}
                />
                <Button
                  className="mt-2 mr-2"
                  type="submit"
                  color="netis-danger"
                  onClick={handleUpload}
                >
                  Cancel
                </Button>
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-success"
                  onClick={uploadExcel}
                >
                  Upload
                </Button>
              </div>
            ) : (
              <Spinner />
            )}
          </Row>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default translate(InternalAssessmentDetail);
