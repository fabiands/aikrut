import React, { useCallback, useMemo, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import DataNotFound from "../../../../components/DataNotFound";
import usePagination from "../../../../hooks/usePagination";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import ModalError from "../../../../components/ModalError";
import { useAuthUser } from "../../../../store";
import request from "../../../../utils/request";
import disableScroll from "disable-scroll";
import { getMe } from "../../../../actions/auth";
import Tour from "reactour";
import { useRecruitmentsFiltersCtx } from "../Context/RecruitmentContext";
import { SearchGroupNameFilter } from "../Filters/SearchGroupNameFilter";

import {
  Button,
  Row,
  Card,
  CardBody,
  Col,
  // Modal,
  // ModalBody,
  // ModalHeader,
} from "reactstrap";
// import * as moment from "moment";
import { translate, t } from "react-switch-lang";
// import { useUserBalance } from "../../../../hooks/useUserBalance";
function InternalAssessment() {
  const [loading, setLoading] = useState(false);
  const [filters, setFilters] = useRecruitmentsFiltersCtx();
  const [grups, setGrups] = useState([]);
  const [error, setError] = useState(false);
  const [firstTour, setFirstTour] = useState(false);
  const [isTour, setIsTour] = useState(false);
  const user = useAuthUser();
  const disableBody = () => disableScroll.on();
  const enableBody = () => disableScroll.off();
  const accentColor = "#1d5a8e";
  const dispatch = useDispatch();
  // const { data } = useUserBalance();
  // const today = new Date();
  // const momentToday = moment(today).format("YYYY-MM-DD");
  // const myBalance = useMemo(() => data?.balance ?? 0, [data]);
  // const isExpired = useMemo(() => data?.isExpired, [data]);
  // const [publish, setPublish] = useState(false);
  console.log(filters, useRecruitmentsFiltersCtx());
  useEffect(() => {
    if (user.guidance.layout && user.guidance.header) {
      window.scroll({ top: 0, behavior: "smooth" });
      if (!user.guidance.internalAssessmentBefore) {
        setFirstTour(true);
      }
    }
  }, [user]);

  const filtered = useMemo(() => {
    let data = grups;

    if (filters) {
      data = data.filter((item) =>
        filters?.searchGroup
          ? item?.name
              ?.toLowerCase()
              .includes(filters.searchGroup.toLowerCase())
          : true
      );
    }
    return data;
  }, [filters, grups]);

  const handleChangeCurrentPage = useCallback(
    (page) => {
      setFilters((state) => ({ ...state, paginationGroups: page }));
    },
    [setFilters]
  );

  const { data: groupFiltered, PaginationComponent } = usePagination(
    filtered,
    10,
    filters?.paginationGroups,
    handleChangeCurrentPage
  );

  const disableGuideBefore = () => {
    setFirstTour(false);
    setIsTour(true);
    request
      .put("auth/guidance", { guidance: "internalAssessmentBefore" })
      .then(() => {
        dispatch(getMe());
      });
  };

  const changePublished = (id, status) => {
    request
      .put("v1/recruitment/vacancies/open-close/" + id, { status: status })
      .then(() => {
        listInternal();
      });
  };

  const disableGuideTour = () => {
    setIsTour(false);
  };

  useEffect(() => {
    listInternal();
  }, []);

  function listInternal() {
    setLoading(true);
    request
      .get("v2/recruitment/internal")
      .then((res) => {
        setGrups(res.data.data);
      })
      .catch((err) => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  if (loading) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  const firstStep = [
    {
      selector: ".tour-internalBefore",
      content: ({ goTo, inDOM }) => (
        <div>
          <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5>
          <p>{t("welcomeSubsDesc")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-12 text-center p-0">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={() => {
                    disableGuideBefore();
                  }}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
  ];

  const tourSteps = [
    {
      selector: ".tour-assessmentSearchGroup",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5> */}
          <p>{t("welcomeSubsGuide1")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-12 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(1);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-createGroup",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5> */}
          <p>{t("welcomeSubsGuide2")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(0)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(2);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-groupList",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5> */}
          <p>{t("welcomeSubsGuide3")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(1)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(3);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-groupCard",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5> */}
          <p>{t("welcomeSubsGuide4")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(2)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(4);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-numberOfParticipants",
      content: ({ goTo, inDOM }) => (
        <div>
          <p>{t("welcomeSubsGuide5")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(3)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(5);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-editBtn",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5> */}
          <p>{t("welcomeSubsGuide6")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(4)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(6);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-shareBtn",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeSubs")}
          </h5> */}
          <p>{t("welcomeSubsGuide7")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => goTo(5)}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={() => {
                    disableGuideTour();
                  }}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
  ];

  return (
    <div>
      <Tour
        steps={firstStep}
        accentColor={accentColor}
        rounded={5}
        isOpen={firstTour}
        closeWithMask={false}
        showNumber={false}
        disableFocusLock={true}
        disableInteraction={true}
        disableDotsNavigation={true}
        showNavigation={false}
        showButtons={false}
        onAfterOpen={disableBody}
        onBeforeClose={enableBody}
        onRequestClose={() => {
          disableGuideBefore();
        }}
      />
      <Tour
        steps={tourSteps}
        accentColor={accentColor}
        rounded={5}
        isOpen={isTour}
        closeWithMask={false}
        disableFocusLock={true}
        disableInteraction={true}
        disableDotsNavigation={true}
        showNavigation={true}
        showButtons={false}
        onAfterOpen={disableBody}
        onBeforeClose={enableBody}
        onRequestClose={() => {
          disableGuideTour();
        }}
      />
      <div className={`p-2`}>
        <Row>
          <Col className="tour-assessmentSearchGroup" sm="6" md="3">
            <SearchGroupNameFilter data={t("searchGroupName")} />
          </Col>

          <Col sm="6" md="9" className="text-right ">
            <Link to={"/recruitment/internal/create"}>
              <button className="btn btn-sm btn-netis-primary px-4 py-2 scale-div tour-createGroup ">
                <i className="fa fa-plus mr-2"></i>
                {t("sendRequest")}
              </button>
            </Link>
          </Col>
        </Row>
        <hr />
      </div>
      {groupFiltered && groupFiltered.length > 0 ? (
        <Row className="tour-groupList">
          {groupFiltered.map((grup, idx) => {
            return (
              <>
                <Col sm="12" key={idx}>
                  <Card className="shadow-sm border-0 tour-groupCard">
                    <CardBody>
                      <Row>
                        <Col sm md="8">
                          <Link
                            to={`/recruitment/internal/${grup?.id}/participant`}
                          >
                            <h4 style={{ color: "#305574", display: "inline" }}>
                              {grup?.name}
                            </h4>
                          </Link>
                          <br />
                          <br />
                          {grup?.requirements &&
                            grup?.requirements.map((requirement, id) => {
                              return (
                                <div
                                  style={{
                                    color: "#555",
                                    backgroundColor: "#F2F2F2",
                                    padding: "5px 15px",
                                    borderRadius: "15px",
                                    display: "inline-block",
                                    marginRight: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={id}
                                >
                                  {t(requirement)}
                                </div>
                              );
                            })}
                          <br />
                          <Link
                            to={`/recruitment/internal/${grup?.id}/edit`}
                            className="tour-editBtn"
                          >
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mr-1"
                            >
                              <i className="fa fa-pencil"></i> Edit
                            </Button>
                          </Link>
                          {!grup?.published ? (
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mx-1 mt-1"
                              onClick={() => changePublished(grup.id, 1)}
                            >
                              <p
                                // className="text-info"
                                style={{ display: "inline" }}
                              >
                                <i className="fa fa-refresh"></i> Reopen
                              </p>
                            </Button>
                          ) : (
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mx-1 mt-1"
                              onClick={() => changePublished(grup.id, 0)}
                            >
                              <p
                                // className="text-danger"
                                style={{ display: "inline" }}
                              >
                                <i className="fa fa-window-close"></i>{" "}
                                {t("close")}
                              </p>
                            </Button>
                          )}
                        </Col>
                        <Col sm md="4" className="text-center pt-4 pb-4">
                          <Link
                            to={`/recruitment/internal/${grup?.id}/participant`}
                            className={`btn text-wrap btn-outline-netis-primary mr-2 my-1 text-capitalize btn-job-applicant-status pt-3 pb-3 tour-numberOfParticipants`}
                          >
                            <h4 className="mb-0">{grup?.applicants}</h4>
                            {t("peserta")}
                          </Link>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </>
            );
          })}
          <Col sm="12" className="text-center">
            {filtered.length > 10 ? <PaginationComponent /> : ""}
          </Col>
        </Row>
      ) : (
        <div className="my-5 text-center">
          <DataNotFound />
        </div>
      )}
    </div>
  );
}

// const ModalTokenNull = translate(
//   memo(({ text, btnColor, className, icon }) => {
//     const [modalTokenNull, setModalTokenNull] = useState(false);

//     const toggle = () => {
//       setModalTokenNull(!modalTokenNull);
//     };

//     return (
//       <>
//         <Button
//           style={{ border: 0 }}
//           className={`btn bg-transparent btn-sm mx-1 mt-1 ${className}`}
//           onClick={toggle}
//         >
//           <p style={{ display: "inline" }}>
//             <i className={`fa ${icon}`}></i> {text}
//           </p>
//         </Button>
//         <Modal isOpen={modalTokenNull} style={{ marginTop: "40vh" }}>
//           <ModalHeader className=" text-center border-bottom-0">
//             {t("caution")}
//           </ModalHeader>
//           <ModalBody className="text-center">
//             {t("cannotPublish")}
//             <br />
//             <div
//               className="d-flex justify-content-center mx-auto text-center mt-2"
//               style={{ width: "60%" }}
//             >
//               <Button
//                 className="button-token"
//                 onClick={() => {
//                   setModalTokenNull(false);
//                 }}
//               >
//                 {t("btnUnderstand")}
//               </Button>
//             </div>
//           </ModalBody>
//         </Modal>
//       </>
//     );
//   })
// );
// const ModalShareUrl = (props) => {
//   const [modal, setModal] = useState(false);

//   const toggle = () => setModal(!modal);

//   const closeBtn = (
//     <button className="close" onClick={toggle}>
//       &times;
//     </button>
//   );
//   return (
//     <>
//       <Button
//         style={{ border: 0 }}
//         className="btn bg-transparent btn-sm mx-1 mt-1 tour-shareBtn"
//         onClick={toggle}
//       >
//         <p
//           // className="text-danger"
//           style={{ display: "inline" }}
//         >
//           <i className="fa fa-share"></i> {t("share")}
//         </p>
//       </Button>
//       <Modal
//         isOpen={modal}
//         toggle={toggle}
//         size="md"
//         backdropClassName="share-modal-backdrop"
//       >
//         <ModalHeader
//           className="share-modal-header pb-2"
//           toggle={toggle}
//           close={closeBtn}
//           cssModule={{ "modal-title": "w-100 text-center" }}
//         >
//           <b>{t("shareUrl")}</b>
//           <hr className="m-2" />
//         </ModalHeader>
//         <ModalBody className="p-1">
//           <div className="share-url-wrapper">
//             <input
//               className="urlHolder2"
//               id="url"
//               type="text"
//               value={props.data}
//             />
//             <button
//               className="share-url-button btn-netis-primary"
//               onClick={() => {
//                 let urlHolder = document.getElementById("url");
//                 let copiedText = document.getElementById("copiedText");

//                 urlHolder.select();
//                 urlHolder.setSelectionRange(0, 99999);
//                 document.execCommand("copy");

//                 copiedText.innerHTML = t("modalShareText2");
//               }}
//             >
//               <i className="fa fa-copy"></i>
//             </button>
//             <p id="copiedText" className="text-muted">
//               {t("modalShareText1")}
//             </p>
//           </div>
//         </ModalBody>
//       </Modal>
//     </>
//   );
// };

export default translate(InternalAssessment);
