import React, { useEffect, useMemo, useState } from "react";
import { useFormik } from "formik";
import {
  Button,
  Col,
  CustomInput,
  Form,
  Input,
  Label,
  Row,
  Spinner,
} from "reactstrap";
import { useRouteMatch } from "react-router-dom";
import request from "../../../../utils/request";
import ModalError from "../../../../components/ModalError";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import langUtils from "../../../../utils/language/index";
import { t, translate } from "react-switch-lang";
import { toast } from "react-toastify";
import * as Yup from "yup";
import Select from "react-select";

function LinkAssessmentEdit(props) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [req, setReq] = useState([]);
  const [submit, setSubmit] = useState(false);
  // const [isDiscChecked, setIsDiscChecked] = useState(false);
  // const [isVideoChecked, setIsVideoChecked] = useState(false);
  // const [isProfileChecked, setIsProfileChecked] = useState(false);
  const matchRoute = useRouteMatch();

  const ValidationFormSchema = useMemo(() => {
    return Yup.object().shape({
      name: Yup.string().required().label(t("requirement")),
      requirements: Yup.array().test(
        "required",
        t("fillInAssessmentNeeds"),
        function (value) {
          return value.length > 0 && value.length <= 5;
        }
      ),
    });
  }, []);

  const { values, touched, errors, ...formik } = useFormik({
    initialValues: {
      name: "",
      requirements: [],
      companyValue: [],
    },
    validationSchema: ValidationFormSchema,
    onSubmit: (values, { setSubmitting, setErrors }) => {
      setSubmitting(true);
      setSubmit(true);
      request
        .put("v2/recruitment/internal/" + matchRoute?.params?.id, {
          name: values.name,
          requirements: values.requirements,
          companyValue: values.companyValue,
        })
        .then(() => {
          toast.success(t("sucessEditRequestAssessmentLink"));
          props.history.goBack();
        })
        .catch((err) => {
          // console.log(err)
          if (err?.response?.status === 403) {
            toast.error(t("sorryYouAlreadyHaveInternalAssessment"));
          } else if (err?.response?.status) {
            toast.error(t("anErrorOccurred"));
          }
          return;
        })
        .finally(() => {
          setSubmit(false);
          setSubmitting(false);
        });
    },
  });

  useEffect(() => {
    setLoading(true);
    request
      .get("v2/recruitment/internal/" + matchRoute?.params?.id)
      .then((res) => {
        formik.setFieldValue("name", res?.data?.data?.name);
        if (res?.data?.data?.requirements.length > 0) {
          formik.setFieldValue("requirements", res?.data?.data?.requirements);
        }
        if (res?.data?.data?.companyValue.length > 0) {
          formik.setFieldValue("companyValue", res?.data?.data?.companyValue);
        }
        request
          .get("v2/recruitment/vacancies/requirements/sub")
          .then((res) => {
            setReq(res?.data?.data);
          })
          .catch(() => {
            setError(true);
          })
          .finally(() => setLoading(false));
      })
      .catch(() => {
        setError(true);
        setLoading(false);
      });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (values.companyValue) {
      values.companyValue.map((data) => {
        data.label = t(data.label);
        return true;
      });
    } // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [matchRoute?.params?.id, langUtils.getLanguage()]);

  const changeRequirements = (e) => {
    const { value, checked } = e.target;

    let arr = values.requirements;
    arr.push(value);
    // console.log(value, checked);
    const set = new Set(arr);
    if (!checked) {
      set.delete(value);
    }

    if (!checked && value === "papikostick") {
      formik.setFieldValue("companyValue", null);
    }

    const setArr = Array.from(set);
    formik.setFieldValue("requirements", setArr);
  };

  if (loading) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  const formikSetValue = function (field) {
    return (value) => {
      formik.setFieldValue(field, value);
      formik.setFieldTouched(field, true);
    };
  };

  const companyValue = [
    { value: "F", label: t("companyValuePoint1") },
    { value: "W", label: t("companyValuePoint2") },
    { value: "N", label: t("companyValuePoint3") },
    { value: "G", label: t("companyValuePoint4") },
    { value: "A", label: t("companyValuePoint5") },
    { value: "L", label: t("companyValuePoint6") },
    { value: "P", label: t("companyValuePoint7") },
    { value: "I", label: t("companyValuePoint8") },
    { value: "T", label: t("companyValuePoint9") },
    { value: "V", label: t("companyValuePoint10") },
    { value: "X", label: t("companyValuePoint11") },
    { value: "S", label: t("companyValuePoint12") },
    { value: "B", label: t("companyValuePoint13") },
    { value: "O", label: t("companyValuePoint14") },
    { value: "R", label: t("companyValuePoint15") },
    { value: "D", label: t("companyValuePoint16") },
    { value: "C", label: t("companyValuePoint17") },
    { value: "Z", label: t("companyValuePoint18") },
    { value: "E", label: t("companyValuePoint19") },
    { value: "K", label: t("companyValuePoint20") },
  ];

  return (
    <div className="animated fadeIn d-flex flex-column bd-highlight mb-3 p-4">
      <div className="bd-highlight mb-4">
        <h5>{t("Buat Grup Baru Untuk Link Assesment")}</h5>
      </div>
      <Form onSubmit={formik.handleSubmit}>
        <Row>
          <Col md="6">
            <Row className="mb-5">
              <Col xs="3">
                <Label htmlFor="name" className="input-label pt-2">
                  {t("requirement")}
                </Label>
              </Col>
              <Col xs="9">
                <Input
                  type="input"
                  value={values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="name"
                  id="name"
                  required
                  maxLength="50"
                  className="form-control needs-validation"
                />
                {touched.name && errors.name && (
                  <small className="text-danger">{errors.name}</small>
                )}
              </Col>
            </Row>
            <Row className="my-5">
              <Col xs="3">
                <Label htmlFor="all" className="input-label">
                  {t("requirement2")}
                </Label>
              </Col>
              <Col xs="9">
                {/* <CustomInput
                  type="checkbox"
                  id="all"
                  name="all"
                  label={t("semua")}
                  value="all"
                  onChange={changeRequirements}
                  checked={values.requirements.length === req.length}
                /> */}
                {req.length > 0 &&
                  req.map((item, idx) => {
                    return (
                      <>
                        <CustomInput
                          key={idx}
                          type="checkbox"
                          id={item}
                          name={item}
                          value={item}
                          label={
                            item === "agility" || item === "businessInsight"
                              ? `${t(item)}*`
                              : t(item)
                          }
                          onChange={changeRequirements}
                          checked={
                            item === "agility" || item === "businessInsight"
                              ? true
                              : values.requirements.includes(item)
                          }
                          disabled={
                            item === "agility" || item === "businessInsight"
                              ? true
                              : false
                          }
                        />
                      </>
                    );
                  })}
                <small
                  className="text-form text-muted d-block"
                  style={{ fontSize: "9px" }}
                >
                  {t("maxOptional")}
                </small>
                {touched.requirements && errors.requirements && (
                  <small className="text-danger">{errors.requirements}</small>
                )}
              </Col>
            </Row>
            <Row
              className="chooseCharacteristic"
              style={
                values.requirements.length === req.length ||
                values.requirements.includes("papikostick")
                  ? { display: "flex" }
                  : { display: "none" }
              }
            >
              <Col xs="3">
                <Label htmlFor="companyValue" className="input-label">
                  {t("karakteristik")}
                </Label>
              </Col>
              <Col xs="9">
                <Row form>
                  <Col className="mb-2">
                    <Select
                      styles={{
                        menu: (provided) => ({ ...provided, zIndex: 9999 }),
                      }}
                      isSearchable={true}
                      name="companyValue"
                      id="companyValue"
                      closeMenuOnSelect={false}
                      onChange={(val) => {
                        if (val?.length > 3) return false;
                        formikSetValue("companyValue")(val);
                      }}
                      onBlur={formik.handleBlur}
                      value={values.companyValue}
                      options={companyValue}
                      isMulti
                    />
                    <small
                      className="text-form text-muted d-block"
                      style={{ fontSize: "9px" }}
                    >
                      {t("candidateCharacteristict")}
                    </small>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="text-center mt-5 mb-2">
              <Col xs="12" className="text-center">
                <Button
                  className="btn btn-sm btn-netis-primary px-4"
                  type="submit"
                  disabled={submit}
                >
                  {submit ? (
                    <>
                      <Spinner size="sm" color="light" /> Loading...{" "}
                    </>
                  ) : (
                    t("editRequest")
                  )}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default translate(LinkAssessmentEdit);
