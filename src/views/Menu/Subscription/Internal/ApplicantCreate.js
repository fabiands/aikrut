import React, { useMemo, useState } from "react";
import { useFormik } from "formik";
import {
  Button,
  Col,
  // CustomInput,
  Form,
  Input,
  Label,
  Row,
  Spinner,
} from "reactstrap";
import request from "../../../../utils/request";
import ModalError from "../../../../components/ModalError";
import LoadingAnimation from "../../../../components/LoadingAnimation";
// import { useUserBalance } from "../../../../hooks/useUserBalance";
import { t, translate } from "react-switch-lang";
import { toast } from "react-toastify";
// import Tour from "reactour";
import * as Yup from "yup";
import { useRouteMatch } from "react-router-dom";
import Select from "react-select";
import useSWR from "swr";

function ApplicantCreate(props) {
  // const [req, setReq] = useState([]);
  const [submit, setSubmit] = useState(false);
  // const { loading, data } = useUserBalance();
  const matchRoute = useRouteMatch();
  const {data: response, error} = useSWR("v1/recruitment/applicants/internal/" + matchRoute?.params?.id)
  const loadingPage = !response && !error;
  const userData = useMemo(() => response?.data?.data ?? [], [response]);
  const ValidationFormSchema = useMemo(() => {
    return Yup.object().shape({
      name: Yup.string().required().label(t("Nama")),
      gender: Yup.string().required().label(t("Jenis Kelamin")),
      email: Yup.string().email('Isian harus berupa email valid').required().label(t("Email")),
    });
  }, []);

  const { values, touched, errors, ...formik } = useFormik({
    initialValues: {
      name: "",
      email: "",
      gender: "",
    },
    validationSchema: ValidationFormSchema,
    onSubmit: (values, { setSubmitting, setErrors }) => {
      const hasEmail = userData?.find(i => i.userEmail === values.email)
      if(hasEmail){
        toast.error('Email sudah terdaftar')
        return;
      }
      setSubmitting(true);
      setSubmit(true);
      request
        .post("v1/recruitment/applicant/internal/" + matchRoute?.params?.id, {
          name: values.name,
          email: values.email,
          gender: values.gender.value,
        })
        .then(() => {
          toast.success("Berhasil Menambahkan Peserta");
          props.history.goBack();
        })
        .catch((err) => {
          // console.log(err)
          if (err?.response?.status === 403) {
            toast.error(t("sorryYouAlreadyHaveInternalAssessment"));
          } else if (err?.response?.status) {
            toast.error(t("anErrorOccurred"));
          }
          return;
        })
        .finally(() => {
          setSubmit(false);
          setSubmitting(false);
        });
    },
  });

  if (loadingPage) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  const genders = [
    { label: "Pria", value: "Pria" },
    { label: "Wanita", value: "Wanita" },
  ];

  const formikSetValue = function (field) {
    return (value) => {
      formik.setFieldValue(field, value);
      formik.setFieldTouched(field, true);
    };
  };

  return (
    <div className="animated fadeIn d-flex flex-column bd-highlight mb-3 p-4">
      <div className="bd-highlight mb-4">
        <h5>{t("Tambah Data")}</h5>
      </div>
      <Form onSubmit={formik.handleSubmit}>
        <Row>
          <Col md="6">
            <Row className="mb-5 tour-createGroupName">
              <Col xs="3">
                <Label htmlFor="name" className="input-label pt-2">
                  Nama Lengkap
                </Label>
              </Col>
              <Col xs="9">
                <Input
                  type="input"
                  value={values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="name"
                  id="name"
                  // required
                  maxLength="50"
                  className="form-control needs-validation"
                />
                {touched.name && errors.name && <small className='text-danger'>{errors.name}</small>}
              </Col>
            </Row>
            <Row className="mb-5 tour-createGroupName">
              <Col xs="3">
                <Label htmlFor="name" className="input-label pt-2">
                  Jenis Kelamin
                </Label>
              </Col>
              <Col xs="9">
                <Select
                  styles={{
                    menu: (provided) => ({ ...provided, zIndex: 9999 }),
                  }}
                  name="gender"
                  id="gender"
                  onChange={(val) => {
                    formikSetValue("gender")(val);
                  }}
                  onBlur={formik.handleBlur}
                  value={values.gender}
                  options={genders}
                  // required
                />
                {touched.gender && errors.gender && <small className='text-danger'>{errors.gender}</small>}
              </Col>
            </Row>
            <Row className="mb-5 tour-createGroupName">
              <Col xs="3">
                <Label htmlFor="name" className="input-label pt-2">
                  Email
                </Label>
              </Col>
              <Col xs="9">
                <Input
                  type="input"
                  value={values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="email"
                  id="email"
                  // required
                  className="form-control needs-validation"
                />
                {touched.email && errors.email && <small className='text-danger'>{errors.email}</small>}
              </Col>
            </Row>
            <Row className="text-center mt-5 mb-2">
              <Col xs="12" className="text-center">
                <Button
                  className="btn btn-sm btn-netis-primary px-4"
                  type="submit"
                  disabled={submit}
                >
                  {submit ? (
                    <>
                      <Spinner size="sm" color="light" /> Loading...{" "}
                    </>
                  ) : (
                    t("Simpan")
                  )}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default translate(ApplicantCreate);
