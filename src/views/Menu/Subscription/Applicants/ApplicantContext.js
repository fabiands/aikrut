import React, { createContext } from 'react'
import { useState } from 'react'
import { useContext } from 'react'

const generatePdfContext = createContext()
const setGeneratePdfContext = createContext()

export default function GeneratePdfProvider(props) {
    const [generatePdfCtx, setGeneratePdfCtx] = useState({
        photo: '',
        mbti: '',
        papikostick: '',
        disc: '',
        msdt: '',
        spm: '',
    })

    return (
        <setGeneratePdfContext.Provider value={setGeneratePdfCtx}>
            <generatePdfContext.Provider value={generatePdfCtx}>
                {props.children}
            </generatePdfContext.Provider>
        </setGeneratePdfContext.Provider>
    )
}

export const useGeneratePdf = () => {
    return [useContext(generatePdfContext), useContext(setGeneratePdfContext)]
}