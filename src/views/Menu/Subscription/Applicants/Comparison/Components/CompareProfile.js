import React from "react";
import { translate } from "react-switch-lang";
import { Table } from "reactstrap";
import SkeletonApplicantDetail from "../../../Skeleton/SkeletonApplicantDetail";
import ApplicantProfile from "../../Profile/Profile";

const CompareProfile = ({ data, dataToken, data2, dataToken2 }) => {
    if (!data) {
        return <SkeletonApplicantDetail />;
    }
    return (
        <Table borderless responsive>
            <tbody>
                <tr>
                    <td className="w-50" style={{verticalAlign:'top'}}>
                        <ApplicantProfile data={data} dataToken={dataToken} inline={true} />
                    </td>
                    <td className="w-50" style={{verticalAlign:'top'}}>
                        {data2 ?
                        <ApplicantProfile data={data2} dataToken={dataToken} inline={true} />
                        :
                        null}
                    </td>
                </tr>
            </tbody>
        </Table>
    );
};

export default translate(CompareProfile);