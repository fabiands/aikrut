import React, { useState } from "react";
import { translate, t } from "react-switch-lang";
import { Table } from "reactstrap";
import SkeletonApplicantDetail from "../../../Skeleton/SkeletonApplicantDetail";
import NoToken from "../../Assessments/NoToken";
import AssessmentMbti from "./../../Assessments/AssessmentMbti";
import AssessmentShio from "./../../Assessments/AssessmentShio";
import AssessmentFingerprint from "./../../Assessments/AssessmentFingerprint";
import AssessmentMsdt from "./../../Assessments/AssessmentMsdt";
import AssessmentSpm from "./../../Assessments/AssessmentSpm";
import AssessmentDisc from "./../../Assessments/AssessmentDisc";
import AssessmentBazi from "./../../Assessments/AssessmentBazi";
import AssessmentZodiac from "./../../Assessments/AssessmentZodiac";
import AssessmentFisiognomi from "./../../Assessments/AssessmentFisiognomi";
import AssessmentPalmistry from "./../../Assessments/AssessmentPalmistry";
import AssessmentPapikostick from "./../../Assessments/AssessmentPapikostick";
import AssessmentVideoCompare from "./../../Assessments/AssessmentVideoCompare";
import NoAssessment from "./../../Assessments/NoAssessment";
import AssessmentGestureCompare from "../../Assessments/AssessmentGestureCompare";
import AssessmentBusinessInsight from "./../../Assessments/AssessmentBusinessInsight";
import AssessmentAgility from "./../../Assessments/AssessmentAgility";
import { connect } from "react-redux";
import { withRouter } from "react-router";
const CompareAssessment = ({
  user,
  can,
  match,
  getAPI,
  data,
  dataToken,
  data2,
  dataToken2,
}) => {
  const [modalNoToken, setModalNoToken] = useState(true);
  const toggleNoToken = () => setModalNoToken(!modalNoToken);

  const {
    detail: { id, dataAssessment, dateOfBirth: date },
  } = data;

  const {
    mbti: assessmentMbti = [],
    papikostik: assessmentPapikostick = [],
    disc = [],
    video = [],
    gesture = [],
    fisiognomi = [],
    palmistry = [],
    shio = [],
    zodiac = [],
    bazi: assessmentBazi,
    fingerprint = null,
    msdt = null,
    spm = null,
    businessInsight = null,
    agility = null,
  } = dataAssessment;

  if (!data) {
    return <SkeletonApplicantDetail />;
  }

  return (
    <>
      {user.personnel.company.paid === "pre" ? (
        !dataToken.balance || dataToken.isExpired ? (
          <NoToken
            nullData="all"
            isOpen={modalNoToken}
            toggle={toggleNoToken}
          />
        ) : null
      ) : null}
      <Table responsive borderless className={data2 ? "table-compare" : ""}>
        {data2 && (
          <thead>
            <tr>
              <th className="w-50 h-0 bg-white"></th>
              <th className="w-50 h-0 bg-white"></th>
            </tr>
          </thead>
        )}
        <tbody>
          {!data2 ? (
            <tr>
              <td className="w-50" style={{ verticalAlign: "top" }}>
                {assessmentMbti &&
                  Boolean(assessmentMbti.length) &&
                  assessmentMbti.map((ases, index) => (
                    <AssessmentMbti
                      ases={ases}
                      key={index}
                      dataToken={dataToken}
                      position="comparison"
                    />
                  ))}
                {assessmentPapikostick && assessmentPapikostick[0] && (
                  <AssessmentPapikostick
                    papi={assessmentPapikostick[0]}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
                {disc && disc[0] && (
                  <AssessmentDisc
                    can={can}
                    data={disc[0]}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
                {msdt && (
                  <AssessmentMsdt
                    can={can}
                    data={msdt}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
                {spm && (
                  <AssessmentSpm
                    can={can}
                    data={spm}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}

                {businessInsight && (
                  <AssessmentBusinessInsight
                    can={can}
                    data={businessInsight}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}

                {agility && (
                  <AssessmentAgility
                    can={can}
                    data={agility}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}

                {video && video[0] && (
                  <AssessmentVideoCompare
                    can={can}
                    vid={video[0]}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="left"
                  />
                )}
                {gesture &&
                  gesture[0] &&
                  gesture[0]["data"] &&
                  gesture[0]["data"][0] &&
                  gesture[0]["data"][0]["resultData"] && (
                    <AssessmentGestureCompare
                      can={can}
                      vid={gesture[0]}
                      candidate="first"
                      id={id}
                      getAPI={getAPI}
                      dataToken={dataToken}
                      position="left"
                    />
                  )}

                {fisiognomi && fisiognomi[0] && (
                  <AssessmentFisiognomi
                    can={can}
                    fisiognomi={fisiognomi[0]}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
                {palmistry && palmistry[0] && (
                  <AssessmentPalmistry
                    can={can}
                    palmistry={palmistry[0]}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}

                {fingerprint && (
                  <AssessmentFingerprint
                    can={can}
                    data={fingerprint}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}

                {assessmentBazi && (
                  <AssessmentBazi
                    can={can}
                    bazi={assessmentBazi}
                    date={date}
                    candidate="first"
                    id={id}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
                {shio && (
                  <AssessmentShio
                    can={can}
                    shio={shio}
                    candidate="first"
                    id={id}
                    date={date}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
                {zodiac && (
                  <AssessmentZodiac
                    can={can}
                    zodiac={zodiac}
                    candidate="first"
                    id={id}
                    date={date}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    position="comparison"
                  />
                )}
              </td>
              <td className="w-50"></td>
            </tr>
          ) : (
            (() => {
              const {
                detail: {
                  id: id2,
                  dataAssessment: dataAssessment2,
                  dateOfBirth: date2,
                },
              } = data2;
              const {
                mbti: assessmentMbti2 = [],
                papikostik: assessmentPapikostick2 = [],
                disc: disc2 = [],
                video: video2 = [],
                gesture: gesture2 = [],
                fisiognomi: fisiognomi2 = [],
                palmistry: palmistry2 = [],
                shio: shio2 = [],
                zodiac: zodiac2 = [],
                bazi: assessmentBazi2,
                fingerprint: fingerprint2 = null,
                msdt: msdt2 = null,
                spm: spm2 = null,
                businessInsight: businessInsight2 = null,
                agility: agility2 = null,
              } = dataAssessment2;

              return (
                <>
                  <tr>
                    <td>
                      {assessmentMbti && Boolean(assessmentMbti.length) ? (
                        assessmentMbti.map((ases, index) => (
                          <AssessmentMbti
                            ases={ases}
                            key={index}
                            dataToken={dataToken}
                            position="comparison"
                          />
                        ))
                      ) : (
                        <NoAssessment ases={t("MBTITest")} />
                      )}
                    </td>
                    <td>
                      {assessmentMbti2 && Boolean(assessmentMbti2.length) ? (
                        assessmentMbti2.map((ases, index) => (
                          <AssessmentMbti
                            ases={ases}
                            key={index}
                            dataToken={dataToken}
                            position="comparison"
                          />
                        ))
                      ) : (
                        <NoAssessment ases={t("MBTITest")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {assessmentPapikostick && assessmentPapikostick[0] ? (
                        <AssessmentPapikostick
                          papi={assessmentPapikostick[0]}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("PapikostickTest")} />
                      )}
                    </td>
                    <td>
                      {assessmentPapikostick2 && assessmentPapikostick2[0] ? (
                        <AssessmentPapikostick
                          papi={assessmentPapikostick2[0]}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("PapikostickTest")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {disc && disc[0] ? (
                        <AssessmentDisc
                          can={can}
                          data={disc[0]}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("personalityTest")} />
                      )}
                    </td>
                    <td>
                      {disc2 && disc2[0] ? (
                        <AssessmentDisc
                          can={can}
                          data={disc2[0]}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("personalityTest")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {msdt ? (
                        <AssessmentMsdt
                          can={can}
                          data={msdt}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("leadershipTest")} />
                      )}
                    </td>
                    <td>
                      {msdt2 ? (
                        <AssessmentMsdt
                          can={can}
                          data={msdt2}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("leadershipTest")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {spm ? (
                        <AssessmentSpm
                          can={can}
                          data={spm}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("cognitiveAbilityScore")} />
                      )}
                    </td>
                    <td>
                      {spm2 ? (
                        <AssessmentSpm
                          can={can}
                          data={spm2}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("cognitiveAbilityScore")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {businessInsight ? (
                        <AssessmentBusinessInsight
                          can={can}
                          data={businessInsight}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("businessInsight")} />
                      )}
                    </td>
                    <td>
                      {businessInsight2 ? (
                        <AssessmentBusinessInsight
                          can={can}
                          data={businessInsight2}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("businessInsight")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {agility ? (
                        <AssessmentAgility
                          can={can}
                          data={agility}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("agility")} />
                      )}
                    </td>
                    <td>
                      {agility2 ? (
                        <AssessmentAgility
                          can={can}
                          data={agility2}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("agility")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {video && video[0] ? (
                        <AssessmentVideoCompare
                          can={can}
                          vid={video[0]}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="left"
                        />
                      ) : (
                        <NoAssessment ases="Video Assessment" />
                      )}
                    </td>
                    <td>
                      {video2 && video2[0] ? (
                        <AssessmentVideoCompare
                          can={can}
                          vid={video2[0]}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="right"
                        />
                      ) : (
                        <NoAssessment ases="Video Assessment" />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {gesture &&
                      gesture[0] &&
                      gesture[0]["data"] &&
                      gesture[0]["data"][0] &&
                      gesture[0]["data"][0]["resultData"] ? (
                        <AssessmentGestureCompare
                          can={can}
                          vid={gesture[0]}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="left"
                        />
                      ) : (
                        <NoAssessment ases="Gesture" />
                      )}
                    </td>
                    <td>
                      {gesture2 &&
                      gesture2[0] &&
                      gesture2[0]["data"] &&
                      gesture2[0]["data"][0] &&
                      gesture2[0]["data"][0]["resultData"] ? (
                        <AssessmentGestureCompare
                          can={can}
                          vid={gesture2[0]}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="right"
                        />
                      ) : (
                        <NoAssessment ases="Gesture" />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {fisiognomi && fisiognomi[0] ? (
                        <AssessmentFisiognomi
                          can={can}
                          fisiognomi={fisiognomi[0]}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("hasilfisiognomi")} />
                      )}
                    </td>
                    <td>
                      {fisiognomi2 && fisiognomi2[0] ? (
                        <AssessmentFisiognomi
                          can={can}
                          fisiognomi={fisiognomi2[0]}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("hasilfisiognomi")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {palmistry && palmistry[0] ? (
                        <AssessmentPalmistry
                          can={can}
                          palmistry={palmistry[0]}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Palmistry" />
                      )}
                    </td>
                    <td>
                      {palmistry2 && palmistry2[0] ? (
                        <AssessmentPalmistry
                          can={can}
                          palmistry={palmistry2[0]}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Palmistry" />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {fingerprint ? (
                        <AssessmentFingerprint
                          can={can}
                          data={fingerprint}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("fingerprint")} />
                      )}
                    </td>
                    <td>
                      {fingerprint2 ? (
                        <AssessmentFingerprint
                          can={can}
                          data={fingerprint2}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases={t("fingerprint")} />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {assessmentBazi ? (
                        <AssessmentBazi
                          can={can}
                          bazi={assessmentBazi}
                          date={date}
                          candidate="first"
                          id={id}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Bazi" />
                      )}
                    </td>
                    <td>
                      {assessmentBazi2 ? (
                        <AssessmentBazi
                          can={can}
                          bazi={assessmentBazi2}
                          date={date2}
                          candidate="sec"
                          id={id2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Bazi" />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {shio ? (
                        <AssessmentShio
                          can={can}
                          shio={shio}
                          candidate="first"
                          id={id}
                          date={date}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Shio" />
                      )}
                    </td>
                    <td>
                      {shio2 ? (
                        <AssessmentShio
                          can={can}
                          shio={shio2}
                          candidate="sec"
                          id={id2}
                          date={date2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Shio" />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {zodiac ? (
                        <AssessmentZodiac
                          can={can}
                          zodiac={zodiac}
                          candidate="first"
                          id={id}
                          date={date}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Zodiac" />
                      )}
                    </td>
                    <td>
                      {zodiac2 ? (
                        <AssessmentZodiac
                          can={can}
                          zodiac={zodiac2}
                          candidate="sec"
                          id={id2}
                          date={date2}
                          getAPI={getAPI}
                          dataToken={dataToken}
                          position="comparison"
                        />
                      ) : (
                        <NoAssessment ases="Zodiac" />
                      )}
                    </td>
                  </tr>
                </>
              );
            })()
          )}
        </tbody>
      </Table>
    </>
  );
};
const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps)(
  withRouter(translate(CompareAssessment))
);
