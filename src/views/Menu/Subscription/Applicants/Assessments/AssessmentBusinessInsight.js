import React, { useState } from "react";
import {
  // Button,
  Card,
  // Spinner,
  CardBody,
  Row,
  Col,
  // Modal,
  // ModalBody,
  // ModalHeader,
  Progress,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  Collapse,
  UncontrolledTooltip,
} from "reactstrap";
import StarRatingComponent from "react-star-rating-component";
import { translate, t } from "react-switch-lang";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
// import NoToken from "./NoToken";
function AssessmentBusinessInsight({
  data,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position,
}) {
  const [forbidden, setForbidden] = useState(false);
  // const [loadingBuy, setLoadingBuy] = useState(false);
  const [collapse, setCollapse] = useState(-1);
  const toggleResult = (value) => {
    setCollapse(collapse === Number(value) ? null : Number(value));
  };
  // const [modalNoToken, setModalNoToken] = useState(false);
  // const toggleNoToken = () => {
  //   if (can("canManagementToken")) {
  //     setModalNoToken(!modalNoToken);
  //   } else {
  //     setForbidden(true);
  //   }
  // };

  const dataResult = [
    {
      id: 1,
      title: "Autonomy",
      text1: data ? data["autonomy"] : null,
      text2: null,
      score: data?.score["autonomy"] ? (data?.score["autonomy"] / 63) * 100 : 0,
      desc: t("autonomy"),
      level: data?.level["autonomy"] ? t(data?.level["autonomy"]) : null,
      statusLevel: data?.level["autonomy"]
        ? (data?.level["autonomy"]).replace(/\s/g, "")
        : null,
      star: data?.star["autonomy"] ? data?.star["autonomy"] : null,
    },
    {
      id: 2,
      title: "Innovativeness",
      text1: data ? data["innovativeness"] : null,
      text2: null,
      score: data?.score["innovativeness"]
        ? (data?.score["innovativeness"] / 63) * 100
        : 0,
      desc: t("innovativness"),
      level: data?.level["innovativeness"]
        ? t(data?.level["innovativeness"])
        : null,
      statusLevel: data?.level["innovativeness"]
        ? (data?.level["innovativeness"]).replace(/\s/g, "")
        : null,
      star: data?.star["innovativeness"] ? data?.star["innovativeness"] : null,
    },
    {
      id: 3,
      title: "Risk Taking",
      text1: data ? data["risk_taking"] : null,
      text2: null,
      score: data?.score["risk_taking"]
        ? (data?.score["risk_taking"] / 70) * 100
        : 0,
      desc: t("risk"),
      level: data?.level["risk_taking"] ? t(data?.level["risk_taking"]) : null,
      statusLevel: data?.level["risk_taking"]
        ? (data?.level["risk_taking"]).replace(/\s/g, "")
        : null,
      star: data?.star["risk_taking"] ? data?.star["risk_taking"] : null,
    },
    {
      id: 4,
      title: "Proactiveness",
      text1: data ? data["proactiveness"] : null,
      text2: null,
      score: data?.score["proactiveness"]
        ? (data?.score["proactiveness"] / 56) * 100
        : 0,
      desc: t("proactiveness"),
      level: data?.level["proactiveness"]
        ? t(data?.level["proactiveness"])
        : null,
      statusLevel: data?.level["proactiveness"]
        ? (data?.level["proactiveness"]).replace(/\s/g, "")
        : null,
      star: data?.star["proactiveness"] ? data?.star["proactiveness"] : null,
    },
    {
      id: 5,
      title: "Competitive Aggresiveness",
      text1: data ? data["competitive_agresiveness"] : null,
      text2: null,
      score: data?.score["competitive_agresiveness"]
        ? (data?.score["competitive_agresiveness"] / 56) * 100
        : 0,
      desc: t("competitive"),
      level: data?.level["competitive_agresiveness"]
        ? t(data?.level["competitive_agresiveness"])
        : null,
      statusLevel: data?.level["competitive_agresiveness"]
        ? (data?.level["competitive_agresiveness"]).replace(/\s/g, "")
        : null,
      star: data?.star["competitive_agresiveness"]
        ? data?.star["competitive_agresiveness"]
        : null,
    },
    {
      id: 6,
      title: t("conclusion"),
      text1: data
        ? data["conclusion"]
          ? data["conclusion"]["high"]
          : null
        : null,
      text2: data
        ? data["conclusion"]
          ? data["conclusion"]["low"]
          : null
        : null,
      desc: t("conclusion"),
    },
    {
      id: 7,
      title: t("suggestion"),
      text1: data
        ? data["suggestion"]
          ? data["suggestion"]["high"]
          : null
        : null,
      text2: data
        ? data["suggestion"]
          ? data["suggestion"]["low"]
          : null
        : null,
      desc: t("suggestion"),
    },
  ];

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex flex-column">
              <h5 className="text-uppercase content-sub-title mb-4">
                <strong>{t("businessInsight")}</strong>
              </h5>
              <div className="pdf-business-insight">
                {dataResult.map((data, i) =>
                  i < 5 ? (
                    <ProgressGroup type={data.title} value={data.score} />
                  ) : null
                )}
              </div>
              <ListGroup className="my-2">
                <div className="fingerprint">
                  {dataResult.map((dataRes, i) => (
                    <div key={i}>
                      <ListGroupItem
                        className="outline-netis-primary mb-1"
                        style={{ borderRadius: 12 }}
                      >
                        <ListGroupItemHeading
                          className="d-flex justify-content-between mb-0"
                          id={dataRes.title + i}
                          onClick={() => {
                            toggleResult(i);
                          }}
                          style={{ cursor: "pointer" }}
                        >
                          <p>{dataRes.title}</p>
                          <p>
                            {i < 5 ? (
                              <>
                                <i
                                  id={"UncontrolledTooltip" + i}
                                  className="fa fa-question-circle mr-2"
                                  style={{
                                    position: "relative",
                                    top: "1px",
                                    color: "#1472BA",
                                    fontSize: "15px",
                                  }}
                                />
                                <UncontrolledTooltip
                                  target={"UncontrolledTooltip" + i}
                                >
                                  {dataRes.desc}
                                </UncontrolledTooltip>
                              </>
                            ) : null}
                            {collapse === i ? (
                              <i className="mt1 fa-sm fa fa-chevron-up" />
                            ) : (
                              <i className="mt1 fa-sm fa fa-chevron-down" />
                            )}
                          </p>
                        </ListGroupItemHeading>
                      </ListGroupItem>

                      <Collapse
                        isOpen={collapse === i}
                        className="bg-white border mb-2"
                        style={{ borderRadius: 12 }}
                      >
                        <Row>
                          <Col
                            md={position === "comparison" ? "12" : "12"}
                            lg={position === "comparison" ? "12" : "12"}
                          >
                            <div className="p-3">
                              <div
                                style={{
                                  position: "relative",
                                  top: "12px",
                                  textAlign: "center",
                                }}
                                className="ml-2"
                              >
                                <p
                                  style={{ fontSize: "30px", marginBottom: 0 }}
                                >
                                  {
                                    i < 5 ? (
                                      <StarRatingComponent
                                        starCount={5}
                                        value={dataRes.star}
                                      />
                                    ) : null}
                                </p>
                              </div>
                              <div className="text-center">
                                <div
                                  className={`badge badge-${dataRes.statusLevel}`}
                                  style={{ color: "black", fontSize: "14px" }}
                                >
                                  {dataRes.level}
                                </div>
                              </div>{" "}
                              <br /> {dataRes.text1}
                              <br />
                              <br />
                              {dataRes.text2}
                            </div>
                          </Col>
                        </Row>
                      </Collapse>
                      {i === 4 ? <hr /> : null}
                    </div>
                  ))}
                </div>
              </ListGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
}

function ProgressGroup({ type, value }) {
  return (
    <Row className="progress-group">
      <Col xs="12" sm="12">
        <span className={"progress-group-text font-weight-bold"}>{type}</span>
      </Col>
      <Col col="12" className="progress-group-bars">
        {Math.floor(value / 20) >= 5 ? (
          <Progress barClassName="bar-success" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        ) : Math.floor(value / 20) === 4 ? (
          <Progress
            barClassName="bar-most-success"
            value={Math.floor(value)}
            className="black-text"
          >
            {Math.floor(value)} %
          </Progress>
        ) : Math.floor(value / 20) === 3 ? (
          <Progress barClassName="bar-normal" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        ) : Math.floor(value / 20) === 2 ? (
          <Progress barClassName="bar-warning" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        ) : (
          <Progress color="danger" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        )}
      </Col>
    </Row>
  );
}
export default translate(AssessmentBusinessInsight);
