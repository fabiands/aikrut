import React, { useState, useMemo } from 'react';
import { Row, Col, Card, CardBody, Button, Modal, ModalBody, ModalHeader, Spinner } from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { t, translate } from "react-switch-lang";
import NoToken from "./NoToken";
import 'react-circular-progressbar/dist/styles.css';
import ModalPrivilegeForbidden from '../../../../../components/ModalPrivilegeForbidden';

function AssessmentPalmistry({ palmistry, candidate, id, getAPI, dataToken, can, position = 'detailApplicant' }) {
    const data = palmistry?.answers?.data;
    const [modalPalmistry, setModalPalmistry] = useState(false);
    const [loadingBuy, setLoadingBuy] = useState(false);
    const [forbidden, setForbidden] = useState(false)

    const togglePalmistry = () => setModalPalmistry(!modalPalmistry);

    const karakterList = useMemo(() => {
        const arrKarakter = Object.values(data.result);
        return shuffleArray(arrKarakter);
    }, [data.result]);

    const [modalNoToken, setModalNoToken] = useState(false);
    const toggleNoToken = () => {
        if (can('canManagementToken')) {
            if(palmistry?.purchased) {
                setModalPalmistry(!modalPalmistry)
            }
            else {
                setModalNoToken(!modalNoToken)
            }
        }
        else {
            setForbidden(true)
        }
    }

    return (
        <>
            {forbidden && <ModalPrivilegeForbidden isOpen={true} forbidden="canManagementToken" isClose={() => setForbidden(false)} />}
            <NoToken candidate={candidate} buy="palmistry" nullData="assessment" isOpen={modalNoToken} toggle={toggleNoToken} isPurchase={!dataToken.balance || dataToken.isExpired ? false : true} idAssessment={id} getAPI={getAPI} isDone={(e) => setLoadingBuy(e)} type={palmistry?.tokenType} />
            <Card>
                <CardBody>
                    <Row className="md-company-header mb-3 mt-2">
                        <Col xs="6">
                            <h5 className="text-uppercase content-sub-title mb-0">
                                <strong>{t("palmistryResult")}</strong>
                            </h5>
                        </Col>
                        {position !== 'detailApplicant' &&
                        <Col xs="6">
                            <div className="d-flex flex-row-reverse">
                                <Button
                                    disabled={loadingBuy}
                                    className={`btn ${palmistry?.purchased ? 'button-video' : 'btn-netis-color'}`}
                                    onClick={toggleNoToken}
                                    style={{ borderRadius: "8px" }}
                                >
                                    {palmistry?.purchased ?
                                        t('seeDetail')
                                        : loadingBuy ? <><Spinner color="light" size="sm" />&nbsp;&nbsp;Loading... </>
                                            : <><i className="fa fa-lock mr-1" /> {t("openDetail")}</>
                                    }
                                </Button>
                                {!palmistry?.purchased &&
                                    <div style={{ marginTop: "4px" }} className="mr-2">
                                        <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#e0bc47" }} /><b>{palmistry.tokenPrice}</b>
                                    </div>
                                }
                            </div>
                        </Col>
                        }
                    </Row>
                    <Row>
                        {
                            position !== 'detailApplicant' ?
                                <Col xs="12">
                                    {Object.values(data.result).length > 0 ? (
                                        <>
                                            <div className="mb-3">
                                                {t("palmistryDataType")} : <br />
                                                <br />
                                                {Object.keys(data?.result ?? {}).map((data, idx) => (
                                                    <ul key={idx}>
                                                        <li>{data}</li>
                                                    </ul>
                                                ))}
                                            </div>
                                        </>
                                    ) : (
                                        <div className="alert alert-dark">
                                            {t("Belum ada hasil Palmistry yang tersedia")}
                                        </div>
                                    )}
                                </Col>
                                :
                                <>
                                    <Col md="6" lg="6" className="mb-2">
                                        <img
                                            src={data?.processed ?? data?.raw}
                                            width="100%"
                                            alt="palmistry"
                                        />
                                    </Col>
                                    <Col md="6" lg="6">
                                        {Object.values(data.result).length > 0 ? (
                                            <>
                                                <div className="mb-3">
                                                    {t("palmistryDataType")} : <br />
                                                    <br />
                                                    {Object.keys(data?.result ?? {}).map((data, idx) => (
                                                        <ul key={idx}>
                                                            <li>{data}</li>
                                                        </ul>
                                                    ))}
                                                </div>
                                                <div className="d-flex flex-row-reverse">
                                                    <Button
                                                        disabled={loadingBuy}
                                                        className={`btn ${palmistry?.purchased ? 'button-video' : 'btn-netis-color'}`}
                                                        onClick={toggleNoToken}
                                                        style={{ borderRadius: "8px" }}
                                                    >
                                                        {palmistry?.purchased ?
                                                            t('seeDetail')
                                                            : loadingBuy ? <><Spinner color="light" size="sm" />&nbsp;&nbsp;Loading... </>
                                                                : <><i className="fa fa-lock mr-1" /> {t("openDetail")}</>
                                                        }
                                                    </Button>
                                                    {!palmistry?.purchased &&
                                                        <div style={{ marginTop: "4px" }} className="mr-2">
                                                            <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#e0bc47" }} /><b>{palmistry.tokenPrice}</b>
                                                        </div>
                                                    }
                                                </div>
                                            </>
                                        ) : (
                                            <div className="alert alert-dark">
                                                {t('noPalmistryResult')}
                                            </div>
                                        )}
                                    </Col>
                                </>
                        }
                    </Row>
                </CardBody>

                <Modal isOpen={modalPalmistry} toggle={togglePalmistry} className="modal-lg">
                    <ModalHeader toggle={togglePalmistry}>{t("palmistryDetail")}</ModalHeader>
                    <ModalBody>
                        {
                            position === 'detailApplicant' ?
                                null
                                :
                                <div className="d-flex justify-content-center align-items-center mb-3">
                                    <img
                                        src={data?.processed ?? data?.raw}
                                        width="40%"
                                        alt="palmistry"
                                    />
                                </div>

                        }
                        {t("Bentuk Wajah")} : <br />
                        <br />
                        {Object.keys(data?.result ?? {}).map((data, idx) => (
                            <ul key={idx}>
                                <li>{data}</li>
                            </ul>
                        ))}
                        {t("karakteristik")} : <br />
                        <br />
                        {karakterList.map((ciriDetail, idx) => (
                            <ul key={idx}>
                                <li>{ciriDetail}</li>
                            </ul>
                        ))}
                    </ModalBody>
                </Modal>
            </Card>
        </>
    )
}

function shuffleArray(array) {
    var currentIndex = array.length,
        temporaryValue,
        randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

export default translate(AssessmentPalmistry);