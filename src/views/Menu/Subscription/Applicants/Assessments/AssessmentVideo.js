import React, { useState, useRef } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ListGroup,
  ListGroupItem,
  Table,
  Tooltip,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
import moment from "moment";

const AssessmentVideo = ({
  vid,
  id,
  getAPI,
  dataToken,
  can,
  position = "detailApplicant",
}) => {
  const [firstData, ...restData] = vid?.answers?.data;
  const vidRef = useRef(null);
  const vidRef2 = useRef(null);
  const [showButton, setShowButton] = useState(true);
  const [showButton2, setShowButton2] = useState(true);
  const [modalNoToken, setModalNoToken] = useState(false);
  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };
  const firstVideo = React.useMemo(
    () =>
      vid.answers.data[0] ? { resultUrl: vid.answers.data[0]?.raw } : null,
    [vid]
  );
  const firstWidya = React.useMemo(() => vid.answers.data[0].video, [vid]);
  const firstTitle = React.useMemo(() => vid.answers.data[0].title, [vid]);
  const [activeVideo, setActiveVideo] = useState(firstVideo);
  const [activeWidya, setActiveWidya] = useState(firstWidya);
  const [activeTitle, setActiveTitle] = useState(t(firstTitle));
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [hint, setHint] = useState(false);
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const [modalEyecues, setModalEyecues] = useState(false);
  const [forbidden, setForbidden] = useState(false);

  const toogle = () => {
    setModalEyecues(!modalEyecues);
  };
  const toggleTooltip = () => setTooltipOpen(!tooltipOpen);

  function changeVideo(video) {
    setActiveWidya(video.video);
    setActiveTitle(t(video.title));
    setShowButton(true);
    setShowButton2(true);
    setActiveVideo({ resultUrl: video.raw });
  }

  function changeAnotation(video) {
    setActiveWidya(video.video);
    setActiveTitle(t(video.title));
    setShowButton(true);
    setShowButton2(true);
    setActiveVideo(video.processed);
  }

  let highestEmotion = null;
  const resultEmotion = Object.keys(activeVideo?.resultData?.emotion ?? {}).map(
    (label) => ({ label, value: activeVideo.resultData.emotion[label] })
  );
  for (const emotion of resultEmotion) {
    if (highestEmotion == null) {
      highestEmotion = emotion;
    } else if (parseFloat(emotion.value) > parseFloat(highestEmotion.value)) {
      highestEmotion = emotion;
    }
  }

  let highestEyecues = null;
  const resultEyecues =
    activeVideo?.resultData?.eyecues?.map((item) => ({
      label: item.class,
      value: item.score,
      desc: item.description,
    })) ?? [];

  if (resultEyecues) {
    for (const eyecues of resultEyecues) {
      if (highestEyecues == null) {
        highestEyecues = eyecues;
      } else if (eyecues.value > highestEyecues.value) {
        highestEyecues = eyecues;
      }
    }
  }

  const playVid = () => {
    vidRef2.current.pause();
    vidRef.current.play();
    setShowButton(false);
  };

  const playVid2 = () => {
    vidRef.current.pause();
    vidRef2.current.play();
    setShowButton2(false);
  };

  const pauseVid2 = () => {
    vidRef2.current.pause();
    setShowButton2(true);
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        buy="video"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={vid?.tokenType}
      />

      <Card>
        <CardBody>
          <Row className="md-company-header flex-column mb-3 mt-2">
            <Col className="d-flex justify-content-between align-items-center">
              <div>
                <h5 className="text-uppercase content-sub-title mb-0">
                  <strong>
                    {t("assessmentVideo2")} {vid.testName}{" "}
                    {t("assessmentVideo")}
                  </strong>
                </h5>
                <i>{moment(vid.created_at).format("DD MMMM YYYY")}</i>
              </div>
            </Col>
          </Row>

          <div className="row">
            {position === "detailApplicant" && (
              <div className="col-lg-5 mb-5">
                {activeVideo && (
                  <>
                    <div className="vidwrapper">
                      <video
                        ref={vidRef}
                        className="vid"
                        src={activeVideo.resultUrl}
                        id="dataVid"
                        controls
                        onPlaying={() => setShowButton(false)}
                        onPause={() => setShowButton(true)}
                      />
                      {showButton ? (
                        <>
                          <p className="title text-capitalize">
                            &nbsp;&nbsp;&nbsp;{activeTitle}&nbsp;&nbsp;&nbsp;
                          </p>
                          <Button className="play" onClick={playVid}>
                            <i className="fa fa-2x fa-align-center fa-play"></i>
                          </Button>
                          <div className="vidwrapperWidya">
                            <video
                              ref={vidRef2}
                              className="smallVid"
                              src={activeWidya}
                              onPlaying={() => setShowButton2(false)}
                              onPause={() => setShowButton2(true)}
                            />
                            {showButton2 ? (
                              <>
                                {t("Pertanyaan")} :&nbsp;
                                <Button className="play2" onClick={playVid2}>
                                  <i className="fa fa-sm fa-align-center fa-play"></i>
                                </Button>
                              </>
                            ) : (
                              <Button className="pause" onClick={pauseVid2}>
                                <i className="fa fa-sm fa-align-center fa-pause"></i>
                              </Button>
                            )}
                          </div>
                        </>
                      ) : null}
                    </div>

                    {(highestEmotion || highestEyecues) && (
                      <>
                        {highestEmotion && (
                          <ListGroup>
                            <ListGroupItem className="anotation-list py-2">
                              <Row
                                style={{ marginLeft: "-6px" }}
                                className="pr-lg-2"
                              >
                                <Col
                                  xs="8"
                                  md="9"
                                  className="anotation-name py-2"
                                >
                                  {highestEmotion.label}
                                </Col>
                                <Col
                                  xs="4"
                                  md="3"
                                  className="anotation-value text-center py-2"
                                >
                                  {highestEmotion.value}
                                </Col>
                              </Row>
                            </ListGroupItem>
                          </ListGroup>
                        )}
                        {highestEyecues && (
                          <ListGroup>
                            <ListGroupItem className="anotation-list my-2">
                              <Row
                                style={{ marginLeft: "-6px" }}
                                className="pr-lg-2"
                              >
                                <Col
                                  xs="8"
                                  md="9"
                                  className="anotation-name py-2"
                                >
                                  {highestEyecues.label}
                                </Col>
                                <Col
                                  xs="4"
                                  md="3"
                                  className="anotation-value text-center py-2"
                                >
                                  {highestEyecues.value}
                                </Col>
                              </Row>
                            </ListGroupItem>
                          </ListGroup>
                        )}
                        {highestEyecues && (
                          <div className="text-center mt-3">
                            <Button className="button-video" onClick={toogle}>
                              {t("seeDetail")}
                            </Button>
                          </div>
                        )}
                        <Modal isOpen={modalEyecues} toggle={toogle}>
                          <ModalHeader
                            toggle={toogle}
                            className="border-bottom-0"
                          >
                            {t("deskripsi")} Eyecues
                          </ModalHeader>
                          <ModalBody>
                            <div>
                              <h4 className="text-capitalize">
                                {highestEyecues.label.split("-").join(" ")}
                              </h4>
                              <span>{highestEyecues.desc}</span>
                            </div>
                          </ModalBody>
                        </Modal>
                      </>
                    )}
                  </>
                )}
              </div>
            )}
            <div
              className={`col-lg-7 ${
                position !== "detailAppliacnt" ? "w-100" : ""
              }`}
            >
              {vid.purchased ? null : (
                <div className="lock-token text-center">
                  <i className="fa fa-lock lock-icon" aria-hidden="true" />
                  <br />
                  <div>
                    <span style={{ fontSize: "16pt" }}>
                      <b>{t("openNextVideo")}</b>
                    </span>
                    <br />
                    <span>{t("clickOpenDetail")}</span>
                    <br />
                    {vid.tokenPriceSecond !== 0 ? (
                      <div style={{ position: "relative" }}>
                        <div
                          style={{
                            position: "absolute",
                            top: "50%",
                            width: 45,
                            height: 2,
                            background: "#ff0200",
                            left: "calc(50% - 22px)",
                            transform: "rotate(-15deg)",
                          }}
                        ></div>
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#676767" }}
                        />
                        <b>{vid.tokenPriceSecond}</b>
                        <br />
                      </div>
                    ) : (
                      ""
                    )}
                    <FontAwesomeIcon
                      icon="coins"
                      className="mr-1"
                      style={{ color: "#e0bc47" }}
                    />
                    <b>{vid.tokenPrice}</b>
                    <br />
                    <Button
                      disabled={loadingBuy}
                      className="btn btn-netis-color mb-2 mr-2"
                      onClick={toggleNoToken}
                      style={{ borderRadius: "8px" }}
                    >
                      {loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </div>
                </div>
              )}
              <Table
                responsive
                hover
                className="border border-secondary border-bottom"
              >
                <thead>
                  <tr>
                    <th className="w-10"></th>
                    <th className="text-center w-50">{t("videoName")}</th>
                    <th className="text-center">
                      {t("videoType")}
                      <Button
                        onClick={() => setHint(!hint)}
                        className="text-nowrap"
                        style={{
                          backgroundColor: "transparent",
                          border: "transparent",
                        }}
                        id="TooltipExample"
                      >
                        <i className="fa fa-lg fa-question-circle text-primary" />
                      </Button>
                      <Tooltip
                        placement="bottom"
                        isOpen={tooltipOpen}
                        target="TooltipExample"
                        toggle={toggleTooltip}
                      >
                        {t("withAnotationButtonNotShow")}
                      </Tooltip>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr
                    className={`${
                      activeTitle === firstData.title ? `video-border` : ``
                    }`}
                  >
                    <td>
                      {firstData?.processed !== null ? (
                        <i className="fa fa-check-circle-o text-success ml-1" />
                      ) : (
                        <i className="fa fa-spinner text-info ml-1" />
                      )}
                    </td>
                    <td>1. {t(firstData.title)}</td>
                    <td className="text-nowrap">
                      <Button
                        className={`text-center text-nowrap button-video mr-2 ${
                          activeVideo.resultUrl === firstData.raw
                            ? `button-video-active`
                            : ``
                        }`}
                        onClick={() => changeVideo(firstData)}
                      >
                        {t("withOutAnotation")}
                      </Button>
                      {firstData.processed !== null ? (
                        <Button
                          className={`text-center text-nowrap button-asesmen ml-2 ${
                            activeVideo.resultUrl ===
                            firstData.processed.resultUrl
                              ? `button-asesmen-active`
                              : ``
                          }`}
                          onClick={() => changeAnotation(firstData)}
                          // style={{width:125, height: 38}}
                        >
                          {t("withAnotation")}
                        </Button>
                      ) : (
                        <small className="text-danger">{t("analyzing")}</small>
                      )}
                    </td>
                  </tr>
                  {restData &&
                    restData.map((video, idx) => (
                      <tr
                        key={idx + 1}
                        className={`${
                          activeTitle === video.title ? `video-border` : ``
                        }`}
                      >
                        <td>
                          {vid.purchased ? (
                            video.processed !== null ? (
                              <>
                                <i
                                  className="fa fa-check-circle-o text-success ml-1"
                                  id="process"
                                />
                              </>
                            ) : (
                              <i className="fa fa-spinner text-info ml-1" />
                            )
                          ) : (
                            <i className="fa fa-video-camera ml-1" />
                          )}
                        </td>
                        <td>
                          {idx + 2}. {t(video.title)}
                        </td>
                        <td className="text-nowrap">
                          <Button
                            className={`text-center text-nowrap button-video mr-2 ${
                              activeVideo.resultUrl === video.raw
                                ? `button-video-active`
                                : ``
                            }`}
                            onClick={() => changeVideo(video)}
                            disabled={!vid.purchased}
                          >
                            {t("withOutAnotation")}
                          </Button>
                          {video.processed !== null ? (
                            <Button
                              className={`text-center text-nowrap button-asesmen ml-2 ${
                                activeVideo.resultUrl ===
                                video.processed.resultUrl
                                  ? `button-asesmen-active`
                                  : ``
                              }`}
                              onClick={() => changeAnotation(video)}
                              disabled={!vid.purchased}
                            >
                              {t("withAnotation")}
                            </Button>
                          ) : (
                            <small className="text-danger">
                              {t("analyzing")}
                            </small>
                          )}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </div>
          </div>
        </CardBody>
      </Card>
    </>
  );
};

export default translate(AssessmentVideo);
