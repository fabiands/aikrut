import React from "react";
import { Card, CardBody, Col, Row } from "reactstrap";
import { t, translate } from "react-switch-lang";
function NoAssessment({ ases }) {
  return (
    <Card>
      <CardBody>
        <Row>
          <Col xs="12">
            <h5 className="text-uppercase content-sub-title mt-2 mb-4">
              <strong>{ases ?? "Hasil Asesmen"}</strong>
            </h5>
          </Col>
          <Col xs="12">
            <h6>{t("noAsesmen")}</h6>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
}

export default translate(NoAssessment);
