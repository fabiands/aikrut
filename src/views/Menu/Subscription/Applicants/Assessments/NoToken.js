import React, { useEffect, useState, useMemo } from "react";
import { Modal, ModalBody, ModalHeader, Button, Spinner } from "reactstrap";
import { useUserBalance } from "../../../../../hooks/useUserBalance";
import request from "../../../../../utils/request";
import PurchasedToken from "../../Token/PurchasedToken";
import TopUpToken from "../../Token/TopUpToken";
import { translate, t } from "react-switch-lang";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toast } from "react-toastify";

import { connect } from "react-redux";
function NoToken({
  nullData,
  isOpen,
  toggle,
  isPurchase,
  idAssessment,
  getAPI,
  isDone,
  type,
  candidate,
  buy,
  user,
}) {
  const word = {
    bazi: t("seeBazi"),
    disc: t("seeDisc"),
    fingerprint: t("seeFingerprint"),
    fisiognomi: t("seeFisiognomi"),
    gesture: t("seeGesture"),
    msdt: t("seeMsdt"),
    palmistry: t("seePalmistry"),
    shio: t("seeShio"),
    spm: t("seeSpm"),
    video: t("seeVideo"),
    zodiac: t("seeZodiac"),
    businessInsight: t("seeBusinessInsight"),
    agility: t("seeAgility"),
  };

  const [modalToken, setModalToken] = useState(false);
  const [url, setUrl] = useState(null);
  const [priceList, setPriceList] = useState(null);
  const { data } = useUserBalance();
  const isInvoice = useMemo(() => data?.invoice, [data]);
  const [loadingBuy, setLoadingBuy] = useState(false);

  useEffect(() => {
    if (isOpen) {
      request.get("v1/master/token_prices").then((res) => {
        setPriceList(res.data.data);
      });
    }
  }, [isOpen]);

  const toggleNoToken = () => toggle(!isOpen);
  const toggleBuy = () => setModalToken(!modalToken);
  const hasUrl = (item) => setUrl(item);

  const buyThis = function () {
    setLoadingBuy(true);
    isDone(true);
    let formBuy = new FormData();
    formBuy.append("identity", idAssessment);
    formBuy.append("tokenType", type);
    request
      .post("v1/token/usage", formBuy)
      .then(() => {
        // mutate('v1/token');
        getAPI(idAssessment, candidate).then(() => {
          toast.success(t("exchangeTokenSuccess") + " " + word[buy]);
          toggleNoToken();
          setLoadingBuy(false);
          isDone(false);
        });
      })
      .catch((err) => {
        setLoadingBuy(false);
        isDone(false);
        if (err?.response?.status === 403) {
          toast.error(t("noTokenAlert"));
          return;
        } else {
          toast.error(t("failedToken"));
          return;
        }
      });
  };

  return (
    <>
      {user.personnel.company.paid === "pre" ? (
        isPurchase ? (
          <Modal isOpen={isOpen} style={{ marginTop: "40vh" }}>
            <ModalHeader toggle={toggleNoToken} className="border-bottom-0">
              {t("exchangeToken") + " " + t(word[buy]) + " ?"}
            </ModalHeader>
            <ModalBody className="text-center">
              <FontAwesomeIcon
                icon="coins"
                className="mr-1"
                style={{ color: "#e0bc47" }}
              />
              <span className="text-primary">
                {" "}
                <b>{data.tokenPrice ?? 1}</b>{" "}
              </span>{" "}
              {t("usageTokenReduce")} <br />
              {t("doYouContinue")}
              <br />
              <div
                className="text-center d-flex justify-content-between mx-auto mt-2"
                style={{ width: "50%" }}
              >
                <Button
                  className="button-token"
                  onClick={toggleNoToken}
                  style={{ width: "100px" }}
                >
                  {t("batal")}
                </Button>
                <Button
                  color="netis-color"
                  onClick={buyThis}
                  style={{ width: "100px", borderRadius: "10px" }}
                  disabled={loadingBuy}
                >
                  {loadingBuy ? (
                    <>
                      <Spinner color="light" size="sm" /> Loading...{" "}
                    </>
                  ) : (
                    t("yes")
                  )}
                </Button>
              </div>
            </ModalBody>
          </Modal>
        ) : (
          <>
            {modalToken && (
              <TopUpToken
                data={data}
                hasUrl={hasUrl}
                priceList={priceList}
                isOpen={true}
                isClose={() => setModalToken(false)}
              />
            )}
            {url && (
              <PurchasedToken
                url={url}
                isOpen={true}
                isClose={() => setUrl(null)}
              />
            )}
            <Modal
              isOpen={isOpen}
              toggle={toggleNoToken}
              className="modal-md"
              centered
            >
              <ModalHeader
                toggle={toggleNoToken}
                className="border-bottom-0"
              ></ModalHeader>
              <ModalBody className="pt-0 pb-5">
                <div className="row justify-content-center">
                  <div className="col-12">
                    <div
                      className="text-center"
                      style={{ borderRadius: "5px" }}
                    >
                      <i
                        className="fa fa-4x fa-exclamation-triangle mb-2"
                        style={{ color: "#335877" }}
                      />
                      <h5 className="my-3 font-weight-bold">
                        {nullData === "all"
                          ? t("cannotAccessAssessment")
                          : t("unableAccessAssessment")}
                        <br />
                        <br />
                        {t("pleaseTopUp")}
                      </h5>
                      <div
                        className={`nav-item btn btn-light text-netis-color my-2`}
                        onClick={toggleBuy}
                        style={{ color: "#305574", cursor: "pointer" }}
                      >
                        <i className="fa fa-plus-square mr-1" /> <b>Top Up</b>
                      </div>
                    </div>
                  </div>
                </div>
              </ModalBody>
            </Modal>
          </>
        )
      ) : word[buy] ? (
        <Modal isOpen={isOpen} style={{ marginTop: "40vh" }}>
          <ModalHeader toggle={toggleNoToken} className="border-bottom-0">
            {t("addUsageToken") + " " + t(word[buy]) ?? null + " ?"}
          </ModalHeader>
          <ModalBody className="text-center">
            {isInvoice ? (
              <div
                style={{
                  width: "100%",
                  padding: "15px",
                  backgroundColor: "#c51515",
                  color: "#fff",
                  border: "1px solid #555",
                  borderRadius: "10px",
                  marginBottom: "10px",
                  fontWeight: "bold",
                }}
              >
                {t("areYouSureInvoice")}
              </div>
            ) : null}
            <FontAwesomeIcon
              icon="coins"
              className="mr-1"
              style={{ color: "#e0bc47" }}
            />
            <span className="text-primary">
              {" "}
              <b>{data.tokenPrice ?? 1}</b>{" "}
            </span>{" "}
            {t("usageTokenAdd")} <br />
            {t("doYouContinue")}
            <br />
            <div
              className="text-center d-flex justify-content-between mx-auto mt-2"
              style={{ width: "50%" }}
            >
              <Button
                className="button-token"
                onClick={toggleNoToken}
                style={{ width: "100px" }}
              >
                {t("batal")}
              </Button>
              <Button
                color="netis-color"
                onClick={buyThis}
                style={{ width: "100px", borderRadius: "10px" }}
                disabled={loadingBuy}
              >
                {loadingBuy ? (
                  <>
                    <Spinner color="light" size="sm" /> Loading...{" "}
                  </>
                ) : (
                  t("yes")
                )}
              </Button>
            </div>
          </ModalBody>
        </Modal>
      ) : null}
    </>
  );
}
const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps)(translate(NoToken));
