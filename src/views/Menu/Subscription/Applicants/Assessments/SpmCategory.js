export const category = [
    {name: "Very Superior", range:"129-138"},
    {name: "Superior", range: "120-126"},
    {name: "High Average", range:"113-117"},
    {name: "Average", range: "92-110"},
    {name: "Low Average", range: "80-90"},
    {name: "Borderline Defective", range: "66-79"},
    {name: "Mentally Defective", range: "62-65"}
]