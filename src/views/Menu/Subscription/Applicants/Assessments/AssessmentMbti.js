import React, { useState } from "react";
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Progress,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import moment from "moment";
import NoToken from "./NoToken";
import ReactMarkdown from "react-markdown";
import { connect } from "react-redux";
import { withRouter } from "react-router";
function AssessmentMbti({
  user,
  ases,
  dataToken,
  position = "detailApplicant",
}) {
  const [modalAses, setModalAses] = useState(false);
  const [modalNoToken, setModalNoToken] = useState(false);
  const toggleAses = () => setModalAses(!modalAses);
  const toggleNoToken = () => setModalNoToken(!modalNoToken);

  return (
    <>
      <NoToken
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex justify-content-between align-items-center">
              <h5 className="text-uppercase content-sub-title mb-0">
                <strong>
                  {" "}
                  {t("MBTITest")}{" "}
                  {position === "detailApplicant" ? " - " + ases.result : null}
                </strong>
              </h5>
              <Button
                className="button-video"
                onClick={() =>
                  user.personnel.company.paid === "pre"
                    ? !dataToken.balance || dataToken.isExpired
                      ? toggleNoToken()
                      : toggleAses()
                    : toggleAses()
                }
                style={{ borderRadius: "8px" }}
              >
                {t("seeDetail")}
              </Button>
            </Col>
            {position === "detailApplicant" && (
              <div className="col-12 d-flex mb-3 text-muted">
                <i>{moment(ases.created_at).format("DD MMMM YYYY")}</i>
              </div>
            )}
          </Row>
          <hr />
          {position === "detailApplicant" ? (
            <Chart ases={ases} />
          ) : (
            <h3 className="text-uppercase">{ases.result}</h3>
          )}
        </CardBody>
      </Card>

      <Modal isOpen={modalAses} toggle={toggleAses} className="modal-lg">
        <ModalHeader toggle={toggleAses}>{t("detailasesmen")}</ModalHeader>
        <ModalBody>
          {position === "detailApplicant" ? (
            <Content ases={ases} />
          ) : (
            <>
              <Chart ases={ases} />
              <div className="mt-3"></div>
              <Content ases={ases} />
            </>
          )}
        </ModalBody>
      </Modal>
    </>
  );
}

function Content({ ases }) {
  return (
    <>
      {!ases.result_detail ? (
        t("noToken")
      ) : (
        <>
          <div className="mb-8">
            <h3>{t("karakteristik")}</h3>
            <ReactMarkdown source={ases.result_detail.characterisctics} />
          </div>

          <div className="mb-3 mt-2">
            <h3>{t("fungsikognitif")}</h3>
            <h4 className="h5">
              <i>{t("kemampuanberpikir")}</i>
            </h4>
          </div>

          <div className="row">
            <div className="col-sm-6">
              <div className="border rounded p-3 h-100">
                <h5>{t("fungsidominan")}</h5>
                <i>
                  <ReactMarkdown source={ases.result_detail.dominan.name} />
                </i>
                <ReactMarkdown source={ases.result_detail.dominan.desc} />
              </div>
            </div>

            <div className="col-sm-6">
              <div className="border rounded p-3 h-100">
                <h5>{t("fungsisekunder")}</h5>
                <i>
                  <ReactMarkdown source={ases.result_detail.sekunder.name} />
                </i>
                <ReactMarkdown source={ases.result_detail.sekunder.desc} />
              </div>
            </div>
            <div className="col-sm-12 mt-3">
              <h3>{t("partneralami")}</h3>
              <span className="text-uppercase h5">
                &nbsp;&nbsp;&nbsp;&nbsp;<i>{ases.result_detail.partner1}</i> &{" "}
                <i>{ases.result_detail.partner2}</i>
              </span>
            </div>
            <div className="col-sm-12 mt-3">
              <h3>{t("saranpengembangan")}</h3>
              <ReactMarkdown source={ases.result_detail.suggestion} />
            </div>
          </div>
        </>
      )}
    </>
  );
}

function Chart({ ases }) {
  return (
    <div className="p-3 rounded border pdf-mbti">
      <ProgressGroup
        typeA={"Introvert"}
        valueA={ases.scores.introvert}
        typeB={"Extrovert"}
        valueB={ases.scores.extrovert}
      />
      <ProgressGroup
        typeA={"Sensing"}
        valueA={ases.scores.sensing}
        typeB={"Intuition"}
        valueB={ases.scores.intuition}
      />
      <ProgressGroup
        typeA={"Feeling"}
        valueA={ases.scores.feeling}
        typeB={"Thinking"}
        valueB={ases.scores.thinking}
      />
      <ProgressGroup
        typeA={"Judging"}
        valueA={ases.scores.judging}
        typeB={"Perceiving"}
        valueB={ases.scores.perceiving}
      />
    </div>
  );
}

function ProgressGroup({ typeA, typeB, valueA, valueB }) {
  return (
    <Row className="progress-group">
      <Col xs="6" sm="6">
        <span
          className={
            "progress-group-text" +
            (valueA >= valueB ? " font-weight-bold" : "")
          }
        >
          {typeA}
        </span>
      </Col>
      <Col xs="6" sm="6" className="text-right">
        <span
          className={
            "progress-group-text" +
            (valueA <= valueB ? " font-weight-bold" : "")
          }
        >
          {typeB}
        </span>
      </Col>
      <Col col="12" className="progress-group-bars">
        <Progress multi>
          <Progress bar color="netis-color" value={valueA}>
            {valueA} %
          </Progress>
          <Progress bar color="success" value={valueB}>
            {valueB} %
          </Progress>
        </Progress>
      </Col>
    </Row>
  );
}
const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps)(withRouter(translate(AssessmentMbti)));
