import React, { useState } from "react";
import {
  Button,
  Card,
  // Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";

function AssessmentMsdt({
  data,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position,
}) {
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [forbidden, setForbidden] = useState(false);
  const [showMsdtDescription, setShowMsdtDescription] = useState(false);
   // eslint-disable-next-line
  const [modalNoToken, setModalNoToken] = useState(false);

  const toggleNoToken = () => {
    if (can("canManagementToken")) {
        setShowMsdtDescription(!showMsdtDescription);
    } else {
      setForbidden(true);
    }
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="msdt"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={data?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col md="6" className="d-flex flex-column">
              <h5 className="text-uppercase content-sub-title mb-2">
                <strong>{t("leadershipTest")}</strong>
              </h5>
              { position === "detailApplicant" ? (
                <>
                  <h6>{t("leadershipTestIs")}</h6>
                  {position !== "detailApplicant" && <hr />}
                  <h4>
                    <span className="text-capitalize">
                      {data.description.title}
                    </span>
                  </h4>
                  <div className="mt-4">
                    <p>{data.description.desc}</p>
                    <div>
                      <ul>
                        {data?.description?.list?.map((item, idx) => (
                          <li key={idx}>{item}</li>
                        ))}
                      </ul>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <h4>
                    <span className="text-capitalize">
                      {data.description.title}
                    </span>
                  </h4>
                </>
              )}
            </Col>
            <Col md="6">
              {position !== "detailApplicant" && (
                <div className="float-right">
                  <Button
                    disabled={loadingBuy}
                    className={`btn button-video`}
                    onClick={toggleNoToken}
                    style={{ borderRadius: "8px" }}
                  >
                      {t("seeDetail")}
                    
                  </Button>
                </div>
              )}
              {position === "detailApplicant" && (
                <div className="my-3 text-center msdt-img pdf-msdt">
                  {position === "detailApplicant" && (
                    <img
                      src={require(`./assets/msdt/${data.result}.svg`)}
                      alt="MSDT"
                    />
                  )}
                </div>
              )}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Modal
        isOpen={showMsdtDescription}
        toggle={() => setShowMsdtDescription(false)}
        size="xl"
      >
        <ModalHeader toggle={() => setShowMsdtDescription(false)}>
          {data.description.title}
        </ModalHeader>
        <ModalBody>
          <div className="my-3 text-center msdt-img">
            <img src={require(`./assets/msdt/${data.result}.svg`)} alt="MSDT" />
          </div>
          <div className="mt-4">
            <p>{data.description.desc}</p>
            <div>
              <ul>
                {data?.description?.list?.map((item, idx) => (
                  <li key={idx}>{item}</li>
                ))}
              </ul>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
}

export default translate(AssessmentMsdt);
