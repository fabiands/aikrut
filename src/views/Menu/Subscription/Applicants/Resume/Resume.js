import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Table,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
// import { requestDownload } from "../../../../../utils/request";
import StarRatingComponent from "react-star-rating-component";

const Resume = ({ data: applicant, dataToken }) => {
  const {
    detail: { dataEducation, dataWorkingHistory, dataSkill },
  } = applicant;

  // const [
  //   downloadingProfile,
  //   setdownloadingProfile,
  // ] = useState(false);
  // const [downloadProfileLoading, setdownloadProfileLoading] = useState(false);

  // function downloadProfile() {
  //   if (!setdownloadingProfile) {
  //     return;
  //   }
  //   setdownloadProfileLoading(true);
  //   requestDownload(
  //     `v1/recruitment/applicants/download-profile?email=${applicant.userEmail}`
  //   ).finally(() => {
  //     setdownloadProfileLoading(false);
  //   });
  // }
  return (
    <Card>
      <CardHeader>
        <div className="bd-highlight pull-right">
          {/* {dataToken.balance || !dataToken.isExpired ? (
            <Button
              onClick={downloadProfile}
              color="netis-color"
              disabled={downloadProfileLoading}
            >
              {downloadProfileLoading ? (
                <Fragment>
                  <Spinner size="sm" /> Downloading...
                </Fragment>
              ) : (
                <Fragment>
                  <i className="fa fa-download mr-2"></i> Download PDF
                </Fragment>
              )}
            </Button>
          ) : null} */}
        </div>
      </CardHeader>
      <CardBody>
        <Row className="md-company-header mb-3 mt-2">
          <Col className="d-flex justify-content-between align-items-center">
            <h5 className="content-sub-title mb-0">{t("pendidikanformal")}</h5>
          </Col>
        </Row>
        <Row>
          <Col xs="12" lg="12">
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-center w-10">No.</th>
                  <th className="text-center">{t("jenjangpendidikan")}</th>
                  <th className="text-center">{t("institusi")}</th>
                  <th className="text-center">{t("jurusan")}</th>
                  <th className="text-center">{t("tahunmulai")}</th>
                  <th className="text-center">{t("tahunselesai")}</th>
                </tr>
              </thead>
              <tbody>
                {dataEducation &&
                  dataEducation.map((edu, idx) => (
                    <tr key={edu.id}>
                      <td className="text-center">{idx + 1}</td>
                      <td className="text-center">{t(edu.level)}</td>
                      <td className="text-center">{edu.institution}</td>
                      <td className="text-center">{edu.major}</td>
                      <td className="text-center">{edu.yearStart}</td>
                      <td className="text-center">{edu.yearEnd}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Col>
        </Row>

        <Row className="md-company-header mb-3 mt-5">
          <Col className="d-flex justify-content-between align-items-center">
            <h5 className="content-sub-title mb-0">{t("pengalamankerja")}</h5>
          </Col>
        </Row>
        <Row>
          <Col xs="12" lg="12">
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-center w-10">No.</th>
                  <th className="text-center">{t("institusi")}</th>
                  <th className="text-center">{t("posisi")}</th>
                  <th className="text-center">{t("tahunmulai")}</th>
                  <th className="text-center">{t("tahunselesai")}</th>
                </tr>
              </thead>
              <tbody>
                {dataWorkingHistory &&
                  dataWorkingHistory.map((work, idx) => (
                    <tr key={work.id}>
                      <td className="text-center">{idx + 1}</td>
                      <td className="text-center">{work.institution}</td>
                      <td className="text-center">{work.description}</td>
                      <td className="text-center">{work.yearStart}</td>
                      <td className="text-center">{work.yearEnd}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Col>
        </Row>

        <Row className="md-company-header mb-3 mt-5">
          <Col className="d-flex justify-content-between align-items-center">
            <h5 className="content-sub-title mb-0">{t("keahlian")}</h5>
          </Col>
        </Row>
        <Row>
          <Col xs="12" lg="12">
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-center w-10">No.</th>
                  <th className="text-center">{t("keahlian")}</th>
                  <th className="text-center">Level</th>
                </tr>
              </thead>
              <tbody>
                {dataSkill &&
                  dataSkill.map((skill, idx) => (
                    <tr key={skill.id}>
                      <td className="text-center">{idx + 1}</td>
                      <td className="text-center">{skill.name}</td>
                      <td className="text-center">
                        <StarRatingComponent
                          name="testLevel"
                          starCount={5}
                          value={skill.level}
                        />
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default translate(Resume);
