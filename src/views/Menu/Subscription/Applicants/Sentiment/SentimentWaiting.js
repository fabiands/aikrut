import React from "react";
import { Col, Row } from "reactstrap";
import { translate, t } from "react-switch-lang";
export default translate(function SentimentWaiting() {
  return (
    <Row className="mb-3 mt-5 wait-sentiment">
      <Col className="text-center">
        <img
          src={require("../../../../../assets/img/sentiment/waiting-sentiment.png")}
          alt="wait"
        />
        <br />
        <h5>{t("pleaseWait")}</h5>
      </Col>
    </Row>
  );
});
