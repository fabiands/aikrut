import React from 'react'
import { translate, t } from 'react-switch-lang'
import { Col, Row, Table } from 'reactstrap'
import { Doughnut } from 'react-chartjs-2';

function SentimentCompare({sosmed, sentiment}){
    return(
        <Table borderless>
            <tbody>
                <tr>
                    <td className="border-0" style={{verticalAlign:'top'}}>
                        {sentiment?.data?.facebook ? <SentimentChart name="FACEBOOK" item={sentiment?.data?.facebook} /> : <NoSentimentData name="FACEBOOK" />}
                    </td>
                </tr>
                <tr>
                    <td className="border-0" style={{verticalAlign:'top'}}>
                        {sentiment?.data?.instagram ? <SentimentChart name="INSTAGRAM" item={sentiment?.data?.instagram} /> : <NoSentimentData name="INSTAGRAM" />}
                    </td>
                </tr>
                <tr>
                    <td className="border-0" style={{verticalAlign:'top'}}>
                        {sentiment?.data?.linkedin ? <SentimentChart name="LINKEDIN" item={sentiment?.data?.linkedin} /> : <NoSentimentData name="LINKEDIN" />}
                    </td>
                </tr>
                <tr>
                    <td className="border-0" style={{verticalAlign:'top'}}>
                        {sentiment?.data?.twitter ? <SentimentChart name="TWITTER" item={sentiment?.data?.twitter} /> : <NoSentimentData name="TWITTER" />}
                    </td>
                </tr>
            </tbody>
        </Table>
    )
}

const SentimentChart = ({item, name}) => {
    return(
        <Row className="my-3 d-flex justify-content-around">
            <Col md="12" className={`my-2 py-3`}>
                <div style={{ backgroundColor: "#F9FAFC"}}>
                    <Row className="d-flex align-items-center py-2">
                        <Col xs="12" className="text-center doughnut-sentiment">
                            <div className="doughnut-title">
                                <strong className="text-uppercase">{name}</strong>
                            </div>
                            <Doughnut
                                height={100}
                                data={{
                                    labels: [t('positive'),t('neutral'), t('negative')],
                                    datasets: [{
                                        label: "",
                                        // fill: false,
                                        // lineTension: 0.1,
                                        backgroundColor: ['#16CB60','#168ACB', '#FFB400'],
                                        borderColor: ['#16CB60','#168ACB', '#FFB400'],
                                        data: [Math.round(item?.positif), Math.round(item?.netral), Math.round(item?.negatif)]
                                    }]
                                }}
                                options={{
                                    plugins: {
                                        datalabels: {
                                            display: false,
                                        }
                                    },
                                    cutoutPercentage: 70,
                                    legend: {
                                        display: false
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className="d-flex justify-content-around py-3 px-2">
                        <Col xs="3" className="text-center d-flex flex-column align-items-center sentiment-negative">
                            <h2><b>{Math.round(item?.negatif)}&nbsp;%</b></h2>
                            <small>{t('negative')}</small>
                        </Col>
                        <Col xs="3" className="text-center d-flex flex-column align-items-center sentiment-netral">
                            <h2><b>{Math.round(item?.netral)}&nbsp;%</b></h2>
                            <small>{t('neutral')}</small>
                        </Col>
                        <Col xs="3" className="text-center d-flex flex-column align-items-center sentiment-positive">
                            <h2><b>{Math.round(item?.positif)}&nbsp;%</b></h2>
                            <small>{t('positive')}</small>
                        </Col>
                    </Row>
                </div>
            </Col>
        </Row>
    )
}

const NoSentimentData = ({name}) => {
    return(
        <Row className="my-3 d-flex justify-content-around">
            <Col md="12" className={`my-2 py-3`}>
                <div style={{ backgroundColor: "#F9FAFC"}}>
                    <Row className="d-flex align-items-center py-2">
                        <Col xs="12" className="text-center doughnut-sentiment">
                            <div className="doughnut-title">
                                <strong className="text-uppercase">{name}</strong>
                            </div>
                            <Doughnut
                                height={100}
                                data={{
                                    labels: [''],
                                    datasets: [{
                                        label: "",
                                        // fill: false,
                                        // lineTension: 0.1,
                                        backgroundColor: ['#696969'],
                                        borderColor: ['#696969'],
                                        data: [100]
                                    }]
                                }}
                                options={{
                                    tooltips: {
                                        enabled: false
                                    },
                                    plugins: {
                                        datalabels: {
                                            display: false,
                                        }
                                    },
                                    cutoutPercentage: 70,
                                    legend: {
                                        display: false
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className="d-flex justify-content-around py-3 px-2">
                        <Col xs="5" className="text-center d-flex flex-column align-items-center" style={{borderBottom:'10px solid #696969'}}>
                            <h2><b>0 %</b></h2>
                            <small>{t('noData')}</small>
                        </Col>
                    </Row>
                </div>
            </Col>
        </Row>
    )
}

export default translate(SentimentCompare);
