import React from "react";
import { Row, Col, Card, CardBody } from "reactstrap";
import { translate, t } from "react-switch-lang";
import { Doughnut } from "react-chartjs-2";
import "chartjs-plugin-datalabels";
import SentimentCompare from "./SentimentCompare";
import SentimentWaiting from "./SentimentWaiting";
import SentimentRequest from "./SentimentRequest";

function SentimentAnalysis({
  data,
  match,
  getAPI,
  dataToken,
  guide,
  type,
  candidate,
}) {
  const { dataSentiment: sentiment } = data;
  const sosmed = sentiment && sentiment.data && Object.keys(sentiment?.data);

  return (
    <>
      <Card className="border-0">
        <CardBody className="p-0 p-md-1">
          {!sentiment ? (
            <SentimentRequest
              data={data}
              getAPI={getAPI}
              type={type}
              candidate={candidate}
            />
          ) : !sentiment.status || !sosmed ? (
            <SentimentWaiting />
          ) : type === "applicant" ? (
            <Row className="my-3 d-flex justify-content-around">
              {sosmed?.map((item, idx) => (
                <Col
                  key={idx}
                  md={type === "applicant" ? "5" : "12"}
                  className={`my-2 py-3 ${type === "applicant" && `px-3 mx-2`}`}
                >
                  <div style={{ backgroundColor: "#F9FAFC" }}>
                    <Row className="d-flex align-items-center py-2">
                      <Col xs="12" className="text-center doughnut-sentiment">
                        <div className="doughnut-title">
                          <strong>{item.toUpperCase()}</strong>
                        </div>
                        <Doughnut
                          height={type === "applicant" ? 150 : 100}
                          data={{
                            labels: [
                              t("positive"),
                              t("neutral"),
                              t("negative"),
                            ],
                            datasets: [
                              {
                                label: "",
                                // fill: false,
                                // lineTension: 0.1,
                                backgroundColor: [
                                  "#16CB60",
                                  "#168ACB",
                                  "#FFB400",
                                ],
                                borderColor: ["#16CB60", "#168ACB", "#FFB400"],
                                data: [
                                  Math.round(sentiment.data[item].positif),
                                  Math.round(sentiment.data[item].netral),
                                  Math.round(sentiment.data[item].negatif),
                                ],
                              },
                            ],
                          }}
                          options={{
                            plugins: {
                              datalabels: {
                                display: false,
                              },
                            },
                            cutoutPercentage: 70,
                            legend: {
                              display: false,
                            },
                          }}
                        />
                      </Col>
                    </Row>
                    <Row className="d-flex justify-content-around py-3">
                      <Col
                        xs="3"
                        className="text-center d-flex flex-column align-items-center sentiment-negative"
                      >
                        <h2>
                          <b>
                            {Math.round(sentiment.data[item].negatif)}&nbsp;%
                          </b>
                        </h2>
                        <small>{t("negative")}</small>
                      </Col>
                      <Col
                        xs="3"
                        className="text-center d-flex flex-column align-items-center sentiment-netral"
                      >
                        <h2>
                          <b>
                            {Math.round(sentiment.data[item].netral)}&nbsp;%
                          </b>
                        </h2>
                        <small>{t("neutral")}</small>
                      </Col>
                      <Col
                        xs="3"
                        className="text-center d-flex flex-column align-items-center sentiment-positive"
                      >
                        <h2>
                          <b>
                            {Math.round(sentiment.data[item].positif)}&nbsp;%
                          </b>
                        </h2>
                        <small>{t("positive")}</small>
                      </Col>
                    </Row>
                  </div>
                </Col>
              ))}
            </Row>
          ) : (
            <SentimentCompare sosmed={sosmed} sentiment={sentiment} />
          )}
        </CardBody>
      </Card>
    </>
  );
}

export default translate(SentimentAnalysis);
