import React from "react";
import {
  Document,
  Font,
  Image,
  Page,
  StyleSheet,
  Text,
  Svg,
  Line,
  View,
  Rect,
} from "@react-pdf/renderer";
import logo from "../../../../assets/assets_ari/logo.png";
import BiodataPage from "./AssessmentsPdf/BiodataPage";
import MbtiPage from "./AssessmentsPdf/MbtiPage";
import PapiPage from "./AssessmentsPdf/PapiPage";
import DiscPage from "./AssessmentsPdf/DiscPage";
import MsdtPage from "./AssessmentsPdf/MsdtPage";
import SpmPage from "./AssessmentsPdf/SpmPage";
import BusinessInsightPage from "./AssessmentsPdf/BusinessInsightPage";
import AgilityPage from "./AssessmentsPdf/AgilityPage";
import { memo } from "react";

export default memo(({ data, generatePdf, dataToken }) => {

  return (
    <>
      <Document>
        <Page>
          <Svg height="10" width="100%" fixed>
            <Line
              x1="0"
              y1="0"
              x2="200"
              y2="0"
              strokeWidth={10}
              stroke="rgba(19, 85, 142, 1)"
            />
            <Line
              x1="200"
              y1="0"
              x2="400"
              y2="0"
              strokeWidth={10}
              stroke="rgba(231, 107, 36, 1)"
            />
          </Svg>
          <Image style={styles.logo} src={logo} fixed />
          <Svg height="50" width="100%" style={{ marginTop: "25px" }}>
            <Rect
              x="-20"
              rx="20"
              ry="20"
              width="220"
              height="45"
              fill="rgba(231, 107, 36, 1)"
            />
            <Rect
              x="-20"
              y="5"
              rx="20"
              ry="20"
              width="215"
              height="45"
              fill="rgba(19, 85, 142, 1)"
            />
            <Text x="15" y="32" fill="rgba(255, 255, 255, 1)">
              Asessment Report
            </Text>
          </Svg>
          <View style={styles.body}>
            <BiodataPage data={data} photo={generatePdf.photo} />
            {data.detail.dataAssessment.mbti &&
              data.detail.dataAssessment.mbti.length ? (
              <MbtiPage
                data={data}
                graph={generatePdf.mbti}
                styles={styles}
                dataToken={dataToken}
              />
            ) : null}
            {data.detail.dataAssessment.papikostik &&
              data.detail.dataAssessment.papikostik.length ? (
              <PapiPage
                data={data}
                graph={generatePdf.papikostick}
                styles={styles}
                dataToken={dataToken}
              />
            ) : null}
            {data.detail.dataAssessment.disc[0] ? (
              <DiscPage data={data} graph={generatePdf.disc} styles={styles} />
            ) : null}
            {data.detail.dataAssessment.msdt? (
              <MsdtPage data={data} graph={generatePdf.msdt} styles={styles} />
            ) : null}
            {data.detail.dataAssessment.spm ? (
              <SpmPage data={data} graph={generatePdf.spm} styles={styles} />
            ) : null}
            {data.detail.dataAssessment.businessInsight ? (
              <BusinessInsightPage
                data={data}
                graph={generatePdf.businessInsight}
                styles={styles}
              />
            ) : null}
            {data.detail.dataAssessment.agility ? (
              <AgilityPage
                data={data}
                graph={generatePdf.agility}
                graph2={generatePdf.agility2}
                graph3={generatePdf.agility3}
                styles={styles}
              />
            ) : null}
          </View>
          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) =>
              `${pageNumber} / ${totalPages}`
            }
            fixed
          />
        </Page>
      </Document>
    </>
  );
});

Font.register(
  {
    family: "Oswald",
    src: "https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf",
  },
  {
    family: "Roboto",
    src: "https://fonts.googleapis.com/css2?family=Roboto&display=swap",
  }
);

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 35,
    paddingHorizontal: 35,
  },
  logo: {
    marginVertical: 15,
    marginHorizontal: 25,
    width: 100,
  },
  title: {
    fontSize: 24,
    textTransform: "uppercase",
    fontFamily: "Oswald",
    marginTop: 25,
    marginBottom: 10,
    color: "rgba(19, 85, 142, 1)",
  },
  image: {
    marginBottom: 30,
  },
  subtitle: {
    fontSize: 16,
    margin: 5,
    fontWeight: 500,
    fontFamily: "Oswald",
  },
  text: {
    margin: 2.5,
    lineHeight: 1.5,
    fontSize: 12,
    textAlign: "justify",
    fontFamily: "Times-Roman",
  },
  row: {
    flexDirection: "row",
    fontStyle: "normal",
    fontSize: "18px",
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 15,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
  },
});
