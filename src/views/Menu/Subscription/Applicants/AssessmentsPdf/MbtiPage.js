import React from 'react';
import { Text, View, Image } from '@react-pdf/renderer';
import { t, translate } from "react-switch-lang";

const MbtiPage = ({ data, graph, styles, dataToken }) => {
    return (
        <>
            <Text style={styles.title} break>
                {t("MBTITest")}
            </Text>
            <Image
                style={styles.image}
                src={graph}
            />
            {!dataToken.balance || dataToken.isExpired ?
                null :
                <View>
                    <Text style={styles.subtitle}>
                        {t("karakteristik")}
                    </Text>
                    <Text style={styles.text}>
                        {data.detail.dataAssessment.mbti[0].result_detail.characterisctics}
                    </Text>

                    <Text style={styles.subtitle} break>
                        {t("fungsikognitif")}
                    </Text>
                    <Text style={{ ...styles.text, fontWeight: 'bold' }}>
                        {t("kemampuanberpikir")}
                    </Text>
                    <View style={styles.row}>
                        <Text style={{ ...styles.text, width: '50%' }}>
                            {t("fungsidominan")}
                        </Text>
                        <Text style={{ ...styles.text, width: '50%' }}>
                            {t("fungsisekunder")}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={{ ...styles.text, width: '50%' }}>
                            {data.detail.dataAssessment.mbti[0].result_detail.dominan.name}
                        </Text>
                        <Text style={{ ...styles.text, width: '50%' }}>
                            {data.detail.dataAssessment.mbti[0].result_detail.sekunder.name}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={{ ...styles.text, width: '50%', paddingRight: 5 }}>
                            {data.detail.dataAssessment.mbti[0].result_detail.dominan.desc}
                        </Text>
                        <Text style={{ ...styles.text, width: '50%', paddingLeft: 5 }}>
                            {data.detail.dataAssessment.mbti[0].result_detail.sekunder.desc}
                        </Text>
                    </View>
                    <Text style={styles.subtitle}>
                        {t("partneralami")}
                    </Text>
                    <View style={styles.row}>
                        <Text style={{ ...styles.text, textTransform: 'uppercase' }}>
                            {data.detail.dataAssessment.mbti[0].result_detail.partner1} & {data.detail.dataAssessment.mbti[0].result_detail.partner2}
                        </Text>
                    </View>
                    <Text style={styles.subtitle}>
                        {t("saranpengembangan")}
                    </Text>
                    <View style={styles.row}>
                        <Text style={{ ...styles.text }}>
                            {data.detail.dataAssessment.mbti[0].result_detail.suggestion}
                        </Text>
                    </View>
                </View>
            }
        </>
    )
};

export default translate(MbtiPage)