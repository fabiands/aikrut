import React from 'react';
import { Text, View, StyleSheet, Svg, Image, Line } from '@react-pdf/renderer';
import profilePhotoNotFound from "../../../../../assets/img/no-photo.png";
import { t, translate } from "react-switch-lang";

const styles = StyleSheet.create({
    photoProfile: {
        width: 200,
        height: 200,
        objectFit: 'cover',
        marginBottom: 40
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 24,
        fontStyle: 'normal',
        fontSize: '18px',
        marginVertical: 5
    },
    title: {
        width: '38%',
        textAlign: 'left',
    },
    titikdua: {
        width: '1%',
        textAlign: 'left',
        paddingRight: 8,
    },
    value: {
        width: '50%',
        textAlign: 'left',
        paddingLeft: 8,
    },
});


const BiodataPage = ({ data, photo }) => {

    let age = '0';
    const dob = new Date(data.detail.dateOfBirth);
    if (dob.getTime() !== 0) {
        const now = (new Date()).getTime();
        const diff = now - dob.getTime();
        age = (new Date(diff)).getUTCFullYear() - 1970;
        age += ' tahun';
    }
    else age = '-';

    return (
        <>
            <Image
                style={styles.photoProfile}
                src={data.detail.avatar ? data.detail.avatar : profilePhotoNotFound}
            />
            <Svg height="10" width="100%" style={{ marginBottom: '25px' }}>
                <Line
                    x1="0"
                    y1="0"
                    x2="1000"
                    y2="0"
                    strokeWidth={2}
                    stroke="rgba(19, 85, 142, 1)"
                />
            </Svg>
            <View style={styles.row} >
                <Text style={styles.title}>{t("namalengkap")}</Text>
                <Text style={styles.titikdua}>:</Text>
                <Text style={styles.value}>{data.detail?.fullName}</Text>
            </View>
            <View style={styles.row} >
                <Text style={styles.title}>Email</Text>
                <Text style={styles.titikdua}>:</Text>
                <Text style={styles.value}>{data.detail?.email}</Text>
            </View>
            <View style={styles.row} >
                <Text style={styles.title}>{t("usia")}</Text>
                <Text style={styles.titikdua}>:</Text>
                <Text style={styles.value}>{age}</Text>
            </View>
            <View style={styles.row} >
                <Text style={styles.title}>{t("notelpon")}</Text>
                <Text style={styles.titikdua}>:</Text>
                <Text style={styles.value}>{data.detail?.phone}</Text>
            </View>
            <View style={styles.row} >
                <Text style={styles.title}>{t('Posisi yang dilamar')}</Text>
                <Text style={styles.titikdua}>:</Text>
                <Text style={styles.value}>{data.job_vacancy.name}</Text>
            </View>
            <View style={styles.row} >
                <Text style={styles.title}>{t('jenjangpendidikanterakhir')}</Text>
                <Text style={styles.titikdua}>:</Text>
                <Text style={styles.value}>{data.detail.lastEducation ?? '-'}</Text>
            </View>
        </>
    )
};

export default translate(BiodataPage)