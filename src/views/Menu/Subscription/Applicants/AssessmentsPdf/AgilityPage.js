import React from "react";
import { Text, View, Image } from "@react-pdf/renderer";
import { t, translate } from "react-switch-lang";

const AgilityPage = ({ data, styles, graph, graph2, graph3 }) => {
  const dataResult = [
    {
      id: 1,
      type: "cognitive",
      title: "Cognitive Simulation",
      text1: data.detail.dataAssessment?.agility?.cognitive
        ? data.detail.dataAssessment?.agility?.cognitive.interpretation
        : null,
      text2: null,
      score: data.detail.dataAssessment?.agility?.cognitive?.percent
        ? data.detail.dataAssessment?.agility?.cognitive?.percent
        : null,
      desc: t("cognitiveSimulation"),
      level: data.detail.dataAssessment?.agility?.cognitive?.level
        ? t(data.detail.dataAssessment?.agility?.cognitive?.level)
        : null,
      statusLevel: data.detail.dataAssessment?.agility?.cognitive?.level
        ? (data.detail.dataAssessment?.agility?.cognitive?.level).replace(
            /\s/g,
            ""
          )
        : null,
      star: data.detail.dataAssessment?.agility?.cognitive?.star
        ? data.detail.dataAssessment?.agility?.cognitive?.star
        : null,
    },
    {
      id: 2,
      type: "cognitive",
      title: "Counterfactual Thinking",
      text1:
        data.detail.dataAssessment?.agility?.counterfactual[0]?.interpretation,
      text2:
        data.detail.dataAssessment?.agility?.counterfactual[1]?.interpretation,
      desc: t("counterFactualThinking"),
    },
    {
      id: 3,
      type: "cognitive",
      title: "Pattern Recognition",
      text1: data.detail.dataAssessment?.agility?.pattern
        ? data.detail.dataAssessment?.agility?.pattern?.interpretation
        : null,
      text2: null,
      score: data.detail.dataAssessment?.agility?.pattern?.percent,
      desc: t("patternRecognition"),
      level: data.detail.dataAssessment?.agility?.pattern?.level
        ? t(data.detail.dataAssessment?.agility?.pattern?.level)
        : null,
      statusLevel: data.detail.dataAssessment?.agility?.pattern?.level
        ? (data.detail.dataAssessment?.agility?.pattern?.level).replace(
            /\s/g,
            ""
          )
        : null,
      star: data.detail.dataAssessment?.agility?.pattern?.star,
    },
    {
      id: 4,
      type: "behaviour",
      title: "Experimentation",
      text1: data.detail.dataAssessment?.agility?.experimentation
        ? data.detail.dataAssessment?.agility?.experimentation?.interpretation
        : null,
      text2: null,
      score: data.detail.dataAssessment?.agility?.experimentation?.percent,
      desc: t("experimentation"),
      level: data.detail.dataAssessment?.agility?.experimentation?.level
        ? t(data.detail.dataAssessment?.agility?.experimentation?.level)
        : null,
      statusLevel: data.detail.dataAssessment?.agility?.experimentation?.level
        ? (data.detail.dataAssessment?.agility?.experimentation?.level).replace(
            /\s/g,
            ""
          )
        : null,
      star: data.detail.dataAssessment?.agility?.experimentation?.star,
    },
    {
      id: 5,
      type: "behaviour",
      title: "Feedback Seeking",
      text1: data.detail.dataAssessment?.agility?.feedback
        ? data.detail.dataAssessment?.agility?.feedback?.interpretation
        : null,
      text2: null,
      score: data.detail.dataAssessment?.agility?.feedback?.percent,
      desc: t("feedback"),
      level: data.detail.dataAssessment?.agility?.feedback?.level
        ? t(data.detail.dataAssessment?.agility?.feedback?.level)
        : null,
      statusLevel: data.detail.dataAssessment?.agility?.feedback?.level
        ? (data.detail.dataAssessment?.agility?.feedback?.level).replace(
            /\s/g,
            ""
          )
        : null,
      star: data.detail.dataAssessment?.agility?.feedback?.star,
    },
    {
      id: 6,
      type: "behaviour",
      title: "Reflection",
      text1: data.detail.dataAssessment?.agility?.reflection
        ? data.detail.dataAssessment?.agility?.reflection?.interpretation
        : null,
      text2: null,
      score: data.detail.dataAssessment?.agility?.reflection?.percent,
      desc: t("reflection"),
      level: data.detail.dataAssessment?.agility?.reflection?.level
        ? t(data.detail.dataAssessment?.agility?.reflection?.level)
        : null,
      statusLevel1: data.detail.dataAssessment?.agility?.reflection?.level
        ? (data.detail.dataAssessment?.agility?.reflection?.level).replace(
            /\s/g,
            ""
          )
        : null,
      star: data.detail.dataAssessment?.agility?.reflection?.star,
    },
  ];
  return (
    <>
      <Text style={styles.title} break>
        {t("agility")}
      </Text>
      {/* <View
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Image style={{ ...styles.image, width: "100%" }} src={graph} />
      </View>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Image style={{ ...styles.image, width: "100%" }} src={graph2} />
      </View> */}
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Image style={{ ...styles.image, width: "100%" }} src={graph} />
      </View>
      <View break></View>
      {dataResult.map((data, idx) => {
        return (
          <View style={styles.row} key={idx} break={idx === 2}>
            <View
              style={{
                width: "40%",
                borderWidth: 0.5,
                borderTopLeftRadius: 1,
                borderTopRightRadius: 1,
                borderColor: "rgba(0, 0, 0, 0.125)",
              }}
            >
              <Text
                style={{
                  fontSize: 12.5,
                  margin: 2.5,
                  fontWeight: 800,
                  fontFamily: "Oswald",
                }}
              >
                {data.title}
              </Text>
            </View>
            <View
              style={{
                width: "60%",
                borderWidth: 0.5,
                borderTopLeftRadius: 1,
                borderTopRightRadius: 1,
                borderColor: "rgba(0, 0, 0, 0.125)",
              }}
            >
              <Text style={styles.text}>{data.text1}</Text>
              <br />
              <Text style={styles.text}>{data.text2}</Text>
            </View>
          </View>
        );
      })}
    </>
  );
};

export default translate(AgilityPage);
