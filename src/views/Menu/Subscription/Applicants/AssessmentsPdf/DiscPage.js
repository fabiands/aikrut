import React from 'react';
import { Text, Image, View } from '@react-pdf/renderer';
import { t, translate } from "react-switch-lang";


const DiscPage = translate(({ data, graph, styles }) => {
    return (
        <>
            <Text style={styles.title} break>
                {t('personalityTest')}
            </Text>
            <Image
                style={styles.image}
                src={graph}
            />
            <Text style={styles.subtitle}>
                {t('Uraian Kepribadian')}
            </Text>
            <View style={styles.row}>
                {data.detail.dataAssessment.disc[0].scores &&
                    Object.keys(data.detail.dataAssessment.disc[0].scores).map((category, idx) => {
                        const desc = data.detail.dataAssessment.disc[0].type_description
                            ? data.detail.dataAssessment.disc[0].type_description[category][0]
                            : null;
                        return (
                            <DiscResultDesription
                                desc={desc}
                                styles={styles}
                                key={idx}
                            />
                        )
                    }
                    )}
            </View>
        </>
    )
});


const DiscResultDesription = ({ styles, desc }) => {
    return (
        <View style={{ width: '33.3%' }}>
            <View style={{ borderWidth: .5, borderTopLeftRadius: 1, borderTopRightRadius: 1, borderColor: 'rgba(0, 0, 0, 0.125)' }}>
                <Text style={{ fontSize: 12.5, margin: 2.5, fontWeight: 500, fontFamily: 'Oswald' }}>
                    {t("characteristics")}
                </Text>
                <Text style={{ ...styles.text }}>
                    {desc.karakteristik}
                </Text>
            </View>
            <View style={{ borderWidth: .5, borderColor: 'rgba(0, 0, 0, 0.125)' }} break>
                <Text style={{ fontSize: 12.5, margin: 2.5, fontWeight: 500, fontFamily: 'Oswald' }}>
                    {t("power")}
                </Text>
                <Text style={{ ...styles.text }}>
                    {desc.kekuatan}
                </Text>
            </View>
            <View style={{ borderWidth: .5, borderColor: 'rgba(0, 0, 0, 0.125)' }}>
                <Text style={{ fontSize: 12.5, margin: 2.5, fontWeight: 500, fontFamily: 'Oswald' }}>
                    {t("area")}
                </Text>
                <Text style={{ ...styles.text }}>
                    {desc.area_perkembangan}
                </Text>
            </View>
            <View style={{ borderWidth: .5, borderColor: 'rgba(0, 0, 0, 0.125)' }}>
                <Text style={{ fontSize: 12.5, margin: 2.5, fontWeight: 500, fontFamily: 'Oswald' }}>
                    {t("valueGroup")}
                </Text>
                <Text style={{ ...styles.text }}>
                    {desc.nilai_kelompok}
                </Text>
            </View>
            <View style={{ borderWidth: .5, borderColor: 'rgba(0, 0, 0, 0.125)' }} break>
                <Text style={{ fontSize: 12.5, margin: 2.5, fontWeight: 500, fontFamily: 'Oswald' }}>
                    {t("underPressure")}
                </Text>
                <Text style={{ ...styles.text }}>
                    {desc.kecenderungan_bawah_tekanan}
                </Text>
            </View>
            <View style={{ borderWidth: .5, borderColor: 'rgba(0, 0, 0, 0.125)' }}>
                <Text style={{ fontSize: 12.5, margin: 2.5, fontWeight: 500, fontFamily: 'Oswald' }}>
                    {t("idealEnv")}
                </Text>
                <Text style={{ ...styles.text }}>
                    {desc.lingkungan_ideal}
                </Text>
            </View>
        </View>
    );
}

export default DiscPage