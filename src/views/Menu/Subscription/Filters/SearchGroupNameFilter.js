import React, { memo, useEffect, useState } from "react";
import {
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
} from "reactstrap";
import { useRecruitmentsFiltersCtx } from "../Context/RecruitmentContext";
import { translate, t } from "react-switch-lang";
export const SearchGroupNameFilter = translate(
  memo(({ data }) => {
    const [filters, setFilters] = useRecruitmentsFiltersCtx();
    const [search, setSearch] = useState(filters.searchGroup);

    useEffect(() => {
      setFilters((state) => ({ ...state, searchGroup: search }));
    }, [search, setFilters]);

    return (
      <>
        <Row>
          <Col sm="12">
            <InputGroup className="my-2" style={{ borderRadius: "12px" }}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText className="input-group-transparent">
                  <i className="fa fa-search"></i>
                </InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                placeholder={`${t("search")} ${
                  data ?? t("searchEmployeeName")
                } `}
                className="input-search"
                value={filters.searchGroup}
                onChange={(e) => setSearch(e.target.value)}
              />
            </InputGroup>
          </Col>
        </Row>
      </>
    );
  })
);
