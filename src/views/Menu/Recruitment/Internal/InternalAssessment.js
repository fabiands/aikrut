import React, { useCallback, useMemo, useEffect, useState } from "react";
// import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import DataNotFound from "../../../../components/DataNotFound";
import usePagination from "../../../../hooks/usePagination";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import ModalError from "../../../../components/ModalError";
import { useAuthUser } from "../../../../store";
import request from "../../../../utils/request";
// import disableScroll from "disable-scroll";
// import { getMe } from "../../../../actions/auth";
import { useRecruitmentsFiltersCtx } from "../Context/RecruitmentContext";
import { SearchGroupNameFilter } from "../Filters/SearchGroupNameFilter";
import {
  Button,
  Row,
  Card,
  CardBody,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import { translate, t } from "react-switch-lang";

function InternalAssessment() {
  const [loading, setLoading] = useState(false);
  const [filters, setFilters] = useRecruitmentsFiltersCtx();
  const [grups, setGrups] = useState([]);
  const [error, setError] = useState(false);
  // const [firstTour, setFirstTour] = useState(false);
  // const [isTour, setIsTour] = useState(false);
  const user = useAuthUser();
  // const disableBody = () => disableScroll.on();
  // const enableBody = () => disableScroll.off();
  // const accentColor = "#1d5a8e";
  // const dispatch = useDispatch();

  useEffect(() => {
    if (user.guidance.layout && user.guidance.header) {
      window.scroll({ top: 0, behavior: "smooth" });
      // if (!user.guidance.internalAssessmentBefore) {
      //   setFirstTour(true);
      // }
    }
  }, [user]);

  const filtered = useMemo(() => {
    let data = grups;

    if (filters) {
      data = data.filter((item) =>
        filters.searchGroup
          ? item?.name
              ?.toLowerCase()
              .includes(filters.searchGroup.toLowerCase())
          : true
      );
    }
    return data;
  }, [filters, grups]);

  const handleChangeCurrentPage = useCallback(
    (page) => {
      setFilters((state) => ({ ...state, paginationGroups: page }));
    },
    [setFilters]
  );

  const { data: groupFiltered, PaginationComponent } = usePagination(
    filtered,
    10,
    filters.paginationGroups,
    handleChangeCurrentPage
  );

  // const disableGuideBefore = () => {
  //   setFirstTour(false);
  //   setIsTour(true);
  //   request
  //     .put("auth/guidance", { guidance: "internalAssessmentBefore" })
  //     .then(() => {
  //       dispatch(getMe());
  //     });
  // };

  // const disableGuideTour = () => {
  //   setIsTour(false);
  // };

  const changePublished = (id, status) => {
    request
      .put("v1/recruitment/vacancies/open-close/" + id, { status: status })
      .then(() => {
        listInternal();
      });
  };

  useEffect(() => {
    listInternal();
  }, []);

  function listInternal() {
    setLoading(true);
    request
      .get("v2/recruitment/internal")
      .then((res) => {
        setGrups(res.data.data);
      })
      .catch((err) => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  if (loading) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  return (
    <div>
      <div className={`p-2`}>
        <Row>
          <Col className="tour-assessmentSearchGroup" sm="6">
            <SearchGroupNameFilter data={t("searchGroupName")} />
          </Col>
          <Col sm="6" className="text-right ">
            <Link
              to={
                user?.personnel?.company?.paid === "education"
                  ? "/recruitment/internal/create/edu"
                  : "/recruitment/internal/create"
              }
            >
              <button className="btn btn-sm btn-netis-primary px-4 py-2 scale-div tour-createGroup ">
                <i className="fa fa-plus mr-2"></i>
                {t("sendRequest")}
              </button>
            </Link>
          </Col>
        </Row>
        <hr />
      </div>
      {groupFiltered && groupFiltered.length > 0 ? (
        <Row className="tour-groupList">
          {groupFiltered.map((grup, idx) => {
            return (
              <>
                <Col sm="12" key={idx}>
                  <Card className="shadow-sm border-0 tour-groupCard">
                    <CardBody>
                      <Row>
                        <Col sm md="8">
                          <Link
                            to={
                              user?.personnel?.company?.paid === "education"
                                ? `/recruitment/internal/${grup?.id}/edit/edu`
                                : `/recruitment/internal/${grup?.id}/edit`
                            }
                          >
                            <h4 style={{ color: "#305574", display: "inline" }}>
                              {grup?.name}
                            </h4>
                          </Link>
                          <br />
                          <br />
                          {grup?.requirements &&
                            grup?.requirements.map((requirement, id) => {
                              return (
                                <div
                                  style={{
                                    color: "#555",
                                    backgroundColor: "#F2F2F2",
                                    padding: "5px 15px",
                                    borderRadius: "15px",
                                    display: "inline-block",
                                    marginRight: "10px",
                                    marginBottom: "10px",
                                  }}
                                  key={id}
                                >
                                  {t(requirement)}
                                </div>
                              );
                            })}
                          <br />
                          <Link
                            to={
                              user?.personnel?.company?.paid === "education"
                                ? `/recruitment/internal/${grup?.id}/edit/edu`
                                : `/recruitment/internal/${grup?.id}/edit`
                            }
                            className="tour-editBtn"
                          >
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mr-1"
                            >
                              <i className="fa fa-pencil"></i> Edit
                            </Button>
                          </Link>
                          {!grup?.published ? (
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mx-1 mt-1"
                              onClick={() => changePublished(grup.id, 1)}
                            >
                              <p
                                // className="text-info"
                                style={{ display: "inline" }}
                              >
                                <i className="fa fa-refresh"></i> Reopen
                              </p>
                            </Button>
                          ) : (
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mx-1 mt-1"
                              onClick={() => changePublished(grup.id, 0)}
                            >
                              <p
                                // className="text-danger"
                                style={{ display: "inline" }}
                              >
                                <i className="fa fa-window-close"></i>{" "}
                                {t("close")}
                              </p>
                            </Button>
                          )}
                          <ModalShareUrl data={grup?.skillanaURL} />
                        </Col>
                        <Col sm md="4" className="text-center pt-4 pb-4">
                          <Link
                            to={`/recruitment/internal/${grup?.id}/participant`}
                            className={`btn text-wrap btn-outline-netis-primary mr-2 my-1 text-capitalize btn-job-applicant-status pt-3 pb-3 tour-numberOfParticipants`}
                          >
                            <h4 className="mb-0">{grup?.applicants}</h4>
                            {t("peserta")}
                          </Link>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </>
            );
          })}
          <Col sm="12" className="text-center">
            {filtered.length > 10 ? <PaginationComponent /> : ""}
          </Col>
        </Row>
      ) : (
        <div className="my-5 text-center">
          <DataNotFound />
        </div>
      )}
    </div>
  );
}
const ModalShareUrl = (props) => {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  return (
    <>
      <Button
        style={{ border: 0 }}
        className="btn bg-transparent btn-sm mx-1 mt-1 tour-shareBtn"
        onClick={toggle}
      >
        <p
          // className="text-danger"
          style={{ display: "inline" }}
        >
          <i className="fa fa-share"></i> {t("share")}
        </p>
      </Button>
      <Modal
        isOpen={modal}
        toggle={toggle}
        size="md"
        backdropClassName="share-modal-backdrop"
      >
        <ModalHeader
          className="share-modal-header pb-2"
          toggle={toggle}
          close={closeBtn}
          cssModule={{ "modal-title": "w-100 text-center" }}
        >
          <b>{t("shareUrl")}</b>
          <hr className="m-2" />
        </ModalHeader>
        <ModalBody className="p-1">
          <div className="share-url-wrapper">
            <input
              className="urlHolder2"
              id="url"
              type="text"
              value={props.data}
            />
            <button
              className="share-url-button btn-netis-primary"
              onClick={() => {
                let urlHolder = document.getElementById("url");
                let copiedText = document.getElementById("copiedText");

                urlHolder.select();
                urlHolder.setSelectionRange(0, 99999);
                document.execCommand("copy");

                copiedText.innerHTML = t("modalShareText2");
              }}
            >
              <i className="fa fa-copy"></i>
            </button>
            <p id="copiedText" className="text-muted">
              {t("modalShareText1")}
            </p>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default translate(InternalAssessment);
