import React, { useEffect, useMemo, useState } from "react";
import { useFormik } from "formik";
import {
  Button,
  Col,
  CustomInput,
  Form,
  Input,
  Label,
  Row,
  Spinner,
} from "reactstrap";
import request from "../../../../utils/request";
import ModalError from "../../../../components/ModalError";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import { t, translate } from "react-switch-lang";
import { toast } from "react-toastify";
import Tour from "reactour";
import * as Yup from "yup";
import { useAuthUser } from "../../../../store";
import { useDispatch } from "react-redux";
import { getMe } from "../../../../actions/auth";
import disableScroll from "disable-scroll";
import Select from "react-select";
import useSWR from "swr";
import ModalAddQuestion from "../VideoCustom/ModalAddQuestion";

function LinkAssessmentCreate(props) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [req, setReq] = useState([]);
  const [submit, setSubmit] = useState(false);
  const [isTour, setIsTour] = useState(false);
  const [isDiscChecked, setIsDiscChecked] = useState(false);
  const [isVideoChecked, setIsVideoChecked] = useState(false);
  const [isProfileChecked, setIsProfileChecked] = useState(false);
  const { data: response, error: errorQuestion, mutate } = useSWR('v1/assessment/video')
  const dataQuestion = useMemo(() => response?.data?.data ?? [], [response])
  const listQuestion = dataQuestion?.map((item) => ({
    value: item.id,
    label: item.title,
  }));
  const [modalQuestion, setModalQuestion] = useState(false)
  const toggleQuestion = () => setModalQuestion(!modalQuestion)
  const user = useAuthUser();
  const dispatch = useDispatch();
  const disableBody = () => disableScroll.on();
  const enableBody = () => disableScroll.off();
  const accentColor = "#1d5a8e";
  const ValidationFormSchema = useMemo(() => {
    return Yup.object().shape({
      name: Yup.string().required().label(t("requirement")),
      requirements: Yup.array().test(
        "required",
        t("fillInAssessmentNeeds"),
        function (value) {
          return value.length > 0;
        }
      ),
    });
  }, []);

  const { values, touched, errors, ...formik } = useFormik({
    initialValues: {
      name: "",
      requirements: [],
      companyValue: [],
    },
    validationSchema: ValidationFormSchema,
    onSubmit: (values, { setSubmitting, setErrors }) => {
      setSubmitting(true);
      setSubmit(true);
      request
        .post("v2/recruitment/vacancies", {
          name: values.name,
          published: false,
          isInternal: true,
          requirements: values.requirements,
          companyValue: values.companyValue,
        })
        .then(() => {
          toast.success(t("sucessSendRequestAssessmentLink"));
          props.history.goBack();
        })
        .catch((err) => {
          // console.log(err)
          if (err?.response?.status === 403) {
            toast.error(t("sorryYouAlreadyHaveInternalAssessment"));
          } else if (err?.response?.status) {
            toast.error(t("anErrorOccurred"));
          }
          return;
        })
        .finally(() => {
          setSubmit(false);
          setSubmitting(false);
        });
    },
  });

  useEffect(() => {
    setLoading(true);
    request
      .get("v2/recruitment/vacancies/requirements")
      .then((res) => {
        setReq(res.data.data.filter(i => i !== 'video-custom'));
      })
      .catch(() => setError(true))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    if (user.guidance.layout && user.guidance.header) {
      window.scroll({ top: 0, behavior: "smooth" });
      if (!user.guidance.createInternalAssessmentGroup) {
        setIsTour(true);
      }
    }
  }, [user]);

  const disableGuideTour = () => {
    setIsTour(false);
    request
      .put("auth/guidance", { guidance: "createInternalAssessmentGroup" })
      .then(() => {
        dispatch(getMe());
      });
  };

  const steps = [
    {
      selector: ".tour-createGroupName",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeInternal")}
          </h5> */}
          <p>{t("createInternalAssessmentGuide1")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-12 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(1);
                  }}
                >
                  {t("btnNext")}
                  <i className="fa fa-arrow-right ml-2"></i>
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
    {
      selector: ".tour-chooseAssessment",
      content: ({ goTo, inDOM }) => (
        <div>
          {/* <h5
            className="title-upgrade text-center"
            style={{ color: "#93aad6" }}
          >
            {t("welcomeInternal")}
          </h5> */}
          <p>{t("welcomeInternalGuide1")}</p>
          <div className="col-12 text-center">
            <Row>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2"
                  type="submit"
                  color="netis-color"
                  onClick={() => {
                    goTo(0);
                  }}
                >
                  <i className="fa fa-arrow-left mr-2"></i>
                  {t("btnPrevious")}
                </Button>
              </div>
              <div className="col-6 text-center p-0">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={() => {
                    disableGuideTour();
                  }}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </div>
        </div>
      ),
    },
  ];

  const changeRequirements = (e) => {
    const { value, checked } = e.target;
    if (value === "all") {
      if (checked) {
        const allSet = new Set(req);
        allSet.add('video-custom');
        const allArr = Array.from(allSet);
        formik.setFieldValue("requirements", allArr);
        document.querySelector(".chooseCharacteristic").style.display = "flex";
        setIsDiscChecked(true);
        setIsVideoChecked(true);
        setIsProfileChecked(true);
      } else if (!checked) {
        formik.setFieldValue("requirements", []);
        document.querySelector(".chooseCharacteristic").style.display = "none";
        setIsDiscChecked(false);
        setIsVideoChecked(false);
        setIsProfileChecked(false);
      }
    } else {
      let arr = values.requirements;
      arr.push(value);
      const set = new Set(arr);
      console.log(set);
      if (!checked) {
        set.delete(value);
      }

      const setArr = Array.from(set);
      // console.log(setArr)
      formik.setFieldValue("requirements", setArr);

      if (value === "papikostick") {
        if (checked) {
          document.querySelector(".chooseCharacteristic").style.display =
            "flex";
        } else if (!checked) {
          document.querySelector(".chooseCharacteristic").style.display =
            "none";
        }
      }

      if (value === "disc") {
        if (checked) {
          setIsDiscChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter((e) => e !== "spm");
          setIsDiscChecked(false);
          formik.setFieldValue("requirements", newSetArr);
        }
      }

      if (value === "video") {
        if (checked) {
          setIsVideoChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter(
            (e) => e !== "fisiognomi" && e !== "palmistry"
          );
          formik.setFieldValue("requirements", newSetArr);
          setIsVideoChecked(false);
        }
      }

      if (value === "profile") {
        if (checked) {
          setIsProfileChecked(true);
        } else if (!checked) {
          const newSetArr = setArr.filter(
            (e) => e !== "bazi" && e !== "zodiac" && e !== "shio"
          );
          formik.setFieldValue("requirements", newSetArr);
          setIsProfileChecked(false);
        }
      }
    }
  };

  const changeVideoQuestion = function (vid) {
    formik.setFieldValue("videoCustomId", vid);
    formik.setFieldTouched("videoCustomId", true);
  };

  if (loading) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  const formikSetValue = function (field) {
    return (value) => {
      formik.setFieldValue(field, value);
      formik.setFieldTouched(field, true);
    };
  };

  const companyValue = [
    { value: "F", label: t("companyValuePoint1") },
    { value: "W", label: t("companyValuePoint2") },
    { value: "N", label: t("companyValuePoint3") },
    { value: "G", label: t("companyValuePoint4") },
    { value: "A", label: t("companyValuePoint5") },
    { value: "L", label: t("companyValuePoint6") },
    { value: "P", label: t("companyValuePoint7") },
    { value: "I", label: t("companyValuePoint8") },
    { value: "T", label: t("companyValuePoint9") },
    { value: "V", label: t("companyValuePoint10") },
    { value: "X", label: t("companyValuePoint11") },
    { value: "S", label: t("companyValuePoint12") },
    { value: "B", label: t("companyValuePoint13") },
    { value: "O", label: t("companyValuePoint14") },
    { value: "R", label: t("companyValuePoint15") },
    { value: "D", label: t("companyValuePoint16") },
    { value: "C", label: t("companyValuePoint17") },
    { value: "Z", label: t("companyValuePoint18") },
    { value: "E", label: t("companyValuePoint19") },
    { value: "K", label: t("companyValuePoint20") },
  ];

  return (
    <div className="animated fadeIn d-flex flex-column bd-highlight mb-3 p-4">
      <Tour
        steps={steps}
        accentColor={accentColor}
        rounded={5}
        isOpen={isTour}
        closeWithMask={false}
        showNumber
        disableFocusLock={true}
        disableInteraction={true}
        disableDotsNavigation={true}
        showNavigation={true}
        showButtons={false}
        onAfterOpen={disableBody}
        onBeforeClose={enableBody}
        onRequestClose={() => {
          disableGuideTour();
        }}
      />

      <div className="bd-highlight mb-4">
        <h5>{t("createInternalLink")}</h5>
      </div>
      <Form onSubmit={formik.handleSubmit}>
        <Row>
          <Col md="6">
            <Row className="mb-5 tour-createGroupName">
              <Col xs="3">
                <Label htmlFor="name" className="input-label pt-2">
                  {t("requirement")}
                </Label>
              </Col>
              <Col xs="9">
                <Input
                  type="input"
                  value={values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="name"
                  id="name"
                  required
                  maxLength="50"
                  className="form-control needs-validation"
                />
                {touched.name && errors.name && (
                  <small className="text-danger">{errors.name}</small>
                )}
              </Col>
            </Row>
            <Row className="my-5 tour-chooseAssessment">
              <Col xs="3">
                <Label htmlFor="all" className="input-label">
                  {t("requirement2")}
                </Label>
              </Col>
              <Col xs="9">
                <CustomInput
                  type="checkbox"
                  id="all"
                  name="all"
                  label={t("semua")}
                  value="all"
                  onChange={changeRequirements}
                  checked={values.requirements.length === req.length+1}
                />
                {req.length > 0 &&
                  req.map((item, idx) => {
                    return (
                      <>
                        {!isDiscChecked &&
                          item === "spm" ? null : !isVideoChecked &&
                            item === "palmistry" ? null : !isVideoChecked &&
                              item === "fisiognomi" ? null : !isProfileChecked &&
                                item === "bazi" ? null : !isProfileChecked &&
                                  item === "shio" ? null : !isProfileChecked &&
                                    item === "zodiac" ? null : (
                          <CustomInput
                            key={idx}
                            type="checkbox"
                            id={item}
                            name={item}
                            value={item}
                            label={t(item)}
                            onChange={changeRequirements}
                            checked={values.requirements.includes(item)}
                          />
                        )}
                      </>
                    );
                  })}
                {values.requirements.includes('video') &&
                  <CustomInput
                    type='checkbox'
                    id='custom'
                    name='custom'
                    label={t('Video Custom')}
                    checked={values.requirements.includes('video-custom')}
                    value='video-custom'
                    onChange={changeRequirements}
                  />
                }
                {touched.requirements && errors.requirements && (
                  <small className="text-danger">{errors.requirements}</small>
                )}
              </Col>
            </Row>
            <Row className="chooseCharacteristic" style={{ display: "none" }}>
              <Col xs="3">
                <Label htmlFor="companyValue" className="input-label">
                  {t("karakteristik")}
                </Label>
              </Col>
              <Col xs="9">
                <Row form>
                  <Col className="mb-2">
                    <Select
                      styles={{
                        menu: (provided) => ({ ...provided, zIndex: 9999 }),
                      }}
                      isSearchable={true}
                      name="companyValue"
                      id="companyValue"
                      closeMenuOnSelect={false}
                      onChange={(val) => {
                        if (val?.length > 3) return false;
                        formikSetValue("companyValue")(val);
                      }}
                      onBlur={formik.handleBlur}
                      value={values.companyValue}
                      options={companyValue}
                      isMulti
                    />
                    <small
                      className="text-form text-muted d-block"
                      style={{ fontSize: "9px" }}
                    >
                      {t("candidateCharacteristict")}
                    </small>
                  </Col>
                </Row>
              </Col>
            </Row>
            {values.requirements.includes('video-custom') &&
              <Row>
                <Col xs="3">
                  <Label htmlFor="companyValue" className="input-label">
                    {t("Kustomisasi Video CV")}
                  </Label>
                </Col>
                <Col xs="9">
                  <Select
                    id='customQuestion'
                    name='customQuestion'
                    placeholder={t('Pilih Interview Video CV')}
                    className='w-100 mb-2'
                    disabled={errorQuestion}
                    options={listQuestion}
                    onChange={changeVideoQuestion}
                  />
                  {errorQuestion && <small className='text-danger'>Error</small>}
                  <Button color='secondary' onClick={toggleQuestion} className='w-100 my-2'>{t('Tambah Interview')}</Button>
                </Col>
              </Row>
            }
            <Row className="text-center mt-5 mb-2">
              <Col xs="12" className="text-center">
                <Button
                  className="btn btn-sm btn-netis-primary px-4"
                  type="submit"
                  disabled={submit}
                >
                  {submit ? (
                    <>
                      <Spinner size="sm" color="light" /> Loading...{" "}
                    </>
                  ) : (
                    t("sendRequest")
                  )}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
      <ModalAddQuestion modalQuestion={modalQuestion} toggleQuestion={toggleQuestion} mutate={mutate} />
    </div>
  );
}

export default translate(LinkAssessmentCreate);
