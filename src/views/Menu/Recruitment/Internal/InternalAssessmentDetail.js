import React, { useEffect, useState } from "react";
import DataNotFound from "../../../../components/DataNotFound";
import LoadingAnimation from "../../../../components/LoadingAnimation";
import ModalError from "../../../../components/ModalError";
import request from "../../../../utils/request";
import EmployeeList from "./EmployeeList";
import { translate, t } from "react-switch-lang";
import { useRouteMatch } from "react-router-dom";
import { Card, CardHeader, CardBody } from "reactstrap";
import { useAuthUser } from "../../../../store";

function InternalAssessmentDetail() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(false);
  const matchRoute = useRouteMatch();
  const user = useAuthUser();

  useEffect(() => {
    setLoading(true);
    request
      .get("v1/recruitment/applicants/internal/" + matchRoute?.params?.id)
      .then((res) => {
        setData(res?.data);
      })
      .catch((err) => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  if (loading) {
    return (
      <div style={{ marginTop: "30vh" }}>
        <LoadingAnimation />
      </div>
    );
  }

  if (error) {
    return <ModalError isOpen={true} />;
  }

  return (
    <div>
      <Card>
        <CardHeader className="d-flex align-items-center bg-netis-primary">
          <h5 className="mb-0" style={{ color: "#ffff" }}>
            {data?.job}
          </h5>
        </CardHeader>
        <CardBody style={{ minHeight: "1000px" }}>
          {data && data?.data?.length > 0 ? (
            <EmployeeList data={data?.data} data2={data?.papikostick} />
          ) : (
            // Kondisi ketika sudah ada karyawan yang mengsisi internal asesmen dan data ditampilkan
            <div className="my-5 text-center">
              <div
                className="text-center mx-auto mb-3 bg-secondary rounded  pt-3 pb-2"
                style={{ width: "75%", marginTop: "10vh" }}
              >
                <h5>
                  {user?.personnel?.company?.paid === "education"
                    ? t("noInternalSub")
                    : t("noInternal")}
                </h5>
              </div>
              <DataNotFound />
            </div>
          )}
        </CardBody>
      </Card>
    </div>
  );
}

export default translate(InternalAssessmentDetail);
