import React from "react";
import { translate } from "react-switch-lang";
import { Table } from "reactstrap";
import SkeletonApplicantDetail from "../../../Skeleton/SkeletonApplicantDetail";
import SentimentAnalysis from "../../Sentiment/SentimentAnalysis";

const CompareSentiment = ({ data, match, getAPI, dataToken, data2, dataToken2 }) => {
    if (!data) {
        return <SkeletonApplicantDetail />;
    }
    return (
        <Table borderless responsive>
            <tbody>
                <tr>
                    <td className="sentiment-compare-td" style={{ verticalAlign: 'top' }}>
                        <SentimentAnalysis candidate="first" data={data.detail} match={match} getAPI={getAPI} dataToken={dataToken} type="compare" />
                    </td>
                    <td className="sentiment-compare-td" style={{ verticalAlign: 'top' }}>
                        {data2 ?
                        <SentimentAnalysis candidate="sec" data={data2.detail} match={match} getAPI={getAPI} dataToken={dataToken2} type="compare" />
                        :
                        null}
                    </td>
                </tr>
            </tbody>
        </Table>
    );
};

export default translate(CompareSentiment);