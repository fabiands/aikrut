import React, { useState, useEffect, useMemo } from 'react'
import { Card, CardBody, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap'
import { Link, useHistory, useLocation } from 'react-router-dom'
import SkeletonApplicantCompare from '../../Skeleton/SkeletonApplicantCompare';
import request from '../../../../../utils/request';
import ModalError from '../../../../../components/ModalError';
import useSWR from 'swr';
import { useBalance } from '../../../../../hooks/useBalance';
import profilePhotoNotFound from '../../../../../assets/img/no-photo.png'
import Select from 'react-select'
import CompareProfile from './Components/CompareProfile';
import CompareResume from './Components/CompareResume';
import { useUserPrivileges } from '../../../../../store';
import CompareAssessment from './Components/CompareAssessment';
import "./../videoasesmen.css";
import CompareSentiment from './Components/CompareSentiment';
import { translate, t } from 'react-switch-lang';

function ApplicantCompare({ match }) {
    const tabs = {
        'profile': t("Profil"),
        'resume': t("resume"),
        'assessment': t("assessment"),
        'sentiment': t("sentimenAnalysis")
    }
    
    const tabsArray = Object.keys(tabs);
    
    const location = useLocation();
    const history = useHistory();
    const { can } = useUserPrivileges();
    const isInternal = new URLSearchParams(location.search).get('isInternal')
    const paramSec = new URLSearchParams(location.search).get('with')
    const [id, setId] = useState({
        first: match.params.applicantId ?? null,
        sec: paramSec
    })
    const [applicantFirst, setApplicantFirst] = useState(null)
    const [applicantSec, setApplicantSec] = useState(null)
    const [loading, setLoading] = useState(true)
    const [notFound, setNotFound] = useState(false)
    const { data: applicantResponse, error: applicantError } = useSWR(`v1/recruitment/applicants/user?isInternal=${isInternal}`);
    const applicantData = useMemo(() => applicantResponse?.data?.data ?? [], [applicantResponse]);
    const option = applicantData.map(app =>
        ({ value: app.idUser, label: app.fullName })
    )
    const { data: dataToken } = useBalance();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];

    const getAPI = (id, type) => {
        setLoading(true)
        return request
            .get(`v1/recruitment/applicants/user/${id}`)
            .then((res) => {
                if (type === "first") {
                    setApplicantFirst(res.data.data)
                }
                else if (type === "sec") {
                    setApplicantSec(res.data.data)
                }
            })
            .catch((err) => {
                if (err.response?.status === 404) {
                    setNotFound(true);
                }
            })
            .finally(() => setLoading(false));
    }

    useEffect(() => {
        getAPI(id.first, "first")
        // eslint-disable-next-line
    }, [id.first])

    useEffect(() => {
        if (location.search && id.sec) {
            getAPI(id.sec, "sec")
        }
        // eslint-disable-next-line
    }, [location.search, id.sec])

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    const changeFirstCanditate = (e) => {
        const { value } = e;
        setId({ ...id, first: value })
        if (id.sec) {
            history.replace(`/recruitment/applicants/${value}/compare?isInternal=${isInternal}&with=${id.sec}`)
        }
        else {
            history.replace(`/recruitment/applicants/${value}/compare?isInternal=${isInternal}`)
        }
    }

    const changeSecCandidate = (e) => {
        const { value } = e;
        setId({ ...id, sec: value })
        history.replace(`/recruitment/applicants/${id.first}/compare?isInternal=${isInternal}&with=${value}`)
    }

    if (notFound || applicantError) {
        return <ModalError isOpen={true} />
    }

    return (
        <>
            <Card>
                <CardBody>
                    <div className="text-center my-4">
                        <h4>{t('compareCandidate')}</h4>
                        <h6>{t('compareCandidateDesc')}</h6>
                    </div>
                    {loading ? <SkeletonApplicantCompare /> :
                        <>
                            <Row className="mt-4 mb-5">
                                <Col xs="2" className="d-none d-md-block">
                                    {t('candidateName')}
                                </Col>
                                <Col xs="6" md="5">
                                    <Select
                                        isSearchable={true}
                                        isClearable={false}
                                        options={option}
                                        value={option?.find((item) => item.value === parseInt(id.first)) ?? id.first}
                                        defaultValue={option?.find((item) => item.value === parseInt(id.first)) ?? id.first}
                                        onChange={changeFirstCanditate}
                                        isDisabled={loading}
                                        name="firstPerson"
                                        id="firstPerson"
                                    />
                                    {id.first && <Row className="my-1">
                                        <Col className="text-right mx-auto">
                                            <div className="compare-photo d-flex justify-content-center p-3">
                                                <img src={applicantFirst?.detail?.avatar} alt="photos profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                                            </div>
                                        </Col>
                                        <Col className="pl-3 p-md-1 text-left">
                                            <Row className="mt-sm-3 mt-md-4">
                                                <Col xs="12" className="text-netis-primary">
                                                    <h4 className="font-weight-bold">{applicantFirst?.detail?.fullName}</h4>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>}
                                </Col>
                                <Col xs="6" md="5">
                                    <Select
                                        isSearchable={true}
                                        isClearable={false}
                                        options={option}
                                        value={option?.find((item) => item.value === parseInt(id.sec)) ?? id.sec}
                                        defaultValue={option?.find((item) => item.value === parseInt(id.sec)) ?? id.sec}
                                        onChange={changeSecCandidate}
                                        isDisabled={loading}
                                        name="secPerson"
                                        id="secPerson"
                                    />
                                    {id.sec && <Row className="my-1">
                                        <Col className="text-right mx-auto">
                                            <div className="compare-photo d-flex justify-content-center p-3">
                                                <img src={applicantSec?.detail?.avatar} alt="photos profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                                            </div>
                                        </Col>
                                        <Col className="pl-3 p-md-1 text-left">
                                            <Row className="mt-sm-3 mt-md-4">
                                                <Col xs="12" className="text-netis-primary">
                                                    <h4 className="font-weight-bold">{applicantSec?.detail?.fullName}</h4>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>}
                                </Col>
                            </Row>
                            <div className="d-flex justify-content-center mt-3">
                                <Nav tabs className="mx-auto">
                                    {tabsArray.map(tab => (
                                        <NavItem key={tab}>
                                            <NavLink tag={Link} className="pt-2/5 mx-2" active={selectedTab === tab} replace to={location => ({ ...location, hash: '#' + tab })}>
                                                {tabs[tab]}
                                            </NavLink>
                                        </NavItem>
                                    ))}
                                </Nav>
                            </div>
                            <TabContent activeTab={selectedTab}>
                                <TabPane tabId="profile">
                                    <CompareProfile data={applicantFirst} dataToken={dataToken} data2={applicantSec} dataToken2={dataToken} />
                                </TabPane>
                                <TabPane tabId="resume">
                                    <CompareResume data={applicantFirst} dataToken={dataToken} data2={applicantSec} dataToken2={dataToken} />
                                </TabPane>
                                <TabPane tabId="assessment">
                                    {applicantFirst && <CompareAssessment can={can} match={match} getAPI={getAPI} data={applicantFirst} dataToken={dataToken} data2={applicantSec} dataToken2={dataToken} />}
                                </TabPane>
                                <TabPane tabId="sentiment" className="p-0 p-md-1">
                                    {applicantFirst && <CompareSentiment data={applicantFirst} dataToken={dataToken} dataToken2={dataToken} data2={applicantSec} match={match} getAPI={getAPI} />}
                                </TabPane>
                            </TabContent>
                        </>
                    }
                </CardBody>
            </Card>
        </>
    )
}

export default translate(ApplicantCompare)