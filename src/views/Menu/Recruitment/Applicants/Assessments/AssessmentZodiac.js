import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import moment from "moment";
import ReactMarkdown from "react-markdown";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";

function AssessmentZodiac({
  zodiac,
  candidate,
  date,
  id,
  getAPI,
  dataToken,
  can,
  position = "detailApplicant",
}) {
  const [modalNoToken, setModalNoToken] = useState(false);
  const [showZodiacDescription, setShowZodiacDescription] = useState(false);
  const [loadingBuy, setLoadingBuy] = useState(false);
  // const [modalUnlock, setModalUnlock] = useState(false)
  // const [collapse, setCollapse] = useState(null);
  const [forbidden, setForbidden] = useState(false);
  const [key, setKey] = useState(null);
  // const toggleResult = (value) => {
  //     setCollapse(collapse === Number(value) ? null : Number(value));
  // };

  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  useEffect(() => {
    if (zodiac.purchased) {
      const zodiacKey = zodiac?.key.split(", ");
      if (zodiacKey.length >= 8) {
        const chunkLength = Math.max(zodiacKey.length / 2, 1);
        let chunks = [];
        for (var i = 0; i < 2; i++) {
          if (chunkLength * (i + 1) <= zodiacKey.length)
            chunks.push(
              zodiacKey.slice(chunkLength * i, chunkLength * (i + 1))
            );
        }
        setKey(chunks);
      } else if (zodiacKey.length < 8) {
        setKey(zodiacKey);
      }
    }
    // eslint-disable-next-line
  }, [zodiac]);

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="zodiac"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={zodiac?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex flex-column">
              <Row>
                <Col xs="6">
                  <h5 className="text-uppercase content-sub-title mb-2">
                    <strong>
                      ZODIAC{" "}
                      {position === "detailApplicant" && " - " + zodiac.title}
                    </strong>
                  </h5>
                </Col>
                {position !== "detailApplicant" && (
                  <Col xs="6" className="text-right d-flex justify-content-end">
                    {!zodiac?.purchased && (
                      <div style={{ marginTop: "4px" }} className="mr-2">
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{zodiac.tokenPrice}</b>
                      </div>
                    )}
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        zodiac?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={
                        zodiac.purchased
                          ? () => setShowZodiacDescription(true)
                          : toggleNoToken
                      }
                      style={{ borderRadius: "8px" }}
                    >
                      {zodiac.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          {/* <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#e4d51d" }} /> */}
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </Col>
                )}
              </Row>
              {position !== "detailApplicant" && (
                <>
                  <h4>{zodiac.title}</h4>
                </>
              )}
              <div className="mb-2">
                {t("tanggallahir")} : &nbsp;
                {moment(date).format("DD MMMM YYYY")}
              </div>
              {position === "detailApplicant" ? (
                <>
                  <ReactMarkdown source={zodiac.description} className="my-2" />
                  <div className="d-flex">
                    {!zodiac?.purchased && (
                      <div style={{ marginTop: "4px" }} className="mr-2">
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{zodiac.tokenPrice}</b>
                      </div>
                    )}
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        zodiac?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={
                        zodiac.purchased
                          ? () => setShowZodiacDescription(true)
                          : toggleNoToken
                      }
                      style={{ borderRadius: "8px" }}
                    >
                      {zodiac.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          {/* <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#e4d51d" }} /> */}
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </div>
                </>
              ) : null}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Modal
        isOpen={showZodiacDescription}
        toggle={() => setShowZodiacDescription(false)}
        size="xl"
      >
        <ModalHeader toggle={() => setShowZodiacDescription(false)}>
          {zodiac.title}
        </ModalHeader>
        <ModalBody>
          <div>
            <h3>{t("ciri")}</h3>
            {key && zodiac?.key.split(", ").length >= 8 ? (
              <Row>
                <Col>
                  <ul>
                    {key &&
                      key[0]?.map((keyFirst, idx) => {
                        return (
                          <li key={idx} className="text-capitalize">
                            {keyFirst}
                          </li>
                        );
                      })}
                  </ul>
                </Col>
                <Col>
                  <ul>
                    {key &&
                      key[1]?.map((keySecond, idx) => {
                        return (
                          <li key={idx} className="text-capitalize">
                            {keySecond}
                          </li>
                        );
                      })}
                  </ul>
                </Col>
              </Row>
            ) : (
              <ul>
                {key &&
                  key?.map((item, idx) => {
                    return (
                      <li key={idx} className="text-capitalize">
                        {item}
                      </li>
                    );
                  })}
              </ul>
            )}
            <hr />
            <h3 className="text-capitalize">{t("sugesti")}</h3>
            <ReactMarkdown source={zodiac?.sugestion} />
            <hr />
            <h3>Partner</h3>
            <ul>
              {zodiac.purchased &&
                zodiac?.partner.map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
            </ul>
            <hr />
            <div className="row">
              <div className="col-6">
                <h5>{t("Sifat Positif")}</h5>
                <ReactMarkdown source={zodiac.positive_traits} />
              </div>
              <div className="col-6">
                <h5>{t("Sifat Negatif")}</h5>
                <ReactMarkdown source={zodiac.negative_traits} />
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
}

export default translate(AssessmentZodiac);
