import React, { useState, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { t, translate } from "react-switch-lang";
import {
  Button,
  Card,
  CardBody,
  Col,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  Spinner,
  Table,
  Tooltip,
} from "reactstrap";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
import NoToken from "./NoToken";
import * as moment from "moment";

function AssessmentGesture({ vid, id, getAPI, dataToken, can }) {
  const [firstData, ...restData] = vid?.data;
  const [modalNoToken, setModalNoToken] = useState(false);
  const vidRef = useRef(null);
  const firstVideo = React.useMemo(
    () => (vid.data[0] ? { resultUrl: vid.data[0]?.videoUrl } : null),
    [vid]
  );
  const firstTitle = React.useMemo(() => vid.data[0]?.title, [vid]);
  const [activeVideo, setActiveVideo] = useState(firstVideo);
  const [anotationData, setAnotationData] = useState(null);
  const [activeTitle, setActiveTitle] = useState(t(firstTitle));
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [showButton, setShowButton] = useState(true);
  const [modalGesture, setModalGesture] = useState(false);
  const [hint, setHint] = useState(false);
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const [forbidden, setForbidden] = useState(false);

  const toggleTooltip = () => setTooltipOpen(!tooltipOpen);
  const toggleGesture = () => {
    setModalGesture(!modalGesture);
  };

  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  const playVid = () => {
    vidRef.current.play();
    setShowButton(false);
  };

  function changeVideo(video) {
    setActiveTitle(t(video.title));
    setActiveVideo({ resultUrl: video.videoUrl });
    setAnotationData(null);
    setShowButton(true);
  }

  function changeAnotation(video) {
    setActiveTitle(t(video.title));
    setActiveVideo({ resultUrl: video.resultUrl });
    setAnotationData(video.resultData);
    setShowButton(true);
  }

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        buy="gesture"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={vid?.tokenType}
      />

      <Modal isOpen={modalGesture} toggle={toggleGesture}>
        <ModalHeader toggle={toggleGesture} className="border-bottom-0">
          {t("descGesture")}
        </ModalHeader>
        <ModalBody>
          {anotationData &&
            anotationData.map((data, idx) => (
              <div key={idx}>
                <h4 className="text-capitalize">
                  {data.gesture.split("-").join(" ")}
                </h4>
                <span>{data.description}</span>
                {anotationData.length !== idx + 1 && <hr />}
              </div>
            ))}
          {/* {anotationData.map((text, idx) => ( */}
        </ModalBody>
      </Modal>

      <Card>
        <CardBody>
          <Row className="md-company-header flex-column mb-3 mt-2">
            <Col className="d-flex justify-content-between align-items-center">
              <div>
                <h5 className="text-uppercase content-sub-title mb-0">
                  <strong>
                    {t("assessmentVideo2")} {vid.testName}{" "}
                    {t("assessmentVideo")}
                  </strong>
                </h5>
                <i>{moment(vid.created_at).format("DD MMMM YYYY")}</i>
              </div>
            </Col>
            {/* <img src={require("../../../../assets/img/16personalities/entj.png")} alt="transparent" /> */}
          </Row>

          <div className="row">
            <div className="mb-5 col-lg-5">
              {activeVideo && (
                <>
                  <div className="vidwrapper">
                    <video
                      ref={vidRef}
                      className="vid"
                      src={activeVideo.resultUrl}
                      width="90%"
                      height="auto"
                      id="dataVid"
                      controls
                      onPlaying={() => setShowButton(false)}
                      onPause={() => setShowButton(true)}
                    />
                    {showButton ? (
                      <>
                        <p className="title text-capitalize">
                          &nbsp;&nbsp;&nbsp;{activeTitle}&nbsp;&nbsp;&nbsp;
                        </p>
                        <Button className="play" onClick={playVid}>
                          <i className="fa fa-2x fa-align-center fa-play"></i>
                        </Button>
                      </>
                    ) : null}
                  </div>
                  {anotationData?.length > 0 && (
                    <>
                      {anotationData.map((text, idx) => (
                        <ListGroup key={idx}>
                          <ListGroupItem className="anotation-list my-2">
                            <Row
                              style={{ marginLeft: "-6px" }}
                              className="pr-lg-2"
                            >
                              <Col
                                xs="7"
                                md="9"
                                className="anotation-name text-capitalize py-2"
                              >
                                {text.gesture.split("-").join(" ")}
                              </Col>
                              <Col
                                xs="5"
                                md="3"
                                className="anotation-value text-center py-2"
                              >
                                {text.score}
                              </Col>
                            </Row>
                          </ListGroupItem>
                        </ListGroup>
                      ))}
                      <div className="text-center mt-3">
                        <Button
                          className="button-video"
                          onClick={toggleGesture}
                        >
                          {t("seeDetail")}
                        </Button>
                      </div>
                    </>
                  )}
                </>
              )}
            </div>
            <div className="col-lg-7">
              {vid.purchased ? null : (
                <div className="lock-token text-center">
                  <i className="fa fa-lock lock-icon" aria-hidden="true" />
                  <br />
                  <div>
                    <span style={{ fontSize: "16pt" }}>
                      <b>{t("openNextVideo")}</b>
                    </span>
                    <br />
                    <span>{t("clickOpenDetail")}</span>
                    <br />
                    {vid.tokenPriceSecond !== 0 ? (
                      <div style={{ position: "relative" }}>
                        <div
                          style={{
                            position: "absolute",
                            top: "50%",
                            width: 45,
                            height: 2,
                            background: "#ff0200",
                            left: "calc(50% - 22px)",
                            transform: "rotate(-15deg)",
                          }}
                        ></div>
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#676767" }}
                        />
                        <b>{vid.tokenPriceSecond}</b>
                        <br />
                      </div>
                    ) : (
                      ""
                    )}
                    <FontAwesomeIcon
                      icon="coins"
                      className="mr-1"
                      style={{ color: "#e0bc47" }}
                    />
                    <b>{vid.tokenPrice}</b>
                    <br />
                    <Button
                      disabled={loadingBuy}
                      className="btn btn-netis-color mb-2 mr-2"
                      onClick={toggleNoToken}
                      style={{ borderRadius: "8px" }}
                    >
                      {loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </div>
                </div>
              )}
              <Table
                responsive
                hover
                className="border border-secondary border-bottom"
              >
                <thead>
                  <tr>
                    <th className="w-10"></th>
                    <th className="text-center w-50">{t("videoName")}</th>
                    <th className="text-center">
                      {t("videoType")}
                      <Button
                        onClick={() => setHint(!hint)}
                        className="text-nowrap"
                        style={{
                          backgroundColor: "transparent",
                          border: "transparent",
                        }}
                        id="TooltipExample"
                      >
                        <i className="fa fa-lg fa-question-circle text-primary" />
                      </Button>
                      <Tooltip
                        placement="bottom"
                        isOpen={tooltipOpen}
                        target="TooltipExample"
                        toggle={toggleTooltip}
                      >
                        {t("videoButtonNotShow")}
                      </Tooltip>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr
                    className={`${
                      activeTitle === firstData?.title ? `video-border` : ``
                    }`}
                  >
                    <td>
                      {firstData?.processed !== null ? (
                        <i className="fa fa-check-circle-o text-success ml-1" />
                      ) : (
                        <i className="fa fa-spinner text-info ml-1" />
                      )}
                    </td>

                    <td>1. {t(firstData?.title)}</td>
                    <td className="text-nowrap">
                      {firstData?.resultUrl === null ? (
                        <p>{t("noGesture")}</p>
                      ) : (
                        <>
                          <Button
                            className={`text-center text-nowrap button-video mr-2 ${
                              activeVideo.resultUrl === firstData.videoUrl
                                ? "button-video-active"
                                : ""
                            }`}
                            onClick={() => changeVideo(firstData)}
                          >
                            {t("withOutAnotation")}
                          </Button>
                          <Button
                            className={`text-center text-nowrap button-asesmen ml-2 ${
                              activeVideo.resultUrl === firstData.resultUrl
                                ? "button-asesmen-active"
                                : ""
                            }`}
                            onClick={() => changeAnotation(firstData)}
                          >
                            {t("withAnotation")}
                          </Button>
                        </>
                      )}
                    </td>
                  </tr>
                  {restData &&
                    restData.map((video, idx) => (
                      <tr
                        key={idx + 1}
                        className={`${
                          activeTitle === video?.title ? `video-border` : ``
                        }`}
                      >
                        <td>
                          {vid.purchased ? (
                            video.resultUrl !== null ? (
                              <>
                                <i
                                  className="fa fa-check-circle-o text-success ml-1"
                                  id="process"
                                />
                              </>
                            ) : (
                              <i className="fa fa-spinner text-info ml-1" />
                            )
                          ) : (
                            <i className="fa fa-video-camera ml-1" />
                          )}
                        </td>
                        <td>
                          {idx + 2}. {t(video?.title)}
                        </td>
                        <td className="text-nowrap">
                          {video?.resultUrl === null ? (
                            <p>{t("systemAnalysVideo")}</p>
                          ) : (
                            <>
                              <Button
                                className={`text-center text-nowrap button-video mr-2 ${
                                  activeVideo.resultUrl === video.videoUrl
                                    ? "button-video-active"
                                    : ""
                                }`}
                                onClick={() => changeVideo(video)}
                              >
                                {t("withOutAnotation")}
                              </Button>
                              <Button
                                className={`text-center text-nowrap button-asesmen ml-2 ${
                                  activeVideo.resultUrl === video.resultUrl
                                    ? "button-asesmen-active"
                                    : ""
                                }`}
                                onClick={() => changeAnotation(video)}
                              >
                                {t("withAnotation")}
                              </Button>
                            </>
                          )}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </div>
          </div>
        </CardBody>
      </Card>
    </>
  );
}

export default translate(AssessmentGesture);
