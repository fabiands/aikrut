import React, { useState } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  // ListGroupItemText,
  Collapse,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import moment from "moment";
import ReactMarkdown from "react-markdown";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";

function AssessmentShio({
  shio,
  candidate,
  date,
  id,
  getAPI,
  dataToken,
  can,
  position = "detailApplicant",
}) {
  const [modalNoToken, setModalNoToken] = useState(false);
  const [showShioDescription, setShowShioDescription] = useState(false);
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [collapse, setCollapse] = useState(null);
  const [forbidden, setForbidden] = useState(false);
  const toggleResult = (value) => {
    setCollapse(collapse === Number(value) ? null : Number(value));
  };

  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  const combinationShio = Object.keys(shio.combination);
  const combinationDesc = Object.values(shio.combination);

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="shio"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={shio?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex flex-column">
              <Row>
                <Col xs="6">
                  <h5 className="text-uppercase content-sub-title mb-2">
                    <strong>
                      SHIO
                      {position === "detailApplicant" &&
                        " - " + shio.earth + shio.title}
                    </strong>
                  </h5>
                </Col>
                {position !== "detailApplicant" && (
                  <Col xs="6" className="text-right d-flex justify-content-end">
                    {!shio?.purchased && (
                      <div
                        style={{ marginTop: "4px" }}
                        className="mr-2 d-inline"
                      >
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{shio.tokenPrice}</b>
                      </div>
                    )}
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        shio?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={
                        shio.purchased
                          ? () => setShowShioDescription(true)
                          : toggleNoToken
                      }
                      style={{ borderRadius: "8px" }}
                    >
                      {shio.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </Col>
                )}
              </Row>
              {position !== "detailApplicant" && (
                <>
                  <h4>
                    {shio.earth} {shio.title}
                  </h4>
                </>
              )}
              <div className="mb-2">
                {t("monthYearBirth")} : &nbsp;
                {moment(date).format("MMMM YYYY")}
              </div>
              {position === "detailApplicant" ? (
                <>
                  <ReactMarkdown source={shio.description} className="my-2" />
                  <div className="d-flex">
                    {!shio?.purchased && (
                      <div
                        style={{ marginTop: "4px" }}
                        className="mr-2 d-inline"
                      >
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{shio.tokenPrice}</b>
                      </div>
                    )}
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        shio?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={
                        shio.purchased
                          ? () => setShowShioDescription(true)
                          : toggleNoToken
                      }
                      style={{ borderRadius: "8px" }}
                    >
                      {shio.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </div>
                </>
              ) : null}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Modal
        isOpen={showShioDescription}
        toggle={() => setShowShioDescription(false)}
        size="xl"
      >
        <ModalHeader toggle={() => setShowShioDescription(false)}>
          {shio.earth} {shio.title}
        </ModalHeader>
        <ModalBody>
          <div>
            <h3>{t("kombinasi")}</h3>
            <p>{t("kombinasiDesc")}</p>
            <Row>
              {combinationShio.map((shio, i) => (
                <Col md="6" lg="4" key={i}>
                  <ListGroup className="my-2">
                    <ListGroupItem>
                      <ListGroupItemHeading
                        className="d-flex justify-content-between mb-0"
                        id={shio}
                        onClick={() => {
                          toggleResult(i);
                        }}
                        style={{ cursor: "pointer" }}
                      >
                        <strong>{shio}</strong>
                        {collapse === i ? (
                          <i className="mt1 fa-sm fa fa-chevron-up" />
                        ) : (
                          <i className="mt1 fa-sm fa fa-chevron-down" />
                        )}
                      </ListGroupItemHeading>
                      <Collapse isOpen={collapse === i}>
                        <div className="mt-3">
                          <hr className="hr-main ml-0" />
                          <ReactMarkdown source={combinationDesc[i]} />
                        </div>
                      </Collapse>
                    </ListGroupItem>
                  </ListGroup>
                </Col>
              ))}
            </Row>
            <hr />
            <div className="row">
              <div className="col-6">
                <h5>{t("Sifat Positif")}</h5>
                <ReactMarkdown source={shio.positive_traits} />
              </div>
              <div className="col-6">
                <h5>{t("Sifat Negatif")}</h5>
                <ReactMarkdown source={shio.negative_traits} />
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
}

export default translate(AssessmentShio);
