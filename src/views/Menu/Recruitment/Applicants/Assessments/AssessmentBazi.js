import React, { useState } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import moment from "moment";
import ReactMarkdown from "react-markdown";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
import langUtils from "../../../../../utils/language/index";
function AssessmentBazi({
  bazi,
  date,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position = "detailApplicant",
}) {
  const [modalNoToken, setModalNoToken] = useState(false);
  const [showBaziDescription, setShowBaziDescription] = useState(false);
  const [loadingBuy, setLoadingBuy] = useState(false);
  // const [collapse, setCollapse] = useState(null);
  const [forbidden, setForbidden] = useState(false);
  // const toggleResult = (value) => {
  //     setCollapse(collapse === Number(value) ? null : Number(value));
  // };

  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="bazi"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={bazi?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex flex-column">
              <Row>
                <Col xs="6">
                  <h5 className="text-uppercase content-sub-title mb-2">
                    <strong>
                      BAZI
                      {bazi.purchased &&
                        position === "detailApplicant" &&
                        " - " + bazi.title}
                    </strong>
                  </h5>
                </Col>
                {position !== "detailApplicant" && (
                  <Col xs="6" className="text-right d-flex justify-content-end">
                    {!bazi?.purchased && (
                      <div style={{ marginTop: "4px" }} className="mr-2">
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{bazi?.tokenPrice}</b>
                      </div>
                    )}
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        bazi?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={
                        bazi.purchased
                          ? () => setShowBaziDescription(true)
                          : toggleNoToken
                      }
                      style={{ borderRadius: "8px" }}
                    >
                      {bazi.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock" />
                          &nbsp;&nbsp;{t("openDetail")}
                        </>
                      )}
                    </Button>
                  </Col>
                )}
              </Row>
              {bazi.purchased && position !== "detailApplicant" && (
                <>
                  <h4>{bazi.title}</h4>
                </>
              )}
              <div className="mb-2">
                {t("harilahir")} : &nbsp;
                {moment(date).locale(langUtils.getLanguage()).format("dddd")}
                {/* &nbsp;<i>({moment(date).format("DD MMMM YYYY")})</i> */}
              </div>
              {position === "detailApplicant" ? (
                <>
                  <ReactMarkdown source={bazi.description} className="my-2" />
                  <div className="d-flex">
                    {!bazi?.purchased && (
                      <div style={{ marginTop: "4px" }} className="mr-2">
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{bazi?.tokenPrice}</b>
                      </div>
                    )}
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        bazi?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={
                        bazi.purchased
                          ? () => setShowBaziDescription(true)
                          : toggleNoToken
                      }
                      style={{ borderRadius: "8px" }}
                    >
                      {bazi.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock" />
                          &nbsp;&nbsp;{t("openDetail")}
                        </>
                      )}
                    </Button>
                  </div>
                </>
              ) : null}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Modal
        isOpen={showBaziDescription}
        toggle={() => setShowBaziDescription(false)}
        size="xl"
      >
        <ModalHeader toggle={() => setShowBaziDescription(false)}>
          {bazi.title}
        </ModalHeader>
        <ModalBody>
          <div>
            {/* <h3>Deskripsi</h3>
            <ReactMarkdown source={bazi.description} />
            <hr /> */}
            <h3>{t("charactersAtWork")}</h3>
            <ReactMarkdown source={bazi.working_character} />
            <hr />
            <h3>{t("jobSuggestions")}</h3>
            <ReactMarkdown source={bazi.job_sugestion} />
            <hr />
            <div className="row">
              <div className="col-6">
                <h5>{t("Sifat Positif")}</h5>
                <ReactMarkdown source={bazi.positive_traits} />
              </div>
              <div className="col-6">
                <h5>{t("Sifat Negatif")}</h5>
                <ReactMarkdown source={bazi.negative_traits} />
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
}

export default translate(AssessmentBazi);
