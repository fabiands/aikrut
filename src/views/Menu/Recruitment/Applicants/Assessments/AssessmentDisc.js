import React, { useState } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  // ListGroupItemText,
  Collapse,
} from "reactstrap";
import { translate, t } from "react-switch-lang";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
import { Line } from "react-chartjs-2";
import ReactMarkdown from "react-markdown";

const AssessmentDisc = ({
  data,
  id,
  candidate,
  getAPI,
  dataToken,
  can,
  position = "detailApplicant",
}) => {
  const [modalAses, setModalAses] = useState(false);

  const toggleAses = () => setModalAses(!modalAses);
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [forbidden, setForbidden] = useState(false);

  const [modalNoToken, setModalNoToken] = useState(false);
  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="disc"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={data?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mt-2">
            <Col
              xs="6"
              className="d-flex justify-content-between align-items-center"
            >
              <h5 className="text-uppercase content-sub-title mb-0">
                <strong>{t("personalityTest")}</strong>
              </h5>
            </Col>
            <Col xs="6" className="text-right">
              {data.purchased ? (
                <Button
                  className={
                    data?.purchased ? "button-video" : "btn btn-netis-color"
                  }
                  onClick={data.purchased ? toggleAses : toggleNoToken}
                  style={{ borderRadius: "8px" }}
                >
                  {data.purchased ? (
                    t("seeDetail")
                  ) : (
                    <>
                      <i className="fa fa-lock mr-2" />
                      {t("OpenDetail")}
                    </>
                  )}
                </Button>
              ) : position !== "detailApplicant" ? (
                <>
                  <FontAwesomeIcon
                    icon="coins"
                    className="ml-1"
                    style={{ color: "#e0bc47" }}
                  />
                  <b>{data?.tokenPrice ?? 1}</b>
                  <Button
                    disabled={loadingBuy}
                    className={`btn ${data?.purchased ? "button-video" : "btn-netis-color"
                      } ml-2`}
                    style={{ borderRadius: "8px" }}
                    onClick={toggleNoToken}
                  >
                    {loadingBuy ? (
                      <>
                        <Spinner color="light" size="sm" />
                        &nbsp;&nbsp;Loading...{" "}
                      </>
                    ) : (
                      <>
                        <i className="fa fa-lock mr-1" />
                        {t("openDetail")}
                      </>
                    )}
                  </Button>
                </>
              ) : null}
            </Col>
            <Col sm="12" md="12" lg="12">
              <hr />
              <div className="mt-3">
                {!data.purchased && position === "detailApplicant" && (
                  <div className="lock-disc text-center">
                    <i className="fa fa-lock lock-icon" aria-hidden="true" />
                    <br />
                    <div>
                      <span style={{ fontSize: "12pt" }}>
                        <b>{t("seeNextGrafik")}</b>
                      </span>
                      <br />
                      <span>{t("clickOpenDetail")}</span>
                      <br />
                      <FontAwesomeIcon
                        icon="coins"
                        className="mr-1"
                        style={{ color: "#e0bc47" }}
                      />
                      <b>{data?.tokenPrice ?? 1}</b>
                      <br />
                      <Button
                        disabled={loadingBuy}
                        className={`btn ${data?.purchased ? "button-video" : "btn-netis-color"
                          } mb-2 mr-2`}
                        style={{ borderRadius: "8px" }}
                        onClick={toggleNoToken}
                      >
                        {loadingBuy ? (
                          <>
                            <Spinner color="light" size="sm" />
                            &nbsp;&nbsp;Loading...{" "}
                          </>
                        ) : (
                          <>
                            <i className="fa fa-lock mr-1" />
                            {t("openDetail")}
                          </>
                        )}
                      </Button>
                    </div>
                  </div>
                )}
                {data.purchased && position !== "detailApplicant" && (
                  <h3>
                    {data?.type?.most?.map((type, i) => {
                      return (i === 0 ? "" : ", ") + type;
                    })}
                  </h3>
                )}
                <DiscResult
                  result={data}
                  hideDescription={true}
                  hideGraph={position === "detailApplicant" ? false : true}
                />
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Modal isOpen={modalAses} toggle={toggleAses} className="modal-xl">
        <ModalHeader toggle={toggleAses}>{t("detailasesmen")}</ModalHeader>
        <ModalBody>
          <DiscResult result={data} />
        </ModalBody>
      </Modal>
    </>
  );
};

const label = ["D", "I", "S", "C"];
const title = {
  most: "Mask, Public Self",
  least: "Core, Private Self",
  changes: "Mirror, Perceived Self",
};

function DiscResult({ result, hideDescription = false, hideGraph = false }) {
  const [open, setOpen] = useState("karakter");

  const description = {
    most: t("demonstratesBehavior"),
    least: t("showsInstinctive"),
    changes: t("showSelfImage"),
  };
  return (
    <div>
      <div className="row justify-content-center mb-2">
        <div className="col-sm-8 col-md-6">
          {!hideDescription && (
            <div className="alert alert-success text-center">
              <h4>{t("personalityTestResult")}</h4>
            </div>
          )}
        </div>
      </div>
      <div className="pdf-disc">
        <Row className="my-2">
          {result.scores &&
            Object.keys(result.scores).map((category, idx) => {
              const desc = result?.type_description
                ? result?.type_description[category][0]
                : null;
              return (
                <Col md="4" lg="4" sm="12" key={idx}>
                  {!hideGraph && (
                    <>
                      <div className="text-center mb-3 text1">
                        <h5>{title[category]}</h5>
                        <small>
                          <i>{description[category]}</i>
                        </small>
                      </div>
                      <br />
                      <Line
                        data={{
                          labels: label,
                          datasets: [
                            {
                              label: "",
                              fill: false,
                              lineTension: 0.1,
                              backgroundColor: "rgba(75,192,192,0.4)",
                              borderColor: "rgba(75,192,192,1)",
                              borderCapStyle: "butt",
                              borderDash: [],
                              borderDashOffset: 0.0,
                              borderJoinStyle: "miter",
                              pointBorderColor: "rgba(75,192,192,1)",
                              pointBackgroundColor: "#fff",
                              pointBorderWidth: 1,
                              pointHoverRadius: 5,
                              pointHoverBackgroundColor: "rgba(75,192,192,1)",
                              pointHoverBorderColor: "rgba(220,220,220,1)",
                              pointHoverBorderWidth: 2,
                              pointRadius: 1,
                              pointHitRadius: 10,
                              data: label.map((scores) => {
                                return result.scores[category][scores];
                              }),
                            },
                          ],
                        }}
                        options={{
                          // title: {
                          //     display: true,
                          //     // text: category.toUpperCase()
                          //     text: description[category]
                          // },
                          scales: {
                            xAxes: [
                              {
                                /* For changing color of x-axis coordinates */
                                ticks: {
                                  // fontSize: 18,
                                  // padding: 0,
                                  fontColor: "#000",
                                  fontStyle: "bold",
                                },
                              },
                            ],
                            yAxes: [
                              {
                                ticks: {
                                  min: -8,
                                  max: 8,
                                },
                                gridLines: {
                                  zeroLineWidth: 2,
                                  zeroLineColor: "#555",
                                },
                              },
                            ],
                          },
                          legend: {
                            display: false,
                          },
                        }}
                        height={350}
                      />
                    </>
                  )}
                  <br />
                  {!hideDescription && (
                    <DiscResultDesription
                      category={category}
                      desc={desc}
                      open={open}
                      setOpen={(e) => setOpen(e)}
                    />
                  )}
                </Col>
              );
            })}
        </Row>
      </div>
    </div>
  );
}

function DiscResultDesription({ category, desc, open, setOpen }) {
  const collapseKarakter = open === "karakter";
  const collapseKekuatan = open === "kekuatan";
  const collapsePerkembangan = open === "perkembangan";
  const collapseNilai = open === "nilai";
  const collapseTekanan = open === "tekanan";
  const collapseLingkungan = open === "lingkungan";

  const changeOpen = (item) => {
    if (item === open) {
      setOpen("");
    } else {
      setOpen(item);
    }
  };
  return (
    <ListGroup className="my-2">
      <ListGroupItem>
        <ListGroupItemHeading
          className="d-flex justify-content-between mb-0"
          id={category + "_karakteristik"}
          onClick={() => {
            changeOpen("karakter");
          }}
          style={{ cursor: "pointer" }}
        >
          <strong>{t("characteristics")}</strong>
          {collapseKarakter ? (
            <i className="mt1 fa-sm fa fa-chevron-up" />
          ) : (
            <i className="mt1 fa-sm fa fa-chevron-down" />
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapseKarakter}>
          <div className="mt-3">
            <hr className="hr-main ml-0" />
            <ReactMarkdown source={desc.karakteristik} />
          </div>
        </Collapse>
      </ListGroupItem>

      <ListGroupItem>
        <ListGroupItemHeading
          className="d-flex justify-content-between mb-0"
          id={category + "_kekuatan"}
          onClick={() => {
            changeOpen("kekuatan");
          }}
          style={{ cursor: "pointer" }}
        >
          <strong>{t("power")}</strong>
          {collapseKekuatan ? (
            <i className="mt1 fa-sm fa fa-chevron-up" />
          ) : (
            <i className="mt1 fa-sm fa fa-chevron-down" />
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapseKekuatan}>
          <div className="mt-3">
            <hr className="hr-main ml-0" />
            <ReactMarkdown source={desc.kekuatan} />
          </div>
        </Collapse>
      </ListGroupItem>

      <ListGroupItem>
        <ListGroupItemHeading
          className="d-flex justify-content-between mb-0"
          id={category + "_area_perkembangan"}
          onClick={() => {
            changeOpen("perkembangan");
          }}
          style={{ cursor: "pointer" }}
        >
          <strong>{t("area")}</strong>
          {collapsePerkembangan ? (
            <i className="mt1 fa-sm fa fa-chevron-up" />
          ) : (
            <i className="mt1 fa-sm fa fa-chevron-down" />
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapsePerkembangan}>
          <div className="mt-3">
            <hr className="hr-main ml-0" />
            <ReactMarkdown source={desc.area_perkembangan} />
          </div>
        </Collapse>
      </ListGroupItem>

      <ListGroupItem>
        <ListGroupItemHeading
          className="d-flex justify-content-between mb-0"
          id={category + "_nilai_kelompok"}
          onClick={() => {
            changeOpen("nilai");
          }}
          style={{ cursor: "pointer" }}
        >
          <strong>{t("valueGroup")}</strong>
          {collapseNilai ? (
            <i className="mt1 fa-sm fa fa-chevron-up" />
          ) : (
            <i className="mt1 fa-sm fa fa-chevron-down" />
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapseNilai}>
          <div className="mt-3">
            <hr className="hr-main ml-0" />
            <ReactMarkdown source={desc.nilai_kelompok} />
          </div>
        </Collapse>
      </ListGroupItem>

      <ListGroupItem>
        <ListGroupItemHeading
          className="d-flex justify-content-between mb-0"
          id={category + "_kecenderungan_bawah_tekanan"}
          onClick={() => {
            changeOpen("tekanan");
          }}
          style={{ cursor: "pointer" }}
        >
          <strong>{t("underPressure")}</strong>
          {collapseTekanan ? (
            <i className="mt1 fa-sm fa fa-chevron-up" />
          ) : (
            <i className="mt1 fa-sm fa fa-chevron-down" />
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapseTekanan}>
          <div className="mt-3">
            <hr className="hr-main ml-0" />
            <ReactMarkdown source={desc.kecenderungan_bawah_tekanan} />
          </div>
        </Collapse>
      </ListGroupItem>

      <ListGroupItem>
        <ListGroupItemHeading
          className="d-flex justify-content-between mb-0"
          id={category + "_lingkungan"}
          onClick={() => {
            changeOpen("lingkungan");
          }}
          style={{ cursor: "pointer" }}
        >
          <strong>{t("idealEnv")}</strong>
          {collapseLingkungan ? (
            <i className="mt1 fa-sm fa fa-chevron-up" />
          ) : (
            <i className="mt1 fa-sm fa fa-chevron-down" />
          )}
        </ListGroupItemHeading>
        <Collapse isOpen={collapseLingkungan}>
          <div className="mt-3">
            <hr className="hr-main ml-0" />
            <ReactMarkdown source={desc.lingkungan_ideal} />
          </div>
        </Collapse>
      </ListGroupItem>
    </ListGroup>
  );
}

export default translate(AssessmentDisc);
