import React, { useRef, useState } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Progress,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  Collapse,
  UncontrolledTooltip,
} from "reactstrap";
import { translate, t } from "react-switch-lang";
import StarRatingComponent from "react-star-rating-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Radar } from "react-chartjs-2";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
import NoToken from "./NoToken";

function AssessmentAgility({
  data,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position,
}) {
  const [forbidden, setForbidden] = useState(false);
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [collapse, setCollapse] = useState(-1);

  const toggleResult = (value) => {
    setCollapse(collapse === Number(value) ? null : Number(value));
  };
  const [modalNoToken, setModalNoToken] = useState(false);

  const chartRef = useRef(null);
  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  const dataResult = [
    {
      id: 1,
      type: "behaviour",
      title: "Experimentation",
      text1: data["experimentation"]
        ? data["experimentation"]?.interpretation
        : null,
      text2: null,
      score: data["experimentation"]?.percent,
      desc: t("experimentation"),
      level: data["experimentation"]?.level
        ? t(data["experimentation"]?.level)
        : null,
      statusLevel: data["experimentation"]?.level
        ? (data["experimentation"]?.level).replace(/\s/g, "")
        : null,
      star: data["experimentation"]?.star,
    },
    {
      id: 2,
      type: "behaviour",
      title: "Feedback Seeking",
      text1: data["feedback"] ? data["feedback"]?.interpretation : null,
      text2: null,
      score: data["feedback"]?.percent,
      desc: t("feedback"),
      level: data["feedback"]?.level ? t(data["feedback"]?.level) : null,
      statusLevel: data["feedback"]?.level
        ? (data["feedback"]?.level).replace(/\s/g, "")
        : null,
      star: data["feedback"]?.star,
    },
    {
      id: 3,
      type: "behaviour",
      title: "Reflection",
      text1: data["reflection"] ? data["reflection"]?.interpretation : null,
      text2: null,
      score: data["reflection"]?.percent,
      desc: t("reflection"),
      level: data["reflection"]?.level ? t(data["reflection"]?.level) : null,
      statusLevel1: data["reflection"]?.level
        ? (data["reflection"]?.level).replace(/\s/g, "")
        : null,
      star: data["reflection"]?.star,
    },
    {
      id: 4,
      type: "cognitive",
      title: "Cognitive Simulation",
      text1: data["cognitive"] ? data["cognitive"].interpretation : null,
      text2: null,
      score: data["cognitive"]?.percent ? data["cognitive"]?.percent : null,
      desc: t("cognitiveSimulation"),
      level: data["cognitive"]?.level ? t(data["cognitive"]?.level) : null,
      statusLevel: data["cognitive"]?.level
        ? (data["cognitive"]?.level).replace(/\s/g, "")
        : null,
      star: data["cognitive"]?.star ? data["cognitive"]?.star : null,
    },
    {
      id: 5,
      type: "cognitive",
      title: "Pattern Recognition",
      text1: data["pattern"] ? data["pattern"]?.interpretation : null,
      text2: null,
      score: data["pattern"]?.percent,
      desc: t("patternRecognition"),
      level: data["pattern"]?.level ? t(data["pattern"]?.level) : null,
      statusLevel: data["pattern"]?.level
        ? (data["pattern"]?.level).replace(/\s/g, "")
        : null,
      star: data["pattern"]?.star,
    },
    {
      id: 6,
      type: "cognitive",
      title: "Counterfactual Thinking",
      text1: data["counterfactual"][0]?.interpretation,
      text2: data["counterfactual"][1]?.interpretation,
      desc: t("counterFactualThinking"),
    },
  ];

  const dataGraph = {
    labels: ["Upward", "Substractive", "Downward", "Additive"],
    datasets: [
      {
        data: [
          data["counterfactual"][1]?.type === "upward"
            ? data["counterfactual"][1]?.value
            : 0,
          data["counterfactual"][0]?.type === "substractive"
            ? data["counterfactual"][0]?.value
            : 0,
          data["counterfactual"][1]?.type === "downward"
            ? data["counterfactual"][1]?.value
            : 0,
          data["counterfactual"][0]?.type === "additive"
            ? data["counterfactual"][0]?.value
            : 0,
        ],
        fill: true,
        backgroundColor: "rgba(255, 99, 132, 0.2)",
        borderColor: "#2a8c43",
        pointBackgroundColor: "#2a8c43",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "#2a8c43",
      },
    ],
  };

  const options = {
    plugins: {
      datalabels: {
        display: false,
        color: "#fff",
        font: {
          weight: "bold",
          size: 11,
        },
        borderColor: "#fff",
        borderWidth: 2,
        padding: {
          top: 6,
          bottom: 5,
          left: 8,
          right: 8,
        },
        borderRadius: 999,
      },
    },

    legend: {
      display: false,
    },
    title: {
      display: false,
      text: t("Assessment.agilityTitle"),
    },
    scale: {
      gridLines: {
        display: false,
        circular: true,
      },
      angleLines: {
        // display: false,
      },
      ticks: {
        display: false,
        max: 5,
        min: 0,
        stepSize: 1,
        beginAtZero: true,
        showLabelBackdrop: true,
      },
      pointLabels: {
        display: false,
        fontStyle: "bold",
        fontSize: 12,
      },
    },
  };
  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="agility"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={data?.tokenType}
      />

      <Card>
        <CardBody>
          <div className="d-flex flex-column md-company-header mb-3 mt-2">
            <h5 className="text-uppercase content-sub-title mb-4">
              <strong>{t("agility")}</strong>
            </h5>
            <Col className="d-flex flex-column">
              <div className="pdf-agility">
                <Card>
                  <CardBody>
                    <div className="">
                      <h5 className="mb-3">
                        <strong>Behavior</strong>
                      </h5>
                      {dataResult.map((data, i) =>
                        i < 6 && data.type === "behaviour" ? (
                          <ProgressGroup type={data.title} value={data.score} />
                        ) : null
                      )}
                    </div>
                  </CardBody>
                </Card>
                <Card>
                  <CardBody>
                    <div className="">
                      <h5 className="mb-3">
                        <strong>Cognitive</strong>
                      </h5>
                      {dataResult.map((data, i) =>
                        i < 6 && data.type === "cognitive" && data.id !== 6 ? (
                          <ProgressGroup type={data.title} value={data.score} />
                        ) : null
                      )}
                      <span className="progress-group-text font-weight-bold">
                        {dataResult[5].title}
                      </span>
                    </div>
                    <div className="row">
                      <div className="col-2"></div>
                      <div className="col-8 text-center">
                        <div className="mx-auto grafik-agility">
                          <Radar
                            data={dataGraph}
                            options={options}
                            width={50}
                            height={25}
                            ref={chartRef}
                          />
                        </div>
                      </div>
                      <div className="col-2"></div>
                    </div>
                  </CardBody>
                </Card>
              </div>
              <ListGroup className="my-2">
                <div className="fingerprint">
                  {!data.purchased ? (
                    <div className="lock-fingerprint text-center">
                      <div className="lock-fingerprint-text">
                        <i
                          className="fa fa-lock lock-icon"
                          aria-hidden="true"
                        />
                        <br />
                        <span style={{ fontSize: "12pt" }}>
                          <b>{t("wantSeeAgility")}</b>
                        </span>
                        <br />
                        <span>{t("clickOpenDetail")}</span>
                        <br />
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{data?.tokenPrice ?? 1}</b>
                        <br />
                        <Button
                          disabled={loadingBuy}
                          className={`btn ${
                            data?.purchased ? "button-video" : "btn-netis-color"
                          } mb-2 mr-2`}
                          onClick={toggleNoToken}
                          style={{ borderRadius: "8px" }}
                        >
                          {loadingBuy ? (
                            <>
                              <Spinner color="light" size="sm" />
                              &nbsp;&nbsp;Loading...{" "}
                            </>
                          ) : (
                            <>
                              <i className="fa fa-lock mr-1" />
                              {t("openDetail")}
                            </>
                          )}
                        </Button>
                      </div>
                    </div>
                  ) : null}

                  {dataResult.map((dataRes, i) => (
                    <div key={i}>
                      <ListGroupItem
                        className="outline-netis-primary mb-1"
                        style={{ borderRadius: 12 }}
                      >
                        <ListGroupItemHeading
                          className="d-flex justify-content-between"
                          id={dataRes.title + i}
                          onClick={() => toggleResult(i)}
                          style={{ cursor: "pointer" }}
                        >
                          <p>{dataRes.title}</p>
                          <p>
                            {i < 6 ? (
                              <>
                                <i
                                  id={"UncontrolledTooltipAgility" + i}
                                  className="fa fa-question-circle mr-2"
                                  style={{
                                    position: "relative",
                                    top: "1px",
                                    color: "#1472BA",
                                    fontSize: "15px",
                                  }}
                                />
                                <UncontrolledTooltip
                                  target={"UncontrolledTooltipAgility" + i}
                                >
                                  {dataRes.desc}
                                </UncontrolledTooltip>
                              </>
                            ) : null}
                            {collapse === i ? (
                              <i className="mt1 fa fa-sm fa-chevron-up" />
                            ) : (
                              <i className="mt1 fa fa-sm fa-chevron-down" />
                            )}
                          </p>
                        </ListGroupItemHeading>
                      </ListGroupItem>

                      <Collapse
                        isOpen={collapse === i}
                        className="bg-white border mb-2"
                        style={{ borderRadius: 12 }}
                      >
                        <Row>
                          <Col
                            md={position === "comparison" ? "12" : "12"}
                            lg={position === "comparison" ? "12" : "12"}
                          >
                            <div className="p-3">
                              <div
                                style={{
                                  position: "relative",
                                  top: "12px",
                                  textAlign: "center",
                                }}
                                className="ml-2"
                              >
                                {dataRes.id !== 6 ? (
                                  <>
                                    <p
                                      style={{
                                        fontSize: "30px",
                                        marginBottom: 0,
                                      }}
                                    >
                                      {i < 6 ? (
                                        <StarRatingComponent
                                          starCount={5}
                                          value={dataRes.star}
                                        />
                                      ) : null}
                                    </p>
                                    <div className="text-center">
                                      <div
                                        className={`badge  badge-${dataRes.statusLevel}`}
                                        style={{
                                          color: "black",
                                          fontSize: "14px",
                                        }}
                                      >
                                        {dataRes.level}
                                      </div>
                                    </div>
                                    <br />
                                    <br />
                                  </>
                                ) : null}
                              </div>
                              {dataRes.text1}
                              <br />
                              {dataRes.text2}
                              <br />
                            </div>
                          </Col>
                        </Row>
                      </Collapse>
                    </div>
                  ))}
                </div>
              </ListGroup>
            </Col>
          </div>
        </CardBody>
      </Card>
    </>
  );
}

function ProgressGroup({ type, value }) {
  return (
    <Row className="progress-group">
      <Col xs="12" sm="12">
        <span className={"progress-group-text font-weight-bold"}>{type}</span>
      </Col>
      <Col col="12" className="progress-group-bars">
        {Math.floor(value / 20) >= 5 ? (
          <Progress barClassName="bar-success" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        ) : Math.floor(value / 20) === 4 ? (
          <Progress
            barClassName="bar-most-success"
            value={Math.floor(value)}
            className="black-text"
          >
            {Math.floor(value)} %
          </Progress>
        ) : Math.floor(value / 20) === 3 ? (
          <Progress barClassName="bar-normal" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        ) : Math.floor(value / 20) === 2 ? (
          <Progress barClassName="bar-warning" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        ) : (
          <Progress color="danger" value={Math.floor(value)}>
            {Math.floor(value)} %
          </Progress>
        )}
      </Col>
    </Row>
  );
}

export default translate(AssessmentAgility);
