import React, { useState } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";

function AssessmentMsdt({
  data,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position,
}) {
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [forbidden, setForbidden] = useState(false);
  const [showMsdtDescription, setShowMsdtDescription] = useState(false);
  const [modalNoToken, setModalNoToken] = useState(false);

  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      if (data.purchased) {
        setShowMsdtDescription(!showMsdtDescription);
      } else {
        setModalNoToken(!modalNoToken);
      }
    } else {
      setForbidden(true);
    }
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="msdt"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={data?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col md="6" className="d-flex flex-column">
              <h5 className="text-uppercase content-sub-title mb-2">
                <strong>{t("leadershipTest")}</strong>
              </h5>
              {data.purchased && position === "detailApplicant" ? (
                <>
                  <h6>{t("leadershipTestIs")}</h6>
                  {position !== "detailApplicant" && <hr />}
                  <h4>
                    <span className="text-capitalize">
                      {data.description.title}
                    </span>
                  </h4>
                  <div className="mt-4">
                    <p>{data.description.desc}</p>
                    <div>
                      <ul>
                        {data.description.list.map((item, idx) => (
                          <li key={idx}>{item}</li>
                        ))}
                      </ul>
                    </div>
                  </div>
                </>
              ) : !data.purchased && position === "detailApplicant" ? (
                <h6>{t("seeCognitiveMsdtScore")}</h6>
              ) : !data.purchased && position !== "detailApplicant" ? (
                <h6>{t("seeCognitiveMsdtScore")}</h6>
              ) : (
                <>
                  <h4>
                    <span className="text-capitalize">
                      {data.description.title}
                    </span>
                  </h4>
                </>
              )}
            </Col>
            <Col md="6">
              {position === "detailApplicant" && !data?.purchased ? (
                <div className="float-right">
                  <div className="d-flex mt-4">
                    <div style={{ marginTop: "4px" }} className="mr-2 d-inline">
                      <FontAwesomeIcon
                        icon="coins"
                        className="mr-1"
                        style={{ color: "#e0bc47" }}
                      />
                      <b>{data.tokenPrice}</b>
                    </div>
                    <Button
                      disabled={loadingBuy}
                      className={`btn ${
                        data?.purchased ? "button-video" : "btn-netis-color"
                      }`}
                      onClick={toggleNoToken}
                      style={{ borderRadius: "8px" }}
                    >
                      {data.purchased ? (
                        t("seeDetail")
                      ) : loadingBuy ? (
                        <>
                          <Spinner color="light" size="sm" />
                          &nbsp;&nbsp;Loading...{" "}
                        </>
                      ) : (
                        <>
                          <i className="fa fa-lock mr-1" />
                          {t("openDetail")}
                        </>
                      )}
                    </Button>
                  </div>
                </div>
              ) : null}
              {position !== "detailApplicant" && (
                <div className="float-right">
                  {!data?.purchased && (
                    <div style={{ marginTop: "4px" }} className="mr-2 d-inline">
                      <FontAwesomeIcon
                        icon="coins"
                        className="mr-1"
                        style={{ color: "#e0bc47" }}
                      />
                      <b>{data.tokenPrice}</b>
                    </div>
                  )}
                  <Button
                    disabled={loadingBuy}
                    className={`btn ${
                      data?.purchased ? "button-video" : "btn-netis-color"
                    }`}
                    onClick={toggleNoToken}
                    style={{ borderRadius: "8px" }}
                  >
                    {data.purchased ? (
                      t("seeDetail")
                    ) : loadingBuy ? (
                      <>
                        <Spinner color="light" size="sm" />
                        &nbsp;&nbsp;Loading...{" "}
                      </>
                    ) : (
                      <>
                        <i className="fa fa-lock mr-1" />
                        {t("openDetail")}
                      </>
                    )}
                  </Button>
                </div>
              )}
              {data.purchased && position === "detailApplicant" && (
                <div className="my-3 text-center msdt-img pdf-msdt">
                  {position === "detailApplicant" && (
                    <img
                      src={require(`./assets/msdt/${data.result}.svg`)}
                      alt="MSDT"
                    />
                  )}
                </div>
              )}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Modal
        isOpen={showMsdtDescription}
        toggle={() => setShowMsdtDescription(false)}
        size="xl"
      >
        <ModalHeader toggle={() => setShowMsdtDescription(false)}>
          {data.description.title}
        </ModalHeader>
        <ModalBody>
          <div className="my-3 text-center msdt-img">
            <img src={require(`./assets/msdt/${data.result}.svg`)} alt="MSDT" />
          </div>
          <div className="mt-4">
            <p>{data.description.desc}</p>
            <div>
              <ul>
                {data?.description?.list?.map((item, idx) => (
                  <li key={idx}>{item}</li>
                ))}
              </ul>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
}

export default translate(AssessmentMsdt);
