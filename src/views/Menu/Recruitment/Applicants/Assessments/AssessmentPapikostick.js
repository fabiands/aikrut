import React, { useState, useRef, useEffect } from "react";
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  ListGroup,
  ListGroupItem,
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import NoToken from "./NoToken";
import Star from "../../../../../components/Star.js";
import { Radar } from "react-chartjs-2";
import langUtils from "../../../../../utils/language/index";
import { connect } from "react-redux";
import { withRouter } from "react-router";

const groupingLabel = {
  F: { category: "Followership", color: "#e53935" },
  W: { category: "Followership", color: "#e53935" },
  N: { category: "Work Direction", color: "#8e24aa" },
  G: { category: "Work Direction", color: "#8e24aa" },
  A: { category: "Work Direction", color: "#8e24aa" },
  L: { category: "Leadership", color: "#3949ab" },
  P: { category: "Leadership", color: "#3949ab" },
  I: { category: "Leadership", color: "#3949ab" },
  T: { category: "Activity", color: "#039be5" },
  V: { category: "Activity", color: "#039be5" },
  X: { category: "Social Nature", color: "#00897b" },
  S: { category: "Social Nature", color: "#00897b" },
  B: { category: "Social Nature", color: "#00897b" },
  O: { category: "Social Nature", color: "#00897b" },
  C: { category: "Work Style", color: "#7cb342" },
  D: { category: "Work Style", color: "#7cb342" },
  R: { category: "Work Style", color: "#7cb342" },
  Z: { category: "Temperament", color: "#fb8c00" },
  E: { category: "Temperament", color: "#fb8c00" },
  K: { category: "Temperament", color: "#fb8c00" },
};

const colorsByIndex = Object.keys(groupingLabel).map(
  (label) => groupingLabel[label].color
);

const options = {
  plugins: {
    datalabels: {
      display: true,
      backgroundColor: (option) => colorsByIndex[option.dataIndex],
      color: "#fff",
      font: {
        weight: "bold",
        size: 11,
      },
      borderColor: "#fff",
      borderWidth: 2,
      padding: {
        top: 6,
        bottom: 5,
        left: 8,
        right: 8,
      },
      borderRadius: 999,
    },
  },
  tooltips: {
    callbacks: {
      title: (tooltipItem, data) => {
        return (
          groupingLabel[data.labels[tooltipItem[0].index]].category +
          " (" +
          data.labels[tooltipItem[0].index] +
          ")"
        );
      },
      label: (tooltipItem, data) => {
        if (data.labels[tooltipItem.index] === "Z") {
          return 9 - tooltipItem.value;
        } else if (data.labels[tooltipItem.index] === "K") {
          return 9 - tooltipItem.value;
        } else return tooltipItem.value;
        // return parseInt(tooltipItem.value)
      },
    },
  },
  legend: {
    display: false,
  },
  title: {
    display: false,
    // text: 'Hasil PAPIKostick'
  },
  scale: {
    gridLines: {
      display: false,
      circular: true,
    },
    angleLines: {
      // display: false,
    },
    ticks: {
      display: false,
      max: 9,
      min: 0,
      stepSize: 1,
      beginAtZero: true,
      showLabelBackdrop: true,
    },
    pointLabels: {
      display: false,
      fontStyle: "bold",
      fontSize: 12,
      fontColor: Object.keys(groupingLabel).map(
        (label) => groupingLabel[label].color
      ),
    },
  },
};

const template = {
  Followership: ["F", "W"],
  "Work Direction": ["N", "G", "A"],
  Leadership: ["L", "P", "I"],
  Activity: ["T", "V"],
  "Social Nature": ["X", "S", "B", "O"],
  "Work Style": ["C", "D", "R"],
  Temperament: ["Z", "E", "K"],
};

const groupingDesc = {
  F: t("Keb Membantu Atasan"),
  W: t("Keb Mengikuti Aturan dan Pengawasan"),
  N: t("Keb dalam Menyelesaikan Tugas (Kemandirian)"),
  G: t("Peran Pekerja Keras"),
  A: t("Keb dalam Berprestasi"),
  L: t("Peran Kepemimpinan"),
  P: t("Keb Mengatur Orang Lain"),
  I: t("Peran Dalam Membuat Keputusan"),
  T: t("Peran Dalam Kesibukan Kerja"),
  V: t("Peran Dalam Semangat Kerja"),
  X: t("Keb untuk Diperhatikan"),
  S: t("Peran Dalam Hubungan Sosial"),
  B: t("Keb Diterima dalam Kelompok"),
  O: t("Keb Kedekatan dan Kasih Sayang"),
  C: t("Peran Dalam Mengatur"),
  D: t("Peran Bekerja dengan Hal-hal Rinci"),
  R: t("Peran Penalaran Teoritis"),
  Z: t("Keb untuk Berubah"),
  E: t("Peran Dalam Pengendalian Emosi"),
  K: t("Keb untuk Agresif"),
};

function titleColor(resultTitle) {
  switch (resultTitle) {
    case "Followership":
      return "#e53935";
    case "Work Direction":
      return "#8e24aa";
    case "Leadership":
      return "#3949ab";
    case "Activity":
      return "#039be5";
    case "Social Nature":
      return "#00897b";
    case "Work Style":
      return "#7cb342";
    case "Temperament":
      return "#fb8c00";
    default:
      return "#FFFFFF";
  }
}

function AssessmentPapikostick({
  user,
  papi,
  candidate,
  dataToken,
  position = "detailApplicant",
}) {
  const [modalPapi, setModalPapi] = useState(false);
  const [modalNoToken, setModalNoToken] = useState(false);
  const toggleNoToken = () => setModalNoToken(!modalNoToken);
  const togglePapi = () => setModalPapi(!modalPapi);

  const chartRef = useRef(null);
  const [backgroundSize, setBackgroundSize] = useState(null);

  const updateBackgroundSize = React.useCallback(() => {
    if (chartRef.current) {
      const chartHeight = chartRef?.current?.chartInstance?.height;
      if (chartHeight) {
        setBackgroundSize(chartHeight + chartHeight * 0.25);
      } else {
        setBackgroundSize(null);
      }
    }
  }, [chartRef]);

  useEffect(() => {
    updateBackgroundSize();
    window.addEventListener("resize", updateBackgroundSize);
    return () => {
      window.removeEventListener("resize", updateBackgroundSize);
    };
  }, [updateBackgroundSize]);

  const data = {
    labels: "GALPITVXSBORDCZEKFWN".split(""),
    datasets: [
      {
        label: "RESULT",
        borderWidth: 1,
        pointRadius: 2,
        backgroundColor: "rgba(0,0,0,0.3)",
        borderColor: (option) => {
          return colorsByIndex[option.dataIndex];
        },
        pointBackgroundColor: (option) => colorsByIndex[option.dataIndex],
        pointBorderColor: (option) => colorsByIndex[option.dataIndex],
        data: "GALPITVXSBORDCZEKFWN".split("").map((huruf) => {
          if (huruf === "Z") {
            return 9 - papi.scores[huruf];
          } else if (huruf === "K") {
            return 9 - papi.scores[huruf];
          } else return papi.scores[huruf];
        }),
      },
    ],
  };

  const group =
    papi.result &&
    Object.keys(papi.result).map((category) => {
      const item =
        papi.result &&
        template[category].map((code, idx) => ({
          code: code,
          scores: papi.scores[code],
          description: papi.result[category][idx],
        }));

      return {
        kategory: category,
        items: item,
      };
    });

  return (
    <>
      <NoToken
        candidate={candidate}
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex justify-content-between align-items-center">
              <h5 className="content-sub-title mb-0">
                <strong>{t("PapikostickTest")}</strong>
              </h5>
              <Button
                className="button-video"
                onClick={
                  user.personnel.company.paid === "pre"
                    ? !dataToken.balance || dataToken.isExpired
                      ? toggleNoToken
                      : togglePapi
                    : togglePapi
                }
                style={{ borderRadius: "8px" }}
              >
                {t("seeDetail")}
              </Button>
            </Col>
          </Row>
          {position === "detailApplicant" ? (
            <Chart
              data={data}
              backgroundSize={backgroundSize}
              chartRef={chartRef}
            />
          ) : (
            <ContentCompare group={group} position={position} />
          )}
        </CardBody>
      </Card>

      <Modal isOpen={modalPapi} toggle={togglePapi} className="modal-lg">
        <ModalHeader toggle={togglePapi}>{t("detailasesmen")}</ModalHeader>
        <ModalBody>
          {position === "detailApplicant" ? (
            <Content group={group} />
          ) : (
            <>
              <Chart
                data={data}
                backgroundSize={backgroundSize}
                chartRef={chartRef}
              />
              <div className="mt-3"></div>
              <Content group={group} />
            </>
          )}
        </ModalBody>
      </Modal>
    </>
  );
}

function Content({ group, position }) {
  return (
    <>
      {group &&
        group.map((objKategori, idx) => (
          <div key={idx} className="mt-2 mb-1">
            <h3
              className="mt-2"
              style={{ color: titleColor(objKategori.kategory) }}
            >
              {objKategori.kategory}
            </h3>
            <Row>
              <Col md="4" className="text-center papi-desc">
                {t("aspek")}
              </Col>
              <Col md="2" className="text-center papi-desc">
                {t("nilai")}
              </Col>
              <Col md="6" className="text-center papi-desc">
                {t("deskripsi")}
              </Col>
            </Row>
            {objKategori &&
              objKategori.items.map((objItem, idx) => (
                <ListGroup className="mb-1" key={idx}>
                  <ListGroupItem
                    className="papi-list"
                    style={{
                      borderLeftColor: titleColor(objKategori.kategory),
                    }}
                  >
                    <Row>
                      <Col md="4" className="py-2">
                        <div className="papi-desc-small">
                          {t("aspek")} :<br />
                        </div>
                        <div className="ml-1">
                          {objItem.code} : {groupingDesc[objItem.code]}
                        </div>
                      </Col>
                      <Col md="2" className="py-2 papi-border text-sm-center">
                        <div className="papi-desc-small">
                          Nilai :<br />
                        </div>
                        <Star value={(objItem.scores + 1) / 2} />(
                        {objItem.scores})
                      </Col>
                      <Col md="6" className="py-2 papi-result-desc border-0">
                        <div className="papi-desc-small">
                          {t("deskripsi")} :<br />
                        </div>
                        {objItem.description}
                      </Col>
                    </Row>
                  </ListGroupItem>
                </ListGroup>
              ))}
            <br />
            <br />
          </div>
        ))}
    </>
  );
}

function ContentCompare({ group, position }) {
  return (
    <>
      {group &&
        group.map((objKategori, idx) => (
          <div key={idx}>
            <h5 style={{ color: titleColor(objKategori.kategory) }}>
              {objKategori.kategory}
            </h5>
            <Row className="my-0">
              <Col xs="8" className="py-0 text-left text-md-center">
                <b>{t("aspek")}</b>
              </Col>
              <Col xs="4" className="py-0 text-center">
                <b>{t("nilai")}</b>
              </Col>
            </Row>
            {objKategori &&
              objKategori.items.map((objItem, idx) => (
                <ListGroup key={idx}>
                  <ListGroupItem>
                    <Row>
                      <Col xs="8" className="py-0">
                        {objItem.code} : {groupingDesc[objItem.code]}
                      </Col>
                      <Col xs="4" className="py-0 text-center">
                        {objItem.scores}
                      </Col>
                    </Row>
                  </ListGroupItem>
                </ListGroup>
              ))}
            <br />
          </div>
        ))}
    </>
  );
}

function Chart({ data, backgroundSize, chartRef }) {
  return (
    <div
      className={`pdf-papikostick ${
        langUtils.getLanguage() === "EN"
          ? "mx-auto grafik-papikostick-admin-en"
          : "mx-auto grafik-papikostick-admin"
      }`}
      style={{
        backgroundSize: backgroundSize ?? "auto 95%",
      }}
    >
      <Radar
        data={data}
        options={options}
        width={100}
        height={70}
        ref={chartRef}
      />
    </div>
  );
}

const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});
export default connect(mapStateToProps)(
  withRouter(translate(AssessmentPapikostick))
);
