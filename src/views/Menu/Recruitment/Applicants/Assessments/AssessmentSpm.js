import React, { useState } from 'react';
import { Row, Col, Card, CardBody, Button, Modal, ModalBody, ModalHeader, Spinner } from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { t, translate } from "react-switch-lang";
import NoToken from "./NoToken";
import { category } from "./SpmCategory";
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import ModalPrivilegeForbidden from '../../../../../components/ModalPrivilegeForbidden';

function AssessmentSpm({ data, candidate, id, getAPI, dataToken, can, position = 'detailApplicant' }) {
    const hasPurchased = data?.purchased;
    const [showSpmDescription, setShowSpmDescription] = useState(false);
    const [loadingBuy, setLoadingBuy] = useState(false);
    const [forbidden, setForbidden] = useState(false)
    const [modalNoToken, setModalNoToken] = useState(false);

    const toggleNoToken = () => {
        if (can('canManagementToken')) {
            setModalNoToken(!modalNoToken)
        }
        else {
            setForbidden(true)
        }
    }

    return (
        <>
            {forbidden && <ModalPrivilegeForbidden isOpen={true} forbidden="canManagementToken" isClose={() => setForbidden(false)} />}
            <NoToken candidate={candidate} buy="spm" nullData="assessment" isOpen={modalNoToken} toggle={toggleNoToken} isPurchase={!dataToken.balance || dataToken.isExpired ? false : true} idAssessment={id} getAPI={getAPI} isDone={(e) => setLoadingBuy(e)} type={data?.tokenType} />
            <Card>
                <CardBody>
                    <Row>
                        <Col xs="6">
                            <h5 className="text-uppercase content-sub-title mt-2 mb-4">
                                <strong>
                                    {t('cognitiveAbilityScore')}
                                </strong>
                            </h5>
                        </Col>
                        <Col xs="6">
                            <div className="float-right">
                                {!data?.purchased &&
                                    <div style={{ marginTop: "4px" }} className="mr-2 d-inline">
                                        <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#e0bc47" }} />
                                        <b>{data.tokenPrice}</b>
                                    </div>
                                }
                                {position !== 'detailApplicant' &&
                                    <Button
                                        disabled={loadingBuy}
                                        className={`btn ${hasPurchased ? 'button-video' : 'btn-netis-color'}`}
                                        onClick={hasPurchased ? () => setShowSpmDescription(true) : toggleNoToken}
                                        style={{ borderRadius: "8px" }}
                                    >
                                        {data.purchased ?
                                            t('seeDetail')
                                            : loadingBuy ? <><Spinner color="light" size="sm" />&nbsp;&nbsp;Loading... </>
                                                : <><i className="fa fa-lock mr-1" />{t('openDetail')}</>
                                        }
                                    </Button>
                                }
                                {!hasPurchased && position === 'detailApplicant' &&
                                    <Button
                                        disabled={loadingBuy}
                                        className={`btn ${hasPurchased ? 'button-video' : 'btn-netis-color'}`}
                                        onClick={hasPurchased ? () => setShowSpmDescription(true) : toggleNoToken}
                                        style={{ borderRadius: "8px" }}
                                    >
                                        {loadingBuy ? <><Spinner color="light" size="sm" />&nbsp;&nbsp;Loading... </>
                                            : <><i className="fa fa-lock mr-1" />{t('openDetail')}</>
                                        }
                                    </Button>
                                }
                            </div>
                        </Col>
                    </Row>
                    {(position === 'detailApplicant' && !hasPurchased) && <h6>
                        {t('seeCognitiveAbilityScore')}
                    </h6>}
                    {position !== 'detailApplicant' && <hr />}
                    {hasPurchased &&
                        position === 'detailApplicant' ?
                        <GraphSPM hasPurchased={hasPurchased} data={data} />
                        :
                        <h3 className="text-capitalize">{data.iq}</h3>
                    }
                </CardBody>
            </Card>
            <Modal
                isOpen={showSpmDescription}
                toggle={() => setShowSpmDescription(false)}
                size="xl"
            >
                <ModalHeader toggle={() => setShowSpmDescription(false)}>
                    {data.title}
                </ModalHeader>
                <ModalBody>
                    <div>
                        <GraphSPM hasPurchased={hasPurchased} data={data} />
                    </div>
                </ModalBody>
            </Modal>
        </>
    )
}

function GraphSPM({ hasPurchased, data }) {
    return (
        <Row className="md-company-header mb-3 mt-2">
            <Col md="8" lg="7" className="col-spm text-center py-4">
                <div className="pdf-spm">
                    <h6 className="text-uppercase content-sub-title mb-3">
                        <strong>
                            {t('cognitiveAbilityScore')}
                        </strong>
                    </h6>
                    <div style={{ width: '25%' }} className="mx-auto mb-2">
                        <CircularProgressbar
                            value={hasPurchased ? data.iq : 103}
                            maxValue={138}
                            strokeWidth={15}
                            text={hasPurchased ? data.iq : 103}
                            styles={{
                                path: {
                                    stroke: '#16CB60'
                                },
                                text: {
                                    fontSize: '16pt',
                                    fontWeight: 'bold',
                                    fill: '#000000'
                                }
                            }}
                        />
                    </div>
                    <div className="text-center my-4 mx-auto" style={{ width: '80%' }}>
                        {t('cognitiveAbilityScoreIs')} {hasPurchased ? data.iq : 103}
                        , {t('cognitiveAbilityScoreIsSub1')} {hasPurchased ? data.time : 20} {t('minute')}
                        , {t('cognitiveAbilityScoreIsSub2')} {hasPurchased ? data.type : "Average"}
                    </div>
                    {category.map((item, idx) => {
                        return (
                            <Row key={idx} className="row-spm-category mx-auto my-2">
                                <Col xs="2" className={`text-left py-2 ${data.type === item.name ? `check-icon` : ``}`}>
                                    {data.type === item.name ?
                                        <img src={require('../../../../../assets/img/asesmen/spm-check.png')} alt="category" width={30} />
                                        :
                                        <i className="fa fa-lg fa-circle-thin" style={{ color: "#818181" }} />
                                    }
                                </Col>
                                <Col xs="9" className={`d-flex justify-content-between py-2 ${data.type === item.name ? `spm-check` : `spm-category`}`}>
                                    <span>{item.name}</span>
                                    <span>{item.range}</span>
                                </Col>
                            </Row>
                        )
                    })}
                </div>
            </Col>
        </Row>
    )
}
export default translate(AssessmentSpm);