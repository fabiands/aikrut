import React, { useState, useMemo } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  Spinner,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { t, translate } from "react-switch-lang";
import NoToken from "./NoToken";
import "react-circular-progressbar/dist/styles.css";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";

function AssessmentFisiognomi({
  fisiognomi,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position = "detailApplicant",
}) {
  const data = fisiognomi?.answers?.data;
  const [modalFisiog, setModalFisiog] = useState(false);
  const [loadingBuy, setLoadingBuy] = useState(false);
  const [forbidden, setForbidden] = useState(false);

  const toggleFisiog = () => setModalFisiog(!modalFisiog);

  const karakterList = useMemo(() => {
    const arrKarakter = Object.values(data.result);
    return shuffleArray(arrKarakter);
  }, [data.result]);

  const [modalNoToken, setModalNoToken] = useState(false);
  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="fisiognomi"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={fisiognomi?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col xs="6">
              <h5 className="text-uppercase content-sub-title mb-0">
                <strong>{t("hasilfisiognomi")}</strong>
              </h5>
            </Col>
            {position !== "detailApplicant" ? (
              <Col xs="6" className="text-right">
                {!fisiognomi?.purchased && (
                  <div style={{ marginTop: "4px" }} className="mr-2 d-inline">
                    <FontAwesomeIcon
                      icon="coins"
                      className="mr-1"
                      style={{ color: "#e0bc47" }}
                    />
                    <b>{fisiognomi.tokenPrice}</b>
                  </div>
                )}
                <Button
                  className={`btn ${
                    fisiognomi?.purchased ? "button-video" : "btn-netis-color"
                  }`}
                  onClick={fisiognomi?.purchased ? toggleFisiog : toggleNoToken}
                  style={{ borderRadius: "8px" }}
                  disabled={loadingBuy}
                >
                  {fisiognomi?.purchased ? (
                    t("seeDetail")
                  ) : loadingBuy ? (
                    <>
                      <Spinner color="light" size="sm" />
                      &nbsp;&nbsp;Loading...{" "}
                    </>
                  ) : (
                    <>
                      <i className="fa fa-lock" />
                      &nbsp;&nbsp;{t("openDetail")}
                    </>
                  )}
                </Button>
              </Col>
            ) : null}
          </Row>
          <Row>
            {position === "detailApplicant" ? (
              <>
                <Col md="6" lg="6" className="mb-2">
                  <img
                    src={data?.processed ?? data?.raw}
                    width="100%"
                    alt="fisiognomi"
                  />
                </Col>
                <Col md="6" lg="6">
                  {Object.values(data.result).length > 0 ? (
                    <>
                      <div className="mb-3">
                        {t("Bentuk Wajah")} : <br />
                        <br />
                        {Object.keys(data?.result ?? {}).map(
                          (ciriWajah, idx) => (
                            <ul key={idx}>
                              <li>{t(ciriWajah)}</li>
                            </ul>
                          )
                        )}
                      </div>
                      <div className="d-flex flex-row-reverse">
                        <Button
                          className={`btn ${
                            fisiognomi?.purchased
                              ? "button-video"
                              : "btn-netis-color"
                          }`}
                          onClick={
                            fisiognomi?.purchased ? toggleFisiog : toggleNoToken
                          }
                          style={{ borderRadius: "8px" }}
                          disabled={loadingBuy}
                        >
                          {fisiognomi?.purchased ? (
                            t("seeDetail")
                          ) : loadingBuy ? (
                            <>
                              <Spinner color="light" size="sm" />
                              &nbsp;&nbsp;Loading...{" "}
                            </>
                          ) : (
                            <>
                              <i className="fa fa-lock" />
                              &nbsp;&nbsp;{t("openDetail")}
                            </>
                          )}
                        </Button>
                        {!fisiognomi?.purchased && (
                          <div style={{ marginTop: "4px" }} className="mr-2">
                            <FontAwesomeIcon
                              icon="coins"
                              className="mr-1"
                              style={{ color: "#e0bc47" }}
                            />
                            <b>{fisiognomi.tokenPrice}</b>
                          </div>
                        )}
                      </div>
                    </>
                  ) : (
                    <div className="alert alert-dark">
                      {t("Belum ada hasil fisiognomi yang tersedia")}
                    </div>
                  )}
                </Col>
              </>
            ) : (
              <Col xs="12">
                {Object.values(data.result).length > 0 ? (
                  <>
                    {position === "detailApplicant" ? (
                      <div className="mb-3">
                        {t("Bentuk Wajah")} : <br />
                        <br />
                        {Object.keys(data?.result ?? {}).map(
                          (ciriWajah, idx) =>  (
                            <ul key={idx}>
                              <li>{t(ciriWajah)}</li>
                            </ul>
                          )
                        )}
                        <div className="d-flex flex-row-reverse justify-content-end">
                          <Button
                            className={`btn ${
                              fisiognomi?.purchased
                                ? "button-video"
                                : "btn-netis-color"
                            }`}
                            onClick={
                              fisiognomi?.purchased
                                ? toggleFisiog
                                : toggleNoToken
                            }
                            style={{ borderRadius: "8px" }}
                            disabled={loadingBuy}
                          >
                            {fisiognomi?.purchased ? (
                              t("seeDetail")
                            ) : loadingBuy ? (
                              <>
                                <Spinner color="light" size="sm" />
                                &nbsp;&nbsp;Loading...{" "}
                              </>
                            ) : (
                              <>
                                <i className="fa fa-lock" />
                                &nbsp;&nbsp;{t("openDetail")}
                              </>
                            )}
                          </Button>
                          {!fisiognomi?.purchased && (
                            <div style={{ marginTop: "4px" }} className="mr-2">
                              <FontAwesomeIcon
                                icon="coins"
                                className="mr-1"
                                style={{ color: "#e0bc47" }}
                              />
                              <b>{fisiognomi.tokenPrice}</b>
                            </div>
                          )}
                        </div>
                      </div>
                    ) : fisiognomi?.purchased ? (
                      <div className="mb-3">
                        {t("karakteristik")} : <br />
                        <br />
                        {karakterList.map((ciriDetail, idx) => (
                          <ul key={idx}>
                            <li>{ciriDetail}</li>
                          </ul>
                        ))}
                      </div>
                    ) : null}
                  </>
                ) : (
                  <div className="alert alert-dark">
                    {t("Belum ada hasil fisiognomi yang tersedia")}
                  </div>
                )}
              </Col>
            )}
          </Row>
        </CardBody>
        <Modal isOpen={modalFisiog} toggle={toggleFisiog} className="modal-lg">
          <ModalHeader toggle={toggleFisiog}>
            {t("detailfisiognomi")}
          </ModalHeader>
          <ModalBody>
            {position === "detailApplicant" ? null : (
              <div className="d-flex justify-content-center align-items-center mb-3">
                <img
                  src={data?.processed ?? data?.raw}
                  width="40%"
                  alt="fisiognomi"
                />
              </div>
            )}
            {t("Bentuk Wajah")} : <br />
            <br />
            {Object.keys(data?.result ?? {}).map((ciriWajah, idx) => (
              <ul key={idx}>
                <li>{t(ciriWajah)}</li>
              </ul>
            ))}
            {t("karakteristik")} : <br />
            <br />
            {karakterList.map((ciriDetail, idx) => (
              <ul key={idx}>
                <li>{ciriDetail}</li>
              </ul>
            ))}
          </ModalBody>
        </Modal>
      </Card>
    </>
  );
}

function shuffleArray(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

export default translate(AssessmentFisiognomi);
