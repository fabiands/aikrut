import React, { useState, useRef } from 'react';
import {
    Button,
    Card,
    Spinner,
    CardBody,
    Row,
    Col,
    Modal, ModalHeader, ModalBody,
    ListGroup, ListGroupItem,
    Table, CardHeader
} from "reactstrap";
import { t, translate } from "react-switch-lang";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from '../../../../../components/ModalPrivilegeForbidden';
// import moment from "moment";

const AssessmentVideoCompare = ({ vid, candidate, id, getAPI, dataToken, can, position = 'left' }) => {
    const [firstData, ...restData] = vid?.answers?.data;
    const [modalNoToken, setModalNoToken] = useState(false);
    const [modalDetail, setModalDetail] = useState(false);
    const toggleNoToken = () => {
        if (can('canManagementToken')) {
            setModalNoToken(!modalNoToken)
        }
        else {
            setForbidden(true)
        }
    }
    const firstVideo = React.useMemo(() =>
        vid.answers.data[0] ? { resultUrl: vid.answers.data[0]?.raw } : null,
        [vid]
    );
    const firstWidya = React.useMemo(() => vid.answers.data[0].video, [vid]);
    const firstTitle = React.useMemo(() => vid.answers.data[0].title, [vid]);
    const [activeVideo, setActiveVideo] = useState(firstVideo);
    const [activeWidya, setActiveWidya] = useState(firstWidya);
    const [activeTitle, setActiveTitle] = useState(t(firstTitle));
    const [loadingBuy, setLoadingBuy] = useState(false);
    // const [hint, setHint] = useState(false)
    // const [tooltipOpen, setTooltipOpen] = useState(false);
    const [forbidden, setForbidden] = useState(false)

    const toggleDetail = () => setModalDetail(!modalDetail);

    // const toggleTooltip = () => setTooltipOpen(!tooltipOpen);

    function changeAnotation(video) {
        setActiveWidya(video.video);
        setActiveTitle(t(video.title));
        setActiveVideo(video.processed);
        setModalDetail(true)
    }

    let highestEmotion = null;
    const resultEmotion = Object.keys(
        activeVideo?.resultData?.emotion ?? {}
    ).map((label) => ({ label, value: activeVideo.resultData.emotion[label] }));
    for (const emotion of resultEmotion) {
        if (highestEmotion == null) {
            highestEmotion = emotion;
        } else if (parseFloat(emotion.value) > parseFloat(highestEmotion.value)) {
            highestEmotion = emotion;
        }
    }

    let highestEyecues = null;
    const resultEyecues = activeVideo?.resultData?.eyecues?.map((item) => ({ label: item.class, value: item.score, desc: item.description })) ?? [];

    if (resultEyecues) {
        for (const eyecues of resultEyecues) {
            if (highestEyecues == null) {
                highestEyecues = eyecues;
            } else if (eyecues.value > highestEyecues.value) {
                highestEyecues = eyecues;
            }
        }
    }

    const findEmotion = (obj) => {
        let highestEmotion = null;
        const resultEmotion = Object.keys(
            obj?.resultData?.emotion ?? {}
        ).map((label) => ({ label, value: obj.resultData.emotion[label] }));
        for (const emotion of resultEmotion) {
            if (highestEmotion == null) {
                highestEmotion = emotion;
            } else if (parseFloat(emotion.value) > parseFloat(highestEmotion.value)) {
                highestEmotion = emotion;
            }
        }

        return highestEmotion?.label;
    }

    const findEyecues = (arr) => {
        let highestEyecues = null;
        const resultEyecues = arr?.resultData?.eyecues?.map((item) => ({ label: item.class, value: item.score, desc: item.description })) ?? [];

        if (resultEyecues) {
            for (const eyecues of resultEyecues) {
                if (highestEyecues == null) {
                    highestEyecues = eyecues;
                } else if (eyecues.value > highestEyecues.value) {
                    highestEyecues = eyecues;
                }
            }
        }

        return highestEyecues?.label
    }

    return (
        <>
            {forbidden && <ModalPrivilegeForbidden isOpen={true} forbidden="canManagementToken" isClose={() => setForbidden(false)} />}
            <NoToken candidate={candidate} buy="video" nullData="assessment" isOpen={modalNoToken} toggle={toggleNoToken} isPurchase={!dataToken.balance || dataToken.isExpired ? false : true} idAssessment={id} getAPI={getAPI} isDone={(e) => setLoadingBuy(e)} type={vid?.tokenType} />

            <Card>
                <CardBody>
                    <Row className="md-company-header flex-column mb-3 mt-2">
                        <Col className="d-flex justify-content-between align-items-center">
                            <div>
                                <h5 className="text-uppercase content-sub-title mb-0">
                                    <strong>{vid.testName} {t('assessment')}</strong>
                                </h5>
                                {/* <i>{moment(vid.created_at).format("DD MMMM YYYY")}</i> */}
                            </div>
                        </Col>
                    </Row>

                    <div className="vid-flex">
                        <div className={`col-table-video w-100`}>
                            {vid.purchased ? null
                                :
                                <div className="lock-token text-center" style={{top:'110px'}}>
                                    <i className="fa fa-lock lock-icon" aria-hidden="true" /><br />
                                    <div>
                                        <span style={{ fontSize: "16pt" }}><b>{t('openNextVideo')}</b></span><br />
                                        <span>{t('clickOpenDetail')}</span><br />
                                        {vid.tokenPriceSecond !== 0 ?
                                            <div style={{ position: "relative" }}>
                                                <div style={{ position: "absolute", top: "50%", width: 45, height: 2, background: "#ff0200", left: "calc(50% - 22px)", transform: "rotate(-15deg)" }}></div>
                                                <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#676767" }} />
                                                <b>{vid.tokenPriceSecond}</b><br />
                                            </div>
                                            :
                                            ''
                                        }
                                        <FontAwesomeIcon icon="coins" className="mr-1" style={{ color: "#e0bc47" }} />
                                        <b>{vid.tokenPrice}</b><br />
                                        <Button
                                            disabled={loadingBuy}
                                            className="btn btn-netis-color mb-2 mr-2"
                                            onClick={toggleNoToken} style={{ borderRadius: "8px" }}
                                        >
                                            {
                                                loadingBuy ? <><Spinner color="light" size="sm" />&nbsp;&nbsp;Loading... </>
                                                    :
                                                    <>
                                                        <i className="fa fa-lock mr-1" />
                                                        {t('openDetail')}
                                                    </>
                                            }
                                        </Button>
                                    </div>
                                </div>
                            }
                            <Table responsive hover className={`border border-secondary border-bottom ${firstData.processed ? `video-table-compare-processed` : `video-table-compare `}`}>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th className={`text-center ${firstData.processed ? `w-75` : `w-50`}`}>{t('videoName')}</th>
                                        <th className="text-center">
                                            {/* <Button onClick={() => setHint(!hint)} className="text-nowrap" style={{ backgroundColor: "transparent", border: "transparent" }} id="TooltipExample">
                                                <i className="fa fa-lg fa-question-circle text-primary" />
                                            </Button>
                                            <Tooltip placement="bottom" isOpen={tooltipOpen} target="TooltipExample" toggle={toggleTooltip}>
                                                Jika tombol "Dengan Anotasi" tidak muncul, kemungkinan video wawancara tersebut tidak terekam dengan baik atau
                                                Pelamar tidak melakukan wawancara dengan benar.
                                            </Tooltip> */}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            {firstData?.processed !== null ?
                                                <i className="fa fa-check-circle-o text-success ml-1" />
                                                :
                                                <i className="fa fa-spinner text-info ml-1" />
                                            }
                                        </td>
                                        <td>1. {t(firstData.title)}<br />
                                            {(vid.purchased && firstData.processed) ? <Row className="mt-1 mb-0">
                                                <Col lg="6" className="my-1 my-lg-0" style={{paddingLeft:'5px', paddingRight:'5px'}}>
                                                    <Card>
                                                        <CardHeader className="py-1 text-center">Emotion</CardHeader>
                                                        <CardBody className="py-1 text-center">
                                                            {findEmotion(firstData.processed)}
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                                <Col lg="6" className="my-lg-0" style={{paddingLeft:'5px', paddingRight:'5px'}}>
                                                    <Card>
                                                        <CardHeader className="py-1 text-center">Eyecues</CardHeader>
                                                        <CardBody className="py-1 text-center">
                                                            {findEyecues(firstData.processed)}
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row> : null}
                                        </td>
                                        <td className="text-center align-items-center">
                                            {firstData.processed !== null ?
                                                <Button className={`text-center text-nowrap button-asesmen ml-2 my-auto`}
                                                    onClick={() => changeAnotation(firstData)}
                                                // style={{width:125, height: 38}}
                                                >
                                                    {t('seeDetail')}
                                                </Button>
                                                :
                                                <small className="text-danger">{t('analyzing')}</small>
                                            }
                                        </td>
                                    </tr>
                                    {restData &&
                                        restData.map((video, idx) => (
                                            <tr key={idx + 1} >
                                                <td>
                                                    {vid.purchased ?
                                                        video.processed !== null ?
                                                            <>
                                                                <i className="fa fa-check-circle-o text-success ml-1" id="process" />
                                                            </>
                                                            :
                                                            <i className="fa fa-spinner text-info ml-1" />
                                                        :
                                                        <i className="fa fa-video-camera ml-1" />
                                                    }
                                                </td>
                                                <td>
                                                    {idx + 2}. {t(video.title)}<br />
                                                    {(vid.purchased && video.processed) ? <Row className="mt-1 mb-0">
                                                        <Col lg="6" className="my-1 my-lg-0" style={{paddingLeft:'5px', paddingRight:'5px'}}>
                                                            <Card>
                                                                <CardHeader className="py-1 text-center">Emotion</CardHeader>
                                                                <CardBody className="py-1 text-center">
                                                                    {findEmotion(video.processed)}
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        <Col lg="6" className="my-lg-0" style={{paddingLeft:'5px', paddingRight:'5px'}}>
                                                            <Card>
                                                                <CardHeader className="py-1 text-center">Eyecues</CardHeader>
                                                                <CardBody className="py-1 text-center">
                                                                    {findEyecues(video.processed)}
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                    </Row> : null}
                                                </td>
                                                <td className="text-center">
                                                    {video.processed !== null ?
                                                        <Button className={`text-center text-nowrap button-asesmen ml-2`}
                                                            onClick={() => changeAnotation(video)}
                                                            disabled={!vid.purchased}
                                                        >
                                                            {t('seeDetail')}
                                                        </Button>
                                                        : <small className="text-danger">{t('analyzing')}</small>
                                                    }
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </Table>
                        </div>
                    </div>
                    <ModalDetail activeVideo={activeVideo} activeTitle={activeTitle} activeWidya={activeWidya} highestEmotion={highestEmotion} highestEyecues={highestEyecues} toggle={toggleDetail} isOpen={modalDetail} position={position} />
                </CardBody>
            </Card>
        </>
    );
};

const ModalDetail = ({ activeVideo, highestEmotion, highestEyecues, activeTitle, activeWidya, isOpen, toggle, position }) => {
    const vidRef = useRef(null);
    const vidRef2 = useRef(null);
    const [showButton, setShowButton] = useState(true);
    const [showButton2, setShowButton2] = useState(true);

    const toggleModal = () => toggle(!isOpen);

    const playVid = () => {
        vidRef2.current.pause();
        vidRef.current.play();
        setShowButton(false);
    };

    const playVid2 = () => {
        vidRef.current.pause();
        vidRef2.current.play();
        setShowButton2(false);
    };

    const pauseVid2 = () => {
        vidRef2.current.pause();
        setShowButton2(true);
    };

    return (
        <>
            <Modal modalClassName={`modal-custom ${position === 'left' ? 'left' : 'right'}`} backdropClassName="modal-custom" isOpen={isOpen} className={`shadow ${position === 'left' ? 'left' : 'right'}`}>
                <ModalHeader toggle={toggleModal} className="border-bottom-0">
                    {activeTitle}
                </ModalHeader>
                <ModalBody>
                    <div className="mb-5 mr-3">
                        <div className="vidwrapper">
                            <video
                                ref={vidRef}
                                className="vid"
                                src={activeVideo.resultUrl}
                                id="dataVid"
                                controls
                                onPlaying={() => setShowButton(false)}
                                onPause={() => setShowButton(true)}
                            />
                            {showButton ? (
                                <>
                                    <Button className="play" onClick={playVid}>
                                        <i className="fa fa-2x fa-align-center fa-play"></i>
                                    </Button>
                                    <div className="vidwrapperWidya">
                                        <video
                                            ref={vidRef2}
                                            className="smallVid"
                                            src={activeWidya}
                                            onPlaying={() => setShowButton2(false)}
                                            onPause={() => setShowButton2(true)}
                                        />
                                        {showButton2 ? (
                                            <>
                                                {t("Pertanyaan")} :&nbsp;
                                                            <Button
                                                    className="play2"
                                                    onClick={playVid2}
                                                >
                                                    <i className="fa fa-sm fa-align-center fa-play"></i>
                                                </Button>
                                            </>
                                        ) : (
                                            <Button
                                                className="pause"
                                                onClick={pauseVid2}
                                            >
                                                <i className="fa fa-sm fa-align-center fa-pause"></i>
                                            </Button>
                                        )}
                                    </div>
                                </>
                            ) : null}
                        </div>

                        {(highestEmotion || highestEyecues) && (
                            <>
                                {highestEmotion && (
                                    <ListGroup>
                                        <ListGroupItem className="anotation-list py-2">
                                            <Row style={{ marginLeft: "-6px" }} className="pr-lg-2">
                                                <Col xs="8" md="9" className="anotation-name py-2">
                                                    {highestEmotion.label}
                                                </Col>
                                                <Col xs="4" md="3" className="anotation-value text-center py-2">
                                                    {highestEmotion.value}
                                                </Col>
                                            </Row>
                                        </ListGroupItem>
                                    </ListGroup>
                                )}
                                {highestEyecues && (
                                    <ListGroup>
                                        <ListGroupItem className="anotation-list my-2">
                                            <Row style={{ marginLeft: "-6px" }} className="pr-lg-2">
                                                <Col xs="8" md="9" className="anotation-name py-2">
                                                    {highestEyecues.label}
                                                </Col>
                                                <Col xs="4" md="3" className="anotation-value text-center py-2">
                                                    {highestEyecues.value}
                                                </Col>
                                            </Row>
                                        </ListGroupItem>
                                    </ListGroup>
                                )}
                                {highestEyecues && (
                                    <div>
                                        <h4 className="text-capitalize">{highestEyecues.label.split("-").join(" ")}</h4>
                                        <span>{highestEyecues.desc}</span>
                                    </div>
                                )}
                            </>
                        )
                        }
                    </div>
                    <Button color="netis-primary" onClick={toggleModal} style={{ width: "100px" }}>
                        {t('tutup')}
                    </Button>
                </ModalBody>
            </Modal>
        </>
    )
}
export default translate(AssessmentVideoCompare);