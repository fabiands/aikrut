import React, { useState } from "react";
import {
  Button,
  Card,
  Spinner,
  CardBody,
  Row,
  Col,
  // Modal,
  // ModalBody,
  // ModalHeader,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  Collapse,
} from "reactstrap";
// import request from "../../../../../utils/request";
// import { toast } from "react-toastify";
import { t, translate } from "react-switch-lang";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import NoToken from "./NoToken";
import ModalPrivilegeForbidden from "../../../../../components/ModalPrivilegeForbidden";
import CardFooter from "reactstrap/lib/CardFooter";

function AssessmentFingerprint({
  data,
  candidate,
  id,
  getAPI,
  dataToken,
  can,
  position
}) {
  const [loadingBuy, setLoadingBuy] = useState(false);
  // const [modalUnlock, setModalUnlock] = useState(false)
  const [collapse, setCollapse] = useState(0);
  const [forbidden, setForbidden] = useState(false);
  const toggleResult = (value) => {
    setCollapse(collapse === Number(value) ? null : Number(value));
  };

  // const toggleUnlock = function () {
  //     if (can('canManagementToken')) {
  //         setModalUnlock(!modalUnlock)
  //     }
  //     else {
  //         setForbidden(true)
  //     }
  // }

  const [modalNoToken, setModalNoToken] = useState(false);
  const toggleNoToken = () => {
    if (can("canManagementToken")) {
      setModalNoToken(!modalNoToken);
    } else {
      setForbidden(true);
    }
  };

  return (
    <>
      {forbidden && (
        <ModalPrivilegeForbidden
          isOpen={true}
          forbidden="canManagementToken"
          isClose={() => setForbidden(false)}
        />
      )}
      <NoToken
        candidate={candidate}
        buy="fingerprint"
        nullData="assessment"
        isOpen={modalNoToken}
        toggle={toggleNoToken}
        isPurchase={!dataToken.balance || dataToken.isExpired ? false : true}
        idAssessment={id}
        getAPI={getAPI}
        isDone={(e) => setLoadingBuy(e)}
        type={data?.tokenType}
      />
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col className="d-flex flex-column">
              <h5 className="text-uppercase content-sub-title mb-2">
                <strong>{t("fingerprint")}</strong>
              </h5>
              <ListGroup className="my-2">
                <ListGroupItem
                  className="outline-netis-primary mb-1"
                  style={{ borderRadius: 12 }}
                >
                  <ListGroupItemHeading
                    className="d-flex justify-content-between mb-0"
                    id={data.results[0].title + 0}
                    onClick={() => {
                      toggleResult(0);
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    {data.results[0].title}
                    {collapse === 0 ? (
                      <i className="mt1 fa-sm fa fa-chevron-up" />
                    ) : (
                      <i className="mt1 fa-sm fa fa-chevron-down" />
                    )}
                  </ListGroupItemHeading>
                </ListGroupItem>

                <Collapse
                  isOpen={collapse === 0}
                  className="bg-white border mb-2"
                  style={{ borderRadius: 12 }}
                >
                  <Row>
                    <Col md={position === 'comparison' ? '12' : '6'} lg={position === 'comparison' ? '12' : '5'} className="pt-4">
                      {data.results[0]?.finger && 
                        (() => {
                          const finger = data.results[0].finger.split('_')[0]
                          return (
                            <div className="fingerprint-img py-auto">
                              <Card>
                                <CardBody className="text-center p-2">
                                  <img src={require(`../../../../../assets/img/fingerprint/${data.results[0].type.type_left}.png`)} alt="left" className="left-fingerprint" />
                                </CardBody>
                                <CardFooter className="border-top-0 text-center">
                                  {t(`${finger}_left`)}
                                </CardFooter>
                              </Card>
                              <Card>
                                <CardBody className="text-center p-2">
                                  <img src={require(`../../../../../assets/img/fingerprint/${data.results[0].type.type_right}.png`)} alt="right" />
                                </CardBody>
                                <CardFooter className="border-top-0 text-center">
                                  {t(`${finger}_right`)}
                                </CardFooter>
                              </Card>
                            </div>
                          )
                      })()}
                    </Col>
                    <Col md={position === 'comparison' ? '12' : '6'} lg={position === 'comparison' ? '12' : '7'} className="pl-0">
                      <ul className="my-3">
                        {data.results[0].list && 
                          data.results[0].list.map((list, i) => (
                            <li key={i}>{list}</li>
                        ))}
                      </ul>
                    </Col>
                  </Row>
                
                </Collapse>

                <div className="fingerprint">
                  {!data.purchased ? (
                    <div className="lock-fingerprint text-center">
                      <div className="lock-fingerprint-text">
                        <i
                          className="fa fa-lock lock-icon"
                          aria-hidden="true"
                        />
                        <br />
                        <span style={{ fontSize: "12pt" }}>
                          <b>{t("wantSeeFinger")}</b>
                        </span>
                        <br />
                        <span>{t("clickOpenDetail")}</span>
                        <br />
                        <FontAwesomeIcon
                          icon="coins"
                          className="mr-1"
                          style={{ color: "#e0bc47" }}
                        />
                        <b>{data?.tokenPrice ?? 1}</b>
                        <br />
                        <Button
                          disabled={loadingBuy}
                          className={`btn ${
                            data?.purchased ? "button-video" : "btn-netis-color"
                          } mb-2 mr-2`}
                          onClick={toggleNoToken}
                          style={{ borderRadius: "8px" }}
                        >
                          {loadingBuy ? (
                            <>
                              <Spinner color="light" size="sm" />
                              &nbsp;&nbsp;Loading...{" "}
                            </>
                          ) : (
                            <>
                              <i className="fa fa-lock mr-1" />
                              {t("openDetail")}
                            </>
                          )}
                        </Button>
                      </div>
                    </div>
                  ) : null}
                  {data.results.map((finger, i) =>
                    i === 0 ? (
                      ""
                    ) : (
                      <div key={i}>
                        <ListGroupItem
                          className="outline-netis-primary mb-1"
                          style={{ borderRadius: 12 }}
                        >
                          <ListGroupItemHeading
                            className="d-flex justify-content-between mb-0"
                            id={finger.title + i}
                            onClick={() => {
                              toggleResult(i);
                            }}
                            style={{ cursor: "pointer" }}
                          >
                            {finger.title}
                            {collapse === i ? (
                              <i className="mt1 fa-sm fa fa-chevron-up" />
                            ) : (
                              <i className="mt1 fa-sm fa fa-chevron-down" />
                            )}
                          </ListGroupItemHeading>
                        </ListGroupItem>

                        <Collapse
                          isOpen={collapse === i}
                          className="bg-white border mb-2"
                          style={{ borderRadius: 12 }}
                        >
                          <Row>
                            <Col md={position === 'comparison' ? '12' : '6'} lg={position === 'comparison' ? '12' : '5'} className="pt-4">
                              {finger.finger && 
                                (() => {
                                  const fingerType = finger.finger.split('_')[0]
                                  return (
                                    <div className="fingerprint-img py-auto">
                                      <Card>
                                        <CardBody className="text-center p-2">
                                          <img src={require(`../../../../../assets/img/fingerprint/${finger.type.type_left}.png`)} alt="left" className="left-fingerprint" />
                                        </CardBody>
                                        <CardFooter className="border-top-0 text-center">
                                          {t(`${fingerType}_left`)}
                                        </CardFooter>
                                      </Card>
                                      <Card>
                                        <CardBody className="text-center p-2">
                                          <img src={require(`../../../../../assets/img/fingerprint/${finger.type.type_right}.png`)} alt="right" />
                                        </CardBody>
                                        <CardFooter className="border-top-0 text-center">
                                          {t(`${fingerType}_right`)}
                                        </CardFooter>
                                      </Card>
                                    </div>
                                  )
                              })()}
                            </Col>
                            <Col md={position === 'comparison' ? '12' : '6'} lg={position === 'comparison' ? '12' : '7'} className="pl-0">
                              <ul className="my-3">
                                {finger.list && 
                                  finger.list.map((list, i) => (
                                    <li key={i}>{list}</li>
                                ))}
                              </ul>
                            </Col>
                          </Row>
                        </Collapse>
                      </div>
                    )
                  )}
                </div>
              </ListGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
}

export default translate(AssessmentFingerprint);
