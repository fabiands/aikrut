import React, { useState, useCallback, memo } from "react";
import { Button, Modal, Spinner } from "reactstrap";
import html2canvas from "html2canvas";
import { BlobProvider } from "@react-pdf/renderer";
import { useGeneratePdf } from "./ApplicantContext";
import ApplicantPdf from "./ApplicantPdf";
import Loader from "react-loader-spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import disableScroll from "disable-scroll";

export default memo(({ data, dataToken, onDownload }) => {
  const [generatePdf, setGeneratePdf] = useGeneratePdf();
  const [dataLoading, setDataLoading] = useState(true);
  const [downloadLoading, setDownloadLoading] = useState(false);

  const scrollToTop = () => {
    // disableScroll.on()
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  };

  const handleDownload = useCallback(() => {
    window.scrollTo({ top: 0, left: 0 });
    setDownloadLoading(true);
    disableScroll.on();
    const mbti = window.document.getElementsByClassName("pdf-mbti")[0];
    const papi = window.document.getElementsByClassName("pdf-papikostick")[0];
    const disc = window.document.getElementsByClassName("pdf-disc")[0];
    const msdt = window.document.getElementsByClassName("pdf-msdt")[0];
    const spm = window.document.getElementsByClassName("pdf-spm")[0];
    const businessInsight = window.document.getElementsByClassName(
      "pdf-business-insight"
    )[0];
    const agility = window.document.getElementsByClassName("pdf-agility")[0];
    const agility2 = window.document.getElementsByClassName("pdf-agility2")[0];
    const agility3 = window.document.getElementsByClassName("pdf-agility3")[0];

    Promise.all([
      mbti,
      papi,
      disc,
      msdt,
      spm,
      businessInsight,
      agility,
      agility2,
      agility3,
    ])
      .then(
        async ([
          mbtiGraph,
          papiGraph,
          discGraph,
          msdtGraph,
          spmGraph,
          businessInsightGraph,
          agilityGraph,
          agilityGraph2,
          agilityGraph3,
        ]) => {
          if (mbtiGraph) {
            await html2canvas(mbtiGraph)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({ ...state, mbti: imgData }));
              })
              .catch((err) => console.log(err))
          }

          if (papiGraph) {
            await html2canvas(papiGraph)
              .then((canvas) => {
                console.log(canvas)
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({ ...state, papikostick: imgData }));
              })
              .catch((err) => console.log(err))
          }

          if (discGraph) {
            await html2canvas(discGraph)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({ ...state, disc: imgData }));
              })
              .catch((err) => console.log(err))
          }

          if (msdtGraph) {
            await html2canvas(msdtGraph)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({ ...state, msdt: imgData }));
              })
              .catch((err) => console.log(err))
          }

          if (spmGraph) {
            await html2canvas(spmGraph)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({ ...state, spm: imgData }));
              })
              .catch((err) => console.log(err));
          }

          if (businessInsightGraph) {
            await html2canvas(businessInsightGraph)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({
                  ...state,
                  businessInsight: imgData,
                }));
              })
              .catch((err) => console.log(err));
          }

          if (agilityGraph) {
            await html2canvas(agilityGraph)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({
                  ...state,
                  agility: imgData,
                }));
              })
              .catch((err) => console.log(err));
          }

          if (agilityGraph2) {
            await html2canvas(agilityGraph2)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({
                  ...state,
                  agility2: imgData,
                }));
              })
              .catch((err) => console.log(err));
          }

          if (agilityGraph3) {
            await html2canvas(agilityGraph3)
              .then((canvas) => {
                const imgData = canvas.toDataURL("image/png");
                setGeneratePdf((state) => ({
                  ...state,
                  agility3: imgData,
                }));
              })
              .catch((err) => console.log(err));
          }

          await onDownload(false)
        }
      )
      .finally(() => setDataLoading(false));
  }, [setGeneratePdf, onDownload]);

  return (
    <>
      {/* <PDFViewer style={{ width: '100%', height: 1000 }}>
                <ApplicantPdf data={data} generatePdf={generatePdf} />
            </PDFViewer> */}
      <Button
        color="netis-color"
        onClick={() => {
          scrollToTop();
          setDownloadLoading(true);
          disableScroll.on();
          onDownload(true)
          setTimeout(() => handleDownload(), 5000);
        }}
        disabled={downloadLoading}
        className="float-right"
      >
        {downloadLoading ? (
          <>
            <Spinner color="light" size="sm" className="mr-1" /> Downloading..
          </>
        ) : (
          <>
            <FontAwesomeIcon icon="file-download" className="mr-1" /> Download
            Report Assessment
          </>
        )}
      </Button>
      {!dataLoading && (
        <BlobProvider
          document={
            <ApplicantPdf
              data={data}
              generatePdf={generatePdf}
              dataToken={dataToken}
            />
          }
        >
          {({ blob, url, loading, error }) => {
            if (loading) {
              return null;
            }
            if (!loading && url) {
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", 'Assessment Report ' + data.detail.fullName + '.pdf');
              document.body.appendChild(link);
              link.click();
              link.parentNode.removeChild(link);
              setDataLoading(true);
              setDownloadLoading(false);
              disableScroll.off();
              return null;
            }
            if (error) {
              setDownloadLoading(false);
              console.error(error);
              return <p>An error occurred</p>;
            }
            return null;
          }}
        </BlobProvider>
      )}
      <Modal
        isOpen={downloadLoading}
        size="lg"
        centered
        className="text-light"
        modalClassName="modal-downloading"
        backdropClassName="modal-downloading"
      >
        <div className="loading">
          <div className="d-block text-center">
            <Loader
              color="#fff"
              secondaryColor="#fff"
              type="Rings"
              height={150}
              width={150}
              radius={1}
            />
            <p
              className="mt-3 text"
              style={{ fontSize: "12pt", color: "#fff", fontWeight: "bold" }}
            >
              Downloading...
            </p>
          </div>
        </div>
      </Modal>
    </>
  );
});
