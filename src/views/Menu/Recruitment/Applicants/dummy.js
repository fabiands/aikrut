export default {
  video: [
    {
      "id": "1741",
      "result": "done",
      "testName": "video",
      "scores": null,
      "answers": {
        "data": [
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/A2.mp4",
            "code": "q1",
            "section": "2",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/AGhK06xDEQN5z1OpDoXeXY8Tm1UExoXg/114235.webm",
            "start_video": "2020-12-02 11:41:01",
            "end_video": "2020-12-02 11:41:25",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/tRKZtW3E26_0JIc1VPrRARWJ-mnmXwPH/134642.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "12.61",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "0",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "44.59",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "18.02",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "22.07",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "2.25",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "0",
                  "Disgust": "0",
                  "Scared": "9",
                  "Happy": "28",
                  "Sad": "9",
                  "Surprised": "54"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/f7W1ZYq1bus-RRfcHRAfyyNgLOeHDic8/163600.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "34.4",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "61.69",
                  "menggaruk-leher-satu-tangan": "0.23",
                  "menarik-kerah-baju": "0.23",
                  "jari-di-mulut": "0",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0.23",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "1.37",
                  "menyentuh-telinga-dengan-satu-tangan": "1.84"
                }
              }
            },
            "title": "Perkenalan diri"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/D7.mp4",
            "code": "q7",
            "section": "3",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/t652c0w9wouGdnPv_mvjh6odV7qszIPG/114407.webm",
            "start_video": "2020-12-02 11:42:26",
            "end_video": "2020-12-02 11:43:13",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/XErstvMHZnD9NA7XyS3ZDorflRgMrQdF/134746.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "7.85",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "1.6",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "53.78",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "9.16",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "27.47",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "12.97",
                  "Disgust": "0.16",
                  "Scared": "17.25",
                  "Happy": "4.51",
                  "Sad": "0.55",
                  "Surprised": "64.56"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/DXk9W6QMXmt26CKAP8GiX1wfG8DURIxt/163721.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "37.17",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "43.92",
                  "menggaruk-leher-satu-tangan": "0.16",
                  "menarik-kerah-baju": "18.58",
                  "jari-di-mulut": "0",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0.16"
                }
              }
            },
            "title": "Motivasi"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/B6.mp4",
            "code": "q6",
            "section": "4",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/gUIPTPg0RomgAtyqcFyLRCNr29SfpfIn/114532.webm",
            "start_video": "2020-12-02 11:44:15",
            "end_video": "2020-12-02 11:44:45",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/rCmwefi1QShHc_cDpnVRcO-OGf9wNA8r/134835.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "18",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "2",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "37.78",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "19.11",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "22.67",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0.22",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "0",
                  "Disgust": "0",
                  "Scared": "9.8",
                  "Happy": "11.76",
                  "Sad": "16.99",
                  "Surprised": "61.44"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/eMuUUwu3Yt0oHbxlUHy74QyPj64kg2Q8/163809.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "24.9",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "21.56",
                  "menggaruk-leher-satu-tangan": "6.69",
                  "menarik-kerah-baju": "42.02",
                  "jari-di-mulut": "3.72",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "1.12",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0"
                }
              }
            },
            "title": "Sistematika Kerja"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/C6.mp4",
            "code": "q6",
            "section": "5",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/zg05g0_xe80wnPZG9Tq-EgrLArdkVkbJ/114750.webm",
            "start_video": "2020-12-02 11:45:39",
            "end_video": "2020-12-02 11:46:36",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/gJj5mWb5f1VnsnQEHFaNCLp_AVZU63CF/134947.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "33.11",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "2.87",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "41.72",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "7.06",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "15.12",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "1.92",
                  "Disgust": "0",
                  "Scared": "9.62",
                  "Happy": "17.31",
                  "Sad": "61.54",
                  "Surprised": "9.62"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/aYLkYf3Qty2wMOWxZeNdptGhxsVoqPhg/163931.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "26.93",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "6.45",
                  "menggaruk-leher-satu-tangan": "6.59",
                  "menarik-kerah-baju": "58.66",
                  "jari-di-mulut": "1.38",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0"
                }
              }
            },
            "title": "Daya tahan terhadap tugas"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/F4.mp4",
            "code": "q4",
            "section": "6",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/P-4rWFIOCAlUKZ9BUkvC_C8EoWg8V6IA/115034.webm",
            "start_video": "2020-12-02 11:47:51",
            "end_video": "2020-12-02 11:49:04",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/O5jcykF2b9JT0REvSeVGNF-aJHLHmPng/135114.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "29.5",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "1.5",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "37.92",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "17.98",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "12.79",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0.23",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "8.95",
                  "Disgust": "1.03",
                  "Scared": "22.55",
                  "Happy": "11.88",
                  "Sad": "29.6",
                  "Surprised": "25.99"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/RbTmqJ02tIuhgKpHlfzHMplnba-cKbci/164118.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "21.19",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "38.2",
                  "menggaruk-leher-satu-tangan": "2.79",
                  "menarik-kerah-baju": "37.82",
                  "jari-di-mulut": "0",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0"
                }
              }
            },
            "title": "Berfikir Analitis"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/G2.mp4",
            "code": "q2",
            "section": "7",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/c3H0HcEHf6EqTvhzV3Z1hvPrSNRPWvhv/115117.webm",
            "start_video": "2020-12-02 11:50:27",
            "end_video": "2020-12-02 11:51:00",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/bLyqx5WK3Rs8DLk-17FRiSHyPV_P_kFT/135206.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "44.06",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "2.41",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "13.6",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "28.23",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "8.61",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "2.93",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "2.08",
                  "Disgust": "0",
                  "Scared": "21.96",
                  "Happy": "9.5",
                  "Sad": "27",
                  "Surprised": "39.47"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/r7VvDkklCevZUUncsXpgNvvEIVI7YqU-/164208.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "11.58",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "0",
                  "menggaruk-leher-satu-tangan": "20.26",
                  "menarik-kerah-baju": "64.3",
                  "jari-di-mulut": "3.86",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0"
                }
              }
            },
            "title": "Kepemimpinan"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/E5.mp4",
            "code": "q5",
            "section": "8",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/jE8QLMKInqmfd4En41a5StyFlmNIxNsz/115220.webm",
            "start_video": "2020-12-02 11:51:16",
            "end_video": "2020-12-02 11:52:05",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/CmPXtJ44Vdw-VXXDkNmvHcbXmm1Kohgn/135312.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "45.02",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "0.39",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "26",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "20.18",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "8.28",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "5.79",
                  "Disgust": "1.65",
                  "Scared": "24.79",
                  "Happy": "8.26",
                  "Sad": "18.6",
                  "Surprised": "40.91"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/wsLXDBSnE9ozVjFuIlLiubhbpU6GdTHC/164315.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "14.69",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "12.55",
                  "menggaruk-leher-satu-tangan": "23.19",
                  "menarik-kerah-baju": "45.97",
                  "jari-di-mulut": "3.19",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0.42"
                }
              }
            },
            "title": "Kerjasama"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/H1.mp4",
            "code": "q1",
            "section": "9",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/XpS-9U-gKYTmzzap6KYQcswBNEtoUwUs/115415.webm",
            "start_video": "2020-12-02 11:52:26",
            "end_video": "2020-12-02 11:53:54",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/gOsX-lbT1HW01tWhbZ6qD1BjYunCeFwF/135451.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "38.79",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "2.4",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "26.22",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "14.56",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "17.78",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0.17",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "11.48",
                  "Disgust": "6.56",
                  "Scared": "21.31",
                  "Happy": "6.56",
                  "Sad": "11.48",
                  "Surprised": "42.62"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/l7bQoXLpgYelRE7n7-Q7KbSnvVQO106R/164508.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "26.15",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "9.27",
                  "menggaruk-leher-satu-tangan": "8.11",
                  "menarik-kerah-baju": "53.09",
                  "jari-di-mulut": "3.38",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0"
                }
              }
            },
            "title": "Keterampilan"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/I8.mp4",
            "code": "q8",
            "section": "10",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/JHo9vJRLiy0sdLlZ3WXzuuZEM97s6bvO/115501.webm",
            "start_video": "2020-12-02 11:54:10",
            "end_video": "2020-12-02 11:54:48",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/_-CgUDXFG-mnEbf2Skyd5UtZgb1Jp4AW/135547.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "55.1",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "2.94",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "10",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "19.02",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "12.55",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "0.2",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "5.88",
                  "Disgust": "0",
                  "Scared": "11.76",
                  "Happy": "35.29",
                  "Sad": "0",
                  "Surprised": "47.06"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/mSKs4jB7VT5uaiPHHGEujbib8UjB_lDh/164607.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "40.63",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "2.71",
                  "menggaruk-leher-satu-tangan": "3.61",
                  "menarik-kerah-baju": "47.64",
                  "jari-di-mulut": "3.38",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0.67",
                  "menyentuh-telinga-dengan-satu-tangan": "1.36"
                }
              }
            },
            "title": "Pengembangan Diri"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/K1.mp4",
            "code": "q1",
            "section": "11",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/DxwuLcz8pUvnH57mtTQQ2M2WvHuFr63B/115621.webm",
            "start_video": "2020-12-02 11:55:10",
            "end_video": "2020-12-02 11:55:49",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/yVZWnPjviTEQIp2lCnaB2rmOJnZDRAc8/135644.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "32.1",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "0",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "46.86",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "2.21",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "17.16",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "1.48",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "13.04",
                  "Disgust": "0",
                  "Scared": "21.74",
                  "Happy": "4.35",
                  "Sad": "13.04",
                  "Surprised": "47.83"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/qjQYUSf5qrKUfFqiLl7diyjewmVm2b1i/164709.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "78.63",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "0",
                  "menggaruk-leher-satu-tangan": "0.62",
                  "menarik-kerah-baju": "20.44",
                  "jari-di-mulut": "0.31",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "0",
                  "menyentuh-telinga-dengan-satu-tangan": "0"
                }
              }
            },
            "title": "Komitmen"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/L1.mp4",
            "code": "q1",
            "section": "12",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/r-9YH7YM0vVBmg8KYoHpVThiBquZRLBB/115737.webm",
            "start_video": "2020-12-02 11:56:05",
            "end_video": "2020-12-02 11:57:26",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/YpD2E2hNGrzLLDKj_gHumEBuPkVDqLt0/135816.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "40.62",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "0.15",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "13.74",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "7.85",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "36.56",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "1.02",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "18.18",
                  "Disgust": "0",
                  "Scared": "28.79",
                  "Happy": "3.03",
                  "Sad": "24.24",
                  "Surprised": "25.76"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/NAVnWKldQyeRJy4kwCvhSbWTldsxt9lS/164900.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "40.25",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "0",
                  "menggaruk-leher-satu-tangan": "2.33",
                  "menarik-kerah-baju": "55.17",
                  "jari-di-mulut": "0.35",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "0",
                  "menutup-mulut-satu-tangan": "0.1",
                  "menutup-mulut-dan-hidung": "0.35",
                  "menyentuh-hidung": "0",
                  "menggosok-mata-satu-tangan": "1.35",
                  "menyentuh-telinga-dengan-satu-tangan": "0.1"
                }
              }
            },
            "title": "Kejelasan Visi"
          },
          {
            "type": "web",
            "video": "https://dev.skillana.id/files/video/M3.mp4",
            "code": "q3",
            "section": "13",
            "raw": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/vvbEs8qj3N0WkJcp7Y1g7cgnMzl3RkgZ/115811.webm",
            "start_video": "2020-12-02 11:57:39",
            "end_video": "2020-12-02 11:58:08",
            "processed": {
              "resultUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/sO-cKaONHV2CqTTnonWybomnz63PZe9F/135905.mp4",
              "resultData": {
                "eyecues": [
                  {
                    "class": "Constructed-Visual",
                    "score": "31.67",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan berupaya untuk membayangkan sesuatu yang belum pernah subjek lihat sebelumnya atau sedang mengkombinasikan informasi baru yang subjek dapatkan secara visual."
                  },
                  {
                    "class": "Remember-Visual",
                    "score": "2.86",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan subjek sedang berupaya untuk mengingat pengalaman yang pernah subjek lewati dimasa lampau."
                  },
                  {
                    "class": "Constructed-Auditory",
                    "score": "4.76",
                    "description": "Gerakan mata yang ditunjukkan subjek saat menanggapi pertanyaan aspek ini menunjukkan kecenderungan bahwa subjek sedang membayangkan seperti apa sesuatu akan seharusnya terdengar."
                  },
                  {
                    "class": "Remember-Auditory",
                    "score": "42.62",
                    "description": "Saat menanggapi pertanyaan dalam aspek ini, gerakan mata subjek banyak menunjukkan kecenderungan dimana subjek sedang berupaya mengingat pengalamannya dimasa lampau berdasarkan dengan suara yang pernah subjek dengar sebelumnya."
                  },
                  {
                    "class": "Kinesthetic",
                    "score": "15.24",
                    "description": "Dalam aspek ini, subjek menunjukkan kecenderungan untuk merespon emosi ataupun perasaan yang sedang subjek rasakan saat itu juga."
                  },
                  {
                    "class": "Auditory-Digital",
                    "score": "2.62",
                    "description": "Gerakan mata yang ditunjukkan menunjukkan kecenderungan self-talk dimana subjek berupaya untuk berbicara dengan dirinya sendiri sebelum menyampaikan sebuah informasi kepada pihak lain."
                  }
                ],
                "emotion": {
                  "Angry": "45.78",
                  "Disgust": "1.2",
                  "Scared": "14.46",
                  "Happy": "4.82",
                  "Sad": "0",
                  "Surprised": "33.73"
                }
              }
            },
            "gesture": {
              "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/sHL8wFBgqdLsOfugFrT8InGEEUu7sFvV/164943.mp4",
              "resultData": {
                "gesture": {
                  "gelengan": "37",
                  "kepala-menunduk": "0",
                  "mengangguk": "0",
                  "mengangkat-alis": "0",
                  "menggaruk-leher-satu-tangan": "1.75",
                  "menarik-kerah-baju": "51.74",
                  "jari-di-mulut": "4.25",
                  "menyangga-kepala": "0",
                  "mengusap-dagu": "0",
                  "menepuk-dahi": "2",
                  "menutup-mulut-satu-tangan": "0",
                  "menutup-mulut-dan-hidung": "0.25",
                  "menyentuh-hidung": "0.51",
                  "menggosok-mata-satu-tangan": "1.5",
                  "menyentuh-telinga-dengan-satu-tangan": "1"
                }
              }
            },
            "title": "Kemampuan Adaptasi"
          }
        ],
        "status": "complete",
        "video": [
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/A2.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/D7.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/B6.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/C6.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/F4.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/G2.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/E5.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/H1.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/I8.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/K1.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/L1.mp4"
          },
          {
            "title": null,
            "video": "https://skillana.id/https://dev.skillana.id/files/video/M3.mp4"
          }
        ]
      },
      "created_at": "2021-11-10 00:00:00",
      "tokenType": 1,
      "tokenPrice": 1,
      "tokenPriceSecond": 2,
      "purchased": true
    }
  ],
  gesture: [
    {
      "testName": "gesture",
      "created_at": "2021-11-10 00:00:00",
      "data": [
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/A2.mp4",
          "section": "2",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/AGhK06xDEQN5z1OpDoXeXY8Tm1UExoXg/114235.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/f7W1ZYq1bus-RRfcHRAfyyNgLOeHDic8/163600.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "34.4",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "mengangkat-alis",
              "score": "61.69",
              "description": "Subjek cenderung menunjukkan kecenderungan untuk mengungkapkan keragu-raguan saat menyikapi pembicaraan, disisi lain, gerakan alis saat berbicara menunjukkan keinginan untuk diakui oleh orang-orang disekitarnya."
            }
          ],
          "title": "Perkenalan Diri"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/D7.mp4",
          "section": "3",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/t652c0w9wouGdnPv_mvjh6odV7qszIPG/114407.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/DXk9W6QMXmt26CKAP8GiX1wfG8DURIxt/163721.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "37.17",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "mengangkat-alis",
              "score": "43.92",
              "description": "Subjek cenderung menunjukkan kecenderungan untuk mengungkapkan keragu-raguan saat menyikapi pembicaraan, disisi lain, gerakan alis saat berbicara menunjukkan keinginan untuk diakui oleh orang-orang disekitarnya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "18.58",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Motivasi"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/B6.mp4",
          "section": "4",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/gUIPTPg0RomgAtyqcFyLRCNr29SfpfIn/114532.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/eMuUUwu3Yt0oHbxlUHy74QyPj64kg2Q8/163809.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "24.9",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "mengangkat-alis",
              "score": "21.56",
              "description": "Subjek cenderung menunjukkan kecenderungan untuk mengungkapkan keragu-raguan saat menyikapi pembicaraan, disisi lain, gerakan alis saat berbicara menunjukkan keinginan untuk diakui oleh orang-orang disekitarnya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "42.02",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Sistematika Kerja"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/C6.mp4",
          "section": "5",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/zg05g0_xe80wnPZG9Tq-EgrLArdkVkbJ/114750.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/aYLkYf3Qty2wMOWxZeNdptGhxsVoqPhg/163931.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "26.93",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "58.66",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Daya Tahan Terhadap Tugas"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/F4.mp4",
          "section": "6",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/P-4rWFIOCAlUKZ9BUkvC_C8EoWg8V6IA/115034.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/RbTmqJ02tIuhgKpHlfzHMplnba-cKbci/164118.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "21.19",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "mengangkat-alis",
              "score": "38.2",
              "description": "Subjek cenderung menunjukkan kecenderungan untuk mengungkapkan keragu-raguan saat menyikapi pembicaraan, disisi lain, gerakan alis saat berbicara menunjukkan keinginan untuk diakui oleh orang-orang disekitarnya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "37.82",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Berpikir Analitis"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/G2.mp4",
          "section": "7",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/c3H0HcEHf6EqTvhzV3Z1hvPrSNRPWvhv/115117.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/r7VvDkklCevZUUncsXpgNvvEIVI7YqU-/164208.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "11.58",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menggaruk-leher-satu-tangan",
              "score": "20.26",
              "description": "Subjek memiliki kecenderungan untuk menunjukkan signal ketidaksetujuan terhadap ide atau pendapat yang diutarakan oleh lawan bicaranya. Disisi lain subjek juga menunjukkan refleksi rasa ragu terhadap dirinya sendiri terhadap ide atau pendapat yang subjek dengar."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "64.3",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Kepemimpinan"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/E5.mp4",
          "section": "8",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/jE8QLMKInqmfd4En41a5StyFlmNIxNsz/115220.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/wsLXDBSnE9ozVjFuIlLiubhbpU6GdTHC/164315.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "14.69",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "mengangkat-alis",
              "score": "12.55",
              "description": "Subjek cenderung menunjukkan kecenderungan untuk mengungkapkan keragu-raguan saat menyikapi pembicaraan, disisi lain, gerakan alis saat berbicara menunjukkan keinginan untuk diakui oleh orang-orang disekitarnya."
            },
            {
              "gesture": "menggaruk-leher-satu-tangan",
              "score": "23.19",
              "description": "Subjek memiliki kecenderungan untuk menunjukkan signal ketidaksetujuan terhadap ide atau pendapat yang diutarakan oleh lawan bicaranya. Disisi lain subjek juga menunjukkan refleksi rasa ragu terhadap dirinya sendiri terhadap ide atau pendapat yang subjek dengar."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "45.97",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Kerjasama"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/H1.mp4",
          "section": "9",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/XpS-9U-gKYTmzzap6KYQcswBNEtoUwUs/115415.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/l7bQoXLpgYelRE7n7-Q7KbSnvVQO106R/164508.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "26.15",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "53.09",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Ketrampilan"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/I8.mp4",
          "section": "10",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/JHo9vJRLiy0sdLlZ3WXzuuZEM97s6bvO/115501.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/mSKs4jB7VT5uaiPHHGEujbib8UjB_lDh/164607.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "40.63",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "47.64",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Pengembangan Diri"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/K1.mp4",
          "section": "11",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/DxwuLcz8pUvnH57mtTQQ2M2WvHuFr63B/115621.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/qjQYUSf5qrKUfFqiLl7diyjewmVm2b1i/164709.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "78.63",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "20.44",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Komitmen"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/L1.mp4",
          "section": "12",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/r-9YH7YM0vVBmg8KYoHpVThiBquZRLBB/115737.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/NAVnWKldQyeRJy4kwCvhSbWTldsxt9lS/164900.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "40.25",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "55.17",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Kejelasan Visi"
        },
        {
          "type": "web",
          "video": "https://dev.skillana.id/files/video/M3.mp4",
          "section": "13",
          "videoUrl": "https://widyaskilloka-dev.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20201202/vvbEs8qj3N0WkJcp7Y1g7cgnMzl3RkgZ/115811.webm",
          "resultUrl": "https://widyaskilloka.s3.ap-southeast-1.amazonaws.com/assessment/video/result/20210203/sHL8wFBgqdLsOfugFrT8InGEEUu7sFvV/164943.mp4",
          "resultData": [
            {
              "gesture": "gelengan",
              "score": "37",
              "description": "Subjek cenderung menunjukkan penolakan dengan upaya menggelengkan kepalanya."
            },
            {
              "gesture": "menarik-kerah-baju",
              "score": "51.74",
              "description": "Subjek berupaya meraih kerah baju sebenarnya cenderung menunjukkan refleksi stress terhadap situasi diskusi yang sedang subjek jalani. subjek berupaya untuk memberikan tubuhnya aliran udara agar bisa meredam rasa gerah yang subjek rasakan."
            }
          ],
          "title": "Kemampuan Adaptasi"
        }
      ],
      "tokenType": 8,
      "tokenPrice": 1,
      "tokenPriceSecond": 2,
      "purchased": true
    }
  ]
}