import React from 'react';
import { Text, Image } from '@react-pdf/renderer';
import { t, translate } from "react-switch-lang";


const SpmPage = ({ data, graph, styles }) => {

    return (
        <>
            <Text style={styles.title} break>
                {t('cognitiveAbilityScore')}
            </Text>
            <Image
                style={styles.image}
                src={graph}
            />
        </>
    )
};

export default translate(SpmPage)