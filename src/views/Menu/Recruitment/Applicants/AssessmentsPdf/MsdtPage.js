import React from "react";
import { Text, View, Image } from "@react-pdf/renderer";
import { t, translate } from "react-switch-lang";

const MsdtPage = ({ data, graph, styles }) => {
  return (
    <>
      <Text style={styles.title} break>
        {t("leadershipTest")}
      </Text>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Image style={{ ...styles.image, width: "100%" }} src={graph} />
      </View>
      <Text style={styles.subtitle}>
        {t("leadershipTestIs")}{" "}
        {data.detail.dataAssessment.msdt.description.title}
      </Text>
      <Text style={styles.text}>
        {data.detail.dataAssessment.msdt.description.desc}
      </Text>
      {data.detail.dataAssessment.msdt.description.list?.map((item, idx) => (
        <Text style={styles.text} key={idx}>
          {item}
        </Text>
      ))}
    </>
  );
};

export default translate(MsdtPage);
