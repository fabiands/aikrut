import React from "react";
import { Text, View, Image } from "@react-pdf/renderer";
import { t, translate } from "react-switch-lang";

const BusinessInsightPage = ({ data, styles, graph }) => {
  const dataResult = [
    {
      id: 1,
      title: "Autonomy",
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["autonomy"]
        : null,
      text2: null,
    },
    {
      id: 2,
      title: "Innovativeness",
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["innovativeness"]
        : null,
      text2: null,
    },
    {
      id: 3,
      title: "Risk Taking",
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["risk_taking"]
        : null,
      text2: null,
    },
    {
      id: 4,
      title: "Proactiveness",
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["proactiveness"]
        : null,
      text2: null,
    },
    {
      id: 5,
      title: "Competitive Aggresiveness",
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight[
            "competitive_agresiveness"
          ]
        : null,
      text2: null,
    },
    {
      id: 6,
      title: t("conclusion"),
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["conclusion"]
          ? data?.detail?.dataAssessment?.businessInsight["conclusion"]["high"]
          : null
        : null,
      text2: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["conclusion"]
          ? data?.detail?.dataAssessment?.businessInsight["conclusion"]["low"]
          : null
        : null,
    },
    {
      id: 7,
      title: t("suggestion"),
      text1: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["suggestion"]
          ? data?.detail?.dataAssessment?.businessInsight["suggestion"]["high"]
          : null
        : null,
      text2: data?.detail?.dataAssessment?.businessInsight
        ? data?.detail?.dataAssessment?.businessInsight["suggestion"]
          ? data?.detail?.dataAssessment?.businessInsight["suggestion"]["low"]
          : null
        : null,
    },
  ];
  return (
    <>
      <Text style={styles.title} break>
        {t("businessInsight")}
      </Text>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Image style={{ ...styles.image, width: "100%" }} src={graph} />
      </View>
      {dataResult.map((data, idx) => {
        return (
          <View style={styles.row} key={idx} break={idx === 3}>
            <View
              style={{
                width: "40%",
                borderWidth: 0.5,
                borderTopLeftRadius: 1,
                borderTopRightRadius: 1,
                borderColor: "rgba(0, 0, 0, 0.125)",
              }}
            >
              <Text
                style={{
                  fontSize: 12.5,
                  margin: 2.5,
                  fontWeight: 800,
                  fontFamily: "Oswald",
                }}
              >
                {data.title}
              </Text>
            </View>
            <View
              style={{
                width: "60%",
                borderWidth: 0.5,
                borderTopLeftRadius: 1,
                borderTopRightRadius: 1,
                borderColor: "rgba(0, 0, 0, 0.125)",
              }}
            >
              <Text style={styles.text}>{data.text1}</Text>
              <br />
              <Text style={styles.text}>{data.text2}</Text>
            </View>
          </View>
        );
      })}
    </>
  );
};

export default translate(BusinessInsightPage);
