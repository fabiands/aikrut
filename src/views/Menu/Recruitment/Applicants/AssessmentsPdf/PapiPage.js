import React from 'react';
import { Text, View, Image } from '@react-pdf/renderer';
import { t, translate } from "react-switch-lang";

function titleColor(resultTitle) {
    switch (resultTitle) {
        case "Followership":
            return "#e53935";
        case "Work Direction":
            return "#8e24aa";
        case "Leadership":
            return "#3949ab";
        case "Activity":
            return "#039be5";
        case "Social Nature":
            return "#00897b";
        case "Work Style":
            return "#7cb342";
        case "Temperament":
            return "#fb8c00";
        default:
            return "#FFFFFF";
    }
}

const PapiPage = ({ data, graph, styles, dataToken }) => {
    const template = {
        Followership: ["F", "W"],
        "Work Direction": ["N", "G", "A"],
        Leadership: ["L", "P", "I"],
        Activity: ["T", "V"],
        "Social Nature": ["X", "S", "B", "O"],
        "Work Style": ["C", "D", "R"],
        Temperament: ["Z", "E", "K"],
    };

    const groupingDesc = {
        F: t("Keb Membantu Atasan"),
        W: t("Keb Mengikuti Aturan dan Pengawasan"),
        N: t("Keb dalam Menyelesaikan Tugas (Kemandirian)"),
        G: t("Peran Pekerja Keras"),
        A: t("Keb dalam Berprestasi"),
        L: t("Peran Kepemimpinan"),
        P: t("Keb Mengatur Orang Lain"),
        I: t("Peran Dalam Membuat Keputusan"),
        T: t("Peran Dalam Kesibukan Kerja"),
        V: t("Peran Dalam Semangat Kerja"),
        X: t("Keb untuk Diperhatikan"),
        S: t("Peran Dalam Hubungan Sosial"),
        B: t("Keb Diterima dalam Kelompok"),
        O: t("Keb Kedekatan dan Kasih Sayang"),
        C: t("Peran Dalam Mengatur"),
        D: t("Peran Bekerja dengan Hal-hal Rinci"),
        R: t("Peran Penalaran Teoritis"),
        Z: t("Keb untuk Berubah"),
        E: t("Peran Dalam Pengendalian Emosi"),
        K: t("Keb untuk Agresif"),
    };

    const group =
        data.detail.dataAssessment.papikostik[0].result &&
        Object.keys(data.detail.dataAssessment.papikostik[0].result).map((category) => {
            const item =
                data.detail.dataAssessment.papikostik[0].result &&
                template[category].map((code, idx) => ({
                    code: code,
                    scores: data.detail.dataAssessment.papikostik[0].scores[code],
                    description: data.detail.dataAssessment.papikostik[0].result[category][idx],
                }));

            return {
                kategory: category,
                items: item,
            };
        });

    return (
        <>
            <Text style={styles.title} break>
                {t('PapikostickTest')}
            </Text>
            <Image
                style={styles.image}
                src={graph}
            />
            {!dataToken.balance || dataToken.isExpired ?
                null :
                <View>
                    <Text style={styles.subtitle} break>
                        Detail Asesmen
                    </Text>
                    {group.map((objKategori, idx) => (
                        <View key={idx} break={objKategori.kategory === 'Leadership' || objKategori.kategory === 'Social Nature' || objKategori.kategory === 'Work Style'}>
                            <Text style={{ ...styles.subtitle, color: titleColor(objKategori.kategory) }}>
                                {objKategori.kategory}
                            </Text>
                            <View style={styles.row}>
                                <Text style={{ ...styles.text, width: '45%', textAlign: 'center' }}>
                                    {t("aspek")}
                                </Text>
                                <Text style={{ ...styles.text, width: '10%', textAlign: 'center' }}>
                                    {t("nilai")}
                                </Text>
                                <Text style={{ ...styles.text, width: '45%', textAlign: 'center' }}>
                                    {t("deskripsi")}
                                </Text>
                            </View>
                            {objKategori &&
                                objKategori.items.map((objItem, i) => (
                                    <View style={styles.row} key={i}>
                                        <Text style={{ ...styles.text, width: '45%', borderLeftWidth: 5, borderLeftColor: titleColor(objKategori.kategory), borderBottomLeftRadius: 0.1, borderBottomRightRadius: 0.1, borderTopLeftRadius: 0.1, borderTopRightRadius: 0.1, paddingLeft: 8 }}>
                                            {objItem.code} : {groupingDesc[objItem.code]}
                                        </Text>
                                        <Text style={{ ...styles.text, width: '10%', textAlign: 'center', borderLeftWidth: 4, borderRightWidth: 4, borderLeftColor: '#f9fafc', borderRightColor: '#f9fafc' }}>
                                            {objItem.scores} / 9
                                        </Text>
                                        <Text style={{ ...styles.text, width: '45%' }}>
                                            {objItem.description}
                                        </Text>
                                    </View>
                                ))}
                        </View>
                    ))}
                </View>
            }
        </>
    )
};

export default translate(PapiPage)