import React from "react";
import {
    Card,
    CardBody,
    Row,
    Col,
} from "reactstrap";
import { t, translate } from "react-switch-lang";

const ApplicantProfile = ({ data: applicant, dataToken, inline }) => {
    const { detail } = applicant;
    // const censoredEmail = detail?.email.split("").filter(a => a !== " ") ?? [];

    let age = '0';
    const dob = new Date(detail.dateOfBirth);
    if (dob.getTime() !== 0) {
        const now = (new Date()).getTime();
        const diff = now - dob.getTime();
        age = (new Date(diff)).getUTCFullYear() - 1970;
        age += ' ' + t("tahun");
    }
    else age = '-';

    return (
        <Card className="border-0">
            <CardBody>
                <Row className="px-0 px-md-5">
                    {/* Baris 1 */}
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("namalengkap")}</b>
                        <p className="mt-2 mb-1">{detail.fullName}</p>
                    </Col>
                    <Col xs="1" sm="1" md="1"></Col>
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("usia")}</b>
                        <p className="mt-2 mb-1">{age}</p>
                    </Col>
                    {/* Baris 2 */}
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("namapanggilan")}</b>
                        <p className="mt-2 mb-1">{detail.nickName}</p>
                    </Col>
                    <Col xs="1" sm="1" md="1"></Col>
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("jk")}</b>
                        <p className="mt-2 mb-1">{detail.gender?.name ? t(detail.gender.name) : ""}</p>
                    </Col>
                    {/* Baris 3 */}
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>Email</b>
                        <p className="mt-2 mb-1">{detail.email}</p>
                        {/* <p className="mt-2 mb-1">{dataToken.balance ? detail.email : censoredEmail[0] + "******." + censoredEmail[censoredEmail.length-3] + censoredEmail[censoredEmail.length-2] + censoredEmail[censoredEmail.length-1]}</p> */}
                    </Col>
                    <Col xs="1" sm="1" md="1"></Col>
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("statuspernikahan")}</b>
                        <p className="mt-2 mb-1">{detail.maritalStatus?.name ?? "-"}</p>
                    </Col>
                    {/* Baris 4 */}
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("alamat")}</b>
                        <p className="mt-2 mb-1">
                            {(dataToken.balance || !dataToken.isExpired) ? [
                                detail.currentAddress.city?.name,
                                detail.currentAddress.province?.name,
                                detail.currentAddress.country?.name,
                            ]
                                .filter(Boolean)
                                .join(", ") : "**********"}
                        </p>
                    </Col>
                    <Col xs="1" sm="1" md="1"></Col>
                    <Col xs="12" sm="12" md={inline ? 12 : 5} className="border-bottom mt-4 px-0">
                        <b>{t("notelpon")}</b>
                        <p className="mt-2 mb-1">{detail.phone}</p>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
};

export default translate(ApplicantProfile);