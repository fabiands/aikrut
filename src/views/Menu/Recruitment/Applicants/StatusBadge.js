import React from "react";
import { Badge } from "reactstrap";
import { t } from "react-switch-lang";
import { useAuthUser } from "../../../../store";

const statuses = {
  pending: "warning",
  accepted: "success",
  rejected: "danger",
};

const statusLamaran = (stat) => {
  let status = "";
  if (stat === "pending") {
    status = t("unprocessed");
  }
  if (stat === "accepted") {
    status = t("sesuai");
  }
  if (stat === "rejected") {
    status = t("unsuitable");
  }

  return status;
};

const statusLamaranEdu = (stat) => {
  let status = "";
  if (stat === "pending") {
    status = t("unprocessedEdu");
  }
  if (stat === "accepted") {
    status = t("processedEdu");
  }
  if (stat === "rejected") {
    status = t("Ditolak");
  }

  return status;
};

function StatusBadge({ status, size = 14 }) {
  const user = useAuthUser();
  return (
    <Badge
      className={`text-capitalize`}
      style={{ fontSize: size, fontHeight: 16, color: "white" }}
      color={statuses[status]}
    >
      {user?.personnel?.company?.paid === "education"
        ? statusLamaranEdu(status)
        : statusLamaran(status)}
    </Badge>
  );
}

export default StatusBadge;
