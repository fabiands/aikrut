import React, { useState, useEffect, useCallback } from "react";
import {
  Button,
  Spinner,
  Row,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
} from "reactstrap";
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import DataNotFound from "../../../../components/DataNotFound";
import request from "../../../../utils/request";
import StatusBadge from "./StatusBadge";
import { toast } from "react-toastify";
import { t } from "react-switch-lang";
import moment from "moment";
import { useAuthUser, useUserPrivileges } from "../../../../store";
import "./videoasesmen.css";
// import { groupingLabel, colorsByIndex } from "./labels";
import "chartjs-plugin-datalabels";
import profilePhotoNotFound from "../../../../assets/img/no-photo.png";
import SkeletonApplicantDetail from "../Skeleton/SkeletonApplicantDetail";
import NoToken from "./Assessments/NoToken";
import { Link, useLocation } from "react-router-dom";
import { useBalance } from "../../../../hooks/useBalance";
import SentimentAnalysis from "./Sentiment/SentimentAnalysis";
import AssessmentMbti from "./Assessments/AssessmentMbti";
import AssessmentShio from "./Assessments/AssessmentShio";
import AssessmentFingerprint from "./Assessments/AssessmentFingerprint";
import AssessmentMsdt from "./Assessments/AssessmentMsdt";
import AssessmentSpm from "./Assessments/AssessmentSpm";
import AssessmentBusinessInsight from "./Assessments/AssessmentBusinessInsight";
import AssessmentAgility from "./Assessments/AssessmentAgility";
import AssessmentDisc from "./Assessments/AssessmentDisc";
import { useDispatch } from "react-redux";
import disableScroll from "disable-scroll";
import Tour from "reactour";
import { getMe } from "../../../../actions/auth";
import { useMediaQuery } from "react-responsive";
import ModalPrivilegeForbidden from "../../../../components/ModalPrivilegeForbidden";
import AssessmentBazi from "./Assessments/AssessmentBazi";
import AssessmentZodiac from "./Assessments/AssessmentZodiac";
import AssessmentFisiognomi from "./Assessments/AssessmentFisiognomi";
import AssessmentPalmistry from "./Assessments/AssessmentPalmistry";
import AssessmentPapikostick from "./Assessments/AssessmentPapikostick";
import AssessmentVideo from "./Assessments/AssessmentVideo";
import ApplicantProfile from "./Profile/Profile";
import ApplicantResume from "./Resume/Resume";
import AssessmentGesture from "./Assessments/AssessmentGesture";
import { memo } from "react";
import ApplicantAssessmentDownload from "./ApplicantAssessmentDownload";
import dummy from "./dummy";
import { connect } from "react-redux";
const mapStateToProps = ({ menu: panelMenu, user, token }) => ({
  panelMenu,
  user,
  token,
});

export default connect(mapStateToProps)(
  memo((props) => {
    const confirm = {
      accepted: t("suitable"),
      rejected: t("unsuitable"),
    };

    const tabs = {
      profile: t("Profil"),
      resume: t("resume"),
      assessment: t("assessment"),
    };

    const tabsArray = Object.keys(tabs);
    const location = useLocation();
    const selectedTab = location.hash
      ? location.hash.substring(1)
      : tabsArray[0];
    const isSmallSize = useMediaQuery({ query: "(max-width: 768px)" });
    const [applicant, setApplicant] = useState(null);
    const [notFound, setNotFound] = useState(false);
    const [loading, setLoading] = useState(true);
    const { can } = useUserPrivileges();
    const [isLoading, setIsLoading] = useState(false);
    const [stat, setStat] = useState(null);
    const { data: dataToken } = useBalance();
    const user = useAuthUser();
    const [isTour, setIsTour] = useState(false);
    const [isTourAssessment, setIsTourAsessment] = useState(false);
    const disableBody = () => disableScroll.on();
    const enableBody = () => disableScroll.off();
    const accentColor = "#1d5a8e";
    const dispatch = useDispatch();
    const [forbidden, setForbidden] = useState(false);
    console.log(applicant?.detail);
    useEffect(() => {
      if (user.guidance.layout && user.guidance.header) {
        if (!user.guidance.applicantDetail) {
          isSmallSize
            ? window.scroll({ top: 300, behavior: "smooth" })
            : window.scroll({ top: 0, behavior: "smooth" });
          setIsTour(true);
        }
      }
    }, [user, isSmallSize]);

    useEffect(() => {
      if (
        user.guidance.layout &&
        user.guidance.header &&
        user.guidance.applicantDetail
      ) {
        if (
          location.hash.substring(1) === "assessment" &&
          !user.guidance.applicantAssessment
        ) {
          setIsTourAsessment(true);
        }
      }
    }, [user, location.hash]);

    const disableGuideApplicantDetail = () => {
      setIsTour(false);
      request.put("auth/guidance", { guidance: "applicantDetail" }).then(() => {
        dispatch(getMe());
      });
    };

    const disableGuideApplicantAssessment = () => {
      setIsTourAsessment(false);
      request
        .put("auth/guidance", { guidance: "applicantAssessment" })
        .then(() => {
          dispatch(getMe());
        });
    };

    const steps = [
      {
        selector: ".tour-tabApplicantDetail",
        content: ({ goTo, inDOM }) => (
          <div>
            <p>
              {t("jobVacanciesDetailApplicantsPoint1")}{" "}
              {applicant.job_vacancy.isInternal
                ? t("Employee")
                : t("Applicant")}
              , resume{" "}
              {applicant.job_vacancy.isInternal
                ? t("Employee")
                : t("Applicant")}{" "}
              {t("jobVacanciesDetailApplicantsPoint1Sub1")}{" "}
              {applicant.job_vacancy.isInternal
                ? t("Employee")
                : t("Applicant")}
              .
            </p>
            <Row>
              <div className="col-12 text-center">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={disableGuideApplicantDetail}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </div>
        ),
      },
    ];

    const getAPI = useCallback(() => {
      return request
        .get(`v1/recruitment/applicants/${props.match.params.applicantId}`)
        .then((res) => {
          setApplicant(res.data.data);
        })
        .catch((err) => {
          if (err.response?.status === 404) {
            setNotFound(true);
          }
        })
        .finally(() => setLoading(false));
    }, [props.match]);

    const changeStatus = (status) => {
      if (can("canManagementJob")) {
        setStat(status);
      } else {
        setForbidden(true);
      }
    };

    const toggleStat = () => {
      if (!!stat) {
        setStat(null);
      }
    };

    const UbahStatus = (stat) => {
      setIsLoading(true);
      request
        .put(`v1/recruitment/applicants/${props.match.params.applicantId}`, {
          status: stat,
        })
        .then((res) => {
          toast.success(t("statuspelamarberhasildiubah"));
          props.history.goBack();
        })
        .catch((err) => {
          toast.error(t("statuspelamargagaldiubah"));
          throw err;
        })
        .finally(() => {
          setIsLoading(false);
          setStat(null);
        });
    };

    useEffect(() => {
      getAPI();
    }, [getAPI]);

    const [isDummy, setIsDummy] = useState(false);
    const handleDownload = useCallback(
      (e) => {
        if (e) {
          if (
            !applicant.detail.dataAssessment.video ||
            !applicant.detail.dataAssessment.video.length
          ) {
            setIsDummy(true);
          }
        } else {
          setIsDummy(false);
        }
      },
      [applicant]
    );

    if (loading) {
      return <SkeletonApplicantDetail />;
    } else if (notFound) {
      return <DataNotFound />;
    }

    const onErrorImage = (e) => {
      e.target.src = profilePhotoNotFound;
      e.target.onerror = null;
    };

    return (
      <>
        <Tour
          steps={steps}
          accentColor={accentColor}
          showButtons={false}
          disableDotsNavigation={true}
          rounded={5}
          showNavigation={false}
          isOpen={isTour}
          closeWithMask={false}
          disableInteraction={true}
          disableFocusLock={true}
          onAfterOpen={disableBody}
          onBeforeClose={enableBody}
          onRequestClose={disableGuideApplicantDetail}
        />
        {forbidden && (
          <ModalPrivilegeForbidden
            isOpen={true}
            forbidden="canManagementJob"
            isClose={() => setForbidden(false)}
          />
        )}
        <Modal
          isOpen={!!isTourAssessment}
          style={{ marginTop: "40vh" }}
          backdropClassName="back-home"
        >
          <ModalHeader
            className="border-bottom-0"
            toggle={disableGuideApplicantAssessment}
          >
            {t("jobVacanciesDetailApplicantsPoint2")}{" "}
            {applicant.job_vacancy.isInternal ? t("Employee") : t("Applicant")}
          </ModalHeader>
          <ModalBody>
            <p>
              {t("jobVacanciesDetailApplicantsPoint3")}{" "}
              {applicant.job_vacancy.isInternal
                ? t("Employee")
                : t("Applicant")}{" "}
              {t("jobVacanciesDetailApplicantsPoint3Sub1")}{" "}
            </p>
            <Row>
              <div className="col-12 text-center">
                <Button
                  className="mt-2 px-2"
                  type="submit"
                  color="netis-success"
                  onClick={disableGuideApplicantAssessment}
                >
                  {t("btnOk")}
                </Button>
              </div>
            </Row>
          </ModalBody>
        </Modal>
        <div className="pl-4 p-md-4">
          <Modal
            isOpen={!!stat}
            backdropClassName="back-home"
            toggle={toggleStat}
          >
            <ModalHeader toggle={toggleStat}>
              {t("changingStatusConfirm")}
            </ModalHeader>
            <ModalBody>
              <h5>
                {t("changeApplicantStatus")}
                &nbsp;
                <span
                  className={
                    stat === "accepted" ? "text-success" : "text-danger"
                  }
                >
                  {confirm[stat]}
                </span>{" "}
                ?
              </h5>
            </ModalBody>
            <ModalFooter className="border-top-0">
              <div className="d-flex justify-content-end">
                {!isLoading && (
                  <Button
                    className="mr-2"
                    color="light"
                    onClick={() => {
                      setStat(null);
                    }}
                  >
                    {t("batal")}
                  </Button>
                )}
                <Button
                  color={stat === "accepted" ? "netis-success" : "netis-danger"}
                  onClick={() => UbahStatus(stat)}
                  disabled={isLoading}
                >
                  {isLoading ? (
                    <>
                      <Spinner size="sm" color="light" /> loading...
                    </>
                  ) : (
                    t("changeStatus")
                  )}
                </Button>
              </div>
            </ModalFooter>
          </Modal>
          <div
            className="d-flex bd-highlight mt-md-n4 mr-md-n4 card ribbons border-0"
            data-label={`${
              user?.personnel?.company?.paid === "education"
                ? t("filledOn")
                : t("appliedOn")
            } ${moment(applicant.createdAt).format("DD MMMM YYYY")}`}
          >
            <div className="card-container">
              <div className="d-flex mt-4 justify-content-between">
                <div className="bd-highlight">
                  {!applicant.job_vacancy.isInternal ||
                  user?.personnel?.company?.paid === "education" ? (
                    <StatusBadge
                      status={
                        user?.personnel?.company?.paid === "education"
                          ? applicant?.detail?.isExistAssessment
                            ? "accepted"
                            : "pending"
                          : applicant.status
                      }
                    />
                  ) : null}
                </div>
                <div className="mt-4 mr-4 bd-highlight">
                  <Link
                    to={`/recruitment/applicants/${
                      applicant.detail.id
                    }/compare?isInternal=${
                      applicant.job_vacancy.isInternal ? 1 : 0
                    }`}
                    className={`btn btn-sm btn-netis-primary px-3 py-2`}
                    style={{ borderRadius: "5px" }}
                  >
                    {t("compare")}
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <Row className="mt-n4 mb-1">
            <Col>
              <div className="detail-applicant-photo d-flex justify-content-center p-3 pdf-photo">
                <img
                  src={applicant.detail.avatar}
                  alt="photos profile"
                  className="rounded-circle"
                  onError={(e) => onErrorImage(e)}
                />
              </div>
            </Col>
            <Col xs="12" sm="12" md="6" lg="6" xl="6" className="p-4">
              <Row className="mt-5 text-sm-center text-md-left text-lg-left text-xl-left">
                <Col xs="12" className="text-netis-primary">
                  <h4 className="font-weight-bold">
                    {applicant.detail.fullName}
                  </h4>
                </Col>
                <Col xs="12" className="text-netis-primary">
                  <h5>{applicant.job_vacancy.name}</h5>
                </Col>
                <Col xs="12" className="text-muted">
                  <h6>{applicant.job_vacancy.type}</h6>
                </Col>
                <Col xs="12">
                  {(function () {
                    if (
                      applicant.status === "pending" &&
                      !applicant.job_vacancy.isInternal
                    ) {
                      return (
                        <Row className="bd-highlight my-3">
                          <Col
                            xs="6"
                            sm="6"
                            md="12"
                            lg="6"
                            xl="4"
                            className="px-3 py-1 bd-highlight"
                          >
                            <Button
                              color="netis-success"
                              className={`w-100`}
                              onClick={() => {
                                changeStatus("accepted");
                              }}
                            >
                              <i className="fa fa-check mr-1"></i>{" "}
                              {t("suitable")}
                            </Button>
                          </Col>
                          <Col
                            xs="6"
                            sm="6"
                            md="12"
                            lg="6"
                            xl="4"
                            className="px-3 py-1 bd-highlight"
                          >
                            <Button
                              color="netis-danger"
                              className={`w-100`}
                              onClick={() => {
                                changeStatus("rejected");
                              }}
                            >
                              <i className="fa fa-times mr-1"></i>{" "}
                              {t("unsuitable")}
                            </Button>
                          </Col>
                        </Row>
                      );
                    }
                  })()}
                </Col>
              </Row>
            </Col>
          </Row>
          <Nav tabs className="tour-tabApplicantDetail">
            {tabsArray.map((tab) => (
              <NavItem key={tab}>
                <NavLink
                  tag={Link}
                  className="pt-2/5"
                  active={selectedTab === tab}
                  replace
                  to={{ hash: "#" + tab }}
                >
                  {tabs[tab]}
                </NavLink>
              </NavItem>
            ))}
          </Nav>

          <TabContent activeTab={selectedTab}>
            <TabPane tabId="profile">
              <Row>
                <Col sm="12">
                  <ApplicantProfile data={applicant} dataToken={dataToken} />
                </Col>
              </Row>
            </TabPane>

            <TabPane tabId="resume">
              <Row>
                <Col sm="12">
                  <ApplicantResume data={applicant} dataToken={dataToken} />
                </Col>
              </Row>
            </TabPane>

            <TabPane tabId="assessment">
              <Row>
                <Col sm="12" className="mb-3 d-none d-lg-block">
                  {user?.personnel?.company?.paid === "education" ? (
                    applicant?.detail?.isExistAssessment ? (
                      <ApplicantAssessmentDownload
                        data={applicant}
                        dataToken={dataToken}
                        onDownload={handleDownload}
                      />
                    ) : null
                  ) : (
                    <ApplicantAssessmentDownload
                      data={applicant}
                      dataToken={dataToken}
                      onDownload={handleDownload}
                    />
                  )}
                </Col>
                <Col sm="12">
                  {user?.personnel?.company?.paid === "education" ? (
                    applicant?.detail?.isExistAssessment ? (
                      <ApplicantAssesment
                        can={can}
                        data={applicant}
                        match={props.match}
                        getAPI={getAPI}
                        dataToken={dataToken}
                        guide={user.guidance}
                        user={props.user}
                        isDummy={isDummy}
                      />
                    ) : (
                      <div className="text-center">
                        <img
                          src={require(`../../../../assets/img/sorry.png`)}
                          alt="No Job"
                          width="200px"
                        />
                        <h4 className="text-netis-primary">
                          {t("Asesmen Belum Diisi")}
                        </h4>
                        <p>
                          Peserta belum mengisi atau belum menyelesaikan asesmen
                        </p>
                      </div>
                    )
                  ) : (
                    <ApplicantAssesment
                      can={can}
                      data={applicant}
                      match={props.match}
                      getAPI={getAPI}
                      dataToken={dataToken}
                      guide={user.guidance}
                      user={props.user}
                      isDummy={isDummy}
                    />
                  )}
                </Col>
              </Row>
            </TabPane>

            <TabPane tabId="sentiment">
              <Row>
                <Col sm="12">
                  <SentimentAnalysis
                    data={applicant.detail}
                    match={props.match}
                    getAPI={getAPI}
                    dataToken={dataToken}
                    guide={user.guidance}
                    type="applicant"
                  />
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </div>
      </>
    );
  })
);
const ApplicantAssesment = ({
  data: applicant,
  match,
  getAPI,
  dataToken,
  guide,
  can,
  user,
  isDummy,
}) => {
  const {
    detail: { id, dataAssessment, dateOfBirth: date },
  } = applicant;

  const {
    mbti: assessmentMbti = [],
    papikostik: assessmentPapikostick = [],
    disc = [],
    fisiognomi = [],
    palmistry = [],
    shio = [],
    zodiac = [],
    bazi: assessmentBazi,
    fingerprint = null,
    msdt = null,
    spm = null,
    businessInsight = null,
    agility = null,
  } = dataAssessment;

  const video = isDummy ? dummy.video : dataAssessment.video ?? [];
  const gesture = isDummy ? dummy.gesture : dataAssessment.gesture ?? [];

  const [modalNoToken, setModalNoToken] = useState(true);
  const toggleNoToken = () => setModalNoToken(!modalNoToken);

  return (
    <div>
      {user.personnel.company.paid === "pre" ? (
        guide.layout &&
        guide.header &&
        guide.applicantDetail &&
        guide.applicantAssessment &&
        (!dataToken.balance || dataToken.isExpired) ? (
          <NoToken
            nullData="all"
            isOpen={modalNoToken}
            toggle={toggleNoToken}
          />
        ) : null
      ) : null}

      {assessmentMbti &&
        Boolean(assessmentMbti.length) &&
        assessmentMbti.map((ases, index) => (
          <AssessmentMbti
            ases={ases}
            key={index}
            dataToken={dataToken}
            position="detailApplicant"
          />
        ))}
      {assessmentPapikostick && assessmentPapikostick[0] && (
        <AssessmentPapikostick
          papi={assessmentPapikostick[0]}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {disc && disc[0] && (
        <AssessmentDisc
          can={can}
          data={disc[0]}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {businessInsight && (
        <AssessmentBusinessInsight
          can={can}
          data={businessInsight}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {agility && (
        <AssessmentAgility
          can={can}
          data={agility}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {msdt && (
        <AssessmentMsdt
          can={can}
          data={msdt}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {spm && (
        <AssessmentSpm
          can={can}
          data={spm}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}

      {video && video[0] && (
        <AssessmentVideo
          can={can}
          vid={video[0]}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {gesture &&
        gesture[0] &&
        gesture[0]["data"] &&
        gesture[0]["data"][0] &&
        gesture[0]["data"][0]["resultData"] && (
          <AssessmentGesture
            can={can}
            vid={gesture[0]}
            id={id}
            getAPI={getAPI}
            dataToken={dataToken}
            position="detailApplicant"
          />
        )}

      {fisiognomi && fisiognomi[0] && (
        <AssessmentFisiognomi
          can={can}
          fisiognomi={fisiognomi[0]}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {palmistry && palmistry[0] && (
        <AssessmentPalmistry
          can={can}
          palmistry={palmistry[0]}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}

      {fingerprint && (
        <AssessmentFingerprint
          can={can}
          data={fingerprint}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}

      {assessmentBazi && (
        <AssessmentBazi
          can={can}
          bazi={assessmentBazi}
          date={date}
          id={id}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {shio && (
        <AssessmentShio
          can={can}
          shio={shio}
          id={id}
          date={date}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
      {zodiac && (
        <AssessmentZodiac
          can={can}
          zodiac={zodiac}
          id={id}
          date={date}
          getAPI={getAPI}
          dataToken={dataToken}
          position="detailApplicant"
        />
      )}
    </div>
  );
};

// export default memo(translate(ApplicantDetail));
