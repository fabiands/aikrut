import React, { useState } from "react";
import { translate, t } from "react-switch-lang";
import { toast } from "react-toastify";
import { Button, Col, Modal, ModalBody, Row, Spinner } from "reactstrap";
import request from "../../../../../utils/request";
import NoSentiment from "../../../../../assets/img/sentiment/no-sentiment.png";
import NoSentimentEn from "../../../../../assets/img/sentiment/no-sentiment-en.png";
import langUtils from "../../../../../utils/language/index";

function SentimentRequest({ data, getAPI, type, candidate }) {
  const [modal, setModal] = useState(false);
  const [loading, setLoading] = useState(false);

  const toggle = function () {
    setModal(!modal);
  };

  const contactus = () => {
    setLoading(true);
    request
      .post("v1/company/request/sentiment", {
        userId: data.id,
        userName: data.fullName,
      })
      .then(() => {
        // setModal(true)
        getAPI(data.id, candidate);
      })
      .catch((err) => {
        toast.error(t("failedSentiment"));
        return;
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <>
      <Row className="no-sentiment d-flex justify-content-center">
        <Col
          xs={type === "applicant" ? "12" : "10"}
          className={
            type === "applicant"
              ? "no-sentiment-img"
              : "no-sentiment-img-compare"
          }
        >
         {langUtils.getLanguage() === "ID" ? (
                                        <img src={NoSentiment} alt="no-sentiment" />

                                    ): (
                                        <img src={NoSentimentEn} alt="no-sentiment-en" />
                                    )
                                    }
        </Col>
        <Col xs="12" className="no-sentiment-content text-center">
          <h6>
            {t("sentimentRequestDesc")}
            <br />
            <br />
            {t("sentimentRequestDescSub1")}
          </h6>
          <Button
            onClick={() => contactus()}
            color="netis-color"
            className="my-3"
            disabled={loading}
          >
            {loading ? (
              <>
                <Spinner color="light" size="sm" />
                &nbsp;&nbsp;Loading...{" "}
              </>
            ) : (
              `${t('contactUs')}`
            )}
          </Button>
        </Col>
      </Row>
      <Modal isOpen={modal} toggle={toggle} style={{ marginTop: "40vh" }}>
        <ModalBody className="text-center">
          <p>{t("pleaseWait")}</p>
          <Button
            color="netis-color"
            onClick={toggle}
            style={{ width: "100px" }}
          >
            {t("ok")}
          </Button>
        </ModalBody>
      </Modal>
    </>
  );
}

export default translate(SentimentRequest);
