import React from "react";
import Skeleton from "react-loading-skeleton";
import { Card, CardBody, Row, Col } from "reactstrap";

const SkeletonVacancyApplicantList = () => {
  return (
    <Row>
      {Array(6)
        .fill()
        .map((item, index) => (
          <Col key={index} xs="12" sm="6" md="6" lg="6" className="text-center">
            <Card className="shadow-sm border-0">
              <CardBody>
                <Row>
                  <Col sm="4" className="text-center">
                    <div className="card-image d-flex justify-content-center p-3">
                      <Skeleton circle={true} height={80} width={80} />
                    </div>
                  </Col>
                  <Col sm="8" className="text-left">
                    <Skeleton height={19} width={`70%`} className="mt-4" />
                    <br />
                    <Skeleton height={16} width={`50%`} />
                    <br />
                    <Skeleton height={16} width={`50%`} />
                    <br />
                    <Skeleton height={16} width={`50%`} />
                  </Col>
                  {/* <Col xs="12">
                                        <Skeleton height={`100%`} width={`45%`} className="mr-1" />
                                        <Skeleton height={`100%`} width={`45%`} className="mr-1" />
                                    </Col> */}
                </Row>
              </CardBody>
            </Card>
          </Col>
        ))}
    </Row>
  );
};
export default SkeletonVacancyApplicantList;
