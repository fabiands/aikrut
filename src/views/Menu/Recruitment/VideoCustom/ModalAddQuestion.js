import { useFormik } from 'formik'
import React, { useState } from 'react'
import { translate, t } from 'react-switch-lang'
import { toast } from 'react-toastify'
import {Modal, ModalHeader, ModalBody, Input, Button, ModalFooter, CustomInput, Spinner} from 'reactstrap'
import request from '../../../../utils/request'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useAuthUser } from '../../../../store'

function ModalAddQuestion({modalQuestion, toggleQuestion, mutate}){
  const user = useAuthUser();
  const [agree, setAgree] = useState(false)
  const [confirm, setConfirm] = useState(false)
  const toggleConfirm = () => setConfirm(!confirm)
  const { values, touched, errors, isSubmitting, ...formik } = useFormik({
    initialValues: {
      title: '',
      questions: ['']
    },
    onSubmit: (values, { setSubmitting }) => {
      setSubmitting(true)
      request.post('v1/assessment/video', values)
        .then(() => {
          request.post('v1/token/usage', {
            identity: 0,
            tokenType: 14
          })
          .then(() => {
            mutate()
            toast.success(t('Berhasil membuat pertanyaan'))
            toggleQuestion()
          })
        })
        .catch((err) => {
          if(err.response?.status === 422){
            toast.error(t('Data yang anda isikan belum lengkap'))
            return;
          }
          else{
            toast.error(t('Terjadi Kesalahan, silahkan coba lagi'))
            return;
          }
        })
        .finally(() => setSubmitting(false))
    }
  })

  const addQuestion = () => {
    const arr = values.questions
    arr.push('')
    formik.setFieldValue('questions', arr)
  }

  const handleChangeQuestion = (value, idx) => {
    const arr = values.questions
    arr[idx] = value
    formik.setFieldValue('questions', arr)
  }

  const deleteQuestion = (idx) => {
    const arr = values.questions
    arr.splice(idx, 1)
    formik.setFieldValue('questions', arr)
  }

  return(
    <>
    <Modal isOpen={modalQuestion} toggle={toggleQuestion} size='xl'>
      <ModalHeader className='border-bottom-0'>{t('Tambah Interview Video CV')}</ModalHeader>
      <ModalBody>
        <Input
          className='mb-4'
          name='title'
          id='title'
          placeholder={t('Masukkan Judul Interview')}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={values.title}
          required
        />
        <hr />
        {values.questions.map((item, idx) =>
          <div key={idx} className={idx === 0 ? 'mt-4 mb-2' : 'my-2'} style={{position:'relative'}}>
            <Input
              name={`questions-${idx+1}`}
              id={`questions-${idx+1}`}
              value={item}
              placeholder={t('Masukkan Pertanyaan')}
              onChange={(e) => handleChangeQuestion(e.target.value, idx)}
              required
            />
            {values.questions.length > 1 &&
            <Button className='bg-transparent border-0 btn-sm' style={{position:'absolute', top:'5px', right:'5px'}} onClick={() => deleteQuestion(idx)}>
              <i className='fa fa-times' />
            </Button>
            }
          </div>
        )}
        <Button color='netis-primary' className='mt-3' onClick={addQuestion}>{t('Tambah Pertanyaan')}</Button>
        <div className='my-3'>
          <CustomInput
            type='checkbox'
            name='term'
            id='term'
            checked={agree}
            onChange={() => setAgree(!agree)}
            label={t('videocustomagreement')}
          />
        </div>
      </ModalBody>
      <ModalFooter className='border-top-0'>
        <Button color='outline-netis-primary' className='mr-2' onClick={toggleQuestion}>{t('Batal')}</Button>
        <Button color='netis-primary' className='ml-2' disabled={!agree || isSubmitting} onClick={toggleConfirm}>
          {isSubmitting ? <><Spinner color='light' size='sm' />Loading...</> : t('Simpan Pertanyaan')}
        </Button>
      </ModalFooter>
    </Modal>
    <Modal isOpen={confirm}>
      <ModalBody className='text-center'>
        <h5 className='mb-2'>
          {user.personnel.company.paid === 'pre' ? t('Pakai') : t('Tambah')}&nbsp;
          {t('Token untuk menambah fitur ini?')}
        </h5>
        <h6 className='my-4'>
          {user.personnel.company.paid === 'pre' ? t('Token Anda akan berkurang sebanyak') : t('Token Anda akan bertambah sebanyak')}&nbsp;
          <FontAwesomeIcon icon='coins' className='mx-1 text-warning' />&nbsp;1 Token.<br />
          {('Apakah Anda ingin melanjutkannya ?')}
        </h6>
        <div className='d-flex justify-content-center mt-2'>
        <Button color='outline-netis-primary' className='mr-2' onClick={toggleConfirm}>{t('Batal')}</Button>
        <Button color='netis-primary' className='ml-2' onClick={() => {
          formik.handleSubmit()
          toggleConfirm()
        }}>{t('Ya')}</Button>
        </div>
      </ModalBody>
    </Modal>
    </>
  )
}

export default translate(ModalAddQuestion)