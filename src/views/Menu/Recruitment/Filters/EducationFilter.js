import React from "react";
import { useCallback, memo } from "react";
import Select from "react-select";
import { Row, Col } from "reactstrap";
import { useRecruitmentsFiltersCtx } from "../Context/RecruitmentContext";
import { translate, t } from "react-switch-lang";
export const EducationFilter = translate(
  memo(() => {
    const [filters, setFilters] = useRecruitmentsFiltersCtx();

    const option = [
      { value: "SD", label: t("sd") },
      { value: "SMP", label: t("smp") },
      { value: "SMA", label: t("sma") },
      { value: "D1", label: t("d1") },
      { value: "D2", label: t("d2") },
      { value: "D3", label: t("d3") },
      { value: "D4", label: t("d4") },
      { value: "S1", label: t("s1") },
      { value: "S2", label: t("s2") },
      { value: "S3", label: t("s3") },
    ];

    const changeFilter = useCallback(
      (e) => {
        setFilters((state) => ({ ...state, educationApplicant: e }));
      },
      [setFilters]
    );

    return (
      <>
        <Row>
          <Col sm="12">
            <b>{t("lastEducation")}</b>
          </Col>
          <Col sm="12">
            <Select
              className="my-2"
              name="jobtype"
              id="jobtype"
              options={option}
              onChange={changeFilter}
              isClearable={true}
              isMulti
              menuPosition="fixed"
              value={filters.educationApplicant}
            />
          </Col>
        </Row>
      </>
    );
  })
);
