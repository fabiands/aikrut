import React, { memo, useEffect, useState } from "react";
import {
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
} from "reactstrap";
import { useAuthUser } from '../../../../store';
import { useRecruitmentsFiltersCtx } from "../Context/RecruitmentContext";
import { translate, t } from "react-switch-lang";
export const SearchApplicantNameFilter = translate(
  memo((props, { data }) => {
    const user = useAuthUser();
    const [filters, setFilters] = useRecruitmentsFiltersCtx();
    const [search, setSearch] = useState(filters.searchApplicant);

    useEffect(() => {
      setFilters((state) => ({ ...state, searchApplicant: search }));
    }, [search, setFilters]);

    return (
      <>
        <Row>
          <Col sm="12" className={props.classname}>
            <InputGroup className="my-2" style={{ borderRadius: "12px" }}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText className="input-group-transparent">
                  <i className="fa fa-search"></i>
                </InputGroupText>
              </InputGroupAddon>
              <Input
                type="text"
                placeholder={`${t("search")} ${
                  data ?? user?.personnel?.company?.paid === 'education'?t('searchParticipantName'):t("searchEmployeeName")
                } `}
                className="input-search"
                value={filters.searchApplicant}
                onChange={(e) => setSearch(e.target.value)}
              />
            </InputGroup>
          </Col>
        </Row>
      </>
    );
  })
);
